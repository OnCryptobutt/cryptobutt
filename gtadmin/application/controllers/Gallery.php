<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('gallery_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "gallery-list";
		$data['title'] = "Gallery List";
		
		/** Start Activate Records **/
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->gallery_model->galleryActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->gallery_model->gallerySuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->gallery_model->galleryDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('gallery/index');
        $config["total_rows"] = $this->gallery_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->gallery_model->gallery_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('gallery/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "gallery-add";
		$data['title'] = "Add Gallery";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("gallery_name","Name","required|is_unique[".GALLERY.".gallery_name]");
			/** $this->form_validation->set_rules("gallery_content","Content","required"); **/
		    $this->form_validation->set_rules("gallery_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->gallery_model->galleryInsert();
				
				if($_FILES['gallery_photo']['name']!="")
				{
					
					$ext_array = explode(".",$_FILES['gallery_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "gallery_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = GALLERY_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('gallery_photo'))
					{
						// case - failure
						
					
						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->gallery_model->galleryInsertUpdate($file_name,$photo_name,$ext,$pID);	
					}
					
					$source_path = GALLERY_PATH.$file_name;
					$target_path = GALLERY_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."gallery/add/");
			}
			
				
		}
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('gallery/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "gallery-edit";
		$data['title'] = "Edit Gallery";
		

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("gallery_name","Name","required|edit_unique_common[".GALLERY.".gallery_name.".$pID.".gallery_id]");
			/** $this->form_validation->set_rules("gallery_content","Content","required"); **/
			$this->form_validation->set_rules("gallery_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->gallery_model->galleryEdit($pID);
				
				if($_FILES['gallery_photo']['name']!="")
				{
				
				    $this->gallery_model->galleryImageDelete($pID);
					
					$ext_array = explode(".",$_FILES['gallery_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "gallery_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = GALLERY_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('gallery_photo'))
					{
						// case - failure
						
					
						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->gallery_model->galleryInsertUpdate($file_name,$photo_name,$ext,$pID);	
					}
					
					$source_path = GALLERY_PATH.$file_name;
					$target_path = GALLERY_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."gallery/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->gallery_model->getgalleryDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('gallery/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "gallery-view";
		$data['title'] = "View Gallery";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->gallery_model->getgalleryDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('gallery/view',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Photos()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "gallery-photos";
		
		$data['id'] = $this->uri->segment(3);
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		
		if( $this->input->post('act') == "Delete" )
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'] ;
				$checkCount = count($check);
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						$this->gallery_model->galleryPhotosDelete($value);
					}
					
					$this->session->set_flashdata("success","Records are deleted successfully");
					
					redirect(base_url("gallery/photos/".$data['id']));
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
					
					
				}
			}
			else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
					
					
				}
		}
		
		
		
		
		
		$data['gallery_name'] = $this->boost_model->getValue(GALLERY,"gallery_name","gallery_id='".$data['id']."'");
		
		$data['title'] = $data['gallery_name']." Photos";
		
		
		$config = array();
        $config["base_url"] =  base_url('gallery/photos/'.$data['id']);
        $config["total_rows"] = $this->gallery_model->photos_record_count($data['id']);
        //$config["per_page"] = $this->settings->settings_records_per_page;
		$config["per_page"] = 12;
        $config["uri_segment"] = 4;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
        $data["lists"] = $this->gallery_model->photo_list($config["per_page"], $page,$data['id']);
        $data["pagination"] = $this->pagination->create_links();
	
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('gallery/photos',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	
	public function Upload()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "gallery-photos";
		
		
		
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$data['id'] = $this->uri->segment(3);
		
		$id = $data['id'];
		
		$data['gallery_name'] = $this->boost_model->getValue(GALLERY,"gallery_name","gallery_id='".$data['id']."'");
		
		$data['title'] = $data['gallery_name']." Photos";
		
		$output_dir = GALLERY_PHOTOS_PATH;
		
		if(isset($_FILES["myfile"]))

{

	$ret = array();



	$error =$_FILES["myfile"]["error"];

   {

    

    	if(!is_array($_FILES["myfile"]['name'])) //single file

    	{
			echo "step1";
		

            $RandomNum   = time();

            

            $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));

            $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.

         

            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));

            $ImageExt       = str_replace('.','',$ImageExt);

            $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);

            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;
			
		  

            $sample_photo_id = $this->gallery_model->galleryPhotosInsert($id);
			
			
				

            $ext_array = explode(".",$_FILES['myfile']['name']);
			
			

                $photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
                $photoname = implode("-",explode(" ",$photoname));

                $ext = $ext_array[sizeof($ext_array)-1];

                $photo_name = "gallery_photos_".$sample_photo_id."_".$photoname;
				
                $file_name    = $photo_name.".".$ext;
				
				$file_name_150    = $photo_name.".".$ext;

             
             
                $file_pathcheck = GALLERY_PHOTOS_PATH;

                $file_path = GALLERY_PHOTOS_PATH.$file_name;
				
				
			

                

                if(!file_exists($file_pathcheck))

                {

                     mkdir($file_pathcheck);

                     @chmod($file_pathcheck,0777); 

                }

                
                    $config['upload_path'] = GALLERY_PHOTOS_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

				
					
					if (!$this->upload->do_upload('myfile'))
					{
						// case - failure
						
					
						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->gallery_model->galleryPhotosUpdate($file_name,$photo_name,$ext,$sample_photo_id);	
					}
                
 
                 

                

                

                 $originalImage = GALLERY_PHOTOS_PATH.$file_name;

                 $destinationcheck   = GALLERY_PHOTOS_PATH."/thumb";

			$destination   = GALLERY_PHOTOS_PATH."thumb/".$file_name_150;

            

         
			 
			 
			        $source_path = GALLERY_PHOTOS_PATH.$file_name;
					$target_path = GALLERY_PHOTOS_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				

				



       	

       	 	 

	       	   
			   
			   

    	}

    	


    }

    echo json_encode($ret);

 

}
	
		
		exit();
		
	}
}
