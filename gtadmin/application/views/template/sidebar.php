
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">CRYPTOBUTT</li>
       
      
   
    
     <?php
     if($this->session->userdata('admin_id')!=6) {
     ?>
        <li <?php if($basename=="dashboard"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>dashboard/"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
		<li <?php if($basename=="settings"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>settings/"><i class="fa fa-gear"></i> <span>Settings</span></a></li>
		
		<li <?php if($basename=="user-list" || $basename=="user-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>user/"><i class="fa fa-user"></i> <span>User</span></a></li>
		<?php
             }
             if($this->session->userdata('admin_id')!=6 || $this->session->userdata('admin_id')==6) {
		?>
		<li <?php if($basename=="coin-list" || $basename=="coin-add" || $basename=="coin-edit" || $basename=="coin-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>coin/"><i class="fa fa-btc"></i> <span>Coin</span></a></li>
		<?php
         }
         if($this->session->userdata('admin_id')!=6) {
         ?>
		
		<li <?php if($basename=="people-list" || $basename=="people-add" || $basename=="people-edit" || $basename=="people-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>people/"><i class="fa fa-users"></i> <span>People</span></a></li>
		
		<li <?php if($basename=="user_search-list" || $basename=="user_search-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>user_search/"><i class="fa fa-history"></i> <span>User Search History</span></a></li>
		
		<li <?php if($basename=="manage_people-list" || $basename=="manage_people-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>manage_people/"><i class="fa fa-users"></i> <span>Manage People</span></a></li>
		<?php
         }
         ?>
		<!--<li <?php if($basename=="cms-list" || $basename=="cms-add" || $basename=="cms-edit" || $basename=="cms-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>cms/"><i class="fa fa-contao"></i> <span>CMS</span></a></li>
		<li <?php if($basename=="banner-list" || $basename=="banner-add" || $basename=="banner-edit" || $basename=="banner-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>banner/"><i class="fa fa-sliders"></i> <span>Banner</span></a></li>
		<li <?php if($basename=="gallery-list" || $basename=="gallery-add" || $basename=="gallery-edit" || $basename=="gallery-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>gallery/"><i class="fa fa-picture-o"></i> <span>Gallery</span></a></li>
        <li <?php if($basename=="user-list" || $basename=="user-add" || $basename=="user-edit" || $basename=="user-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>user/"><i class="fa fa-user"></i> <span>User</span></a></li>
		
		
		<li class="treeview <?php if($basename=="category-list" || $basename=="category-add" || $basename=="category-edit" || $basename=="category-view"  || $basename=="sub_category-list" || $basename=="sub_category-add" || $basename=="sub_category-edit" || $basename=="sub_category-view"){ echo "active";} ?>">
          <a href="#">
            <i class="fa fa-tags"></i> <span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($basename=="category-list"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>category/"><i class="fa fa-circle-o"></i>Main category</a></li>
            <li <?php if($basename=="sub_category-list"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>sub_category/"><i class="fa fa-circle-o"></i>Sub category</a></li>
            
          </ul>
        </li>
		
		 <li <?php if($basename=="product-list" || $basename=="product-add" || $basename=="product-edit" || $basename=="product-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>product/"><i class="fa fa-product-hunt"></i> <span>Product</span></a></li>
		 
		  <li <?php if($basename=="feature-list" || $basename=="feature-add" || $basename=="feature-edit" || $basename=="feature-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>feature/"><i class="fa fa-product-hunt"></i> <span>Feature</span></a></li>
		  
		   <li <?php if($basename=="Work-list" || $basename=="Work-add" || $basename=="Work-edit" || $basename=="Work-view"){ echo "class='active'";} ?>><a href="<?php echo base_url();?>Work/"><i class="fa fa-product-hunt"></i> <span>Slider</span></a></li>-->
  
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
