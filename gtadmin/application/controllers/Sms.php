<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->helper('sendsms_helper');
    }

	
	
	public function index()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "";
		$data['title'] = "";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			
			$this->form_validation->set_rules("pnone","Phone","required");
			if($this->form_validation->run()==TRUE)
			{
				sendsms( $this->input->post('pnone'), $this->input->post('msg'));
			
				
			}
			
				
		}
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('user/sms_view',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	
}
