<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
class Cron_start extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->load->model('coin_details_model');
		$this->settings = $this->boost_model->loadSettings();
    }

	function index()
	{
		
		//header('Content-type: application/json');
		$success = "";
		
		$this->coin_details_model->completecron();
		
			$this->db->select('*');
			$this->db->from(CRON);
			$this->db->where("status","PROCESSING");
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				echo "process";
			}
			else
			{
				$insert_data = array("start_time"=>NOW,
			                     "end_time"=>NOW,
								 "status"=>"PROCESSING"
								 );
			    $this->db->set($insert_data)->insert(CRON);
				
			}
	
}

}
