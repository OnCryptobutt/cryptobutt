<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('feeds_model');
		$this->detect = new Mobile_Detect();
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url());
		}
    }
	
	public function index()
	{
		$data = "";
		$data['basename'] = "feeds";
		$data['banners'] = $this->feeds_model->getBanner();
		
		$user_id = $this->session->userdata('user_id');

		$this->load->view('template/header',$data);
		$this->load->view('feeds_page',$data);
	}
}
