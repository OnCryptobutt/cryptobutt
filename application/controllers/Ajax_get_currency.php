<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_get_currency extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
    }
	
	function get_currency()
	{
		$currency = $this->input->post('currency');
			$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		$coin_id = $this->input->post('coinid');
		$price = $this->boost_model->getValue(COIN_HISTORY,$currency,"coin_id='".$coin_id."' AND column_id='1' order by id desc");
		echo  $currency_symbol." ".$price;
		exit;
	}	
}


