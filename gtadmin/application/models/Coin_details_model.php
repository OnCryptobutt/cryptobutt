<?php
class Coin_details_model extends CI_Model
{
	public function getParameter()
	{
		$this->db->select('currency_code');
		$this->db->from(CURRENCY);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row->currency_code;
			}
			
			$chunks = array_chunk($row1,"25");
			foreach($chunks as $chunk)
			{
				$parameter[] = implode(",",$chunk);
			}
			
			return $parameter;
		}
	}
	
	public function getCoinList($data)
	{
		
		$this->db->select('*');
		$this->db->from(COIN);
		$this->db->where("status","1");
		if($data->coin_id)
		{
			$this->db->where("id >=",$data->coin_id);
		}
		$this->db->order_by("id","ASC");
		if($this->input->get('limit'))
		{
		$this->db->limit($this->input->get('limit'));
		}
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	public function getCoinListProcess($data,$start,$upto,$process)
	{
		
		$this->db->select('*');
		$this->db->from(COIN);
		//$this->db->where("status","1");
		
			if($process=="1")
			{
				if($data->coin_id_chunk1>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk1);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="2")
			{
				if($data->coin_id_chunk2>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk2);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="3")
			{
				if($data->coin_id_chunk3>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk3);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="4")
			{
				if($data->coin_id_chunk4>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk4);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="5")
			{
				if($data->coin_id_chunk5>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk5);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="6")
			{
				if($data->coin_id_chunk6>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk6);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="7")
			{
				if($data->coin_id_chunk7>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk7);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="8")
			{
				if($data->coin_id_chunk8>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk8);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="9")
			{
				if($data->coin_id_chunk9>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk9);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="10")
			{
				if($data->coin_id_chunk10>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk10);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
			elseif($process=="11")
			{
				if($data->coin_id_chunk11>0)
				{
					$this->db->where("id >=",$data->coin_id_chunk11);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
			}
	
		
		$this->db->where("id <",$upto);
	
		$this->db->order_by("id","ASC");
		if($this->input->get('limit'))
		{
		$this->db->limit($this->input->get('limit'));
		}
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	public function getCoinListStart($data,$upto)
	{
		
		$this->db->select('*');
		$this->db->from(COIN);
		$this->db->where("status","1");
		if($data->coin_id)
		{
			$this->db->where("id >=",$data->coin_id);
			
		}
		$this->db->where("id <",$upto);
		$this->db->order_by("id","ASC");
		if($this->input->get('limit'))
		{
		$this->db->limit($this->input->get('limit'));
		}
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	public function getCoinListStart2($data,$upto,$start)
	{
		
		$this->db->select('*');
		$this->db->from(COIN);
		$this->db->where("status","1");
		if($data->coin_id)
		{
			$this->db->where("id >=",$data->coin_id);
			
		}
		else
		{
			$this->db->where("id >=",$start);
		}
		if($upto)
		{
		$this->db->where("id <",$upto);
		}
		$this->db->order_by("id","ASC");
		if($this->input->get('limit'))
		{
		$this->db->limit($this->input->get('limit'));
		}
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	public function getCoinListStart3($data,$upto,$start)
	{
		
		$this->db->select('*');
		$this->db->from(COIN);
		$this->db->where("status","1");
		if($data->coin_id)
		{
			$this->db->where("id >=",$data->coin_id);
			
		}
		else
		{
			$this->db->where("id >=",$start);
		}
		if($upto)
		{
		$this->db->where("id <",$upto);
		}
		$this->db->order_by("id","ASC");
		if($this->input->get('limit'))
		{
		$this->db->limit($this->input->get('limit'));
		}
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	
	public function getCoinList_new($data)
	{
		$this->db->select('*');
		$this->db->from(COIN);
		$this->db->where("status","1");
		$this->db->order_by("id","ASC");
		if($data->coin_id)
		{
			$this->db->where("id >=",$data->coin_id);
		}
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row->name;
			}
			
			$chunks = array_chunk($row1,CHUNK);
			foreach($chunks as $chunk)
			{
				$parameter[] = implode(",",$chunk);
			}
			
			return $parameter;
		}
	}
	
	public function getCronDetails()
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		else
		{
			$insert_data =  array("start_time"=>NOW,
			                      "end_time"=>NOW,
								  "status"=>"PROCESSING");
			$this->db->set($insert_data)->insert(CRON);
			$insert_id = $this->db->insert_id();
			$this->db->select('*');
			$this->db->from(CRON);
			$this->db->where("status","PROCESSING");
			$this->db->limit("1","0");
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				$row = $query->row();
			}
		}
		
		return $row;
		
	}
	
	
	public function getCronDetailsProcess($process)
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		if($process=="1")
		{
			$this->db->where("chunk1_status","PROCESSING");
		}
		elseif($process=="2")
		{
			$this->db->where("chunk2_status","PROCESSING");
		}
		elseif($process=="3")
		{
			$this->db->where("chunk3_status","PROCESSING");
		}
		elseif($process=="4")
		{
			$this->db->where("chunk4_status","PROCESSING");
		}
		elseif($process=="5")
		{
			$this->db->where("chunk5_status","PROCESSING");
		}
		elseif($process=="6")
		{
			$this->db->where("chunk6_status","PROCESSING");
		}
		elseif($process=="7")
		{
			$this->db->where("chunk7_status","PROCESSING");
		}
		elseif($process=="8")
		{
			$this->db->where("chunk8_status","PROCESSING");
		}
		elseif($process=="9")
		{
			$this->db->where("chunk9_status","PROCESSING");
		}
		elseif($process=="10")
		{
			$this->db->where("chunk10_status","PROCESSING");
		}
		elseif($process=="11")
		{
			$this->db->where("chunk11_status","PROCESSING");
		}
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		else
		{
			echo "No process found for ".$process;
			exit;
		}
		
		return $row;
		
	}
	
	
	public function getCronDetailsStart()
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		$this->db->where("chunk1_status","PROCESSING");
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		else
		{
			$insert_data =  array("start_time"=>NOW,
			                      "end_time"=>NOW,
								  "status"=>"PROCESSING",
								  "chunk1_status"=>"PROCESSING");
			$this->db->set($insert_data)->insert(CRON);
			$insert_id = $this->db->insert_id();
			$this->db->select('*');
			$this->db->from(CRON);
			$this->db->where("status","PROCESSING");
			$this->db->limit("1","0");
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				$row = $query->row();
			}
		}
		
		return $row;
		
	}
	
	
	public function getCronDetailsStart2()
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		$this->db->where("chunk2_status","PROCESSING");
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		
		
		
		return $row;
		
	}
	
	public function getCronDetailsStart3()
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		$this->db->where("chunk3_status","PROCESSING");
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		
		
		
		return $row;
		
	}
	
	public function getColumns()
	{
		
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	public function completecron()
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		$this->db->where("chunk1_status","COMPLETED");
		$this->db->where("chunk2_status","COMPLETED");
		$this->db->where("chunk3_status","COMPLETED");
		$this->db->where("chunk4_status","COMPLETED");
		$this->db->where("chunk5_status","COMPLETED");
		$this->db->where("chunk6_status","COMPLETED");
		$this->db->where("chunk7_status","COMPLETED");
		$this->db->where("chunk8_status","COMPLETED");
		$this->db->where("chunk9_status","COMPLETED");
		$this->db->where("chunk10_status","COMPLETED");
		$this->db->where("chunk11_status","COMPLETED");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$update_array = array("status"=>"COMPLETED");
			$this->db->set($update_array)->where("id",$row->id)->update(CRON);
		}
	}
	
	public function completecron_new()
	{
		$this->db->select('*');
		$this->db->from(CRON_NEW);
		$this->db->where("status","PROCESSING");
		$this->db->where("chunk1_status","COMPLETED");
		$this->db->where("chunk2_status","COMPLETED");
		$this->db->where("chunk3_status","COMPLETED");
		$this->db->where("chunk4_status","COMPLETED");
		$this->db->where("chunk5_status","COMPLETED");
		$this->db->where("chunk6_status","COMPLETED");
		$this->db->where("chunk7_status","COMPLETED");
		$this->db->where("chunk8_status","COMPLETED");
		$this->db->where("chunk9_status","COMPLETED");
		$this->db->where("chunk10_status","COMPLETED");
		$this->db->where("chunk11_status","COMPLETED");
		$this->db->where("chunk12_status","COMPLETED");
		$this->db->where("chunk13_status","COMPLETED");
		$this->db->where("chunk14_status","COMPLETED");
		$this->db->where("chunk15_status","COMPLETED");
		$this->db->where("chunk16_status","COMPLETED");
		$this->db->where("chunk17_status","COMPLETED");
		$this->db->where("chunk18_status","COMPLETED");
		$this->db->where("chunk19_status","COMPLETED");
		$this->db->where("chunk20_status","COMPLETED");
		$this->db->where("chunk21_status","COMPLETED");
		$this->db->where("chunk22_status","COMPLETED");
		$this->db->where("chunk23_status","COMPLETED");
		$this->db->where("chunk24_status","COMPLETED");
		$this->db->where("chunk25_status","COMPLETED");
		$this->db->where("chunk26_status","COMPLETED");
		$this->db->where("chunk27_status","COMPLETED");
		$this->db->where("chunk28_status","COMPLETED");
		$this->db->where("chunk29_status","COMPLETED");
		$this->db->where("chunk30_status","COMPLETED");
		$this->db->where("chunk31_status","COMPLETED");
		$this->db->where("chunk32_status","COMPLETED");
		$this->db->where("chunk33_status","COMPLETED");
		$this->db->where("chunk34_status","COMPLETED");
		$this->db->where("chunk35_status","COMPLETED");
		$this->db->where("chunk36_status","COMPLETED");
		$this->db->where("chunk37_status","COMPLETED");
		$this->db->where("chunk38_status","COMPLETED");
		$this->db->where("chunk39_status","COMPLETED");
		$this->db->where("chunk40_status","COMPLETED");
		
		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$update_array = array("status"=>"COMPLETED");
			$this->db->set($update_array)->where("id",$row->id)->update(CRON_NEW);
		}
		
		$this->db->select('*');
			$this->db->from(CRON_NEW);
			$this->db->where("status","PROCESSING");
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				echo "process";
			}
			else
			{
				$insert_data = array(
								 "status"=>"PROCESSING"
								 );
			    $this->db->set($insert_data)->insert(CRON_NEW);
				
			}
	}
	
	public function getCronDetailsProcess_new($process)
	{
		$this->db->select('*');
		$this->db->from(CRON_NEW);
		$this->db->where("status","PROCESSING");
		if($process=="1")
		{
			$this->db->where("chunk1_status","PROCESSING");
		}
		elseif($process=="2")
		{
			$this->db->where("chunk2_status","PROCESSING");
		}
		elseif($process=="3")
		{
			$this->db->where("chunk3_status","PROCESSING");
		}
		elseif($process=="4")
		{
			$this->db->where("chunk4_status","PROCESSING");
		}
		elseif($process=="5")
		{
			$this->db->where("chunk5_status","PROCESSING");
		}
		elseif($process=="6")
		{
			$this->db->where("chunk6_status","PROCESSING");
		}
		elseif($process=="7")
		{
			$this->db->where("chunk7_status","PROCESSING");
		}
		elseif($process=="8")
		{
			$this->db->where("chunk8_status","PROCESSING");
		}
		elseif($process=="9")
		{
			$this->db->where("chunk9_status","PROCESSING");
		}
		elseif($process=="10")
		{
			$this->db->where("chunk10_status","PROCESSING");
		}
		elseif($process=="11")
		{
			$this->db->where("chunk11_status","PROCESSING");
		}
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		else
		{
			echo "No process found for ".$process;
			exit;
		}
		
		return $row;
		
	}
}
?>