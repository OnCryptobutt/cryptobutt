 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        CMS
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="cms-list.php">CMS</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
			
			
			
			
			<form id="form1" name="form1" method="post" action="">
			 
				  
            <!-- /.box-header -->
            <div class="box-body">
			
			<?php if($success) echo $success;?>
			
			 <div align="right">
                  <a href="<?php echo base_url();?>cms/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>
                  <input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>
                  <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
				  <br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th><input type="checkbox" id="selecctall"/></th>
				  <!--<th>Image</th>-->
                  <th>Page Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				
					  <?php 
					 
					  if(is_array($lists)){
					  foreach($lists as $list){
						
						  ?>
					  <tr>
					  
					    <td><input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $list->cms_id;?>"> </td>
						 <!-- <td>  <?php if($list->cms_photo){ echo "<img width='50' height='50' src='".CMS_URL."thumb/".$list->cms_photo_name."_150.".$list->cms_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?></td>-->
						<td><?php echo $list->cms_name;?></td>
						<td>
						<?php if($list->cms_status=='1'){?><span class="label label-success">Active</span><?php } ?>
						<?php if($list->cms_status=='0'){?><span class="label label-danger">Suspend</span><?php } ?>
						</td>
						<td><a href="<?php echo base_url();?>cms/edit/<?php echo $list->cms_id;?>/"><span class="fa fa-pencil"></span></a> |
						
						<a href="<?php echo base_url();?>cms/view/<?php echo $list->cms_id;?>/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>
					  <?php } }

              else{					  ?>
			  
			  <tr><td align="center" colspan="5"><font color="red">--No records found!--</font></td></tr>
			  <?php } ?>
				
               
                </tbody>
                
              </table>
			  
			  <div align="right"><?php echo $pagination;?></div>
			  
            </div>
			</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 