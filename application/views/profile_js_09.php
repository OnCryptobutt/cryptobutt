<script>

function change_currency(symbol,name)
{
	 $("#change_currency").html(symbol+" "+name);
}

function currency(per_page)
{
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&currency='+per_page;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 $("#change_title").html(per_page);
           }
      });

}

function tops(type,val)
{
	if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
		$("#bal_al").html("Loading...");
		
	}
	else if(type=="sorting")
	{
		<?php if($this->detect->isMobile())
		{ ?>
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val+"_mobile" ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
		}
		<?php }else
		{			?>
	
	$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "active" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('active');
			 $( "#sortby_volume").removeClass('active');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('active');
			 $( "#sortby_volume").removeClass('active');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('active');
			 $( "#sortby_marketcap").removeClass('active');
		}
	
	    <?php } ?>
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#hidden_search").val();
	
	<?php  }else { ?>
	search = $("#hidden_search").val();
	<?php } ?>
	sorting = $("#hidden_sorting").val();
	
      
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&sorting='+sorting+'&search='+search;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Ajax/load_profile');?>',
           data: dataString,
           cache: false,
           success: function(result){
			 //  alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 document.getElementById("row_no").value = 1;
				 document.getElementById("pageids").value = 0;
				 if(type=="tops")
				 {
					$("#change_title").html(per_page);
				 }
				 displaybalance();
           }
      });

}


  function changePagination(pageId,liId){
	  
	  search = $("#hidden_search").val();
	  currency = $("#hidden_currency").val();
	  $("#pageData").html('<span class="flash"></span>');
     $(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
     var dataString = 'pageId='+ pageId+'&search='+ search+'&currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_profile/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
				 if(result!='--No records found--')
				 {
                 $("#pageData").html(result);
				 }
           }
      });
}

changePagination("0","0");


setInterval(function(){ 

$('.well').each(function(){
var id=$(this).attr('id');

var visible = $(this).visible( "partial" );
			
		if(visible)
		{	

currency = $("#hidden_currency").val();
 prev_price = $("#beat"+id+"price").html();
  var dataString = 'id='+ id+'&currency='+currency+'&prev_price='+prev_price;
	$.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/updaterow');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
			  var obj = JSON.parse(result); 
               $("#beat"+id+"price").html(obj.price);
			   $("#beat"+id+"market").html(obj.mktcap);
			   $("#beat"+id+"hour").html(obj.volume24hour);
			   $("#beat"+id+"change").html(obj.change24hour);
			   $("#beat"+id+"price_mobile").html(obj.price);
			   $("#beat"+id+"market_mobile").html(obj.mktcap);
			   $("#beat"+id+"volume24hour_mobile").html(obj.volume24hour);
			   $("#beat"+id+"change_mobile").html(obj.change24hour);
			   if(obj.price_change==="Yes")
			   {
				   
				   if(obj.change24hour<0)
				   {
					     $( "#currency"+id).removeClass('green');
						 $( "#beat"+id+"hour").removeClass('green');
						 
						  $("#beat"+id+"change_mobile").removeClass('green');
						  $( "#currency"+id).addClass('red');
						 $( "#beat"+id+"hour").addClass('red');
						  $("#beat"+id+"change_mobile").addClass('red');
			    $("#c_"+id).addClass( "row_background" );
				setTimeout(function(){
            $( "#c_"+id).removeClass('row_background');
          
    }, 500);
				   }
				   if(obj.change24hour>0)
				   {
					      $( "#currency"+id).removeClass('red');
						 $( "#beat"+id+"hour").removeClass('red');
						 
						  $("#beat"+id+"change_mobile").removeClass('red');
				     $( "#currency"+id).addClass('green');
						 $( "#beat"+id+"hour").addClass('green');
						  $("#beat"+id+"change_mobile").addClass('green');
						 
	 $( "#c_"+id).addClass( "row_background2" );
				setTimeout(function(){
            $( "#c_"+id).removeClass('row_background2');
          
				   }, 500);    
				   }
			   }
			 
           }
      });
		}
});

}, 5000);



//$(window).scroll(function ()
$(document).on('scroll', function(){ 
    
	//alert($(document).height());
	//alert($(document).scrollTop());
		
	  if($(document).height()-400 <= ($(document).scrollTop() + $(document).height()))
	  {
		  
		  totalcoins = document.getElementById('coins_count').value;
		  totalcoins = Number(totalcoins);
		  var val = document.getElementById("row_no").value;
		  pagedisplays = val * <?php echo $this->settings->settings_records_per_page_front;?>;
		  if(totalcoins>pagedisplays)
		  {
			  //alert(val);
			 var pageids = document.getElementById("pageids").value;
			 var pageidsarray = pageids.split(",");
			  var i = pageidsarray.indexOf(val);
             if(i > -1){ 
			 //alert('inside if condition'+i);
			     //loadmore();
			 }
			 else
			 {
				 //alert();
			  loadmore();
			  document.getElementById('pageids').value=pageids+','+val;
			 }
			  
		  }
		 
		
		  
	  }
    });
	
	
	
	
	
	
function loadmore()
{
	type = "";
	 var val = document.getElementById("row_no").value;
	 
	 if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
	}
	else if(type=="sorting")
	{
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_marketcap").removeClass('selecte');
		}
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#hidden_search").val();
	
	<?php  }else { ?>
	search = $("#hidden_search").val();
	<?php } ?>
	
	sorting = $("#hidden_sorting").val();
	

	
	pageId = val;
	liId = "0";
	 var content = document.getElementById("pageData");
     content.innerHTML = content.innerHTML+'<span class="flash"></span>';
	 //$("#pageData").html('<span class="flash"></span>');
	//$(".flash").show();
    // $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&sorting='+sorting+'&search='+search;
	 //alert(dataString);
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_profile/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 //$("#pageData").html(result);
				 var content = document.getElementById("pageData");
				 //alert(result);
				 if(result!="--No records found--")
				 {
					 content.innerHTML = content.innerHTML+result;
					 document.getElementById("row_no").value = Number(val)+1;
					 if(type=="tops")
					 {
						$("#change_title").html(per_page);
					 }
				 }
				 
           }
      });

	 
	
	 
}


function addportfolio(val)
{
	<?php if($this->session->userdata('user_id'))
	{ ?>

   var dataString = 'cid='+ val;
 	$("#heart_"+val).fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader20.gif'>");
	 $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_addportfolio/add');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   
               if(result=="success")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else if(result=="exit")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart-o heart_cor\" aria-hidden=\"true\"></i>");

			   }
                 //$("#pageData").html(result);
				
           }
      });
	<?php }
	else
	{
		
		?>
		$('#myModal').modal('show');
    <?php 
    } 
	?>
}


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#coin_name").css("background","#FFF");
		}
		});
	});
});
//To select country name
/** function selectCountry(val) {
$("#coin_name").val(val);
$("#suggesstion-box").hide();
tops('search','');
} **/


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name_mobile").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});
//To select country name
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val+"?source=search&time="+<?php echo time();?>;
	<?php if($this->detect->isMobile())
	{ ?>
$("#coin_name_mobile").val(val);
$("#suggesstion-box_mobile").hide();

	<?php }else{ ?>
	$("#coin_name").val(val);
$("#suggesstion-box").hide();
	<?php } ?>
//tops('search','');
}

/**
$(window).on('load', function() {
  displaybalance();
}) **/


function displaybalance(){
	
	
	 var currency = $("#hidden_currency").val();
     var dataString = 'currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=home');?>',
           data: dataString,
           cache: false,
           success: function(result){
			
                 $("#pageData3").html(result);
           }
      });
}

function dropValueAtID(id,value,id2,value2)
{
	if(id)
	{
		$("#"+id).html(value);
	}
	if(id2)
	{
		$("#"+id2).val(value2);
	}
	
	if(id2=="sorting_price")
	{
		$("#hidden_sorting").val("price");
		$("#sorting_market_html").html("<span style='margin-left:5px;'>---</span>");
	}
	else if(id2=="sorting_market")
	{
		$("#hidden_sorting").val("marketcap");
		$("#sorting_price_html").html("<span style='margin-left:5px;'>---</span>");
	}
	else if(id2=="sorting_change")
	{
		$("#hidden_sorting").val("volume");
	}
	$("#pageids").val('0');
	$("#row_no").val('1');
}

</script>