<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <table class="table table-bordered">
					<tr><td><b>User Name</b></td><td><?php echo $records->firstName." ".$records->lastName;?></td></tr>
					<tr><td><b>User Email</b></td><td><?php echo $records->email;?></td></tr>
					<tr><td><b>Created Date</b></td><td><?php echo date("Y-m-d h:i:s",strtotime($records->created_time));?></td></tr>
					<tr><td><b>Status</b></td><td> <?php if($records->status=="1"){?><span class="label label-success">Active</span><?php } ?><?php if($records->status=="0"){?><span class="label label-danger">Suspend</span><?php } ?></td></tr>
				</table>
				
              </div>
			   <div class="box-footer">
                
				<a href="<?php echo base_url();?>user/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  