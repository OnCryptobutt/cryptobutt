<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coin extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('coin_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
		$this->db2 = $this->boost_model->getHistoryDb();
		$this->load->library('pagination');
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "coin-list";
		$data['title'] = ucfirst("coin List");
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->session->flashdata('error'))
		{
			$data['success'] = $this->boost_model->showNotify("error",$this->session->flashdata('success'));
		}
		
		/** Start Activate Records **/
		
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->coin_model->coinActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->coin_model->coinSuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->coin_model->coinDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/

		$config = array();
        $config["base_url"] =  base_url('coin/index');
        $config["total_rows"] = $this->coin_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
	     $paginationConfig = $this->boost_model->loadPaginationConfig();
		
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->coin_model->coin_list($config["per_page"], $page);
        $data["coinlist_count"] = $this->coin_model->coinlist_count($config["per_page"], $page);
        $data["active_coin"] = $this->coin_model->active_coin($config["per_page"], $page);
        $data["suspend_coin"] = $this->coin_model->suspend_coin($config["per_page"], $page);
       if(count($data["lists"]) > $this->settings->settings_records_per_page || count($data["coinlist_count"]) > $this->settings->settings_records_per_page || count($data["active_coin"]) > $this->settings->settings_records_per_page || count($data["suspend_coin"]) > $this->settings->settings_records_per_page){
          $data["pagination"] = $this->pagination->create_links(); 
       } else{
           $data["pagination"] = ""; 
       }
        
        
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('coin/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	public function checkstatus()
	{
	    $id    = $this->input->post('id');
	    $value    = $this->input->post('value');
	    $updateid = $this->coin_model->checkstatus($id, $value);
	    echo json_encode(array('status' => 'true'));
	}
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "coin-add";
		$data['title'] = "Add coin";
		
		if($this->uri->segment(2)=='add')
		{
			redirect(base_url('coin/'));
		}	
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("coin_name","Name","required|is_unique[".coin.".coin_name]");
			/** $this->form_validation->set_rules("coin_content","Content","required"); **/
		    $this->form_validation->set_rules("coin_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->coin_model->coinInsert();
				
				if($_FILES['image']['name']!="")
				{
					
					$ext_array = explode(".",$_FILES['image']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "coin_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = COIN_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('coin_photo'))
					{

						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->coin_model->coinInsertUpdate($file_name,$photo_name,$ext,$pID);	
					}
					
					$source_path = coin_PATH.$file_name;
					$target_path = coin_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."coin/add/");
			}
			
				
		}
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('coin/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "coin-edit";
		$data['title'] = "Edit coin";

		if($this->uri->segment(3)=='')
		{
			redirect(base_url('coin/'));
		}	
		
		if($this->input->get('linksid')!="")
		{
			$this->coin_model->deletelinks($this->input->get('linksid'));
			$this->session->set_flashdata('success',"Your requested record has been deleted");
			redirect(base_url()."coin/edit/".$this->uri->segment(3)."/");
		}
		
		if($this->input->get('keypeople')!="")
		{
			$this->coin_model->deletekeypeople($this->input->get('keypeople'));
			$this->session->set_flashdata('success',"Your requested record has been deleted");
			redirect(base_url()."coin/edit/".$this->uri->segment(3)."/");
		}

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
			$this->form_validation->set_rules("name","Coin Code","required");
			$this->form_validation->set_rules("coinname","Coin Name","required");
			//$this->form_validation->set_rules("website","Website","required|edit_unique_common[".coin.".coin_name.".$pID.".coin_id]");
			/** $this->form_validation->set_rules("coin_content","Content","required"); **/
			
			if($this->form_validation->run()==TRUE)
			{
		
				$this->coin_model->coinEdit($pID);
				
				if($_FILES['image']['name']!="")
				{
				    
					$photoname = $this->boost_model->getValue(COIN,"image","id='".$pID."'");
		
		
		if($photoname)
		{
			if(file_exists(COIN_PATH.$photoname))
			{
				unlink(COIN_PATH.$photoname);
			}

		}
					
					$ext_array = explode(".",$_FILES['image']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "coin_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					
					 if (move_uploaded_file($_FILES["image"]["tmp_name"], COIN_PATH.$file_name)) {
						 
						 
						$sql = "UPDATE ".COIN." SET 
		
		image      = '".$file_name."'
		WHERE id='".$pID."'";
		
		$this->db->query($sql);
      
    } else {
       
    }
			}
			
			if(isset($_POST['type']))
				{
			    $type = $_POST['type'];
				$url = $_POST['url'];
				
				foreach($type as $key=>$value)
				{
					if($value)
					{
						$insert_data = "";
						//$insert_data = array('work_slider_name'=>addslashes(trim($value)));
						
						$insert_data = array('links_type'=>addslashes(trim($value)),
						'links_coin_id'=>$pID,
						'links_url'=>$url[$key]);
						$insertid = $this->coin_model->linksInsert($insert_data);
					}
				}
				}
				
				
				if(isset($_POST['type_update']))
				{
					 $type_update = $_POST['type_update'];
				     $url_update = $_POST['url_update'];
				
					
					foreach($type_update as $key=>$value)
					{
						if($value)
						{
							$insert_data = "";
							$insert_data = array('links_type'=>addslashes(trim($value)),
							'links_url'=>$url_update[$key]);
						   $this->coin_model->linksUpdate($insert_data,$key);
						}
					}
				}
				
			$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."coin/edit/".$pID."/");
				
			
		}
		
		}
		
		$data['records'] = $this->coin_model->getcoinDetails($pID);
		$data['role_list'] = $this->coin_model->getRoleList();
		
		$data['link_type_list'] = $this->coin_model->getLink_typeList();
		$data['links'] = $this->coin_model->getLinks($pID);
		$data['people_list'] = $this->coin_model->getPeopleList();
		$data['key_list'] = $this->coin_model->getKeyList($pID);
		$data['coin_name'] = $this->coin_model->getcoinname($pID);
		$data['keypeople_name'] = $this->coin_model->getkeypeoplename($data['coin_name'][0]->name);
		//$data['social_name'] = $this->coin_model->getkeypeoplename($data['coin_name'][0]->keypeople_id);
		//print_r($data['keypeople_name']);
	
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('coin/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
    public function socialpeople(){
        $socialid    = $this->input->post('socialid');
        $peopleid     = $this->input->post('peopleid');
        $keydata = $this->coin_model->getsocialpeople($socialid, $peopleid);
        //$keystatus = $this->coin_model->updatesocialpeoplelink($socialid, $peopleid);
       // print_r($keystatus);
        echo json_encode($keydata);
	}
	 public function socialpeoplelink(){
        $socialid    = $this->input->post('socialid');
        $peopleid     = $this->input->post('peopleid');
        $keystatus = $this->coin_model->updatesocialpeoplelink($socialid, $peopleid);
        
        if($keystatus[0]->social_verify_link=='true'){
           $btn = 'btn btn-success';
           $checked = "checked='true'";
        } else{ 
            $btn ='btn btn-danger'; 
            $checked = "";
        }
        echo '<label for="'.$keystatus[0]->social_id.'" class="'.$btn.'">VERIFY  
<input type="checkbox"  id="'.$keystatus[0]->social_id.'" '.$checked.' class=" badgebox"  name="verify"  onchange="if(this.checked) this.value=true; else this.value=false;verifylinkFun(this.value, '.$keystatus[0]->social_people_id.', '.$keystatus[0]->social_links_type.');">
        <span class="badge">✓</span></label>'; 
	}
	public function updateverifystatus(){
        $verifyid    = $this->input->post('verifyid');
        $peopleid    = $this->input->post('peopleid');
        $keydata        = $this->coin_model->updatesocialpeople($verifyid, $peopleid);
       // echo json_encode($keydata);
       if($keydata) {
         echo json_encode(array('status' =>$verifyid, 'peopleid'=>$peopleid));
       }
	}
		public function updateverifystatuslink(){
        $verifyid    = $this->input->post('verifyid');
        $peopleid    = $this->input->post('peopleid');
        $socialid    = $this->input->post('socialid');
        $keydata        = $this->coin_model->updateverifystatuslinks($verifyid, $peopleid, $socialid);
        $this->coin_model->updatesocialpeople($verifyid,$peopleid);
       if($keydata) {
         echo json_encode(array('status' =>$verifyid, 'peopleid'=>$peopleid));
       }
	}
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "coin-view";
		$data['title'] = "View coin";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->coin_model->getcoinDetails($pID);
		$data['cid']=$pID;
		
		$config = array();
        $config["base_url"] =  base_url('coin/view/'.$pID.'/');
        $config["total_rows"] = $this->coin_model->cron_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 4;
        $config['use_page_numbers'] = TRUE;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
        $data["lists"] = $this->coin_model->getCronList($config["per_page"], $page, $pID);
        $data["pagination"] = $this->pagination->create_links();
		
		//$data['lists'] = $this->coin_model->getCronList($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('coin/view',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	
	public function Details()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "coin-details";
		$data['title'] = "Details coin";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$cid = $this->uri->segment(3);
		$pID = $this->uri->segment(4);
	
		
		 $data['records'] = $this->coin_model->getHistoryDetails($pID,$cid);
		 
		 $data['cid']=$cid;
		 
		// print_r($data['records']);exit;
		 
		//$data['lists'] = $this->coin_model->getCronList($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('coin/details_view',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	
}
