      <div id="pageData2" class="mobile">
		<span class="flash2"></span>
	   </div>
	  
	  <div class="col-md-12 col-sm-12 desktop">
			<div class="pull-right2">
				<div class="head-t">					
					<div class="header_alg desktop">
						<div class="row">
							<div class="col-xs-8 col-sm-3 col-md-3 col-lg-12 alg sect pull-right nowrap dsk_align align2">
								<a href="javascript:void(0)"><span class="sort_new_for2 selecte">Social</span></a>
								<a href="javascript:void(0)"><span class="sort_new_for2">News</span></a>
								<a href="javascript:void(0)"><span class="sort_new_for2">Podcast</span></a>
								<a href="javascript:void(0)"><span class="sort_new_for2">Channel</span></a>
								<span class="sort_alg"><i class="material-icons icon-ag">&#xE164;</i></span><span class="filt">Filter by coin</span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-8 col-sm-3 col-md-3 col-lg-4 alg sect pull-right nowrap dsk_align align2">
								<a href="javascript:void(0)"><span class="sort_new_for2">Filter by coins</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Mobile View End-->
	
	   <div id="pageData" class="<?php if(!$this->detect->isMobile()) { ?> l_r_align <?php } ?>">
		<span class="flash"></span>
	   </div>
	
				
    </div>
</div>

<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div  class="modal-body">
	    <div id="edit_details">
        <span id="flash_details"></span>
		</div>
		<span id="flash_details_edit"></span>
		<span id="edit_details_edit"></span>
      </div>
	  
      <div class="modal-footer">
	     <button onclick="save_edit();" type="button" class="btn btn-danger">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

<style>
input::-webkit-input-placeholder {
color: #753ac9 !important;
}
 
input:-moz-placeholder { /* Firefox 18- */
color: #753ac9 !important;  
}
 
input::-moz-placeholder {  /* Firefox 19+ */
color: #753ac9 !important;  
}
 
input:-ms-input-placeholder {  
color: #753ac9 !important;  
}

.table-info{
background-color: #d4e6e9;
}

.control-label {
    font-size: 14px;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #301c1e;
    margin-bottom: 10px;
}
</style>

<script>
<!-- Portfolio list -->
function changePagination(pageId,liId){
     $(".flash").show();
     $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });
}

changePagination("0","0");

<!-- Heading-->
function changePagination2(pageId,liId){
     $(".flash2").show();
     $(".flash2").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_heading');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash2").hide();
                 $("#pageData2").html(result);
				 
           }
      });
}

changePagination2("0","0");


<!-- Save coin -->
function savecoin(coin_id,pid,refresh)
{
	var bought   = $("#bought_"+coin_id).val();
	var exchange = $("#exchange_"+coin_id).val();
	var amount   = $("#amount_"+coin_id).val();
	
	var err = "";
	if(bought=="")
	{
		err = 'AMOUNT OF BTH BOUGHT should not be empty \n';
	}
	if(exchange=="")
	{
		err = err + 'Please select EXCHANGE TYPE \n';
	}
	if(amount=="")
	{
		err = err +'AMOUNT SPENT should not be empty \n';
	}
	
	if(err=="")
	{
		 $(".flash_"+coin_id).show();
     $(".flash_"+coin_id).fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'coin_id='+ coin_id+'&bought='+ bought+'&exchange='+ exchange+'&amount='+ amount+'&pid='+ pid;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_details/load_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_"+coin_id).hide();
                 $("#details_"+coin_id).html(result);
				 $("#bought_"+coin_id).val('');
				 $("#amount_"+coin_id).val('');
				 
           }
      });
		
	}
	else
	{
		if(refresh=="yes")
		{
			 $(".flash_"+coin_id).show();
     $(".flash_"+coin_id).fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'coin_id='+ coin_id+'&bought='+ bought+'&exchange='+ exchange+'&amount='+ amount+'&pid='+ pid;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_details/load_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_"+coin_id).hide();
                 $("#details_"+coin_id).html(result);
				 $("#bought_"+coin_id).val('');
				 $("#amount_"+coin_id).val('');
				 
           }
      });
		}
		else
		{
			alert(err);
		}
	}
}

<!-- load edit model-->
function get_edit_section(did)
{
	 $(".flash_details").show();
     $(".flash_details").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'did='+ did;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_get_details/edit_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_details").hide();
                 $("#edit_details").html(result);
				
				 
           }
      });
}

<!-- update edit details -->
function save_edit()
{
	
	var id             = $("#edit_id").val();
	var coin_id        = $("#coin_id").val();
	var portfolio_id   = $("#portfolio_id").val();
	var date           = $("#date").val();
	var coin_brought   = $("#coin_brought").val();
	var exchange_type  = $("#exchange_type").val();
	var details_amount = $("#details_amount").val();
	var err="";
	if(coin_brought=="")
	{
		err = "AMOUNT OF COIN BOUGHT should not be empty\n";
	}
	if(exchange_type=="")
	{
		err = err + "Please select Exchange Type\n";
	}
	if(details_amount=="")
	{
		err = err + "AMOUNT SPENT should not be empty\n";
	}
	
	if(err=="")
	{
	
		 $(".flash_details_edit").show();
		 $("#edit_details_edit").show()
     $(".flash_details_edit").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'id='+ id+'&coin_brought='+coin_brought+'&exchange_type='+exchange_type+'&details_amount='+details_amount;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_get_details/save_edit_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_details_edit").hide();
                 $("#edit_details_edit").html(result);
				setTimeout(myfunction, 3000);
				<!-- Refresh list-->
				savecoin(coin_id,portfolio_id,"yes");
				
				 
           }
      });
	}
	else
	{
		alert(err);
	}
	
	function myfunction()
	{
		$("#edit_details_edit").hide()
	}
	
}
</script>

