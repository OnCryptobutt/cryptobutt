<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->load->model('login_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->settings = $this->boost_model->loadSettings();
		$this->load->library('encrypt');
    }
	public function Login(){
		$this->load->view('signin/Logindialog');
	}
	public function resetPassword(){
	    //print_r($_GET['emailToken']);
	    $data['emailToken'] = $_GET['emailToken'];
		$this->load->view('signin/resetPassword', $data);
	}
	public function email_check()
    {
        if($this->input->is_ajax_request()) {
        $email = $this->input->post('email');
        if(!$this->form_validation->is_unique($email, 'ci_user.email')) {
         $this->output->set_content_type('application/json')->set_output(json_encode(array('message' => '<span style="color:#f00;">That email’s already in our system, choose another one</span>')));
            }else{
                $this->output->set_content_type('application/json')->set_output(json_encode(array('message' => '<i class="fa fa-check" id="circle_tick" style="font-size: 18px;color: purple;float: right;position: relative;top: -32px;left: 25px;">')));
            }
        }
    }
    public function email_checklogin()
    {
        if($this->input->is_ajax_request()) {
        $email = $this->input->post('email');
        if(!$this->form_validation->is_unique($email, 'ci_user.email')) {
             $this->output->set_content_type('application/json')->set_output(json_encode(array('message' => '<i class="fa fa-check" id="circle_tick" style="font-size: 18px;color: purple;float: right;position: relative;top: -32px;left: 25px;">')));
            }else{
                $this->output->set_content_type('application/json')->set_output(json_encode(array('message' => '<span style="color:#f00;">This email address doesn’t match any account. Double check and try again.</span>'))); 
            }
        }
    }
    public function signup()
    {
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $data = array(
                'email' => $email,
                'password' => $password
            );
       $insertid = $this->login_model->insertSignupdata($data);
       if($insertid) 
       {
    	    echo "success";
    	} else{
    	    echo "error";
    	}
    }
    public function signin()
    {
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $data = array(
                'email' => $email,
                'password' => $password
            );
       $insertid = $this->login_model->siginData($data);
       if($insertid) 
       {
    	    echo "success";
    	} else{
    	    echo "error";
    	}
    }
    public function changePassword(){
        $password   = $this->input->post('password');
        $username   = $this->input->post('username');
        $email      = str_replace(" ","+",$username);
        $data = array('password' => $password,'username' => $this->encrypt->decode($email));
        $insertid = $this->login_model->changepasswordData($data);
       if($insertid) 
       {
    	    echo "success";
    	} else{
    	    echo "error";
    	}
    }
    public function resetMail()
    {
        $this->load->library('parser');
        
         $email      = $this->input->post('email');
        $config = Array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://smtp.googlemail.com',
      'smtp_port' => 465,
      'smtp_user' => 'rajeshkumarppp@gmail.com', // change it to yours
      'smtp_pass' => 'xxx', // change it to yours
      'mailtype' => 'html',
      'charset' => 'iso-8859-1',
      'wordwrap' => TRUE
    );
    
            $message = '';
            $this->load->library('email', $config);
            $this->load->library('encrypt');
            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
            $this->email->set_header('Content-type', 'text/html');
            $this->email->set_newline("\r\n");
            $this->email->from('no-reply@cryptobutt.com', 'Cryptobutt Team'); // change it to yours
            $this->email->reply_to('no-reply@cryptobutt.com', 'Cryptobutt Team');
            $this->email->to($email);// change it to yours
            $this->email->subject('Reset Your Password');
            //$data = array('userName'=> $email);
            $data['userName'] = $this->encrypt->encode($email);
            $body = $this->load->view('signin/resettemp.php',$data,TRUE);
            $this->email->message($body);   
              if($this->email->send())
                 {
                  echo 'success';
                 }
                 else
                {
                 show_error($this->email->print_debugger());
                }
    
    }
}
