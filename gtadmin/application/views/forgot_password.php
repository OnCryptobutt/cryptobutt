<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url();?>"><b>Admin</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Forgot Password</p>

	<span class="red"><?php if(isset($success)) echo $success;?></span>
	  
    <form action="<?php echo base_url();?>forgot_password/" method="post">
	<input type="hidden" name="act" value="forgot"/>
      <div class="form-group has-feedback">
        <input type="text" name="email" class="form-control" placeholder="Email" value="<?php echo set_value('email');?>">
		
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		<span class="red"><?php echo form_error('email');?></span>
      </div>
	  
	  <div class="form-group has-feedback">
	     <?php echo $captcha;?>
	  </div>
	  
	   <div class="form-group has-feedback">
    
	     <input type="text" name="captcha" class="form-control" placeholder="Captcha">
		
      
		<span class="red"><?php echo form_error('captcha');?></span>
      </div>
      <div class="row">
       
        <!-- /.col -->
        <div class="col-xs-3">
		
          <button type="submit" class="btn btn-primary">Submit</button>
		  </div>
		   <div align="left" class="col-xs-2">
		  <a href="<?php echo base_url('login');?>"><button type="button" class="btn btn-primary">Login</button></a>
        </div>
        <!-- /.col -->
      </div>
    </form>

  

    
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  setTimeout(function() {
    $('#SiteNotifyMessage').fadeOut('slow');
}, 5000); 
</script>
</body>
</html>
