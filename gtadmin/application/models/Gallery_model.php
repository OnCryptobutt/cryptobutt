<?php
class Gallery_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function gallery_list($limit, $start)
	{
		$sql = "SELECT * FROM ".GALLERY." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getgalleryDetails($id)
    {
	   $sql = "SELECT * FROM ".GALLERY." WHERE gallery_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      return $this->db->count_all(GALLERY);
    }
	
	 public function photos_record_count($id)
    {
		//$this->db->where("gallery_photos_gallery_id",$id);
       //return $this->db->count_all(GALLERY_PHOTOS);
	   
	    $this->db->where('gallery_photos_gallery_id',$id);
		$this->db->from(GALLERY_PHOTOS);
		return $this->db->count_all_results();
    }
   
    public function galleryActivate($id)
    {
	   $sql = "UPDATE ".GALLERY." SET gallery_status='1' WHERE gallery_id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function gallerySuspend($id)
	{
		$sql = "UPDATE ".GALLERY." SET gallery_status='0' WHERE gallery_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function galleryDelete($id)
	{
		$photoname = $this->boost_model->getValue(GALLERY,"gallery_photo_name","gallery_id='".$id."'");
		$photoext = $this->boost_model->getValue(GALLERY,"gallery_photo_ext","gallery_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(GALLERY_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(GALLERY_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(GALLERY_PATH.$photoname.".".$photoext))
			{
				unlink(GALLERY_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".GALLERY." WHERE gallery_id='$id'";
		$query = $this->db->query($sql);
	}
	
	public function galleryImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(GALLERY,"gallery_photo_name","gallery_id='".$id."'");
		$photoext = $this->boost_model->getValue(GALLERY,"gallery_photo_ext","gallery_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(GALLERY_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(GALLERY_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(GALLERY_PATH.$photoname.".".$photoext))
			{
				unlink(GALLERY_PATH.$photoname.".".$photoext);
			}
		}
	
	}
	
	public function galleryPhotosDelete($id)
	{
		$photoname = $this->boost_model->getValue(GALLERY_PHOTOS,"gallery_photos_photo_name","gallery_photos_id='".$id."'");
		$photoext = $this->boost_model->getValue(GALLERY_PHOTOS,"gallery_photos_photo_ext","gallery_photos_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(GALLERY_PHOTOS_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(GALLERY_PHOTOS_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(GALLERY_PHOTOS_PATH.$photoname.".".$photoext))
			{
				unlink(GALLERY_PHOTOS_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".GALLERY_PHOTOS." WHERE gallery_photos_id='".$id."'";
		$query = $this->db->query($sql);
	}
		
	public function galleryInsert()
	{
		$slug = url_title($this->input->post('gallery_name'), 'dash', true);
		
		$sql = "INSERT INTO ".GALLERY." SET 
		
		gallery_name         = '".$this->input->post('gallery_name')."',
		gallery_content      = '".$this->input->post('gallery_content')."',
		gallery_status       = '".$this->input->post('gallery_status')."',
		gallery_order        = '".$this->input->post('gallery_order')."',
		gallery_seo          = '".$slug."',
		gallery_created_time = '".NOW."',
		gallery_updated_time = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function galleryEdit($id)
	{
		$slug = url_title($this->input->post('gallery_name'), 'dash', true);
			
		$sql = "UPDATE ".GALLERY." SET 
		
		gallery_name         = '".$this->input->post('gallery_name')."',
		gallery_content      = '".$this->input->post('gallery_content')."',
		gallery_status       = '".$this->input->post('gallery_status')."',
		gallery_order        = '".$this->input->post('gallery_order')."',
		gallery_seo          = '".$slug."',
		gallery_updated_time = '".NOW."' WHERE gallery_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function galleryInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".GALLERY." SET 
		
		gallery_photo      = '".$file_name."',
		gallery_photo_name = '".$photo_name."',
		gallery_photo_ext  = '".$ext."' WHERE gallery_id='".$id."'";
		
		$this->db->query($sql);
	}
	
	public function galleryPhotosInsert($id)
	{
		$sql = "INSERT INTO ".GALLERY_PHOTOS." SET gallery_photos_gallery_id = '".$id."',

        gallery_photos_created_time = '".NOW."',
		gallery_photos_updated_time = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
		
	}
	
	
	public function galleryPhotosUpdate($file_name,$photo_name,$ext,$sample_photo_id)
	{
		$sql = "UPDATE ".GALLERY_PHOTOS." SET
		
		gallery_photos_photo      = '".$file_name."',
	    gallery_photos_photo_name ='".$photo_name."',
		gallery_photos_photo_ext  = '".$ext."'
		
		WHERE gallery_photos_id = '".$sample_photo_id."'";
		
		$this->db->query($sql);
		
		
	}
	
	public function photo_list($limit, $start , $id)
	{
		$sql = "SELECT * FROM ".GALLERY_PHOTOS." WHERE gallery_photos_gallery_id = '".$id."' limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
		
}
?>