<?php
class Work_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function work_list($limit, $start)
	{
		$ser=$this->input->get('search_value');
		$sql = "SELECT * FROM ".WORK." where work_name like '%$ser%' limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getWorkDetails($id)
    {
	   $sql = "SELECT * FROM ".WORK." WHERE work_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
     
	 
    public function record_count()
    {
      return $this->db->count_all(WORK);
    }
	
	
 
   
    public function workActivate($id)
    {
	   $sql = "UPDATE ".WORK." SET work_status='1' WHERE work_id='$id'";
	   $query = $this->db->query($sql);
    }
	
	
		
	public function workSuspend($id)
	{
		$sql = "UPDATE ".WORK." SET work_status='0' WHERE work_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function workDelete($pID)
	{
		
		$this->db->select("*");
		$this->db->from(WORK_SLIDER);
		$this->db->where("work_slider_work_id",$pID);
		$query = $this->db->get();
		
		foreach($query->result() as $row)
		{
			$id = $row->work_slider_id;
			
			$photoname = $this->boost_model->getValue(WORK_SLIDER,"work_slider_photo_name","work_slider_id='".$id."'");
			$photoext = $this->boost_model->getValue(WORK_SLIDER,"work_slider_photo_ext","work_slider_id='".$id."'");
			
			if($photoname)
			{
				if(file_exists(WORK_PATH."thumb/".$photoname."_150.".$photoext))
				{
					unlink(WORK_PATH."thumb/".$photoname."_150.".$photoext);
				}
				
				if(file_exists(WORK_PATH.$photoname.".".$photoext))
				{
					unlink(WORK_PATH.$photoname.".".$photoext);
				}
			}
			
			$this->db->where('work_slider_id', $id);
			$this->db->delete(WORK_SLIDER);
			
		}
		
		$this->db->where('work_id', $pID);
		$this->db->delete(WORK);
		
    }
		
	public function workInsert()
	{
		//$slug = url_title($this->input->post('work_name'), 'dash', true);
		
		$sql = "INSERT INTO ".WORK." SET 
		
		work_name              		      = '".$this->input->post('work_name')."',
		work_order              		  = '".$this->input->post('work_order')."',
		work_status              		  = '".$this->input->post('work_status')."',
		work_create_date      		      = '".NOW."',
		work_update_date      		      = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function workEdit($id)
	{
		//$slug = url_title($this->input->post('service_name'), 'dash', true);
			
		$sql = "UPDATE ".WORK." SET 
		
		work_name              		      = '".$this->input->post('work_name')."',
		work_order              		  = '".$this->input->post('work_order')."',
		work_status              		  = '".$this->input->post('work_status')."',
		work_update_date      		  = '".NOW."' WHERE work_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	
	public function workSliderInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".WORK_SLIDER." SET 
		
		work_slider_photo      = '".$file_name."',
		work_slider_photo_name = '".$photo_name."',
		work_slider_photo_ext  = '".$ext."' WHERE work_slider_id='".$id."'";
		
		$this->db->query($sql);
	}
	
	
	
	
	
	
	public function workSliderInsert($data)
	{
		$this->db->insert(WORK_SLIDER,$data);
		
		return $this->db->insert_id();
	}
	
	
	public function workSliderUpdate($data,$id)
	{
		$this->db->where('work_slider_id', $id);
		$this->db->update(WORK_SLIDER,$data);
	
	}
	/*
	public function workSliderInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".WORK_SLIDER." SET 
		
		work_slider_photo      = '".$file_name."',
		work_slider_photo_name = '".$photo_name."',
		work_slider_photo_ext  = '".$ext."' WHERE work_slider_id='".$id."'";
		
		$this->db->query($sql);
	}*/
	
	
	public function workSliderList($pID)
	{
		$this->db->select("*");
		$this->db->from(WORK_SLIDER);
		$this->db->where("work_slider_work_id",$pID);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
	}
	
	public function deleteWorkSlider($id)
	{
		
		$photoname = $this->boost_model->getValue(WORK_SLIDER,"work_slider_photo_name","work_slider_id='".$id."'");
		$photoext = $this->boost_model->getValue(WORK_SLIDER,"work_slider_photo_ext","work_slider_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(WORK_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(WORK_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(WORK_PATH.$photoname.".".$photoext))
			{
				unlink(WORK_PATH.$photoname.".".$photoext);
			}
		}
		
		$this->db->where('work_slider_id', $id);
        $this->db->delete(WORK_SLIDER);
	}
	
	/*
	public function getServiceList($id)
	{
	
		$sql = "SELECT service_id,service_name FROM ".SERVICE." WHERE service_status='1'";
	    $query = $this->db->query($sql);
		if($query->num_rows()>0)
			{
				$data = '';
				foreach($query->result_array() as $row_sel)
				{
					$data .= "<option value=\"".$row_sel["service_id"]."\"";
					if($row_sel["service_id"]==$id)
					{
						$data .= " selected=\"SELECTED\"";
					}
					$data .= ">".stripslashes($row_sel["service_name"])."</option>";
				}
			}
			return $data;
	}*/
		
}
?>