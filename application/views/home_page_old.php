<div class="col-md-12 col-sm-12">
	<div class="row">
		<div class="pull-right2">
			<div class="head-t">
				<div class="col-xs-4 col-sm-3 col-md-3 col-lg-7 alg sect nowrap dsk_align desktop pad-top">
					Listing <span id="coins_count"><?php if(isset($total_list)) { echo $total_list; }?></span> coins
				</div>
			<!--<div class="header_alg">
					<div class="col-xs-5 col-sm-3 col-md-3 col-lg-8 alg sect pull-right nowrap dsk_align">Sort by:
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							<?php 
							 if($this->input->get('per_page'))
							  {
								  $per_page = $this->input->get('per_page');
							  }
							  else
							  {
								  $per_page = '10';
							  }?>
								Top <span id="change_title"><?php echo $per_page;?></span> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a onclick="tops('tops','10');" href="javascript:void(0)">Top 10</a></li>
								<li><a onclick="tops('tops','25');" href="javascript:void(0)">Top 25</a></li>
								<li><a onclick="tops('tops','50');" href="javascript:void(0)">Top 50</a></li>
								<li><a onclick="tops('tops','100');" href="javascript:void(0)">Top 100</a></li>
							</ul>
						</div>
					</div>
				</div>-->
				
				<div class="header_alg desktop">
					<div class="col-xs-8 col-sm-3 col-md-3 col-lg-5 alg sect pull-right nowrap dsk_align align2">Sort by:
						<a href="javascript:void(0)" onclick="tops('sorting','price');"><span onclick="" id="sortby_price" class="sort_new_for">Coin price</span></a>
						<a href="javascript:void(0)" onclick="tops('sorting','marketcap');"><span id="sortby_marketcap" class="sort_new_for selecte">Market Cap</span></a>
						<a href="javascript:void(0)" onclick="tops('sorting','volume');"><span id="sortby_volume" class="sort_new_for">Volume 24 hr</span></a>
					</div>
				</div>
				
				<!-- Mobile View Start -->
				<div class="header_alg mobile">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-5 alg sect pull-right nowrap dsk_align align2">Sort by:
						<a href="javascript:void(0)" onclick="tops('sorting','price');"><span class="sort_new_for" id="sortby_price_mobile">Coin price</span></a>
						<a href="javascript:void(0)" onclick="tops('sorting','marketcap');"><span id="sortby_marketcap_mobile" class="sort_new_for  selecte">Market Cap</span></a>
						<a href="javascript:void(0)" onclick="tops('sorting','volume');"><span id="sortby_volume_mobile" class="sort_new_for">Volume 24 hr</span></a>
					</div>
				</div>
				<!-- Mobile View End -->
			</div>
		</div>
	</div>
</div>
		
<input type="hidden" id="hidden_search" value=""/>
<input type="hidden" id="hidden_per_page" value=""/>
<input type="hidden" id="hidden_currency" value=""/>
<input type="hidden" id="hidden_sorting" value=""/>

<div class="col-md-12 col-sm-12">
	<div id="pageData">
	</div>
	<div id="pageData3">
	</div>
	
	<input type="hidden" id="row_no" value="1">
	<input type="hidden" id="pageids" value="0">
</div>

		
		<!--<div class="col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-3 col-md-1 text-center mob_res">
                        <img src="images/ethereum.png" alt="bootsnipp" class="img-rounded img-responsive img_width" />
						<div class="text-col dis">ETH</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
						<div class="text-col3"><a href="#" target="_blank">Ethereum</a></div>
						<div class="text-col2"><i class="material-icons">school</i> Rank 2</div>
					</div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                        <div class="text-col4">£332.44</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col6">0.24%</div>
						<div class="text-col2">% Change (24 hr)</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col4">£23,000,268,766</div>
						<div class="text-col2">Market Cap</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col4">£549,953,000</div>
						<div class="text-col2">Volume (24 hr)</div>
                    </div>
					<div class="col-xs-3 col-md-1 text-center set desktop">
                       <div class="text-col4"><span class="glyphicon glyphicon-heart"></span></div>
                    </div>
					
					
					<div class="col-xs-7 col-md-2 text-center set mobile div_bt_red">
						<div class="text-col3"><a href="#" target="_blank">Ethereum</a></div>
						<div class="text-col2 mob"><i class="material-icons">school</i> Rank 2</div>
						<div class="mobile-second"><span class="mobile-second1">&#x20A4;332.44</span><span class="mobile-second3">0.24%</span></div>
					</div>
					<div class="col-xs-2 col-md-2 text-center set mobile">
						<div class="text-col4"><span class="glyphicon glyphicon-heart heart_cor"></span></div>
						
					</div>
					
					<div style="border-right: 1px solid #A1C3C9;" class="col-xs-6 col-md-2 text-center mobile">
						<div class="text-col2">Market Cap</div>
						<div class="text-col4 value_alg">£23,000,268,766</div>
					</div>
					
					<div class="col-xs-6 col-md-2 text-center mobile">
						<div class="text-col2">Volume (24 hr)</div>
						<div class="text-col4 value_alg">£549,953,000</div>
					</div>
					
					
                </div>
            </div>
        </div>
		
		<div class="col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-3 col-md-1 text-center mob_res">
                        <img src="images/bitcoin.png" alt="bootsnipp" class="img-rounded img-responsive img_width" />
						<div class="text-col dis">BTC</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
						<div class="text-col3"><a href="#" target="_blank">Bitcoin Cash</a></div>
						<div class="text-col2"><i class="material-icons">school</i> Rank 3</div>
					</div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                        <div class="text-col4">£628.64</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col5">- 0.87%</div>
						<div class="text-col2">% Change (24 hr)</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col4">£7,004,842,839</div>
						<div class="text-col2">Market Cap</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col4">£202,704,000</div>
						<div class="text-col2">Volume (24 hr)</div>
                    </div>
					<div class="col-xs-3 col-md-1 text-center set desktop">
                       <div class="text-col4"><i class="fa fa-heart-o heart_cor" aria-hidden="true"></i></div>
                    </div>
					
					
					<div class="col-xs-7 col-md-2 text-center set mobile div_bt_red">
						<div class="text-col3"><a href="#" target="_blank">Bitcoin Cash</a></div>
						<div class="text-col2 mob"><i class="material-icons">school</i> Rank 3</div>
						<div class="mobile-second"><span class="mobile-second1">&#x20A4;628.64</span><span class="mobile-second2">- 0.06%</span></div>
					</div>
					<div class="col-xs-2 col-md-2 text-center set mobile">
						<div class="text-col4"><i class="fa fa-heart-o heart_cor" aria-hidden="true"></i></div>
					</div>
					
					<div class="col-xs-6 col-md-2 text-center mobile bor_set">
						<div class="text-col2">Market Cap</div>
						<div class="text-col4 value_alg">£7,004,842,839</div>
					</div>
					
					<div class="col-xs-6 col-md-2 text-center mobile">
						<div class="text-col2">Volume (24 hr)</div>
						<div class="text-col4 value_alg">£202,704,000</div>
					</div>
					
					
                </div>
            </div>
        </div>
		<div class="col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-3 col-md-1 text-center mob_res">
                        <img src="images/bitcoin.png" alt="bootsnipp" class="img-rounded img-responsive img_width"/>
						<div class="text-col dis">BTC</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
						<div class="text-col3"><a href="#" target="_blank">Bitcoin</a></div>
						<div class="text-col2"><i class="material-icons">school</i> Rank 1</div>
					</div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                        <div class="text-col4">&#x20A4;4353.49</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col5">- 0.06%</div>
						<div class="text-col2">% Change (24 hr)</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col4">£72,020,598,720</div>
						<div class="text-col2">Market Cap</div>
                    </div>
					<div class="col-xs-3 col-md-2 text-center set desktop">
                       <div class="text-col4">£1,524,550,000</div>
						<div class="text-col2">Volume (24 hr)</div>
                    </div>
					<div class="col-xs-3 col-md-1 text-center set desktop">
                       <div class="text-col4"><i class="fa fa-heart-o heart_cor" aria-hidden="true"></i></div>
                    </div>
				
					
					<div class="col-xs-7 col-md-2 text-center set mobile first_div">
						<div class="text-col3"><a href="#" target="_blank">Bitcoin</a></div>
						<div class="text-col2 mob"><i class="material-icons">school</i> Rank 1</div>
						<div class="mobile-second"><span class="mobile-second1">&#x20A4;4353.49</span><span class="mobile-second2">- 0.06%</span></div>
					</div>
					<div class="col-xs-2 col-md-2 text-center set mobile">
						<div class="text-col4"><i class="fa fa-heart-o heart_cor" aria-hidden="true"></i></div>
					</div>
					
					<div class="col-xs-6 col-md-2 text-center mobile bor_set">
						<div class="text-col2">Market Cap</div>
						<div class="text-col4 value_alg">£72,020,598,720</div>
					</div>
					
					<div class="col-xs-6 col-md-2 text-center mobile">
						<div class="text-col2">Volume (24 hr)</div>
						<div class="text-col4 value_alg">£1,524,550,000</div>
					</div>
					
				
                </div>
            </div>
        </div>-->
		
		
    </div>
</div>

<script>

function change_currency(symbol,name)
{
	 $("#change_currency").html(symbol+" "+name);
}

function currency(per_page)
{
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&currency='+per_page;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 $("#change_title").html(per_page);
           }
      });

}

function tops(type,val)
{
	if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
		$("#bal_al").html("Loading...");
		
	}
	else if(type=="sorting")
	{
		<?php if($this->detect->isMobile())
		{ ?>
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val+"_mobile" ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
		}
		<?php }else
		{			?>
	
	$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_marketcap").removeClass('selecte');
		}
	
	    <?php } ?>
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#coin_name_mobile").val();
	
	<?php  }else { ?>
	search = $("#coin_name").val();
	<?php } ?>
	sorting = $("#hidden_sorting").val();
	
      
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&search='+search+'&sorting='+sorting;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			 //  alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 document.getElementById("row_no").value = 1;
				 document.getElementById("pageids").value = 0;
				 if(type=="tops")
				 {
					$("#change_title").html(per_page);
				 }
				 displaybalance();
           }
      });

}


  function changePagination(pageId,liId){
	  $("#pageData").html('<span class="flash"></span>');
     $(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });
}

changePagination("0","0");


setInterval(function(){ 

$('.well').each(function(){
var id=$(this).attr('id');

currency = $("#hidden_currency").val();
 prev_price = $("#beat"+id+"price").html();
  var dataString = 'id='+ id+'&currency='+currency+'&prev_price='+prev_price;
	$.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/updaterow');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
			  var obj = JSON.parse(result); 
               $("#beat"+id+"price").html(obj.price);
			   $("#beat"+id+"market").html(obj.mktcap);
			   $("#beat"+id+"hour").html(obj.volume24hour);
			   $("#beat"+id+"change").html(obj.change24hour);
			   $("#beat"+id+"price_mobile").html(obj.price);
			   $("#beat"+id+"market_mobile").html(obj.mktcap);
			   $("#beat"+id+"volume24hour_mobile").html(obj.volume24hour);
			   $("#beat"+id+"change_mobile").html(obj.change24hour);
			   if(obj.price_change==="Yes")
			   {
				   if(obj.change24hour<0)
				   {
			    $( "#"+id ).addClass( "row_background" );
				setTimeout(function(){
            $( "#"+id ).removeClass('row_background');
          
    }, 500);
				   }
				   if(obj.change24hour>0)
				   {
				   
	 $( "#"+id ).addClass( "row_background2" );
				setTimeout(function(){
            $( "#"+id ).removeClass('row_background2');
          
				   }, 500);    
				   }
			   }
			   else
			   {
				    $( "#"+id ).addClass( "row_background3" );
				setTimeout(function(){
            $( "#"+id ).removeClass('row_background3');
          
    }, 500);
			   }
           }
      });
});

}, 5000);



//$(window).scroll(function ()
$(document).on('scroll', function(){ 
    
	//alert($(document).height());
	//alert($(document).scrollTop());
		
	  if($(document).height()-400 <= ($(document).scrollTop() + $(document).height()))
	  {
		  totalcoins = document.getElementById('coins_count').innerHTML;
		  totalcoins = Number(totalcoins);
		  var val = document.getElementById("row_no").value;
		  pagedisplays = val * <?php echo $this->settings->settings_records_per_page_front;?>;
		  if(totalcoins>pagedisplays)
		  {
			  //alert(val);
			 var pageids = document.getElementById("pageids").value;
			 var pageidsarray = pageids.split(",");
			  var i = pageidsarray.indexOf(val);
             if(i > -1){ 
			 }
			 else
			 {
			  loadmore();
			  document.getElementById('pageids').value=pageids+','+val;
			 }
			  
		  }
		 
		
		  
	  }
    });
	
	
	
	
	
	
function loadmore()
{
	type = "";
	 var val = document.getElementById("row_no").value;
	 
	 if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
	}
	else if(type=="sorting")
	{
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_marketcap").removeClass('selecte');
		}
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#coin_name_mobile").val();
	
	<?php  }else { ?>
	search = $("#coin_name").val();
	<?php } ?>
	
	sorting = $("#hidden_sorting").val();
	

	
	pageId = val;
	liId = "0";
	 var content = document.getElementById("pageData");
     content.innerHTML = content.innerHTML+'<span class="flash"></span>';
	 //$("#pageData").html('<span class="flash"></span>');
	//$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&search='+search+'&sorting='+sorting;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 //$("#pageData").html(result);
				 var content = document.getElementById("pageData");
                 content.innerHTML = content.innerHTML+result;
				 document.getElementById("row_no").value = Number(val)+1;
				 if(type=="tops")
				 {
					$("#change_title").html(per_page);
				 }
           }
      });

	 
	
	 
}


function addportfolio(val)
{
	<?php if($this->session->userdata('user_id'))
	{ ?>

   var dataString = 'cid='+ val;
 	$("#heart_"+val).fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader20.gif'>");
	 $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_addportfolio/add');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   
               if(result=="success")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else if(result=="exit")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart-o heart_cor\" aria-hidden=\"true\"></i>");

			   }
                 //$("#pageData").html(result);
				
           }
      });
	<?php }
	else
	{
		
		?>
		$('#myModal').modal('show');
    <?php 
    } 
	?>
}


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#coin_name").css("background","#FFF");
		}
		});
	});
});
//To select country name
/** function selectCountry(val) {
$("#coin_name").val(val);
$("#suggesstion-box").hide();
tops('search','');
} **/


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name_mobile").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});
//To select country name
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val+"?source=search&time="+<?php echo time();?>;
	<?php if($this->detect->isMobile())
	{ ?>
$("#coin_name_mobile").val(val);
$("#suggesstion-box_mobile").hide();

	<?php }else{ ?>
	$("#coin_name").val(val);
$("#suggesstion-box").hide();
	<?php } ?>
//tops('search','');
}

$(window).on('load', function() {
  displaybalance();
})


function displaybalance(){
	
	
	 var currency = $("#hidden_currency").val();
     var dataString = 'currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=home');?>',
           data: dataString,
           cache: false,
           success: function(result){
			
                 $("#pageData3").html(result);
           }
      });
}

</script>
<style>
.row_background
{
	
background-color : #FFCDD2;
}

.row_background3
{
	
background-color : #ccdefc;
}
.row_background2
{
	
background-color : #B9F6CA;
}
.flash
{
	margin-left: 45%;
}

</style>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alert!</h4>
      </div>
      <div class="modal-body">
        <p>Please login to continue. <a href="<?php echo base_url("hauth/Login/Google");?>">Click Here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</body>
</html>

