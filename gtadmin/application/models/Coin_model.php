<?php
class Coin_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
		
    }

	public function coinlist_count($limit, $start)
	{
	   $currency = $this->input->get('currency');
		
		if($currency==""){ $currency = "USD"; }
		
		$sorting_option = $this->input->get('sortby');
			
			
			if($sorting_option=="price")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) desc";
			    }
				
			}
			elseif($sorting_option=="marketcap")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
				    $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			    }
			}
			elseif($sorting_option=="volume24hr")
			{
				$sorting = "ORDER BY CAST(h.volume24hour AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			}
			$search_text = $this->input->get('search');
			
			if($this->input->get('status')=="0" || $this->input->get('status')=="1")
			{
				if($this->input->get('status')=="0")
				{
				$ser_status = "c.status='0'";
				}
				if($this->input->get('status')=="1")
				{
				$ser_status = "c.status='1'";
				}
			}
			else
			{
				$ser_status = "c.status!=''";
			}
			
			if(trim($search_text)!="")
			{
				$ser = "AND c.coinname LIKE '".$this->input->get('search')."%' or c.name LIKE '".$this->input->get('search')."%'";
			}
			else
			{
				$ser ="";  
			}
		if(trim($search_text)!="") {
		    $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id where  $ser_status $ser $sorting limit $start,$limit";
		} else{
		     $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id";
		}
	   
		$query =$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
			return $data;
        }
	}
	public function active_coin($limit, $start)
	{
	     $currency = $this->input->get('currency');
		
		if($currency==""){ $currency = "USD"; }
		
		$sorting_option = $this->input->get('sortby');
			
			
			if($sorting_option=="price")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) desc";
			    }
				
			}
			elseif($sorting_option=="marketcap")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
				    $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			    }
			}
			elseif($sorting_option=="volume24hr")
			{
				$sorting = "ORDER BY CAST(h.volume24hour AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			}
			$search_text = $this->input->get('search');
			
			if($this->input->get('status')=="0" || $this->input->get('status')=="1")
			{
				if($this->input->get('status')=="0")
				{
				$ser_status = "c.status='0'";
				}
				if($this->input->get('status')=="1")
				{
				$ser_status = "c.status='1'";
				}
			}
			else
			{
				$ser_status = "c.status!=''";
			}
			
			
		if(trim($search_text)!="") {
		    	$ser = "AND c.coinname LIKE '".$this->input->get('search')."%' or c.name LIKE '".$this->input->get('search')."%'";
		    $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id where  c.status='1' $ser $sorting limit $start,$limit";
		} else{
		    $ser =""; 
		    $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id where c.status='1'";
		}
	    
		$query =$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
			return $data;
        }
	}
	public function suspend_coin($limit, $start)
	{
	    $currency = $this->input->get('currency');
		
		if($currency==""){ $currency = "USD"; }
		
		$sorting_option = $this->input->get('sortby');
			
			
			if($sorting_option=="price")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) desc";
			    }
				
			}
			elseif($sorting_option=="marketcap")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
				    $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			    }
			}
			elseif($sorting_option=="volume24hr")
			{
				$sorting = "ORDER BY CAST(h.volume24hour AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			}
			$search_text = $this->input->get('search');
			
			if($this->input->get('status')=="0" || $this->input->get('status')=="1")
			{
				if($this->input->get('status')=="0")
				{
				$ser_status = "c.status='0'";
				}
				if($this->input->get('status')=="1")
				{
				$ser_status = "c.status='1'";
				}
			}
			else
			{
				$ser_status = "c.status!=''";
			}
			
			
		if(trim($search_text)!="") {
		    $ser = "AND c.coinname LIKE '".$this->input->get('search')."%' or c.name LIKE '".$this->input->get('search')."%'";
		    $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id where  c.status='0' $ser $sorting limit $start,$limit";
		} else{
		    $ser =""; 
		    $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id  where c.status='0'";
		}
	   
		$query =$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
			return $data;
        }
	}
	public function coin_list($limit, $start)
	{
		
		$currency = $this->input->get('currency');
		
		if($currency==""){ $currency = "USD"; }
		
		$sorting_option = $this->input->get('sortby');
			
			
			if($sorting_option=="price")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) desc";
			    }
				
			}
			elseif($sorting_option=="marketcap")
			{
			    if($this->input->get('sortby1')=="lowtohigh"){
				    $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) asc";
			    }else{
			        $sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			    }
			}
			elseif($sorting_option=="volume24hr")
			{
				$sorting = "ORDER BY CAST(h.volume24hour AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			}
			$search_text = $this->input->get('search');
			
			if($this->input->get('status')=="0" || $this->input->get('status')=="1")
			{
				if($this->input->get('status')=="0")
				{
				$ser_status = "c.status='0'";
				}
				if($this->input->get('status')=="1")
				{
				$ser_status = "c.status='1'";
				}
			}
			else
			{
				$ser_status = "c.status!=''";
			}
			
			if(trim($search_text)!="")
			{
				$ser = "AND c.coinname LIKE '".$this->input->get('search')."%' or c.name LIKE '".$this->input->get('search')."%'";
			}
			else
			{
				$ser ="";
			}
        $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id where  $ser_status $ser $sorting limit $start,$limit";
		$query =$this->db->query($sql);
		$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
			return $data;
        }
		
		
    }
   
    public function getcoinDetails($id)
    {
	   $sql = "SELECT * FROM ".COIN." WHERE id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
		
		
		$currency = $this->input->get('currency');
		
		if($currency==""){ $currency = "USD"; }
		
		$sorting_option = $this->input->get('sortby');
			
			
			if($sorting_option=="price")
			{
				$sorting = "ORDER BY CAST(h.price AS DECIMAL(38,10)) desc";
			}
			elseif($sorting_option=="marketcap")
			{
				$sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			}
			elseif($sorting_option=="volume24hr")
			{
				$sorting = "ORDER BY CAST(h.volume24hour AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(h.mktcap AS DECIMAL(38,10)) desc";
			}
			
			
			$search_text = $this->input->get('search');
			
			if($this->input->get('status')=="0" || $this->input->get('status')=="1")
			{
				if($this->input->get('status')=="0")
				{
				$ser_status = "c.status='0'";
				}
				if($this->input->get('status')=="1")
				{
				$ser_status = "c.status='1'";
				}
			}
			else
			{
				$ser_status = "c.status!=''";
			}
			
			if(trim($search_text)!="")
			{
				$ser = "AND c.coinname LIKE '".$this->input->post('search')."%' or c.name LIKE '".$this->input->post('search')."%'";
			}
			else
			{
				
				$ser ="";
			}
			
		$sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id  where  $ser_status $ser $sorting";

		$query =$this->db->query($sql);
      return $query->num_rows();
    }
   
    public function coinActivate($id)
    {
	   $sql = "UPDATE ".COIN." SET status='1' WHERE id='$id'";
	   $query = $this->db->query($sql);
    }
	public function checkstatus($id, $value)
    {
	   $sql = "UPDATE ".COIN." SET checkstatus='$value' WHERE id='$id'";
	   $query = $this->db->query($sql);
    }	
	public function coinSuspend($id)
	{
		$sql = "UPDATE ".COIN." SET status='0' WHERE id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function coinDelete($id)
	{
		$photoname = $this->boost_model->getValue(COIN,"image","id='".$id."'");
		
		
		if($photoname)
		{
			if(file_exists(COIN_PATH.$photoname))
			{
				unlink(COIN_PATH.$photoname);
			}
			
		
		}
		$sql = "DELETE FROM ".COIN." WHERE id='$id'";
		$query = $this->db->query($sql);
	}
	
	
	public function coinImageDelete($id)
	{
	$photoname = $this->boost_model->getValue(COIN,"image","id='".$id."'");
		
		
		if($photoname)
		{
			if(file_exists(COIN_PATH.$photoname))
			{
				unlink(COIN_PATH.$photoname);
			}
			
		
		}
		$sql = "DELETE FROM ".COIN." WHERE id='$id'";
		$query = $this->db->query($sql);
		
	}
		
	public function coinInsert()
	{
		$slug = url_title($this->input->post('coin_name'), 'dash', true);
		
		$sql = "INSERT INTO ".COIN." SET 
		
		coin_name              = '".$this->input->post('coin_name')."',
		coin_content           = '".$this->input->post('coin_content')."',
		coin_status            = '".$this->input->post('coin_status')."',
		coin_order        	   = '".$this->input->post('coin_order')."',
		coin_site_title        = '".$this->input->post('coin_site_title')."',
		coin_meta_description  = '".$this->input->post('coin_meta_description')."',
		coin_meta_keyword      = '".$this->input->post('coin_meta_keyword')."',
		coin_seo               = '".$slug."',
		coin_created_time      = '".NOW."',
		coin_updated_time      = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function coinEdit($id)
	{
		$slug = url_title($this->input->post('coin_name'), 'dash', true);
			
		$sql = "UPDATE ".COIN." SET 
		
		name            	= '".$this->input->post('name')."',
		coinname            = '".$this->input->post('coinname')."',
		website            	= '".$this->input->post('website')."',
		website2         	= '".$this->input->post('website2')."',
		explore            	= '".$this->input->post('explore')."',
		explore2            = '".$this->input->post('explore2')."',
		message_board1      = '".$this->input->post('message_board1')."',
		message_board2  	= '".$this->input->post('message_board2')."',
		Application      	= '".$this->input->post('Application')."',
		Coin      		    = '".$this->input->post('Coin')."',
		currency      	    = '".$this->input->post('Currency')."',
		erc20          	    = '".$this->input->post('erc20')."',
		Exchange      	    = '".$this->input->post('Exchange')."',
		Hybrid          	= '".$this->input->post('Hybrid')."',
		Mineable      		= '".$this->input->post('Mineable')."',
		Platform          	= '".$this->input->post('Platform')."',
		Privacy          	= '".$this->input->post('Privacy')."',
	    Token          	    = '".$this->input->post('Token')."',
		updated_time   		= '".NOW."' WHERE id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function coinInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".COIN." SET 
		
		coin_photo      = '".$file_name."',
		coin_photo_name = '".$photo_name."',
		coin_photo_ext  = '".$ext."' WHERE coin_id='".$id."'";
		
		$this->db->query($sql);
	}
		
	public function getCronList($limit,$start,$id)
	{   
	      // $sql = "SELECT * FROM ".CRON;
		
		
		$sql = "SELECT * FROM ".CRON_NEW." where status='COMPLETED' order by id desc limit ".$start.",".$limit."";
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{ $i=1;
            foreach ($query->result() as $row)
			{

				$sql1 = "SELECT * FROM ci_coin_history_USD where cron_id='".$row->id."' and coin_id='".$id."'";
				
                $query1 = $this->db2->query($sql1);

				if($query1->num_rows()>0)
				{
				  $row->history=$query1->row();
				  $row->currency=$this->boost_model->getValue(CURRENCY,"symbol","id='1'"); 
 
				}
				$row->sno=$i;
				 // $data[]=$query1->result();
				
				  $data[] = $row;
				  
				$i++;  
            }
            return $data;
        }
        return false;
    }
	
	public function cron_count()
    {
      return $this->db->count_all(CRON_NEW);
    }
	
	public function getHistoryDetails($id,$cid)
    {
	   $sql = "SELECT * FROM ci_coin_history_USD WHERE cron_id='".$id."' and coin_id='".$cid."'";
       $query = $this->db2->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {

		  return $query->row();

       }
	   
       
    }

	public function getRoleList()
	{
		$query = $this->db->select('*')->from(ROLE)->where("role_status","1")->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	
	public function getLink_typeList()
	{
		$query = $this->db->select('*')->from(LINK_TYPE)->where("link_type_status","1")->get();
	
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	
	public function getPeopleList()
	{
		$query = $this->db->select('*')->from(PEOPLE)->get();
	
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	public function getsocialpeople($socialid, $peopleid) {
	     $array = array('social_links_type' => $socialid, 'social_people_id' => $peopleid);
		$this->db->select('*');
		$this->db->from('ci_keypeoplesociallink');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->result();
			return $row;
			
		}
	}
	public function updatesocialpeople($verifyid, $peopleid) {
		$sql = "UPDATE  ci_addkeypeople SET verify_link = '".$verifyid."' WHERE keypeople_id = '".$peopleid."'";
       return $this->db->query($sql);
	}
	public function updateverifystatuslinks($verifyid, $peopleid, $socialid) {
		$sql = "UPDATE  ci_keypeoplesociallink SET social_verify_link = '".$verifyid."' WHERE social_people_id = '".$peopleid."' AND social_links_type ='".$socialid."'";
       return $this->db->query($sql);
	}
	public function updatesocialpeoplelink($socialid, $peopleid) {
	    
	    $sql = "SELECT * FROM ci_keypeoplesociallink WHERE social_links_type='".$socialid."' AND social_people_id= '".$peopleid."'";
	    $query = $this->db->query($sql);
	    if ($query->num_rows() > 0)
    	   {
    			foreach($query->result() as $row)
    			{
    				$row1[] = $row;
    			}
    			
    			return $row1;	
           }
	}
	
	public function getLinks($id)
	{
		$query = $this->db->select('*')->from(LINKS)->where("links_coin_id",$id)->get();
		
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$type_array[] = $row->links_type;
				if(in_array($row->links_type,$type_array))
				{
					
					$counts = array_count_values($type_array);
					if($counts[$row->links_type]>1)
					{
						$row->links_type_is = $row->links_type;
						$row->links_type_count = $counts[$row->links_type];
					}
					else
					{
						$row->links_type_is = "";
						$row->links_type_count = "";
					}
				}
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	

	public function getcoinname($id) {
	    
	   $sql = "SELECT * FROM ci_coin WHERE id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
       }
	}
	public function getkeypeoplename($name) {
	    $sql = "SELECT * FROM ci_addkeypeople WHERE keypeople_coinid='".$name."'";
	    $query = $this->db->query($sql);
	    if ($query->num_rows() > 0)
	   {
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
       }
	}
	public function getKeyList($id)
	{
	    
		$query = $this->db->select('*')->from(KEY_PEOPLE)->join(PEOPLE,PEOPLE.".people_id=".KEY_PEOPLE.".key_people_people_id")->join(ROLE,ROLE.".role_id=".KEY_PEOPLE.".key_people_role_id")->where(KEY_PEOPLE.".key_people_coin_id",$id)->get();
		
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	
	public function linksInsert($data)
	{
		$this->db->insert(LINKS,$data);
		
		return $this->db->insert_id();
	}
	
	
	public function deletelinks($id)
	{
		
		$this->db->where('links_id', $id);
        $this->db->delete(LINKS);
	}
	
	public function deletekeypeople($id)
	{
		
		$this->db->where('key_people', $id);
        $this->db->delete(KEY_PEOPLE);
	}
	
	public function linksUpdate($data,$id)
	{
		$this->db->where('links_id', $id);
		$this->db->update(LINKS,$data);
	
	}
	
	
}
?>