<?php
class Feature_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function feature_list($limit, $start)
	{
		
		
		$this->db->select('*')->from(FEATURE);
		
		if($this->input->get('act')=="search")
		{
			$this->db->like('feature_name',$this->input->get('search'));
		}
		$this->db->limit($limit,$start);
		
		$query = $this->db->get();
		
		
		
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getfeatureDetails($id)
    {
	  
	   $query = $this->db->select('*')->from(FEATURE)->where('feature_id',$id)->get();
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
		
		$this->db->select('*')->from(FEATURE);
		
		if($this->input->get('act')=="search")
		{
			$this->db->like('feature_name',$this->input->get('search'));
		}
		
      return $this->db->count_all_results();
    }
   
    public function featureActivate($id)
    {
		$this->db->set('feature_status','1');
		$this->db->where('feature_id',$id);
		$this->db->update(FEATURE);
	 
    }
		
	public function featureSuspend($id)
	{
		
		$this->db->set('feature_status','0');
		$this->db->where('feature_id',$id);
		$this->db->update(FEATURE);
		
	}
		
	public function featureDelete($id)
	{
		$photoname = $this->boost_model->getValue(FEATURE,"feature_photo_name","feature_id='".$id."'");
		$photoext = $this->boost_model->getValue(FEATURE,"feature_photo_ext","feature_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(feature_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(feature_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(FEATURE_PATH.$photoname.".".$photoext))
			{
				unlink(FEATURE_PATH.$photoname.".".$photoext);
			}
		}
		
		$this->db->where('feature_id',$id);
		$this->db->delete(FEATURE);
		
	}
	
	public function featureImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(FEATURE,"feature_photo_name","feature_id='".$id."'");
		$photoext = $this->boost_model->getValue(FEATURE,"feature_photo_ext","feature_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(FEATURE_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(FEATURE_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(FEATURE_PATH."thumb/".$photoname."_crop.".$photoext))
			{
				unlink(FEATURE_PATH."thumb/".$photoname."_crop.".$photoext);
			}
			
			if(file_exists(FEATURE_PATH.$photoname.".".$photoext))
			{
				unlink(FEATURE_PATH.$photoname.".".$photoext);
			}
		}
	}
		
	public function featureInsert()
	{
		$slug = url_title($this->input->post('feature_name'), 'dash', true);
		
		
		$data = array(	"feature_category_id"  => $this->input->post('feature_category_id'),
		"feature_sub_category_id"   => $this->input->post('feature_sub_category_id'),
		"feature_name"              => $this->input->post('feature_name'),
		"feature_price"             => $this->input->post('feature_price'),
		"feature_content"            => $this->input->post('feature_content'),
		"feature_status"             => $this->input->post('feature_status'),
		"feature_order"              => $this->input->post('feature_order'),
		"feature_site_title"         => $this->input->post('feature_site_title'),
		"feature_meta_description"   => $this->input->post('feature_meta_description'),
		"feature_meta_keyword"       => $this->input->post('feature_meta_keyword'),
		"feature_seo"                => $slug,
		"feature_created_time"       => NOW,
		"feature_updated_time"       => NOW);
		
		$this->db->set($data)->insert(FEATURE);
		return $this->db->insert_id();
	}
	
	public function featureEdit($id)
	{
		$slug = url_title($this->input->post('feature_name'), 'dash', true);
		
		
		$data = array("feature_category_id"       => $this->input->post('feature_category_id'),
		"feature_sub_category_id"   => $this->input->post('feature_sub_category_id'),
		"feature_name"              => $this->input->post('feature_name'),
		"feature_price"             => $this->input->post('feature_price'),
		"feature_content"           => $this->input->post('feature_content'),
		"feature_status"            => $this->input->post('feature_status'),
		"feature_order"             => $this->input->post('feature_order'),
		"feature_site_title"        => $this->input->post('feature_site_title'),
		"feature_meta_description"  => $this->input->post('feature_meta_description'),
		"feature_meta_keyword"      => $this->input->post('feature_meta_keyword'),
		"feature_seo"               => $slug,
		"feature_updated_time"      => NOW);
		
		$this->db->set($data)->where('feature_id',$id)->update(FEATURE);
	}
		
	public function featureInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		
		$data = array("feature_photo"      => $file_name,
					  "feature_photo_name" => $photo_name,
					  "feature_photo_ext"  => $ext);
		
		$this->db->set($data)->where('feature_id',$id)->update(FEATURE);
	}
	
	public function getCategoryList($id)
	{
	
		$query = $this->db->select(array('category_id','category_name'))->from(CATEGORY)->where('category_status',"1")->get();
		
		if($query->num_rows()>0)
			{
				$data = '';
				foreach($query->result_array() as $row_sel)
				{
					$data .= "<option value=\"".$row_sel["category_id"]."\"";
					if($row_sel["category_id"]==$id)
					{
						$data .= " selected=\"SELECTED\"";
					}
					$data .= ">".stripslashes($row_sel["category_name"])."</option>";
				}
			}
			return $data;
	}
	
	public function getSubCategoryList($id,$parent_id)
	{
	
	
		
		$query = $this->db->select(array('sub_category_id','sub_category_name'))->from(SUB_CATEGORY)->where(array('sub_category_status'=>"1",'sub_category_parent'=>$parent_id))->get();
		
	   
		$data = '';
		if($query->num_rows()>0)
			{
				
				foreach($query->result_array() as $row_sel)
				{
					
					$data .= "<option value=\"".$row_sel["sub_category_id"]."\"";
					if($row_sel["sub_category_id"]==$id)
					{
						$data .= " selected=\"SELECTED\"";
					}
					$data .= ">".stripslashes($row_sel["sub_category_name"])."</option>";
				}
			}
			return $data;
	}
	
	public function getCategory()
	{
	
		$query = $this->db->select(array('category_id','category_name'))->from(CATEGORY)->where('category_status',"1")->get();
		
		if($query->num_rows()>0)
			{
				$data = '';
				foreach($query->result() as $row_sel)
				{
					$data[] = $row_sel;
				}
			}
			return $data;
	}
		
}
?>