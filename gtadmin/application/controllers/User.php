<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "user-list";
		$data['title'] = ucfirst("user List");
		
		/** Start Activate Records **/
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->user_model->userActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->user_model->userSuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->user_model->userDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('user/index');
        $config["total_rows"] = $this->user_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->user_model->user_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('user/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		
		if($this->uri->segment(2)=="add")
		{
			redirect(base_url('user/'));
		}	
		
		$data = "";
		$data['success'] = "";
		$data['basename'] = "user-add";
		$data['title'] = "Add user";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("admin_username","Username","required|is_unique[".ADMIN.".admin_username]");
		    $this->form_validation->set_rules("admin_password","Password","required|min_length[6]");
			$this->form_validation->set_rules("admin_email","Email","required|valid_email");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->user_model->userInsert();
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."user/add/");
			}
			
				
		}
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('user/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		if($this->uri->segment(2)=="edit")
		{
			redirect(base_url('user/'));
		}
		
		$data = "";
		$data['success'] = "";
		$data['basename'] = "user-edit";
		$data['title'] = "Edit user";
		

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("admin_username","Username","required|edit_unique_common[".ADMIN.".admin_username.".$pID.".admin_id]");
			$this->form_validation->set_rules("admin_password","Password","required|min_length[6]");
			$this->form_validation->set_rules("admin_email","Email","required|valid_email");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->user_model->userEdit($pID);
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."user/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->user_model->getuserDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('user/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "user-view";
		$data['title'] = "View user";
		
		if($this->uri->segment(3)=='')
		{
			redirect(base_url('user/'));
		}
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
				
		$data['records'] = $this->user_model->getuserDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('user/view',$data);
		$this->load->view('template/footer',$data);
		
	}
}
