<input type="hidden" id="hidden_currency" value="<?php echo $currency;?>"/>
<?php
$meeting_time1 = date('Y-m-d H:i:s', time() - 60 * 60 * 168);
$meeting_time1_string = strtotime($meeting_time1);
$Data2 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym='.$details[0]['coin_name'].'&tsyms='.$currency.'&ts='.$meeting_time1_string),true);
$old_price = $Data2[$details[0]['coin_name']][$currency];
$newprice = $details[0]['price'] - $old_price;
$change24 = $newprice / $details[0]['price'];
$change24 = number_format($change24 * 100,2);
$calc = ($change24 / 100);
$details[0]['volume24hour'] = $calc * $details[0]['price'];
$details[0]['change24hour'] = $change24; 
$Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym='.$details[0]['coin_name'].'&limit=168&tryConversion=false&tsym=USD'),true);
?>
    

<section id="coin_bundle" class="cd_main_bg">
			<div class="overall_counts" >
				<div class="container-fluid over_bg_purple">
					<div class="container-fluid" >
					
						<div class="row cd_value_diff cd_bit_top">
							<div class="col-2 col-md-2 text-left cd_coin_1l">
								<span class="coin_img"><img class="cd_ind_coin" src="<?php echo COIN_URL.$details[0]['coin_image_name']; ?>">
										<span class="coin_sh cd_coin_sh"> <?php echo $details[0]['coin_name']; ?></span>
									</span>
							</div>
							<div class="col-2 col-md-2 cd_align_center">
								<p class="coin_name"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i><?php echo $details[0]['price']; ?> </p>
								
								<p class="btc_txt"><?php echo $details[0]['coinname']; ?> price</p>
							</div>
							<div class="col-2 col-md-2 cd_align_center">
								     <?php
								     setlocale(LC_MONETARY, 'en_US');
								     ?>
								     	<p class="coin_name"><span id="lastprice">
								      <?php //if($details[0]['change24hour']>0){ echo "+";} echo money_format('%.2n', $details[0]['change24hour']) . "\n"; ?> 
								       <?php if($details[0]['change24hour']>0){ echo "+";} echo money_format('%.2n', $details[0]['volume24hour']) . "\n"; ?> 
								     </span></p>
								<p class="btc_txt" id="lastpricetxt">Last 1 week</p>
							</div>
							<div class="col-2 col-md-2 cd_align_center cd_coin_1r">
								<p class="coin_name"><span id="changeprice">
								     <?php //if($details[0]['change24hour']>0){ echo "+";} echo number_format($details[0]['changepct24hour'],2); ?>
								     <?php if($details[0]['change24hour']>0){ echo "+";} echo number_format($details[0]['change24hour'],2); ?>% 
								     
								     </span></p>
								     	<p class="btc_txt" id="changepricetxt">Change 1 week</p>
							</div>
								<div class="col-2 col-md-2 cd_align_center cd_coin_1r">
								<p class="coin_name"><span id="changeprice_high"></span></p>
								     	<p class="btc_txt" id="changepricetxt_high">1 week high</p>
							</div>
								<div class="col-2 col-md-2 cd_align_center cd_coin_1r">
								<p class="coin_name"><span id="changeprice_low"></span></p>
								     	<p class="btc_txt" id="changepricetxt_low">1 week low</p>
							</div>
						</div>
					</div>	
				</div>
				<div style="background:#F3E5F5">
				<div id="overall_counts" style="height:400px;position:relative;">
						<div id="container1" >
<div align="center"  id="loader"></div>
						</div>
								   <div align="center" class="flash2"></div>
								   
				</div>
				<div class="row">
							<div class="col-12 col-md-12">
								<div class="cd_hours">
									<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/onehour.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(1)">1 Hour</a>
									<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/oneday.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(24)">1 Day</a>
									<a class="cd_hours_active" href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/oneweek.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(168)">1 Week</a>
									<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/onemonth.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(720)">1 Month</a>
									<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/threemonth.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(2160)">3 Months</a>
									<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/sixmonth.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(4320)">6 Months</a>
									<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/oneyear.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange(8640)">1 Year</a>
									<!--<a href="javascript:;" onclick="chartData('<?php echo base_url(); ?>img/cryptobuttchart/alldata.php?coinName=<?php echo $this->uri->segment(1); ?>');priceChange('1')">All</a>-->
								</div>
							</div>
						</div>
						</div>
				<div class="container-fluid">
				    
					<div class="row cd_value_diff">
						<div class="col-3 col-md-3 cd_box cd_align_center c_right">
							<p>Marketing Cap</p>
							<p class="cd_amount"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo number_format($details[0]['mktcap']); ?></p>
						</div>
						<div class="col-3 col-md-3 cd_box cd_align_center c_right">
							<p>Volume 24 hours</p>
							<p class="cd_amount"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo number_format($details[0]['volume24hour']); ?></p>
						</div>
						<div class="col-3 col-md-3 cd_box cd_align_center  <?php if($details[0]['total_coin'] !=0){?>c_right <?php } ?>">
							<p>Circulating Supply</p>
							<p class="cd_amount"><!--<i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i>--> <?php echo number_format($details[0]['supply']); ?></p>
						</div>
					
						    <?php
						    if($details[0]['total_coin'] !=0){
						    ?>
						    <div class="col-3 col-md-3 cd_box cd_align_center b_r_none">
							<p>Max Supply</p>
							<p class="cd_amount"><!--<i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i>--> <?php echo number_format($details[0]['total_coin']); ?><span>  <?php //echo $details[0]['coin_name']; ?></span></p>
								</div>
							<?php
						    }
							?>
					
					</div>
				</div>
				<div class="container-fluid over_bg_block">
					<div class="container">
						<div class="row cd_value_diff cd_bit_top">
							<div class="col-12 col-md-12 cd_align_center cd_coin_1r">
								<h5><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Add key people and contributors on this project <i id="key-icons" class="fa fa-plus" aria-hidden="true"></i></a></h5>
							</div>
						</div>
					</div>	
				</div>
				<div class="box-panel" id="box-panel">
				<div id="collapseOne" class="panel-collapse collapse "> 
				<div class="box box-info ">
                    <div class="panel-body container">
                        <div class="row" id="sprofile">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                               <div class="triangle-down"></div>
                               <p class="error"></p>
                               <div id="loaderkeypeople" style="display:none;"></div>
                                 <form id="form-addkeypeople" method="POST" action="<?php echo site_url('coin_page/save_key_people');?>">
                                     <input type="hidden" class="form-control" id="coin_id" name="coin_id" value="<?php echo $this->uri->segment(1);?>">
                                  <div class="form-group key-text">
                                    <label for="fullname">Full Name</label>
                                    <small class="form-text text-muted">Key people or contributors</small>
                                    <input type="text" class="form-control" id="fullname" name="fullname" value="">
                                  </div>
                                  <div class="form-group key-text">
                                    <label for="roletype">Role type</label>
                                    <small class="form-text text-muted">Type his role on this project</small>
                                    <select id="roletype" name="roletype" class="form-control">
										<option value="Advisor">Advisor</option>
										<option value="Management">Management</option>
										<option value="Team">Team</option>
										<option value="Board of Directors">Board of Directors</option>
                                    </select> 
                                  </div>
                                   <div class="form-group key-text">
                                    <label for="roletitle">Role title</label>
                                    <small class="form-text text-muted">e.g. Developer, Marketing, Advisor</small>
                                     <input type="text" class="form-control" id="roletitle" name="roletitle" value="Advisor">
                                  </div>
                                  <div class="form-group key-text">
                                    <label for="profile">Add profile photo</label>
                                    <input type="file" id="image" name="image" class="file">
                                    <div class="input-group col-xs-12">
                                      <!--<span class="input-group-addon"><i class="fa fa-upload"></i></span>-->
                                      <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                                      <span class="input-group-btn">
                                        <button class="browse btn btn-primary1 input-lg" type="button"><!--<i class="fa fa-search"></i>--> Browse</button>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="form-group sve-btn">
                                    <label for="button"></label>
                                    <button class=" btn btn-primary1 input-lg input-lg1" type="submit" id="btn-submit">Save</button>
                                    <a href="javascript:;" class="cancel-btn">Cancel</a>
                                </div>
                                </form>
                                <div id="matchingcontacts" style="display:none">
                                    <input type="hidden" class="form-control" id="profilecoin_id" name="profilecoin_id" value="<?php echo $this->uri->segment(1);?>">
                                    <input type="hidden" class="form-control" id="peoplecoin_id" name="peoplecoin_id" value="">
                                    <input type="hidden" class="form-control" id="profileroletype" name="profileroletype" value="">
                                    <input type="hidden" class="form-control" id="profileroletitle" name="profileroletitle" value="">
                                    <div><img id="imageid" class="img-fluid c_rad" src="" style="width:80px;height:80px" /></div>
                                    <p>&nbsp;</p>
                                    <div style="font-weight:bold;"><span id="peoplename" style="color:#753ac8;"></span> profile matching with other contacts,</div>
                                    <p>&nbsp;</p>
                                    <div style="font-weight:bold;" >Do you want to ignore the matching profile and add <span id="peoplename1" style="color:#753ac8;"></span> as a new profile?</div>
                                    <p>&nbsp;</p><p>&nbsp;</p>
                                    <button class=" btn btn-primary1 input-lg input-lg1" type="submit" id="btn-submit-yes">Yes</button>
                                    <a href="javascript:;" class="cancel-btn">No</a>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-12" style="flex: 0 0 4.333333%;max-width: 4.333333%;"></div>
                            <!--<div class="vertical-line" style="height: 340px;"></div>-->
                            <div class="col-lg-1 col-md-1 col-sm-1 col-12" style="flex: 0 0 4.333333%;max-width: 4.333333%;"></div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                                <div class="form-group" style="margin:0">
                                    <label for="matching-profile" class="match-text" >Matchng profile:</label>
                                </div>
                                <div id="loaderkeypeople1" style="display:none;"></div>
                                <div class="keypeopleData"><div class="containg-profile"></div>
                                    <div id="keypeopleData">
                                    
                                </div>
                                </div>
                                
                            </div>
                        </div>
                        <div id="socialprofile" class="row" style="display:none" >
                            <form method="post" id="sociallinkform" action="">
                            <div class="triangle-down"></div>
                            <h3 class="text-center" style="font-weight:bold;">Do you have social profile?</h3>
                             <div class="text-center"><img id="imageid1" class="img-fluid c_rad" src="" style="width:80px;height:80px" /></div>
                             <h4 class="text-center" id="spname" style="font-weight:bold;"></h4>
                             <h6 class="text-center" id="sproletitle" style="color:#868e96"></h6>
                             <h6 class="text-center" id="sproletype" style="color:#868e96"></h6>
                             <input type="hidden" class="form-control" id="profileid1" name="profileid" value="">
                             <div id="InputSlider2">
                             <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-12"></div>
                             <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                                 <div class="form-group key-text">
                                    <label for="fullname" style="font-weight:bold;">Add profile link</label>
                                    <small class="form-text text-muted">e.g. Linkedin, Twitter, Blog</small>
                                    <input type="text" class="form-control ui-autocomplete-input" id="profilelink1" name="url1" value="" autocomplete="off">
                                  </div>
                             </div>
                             <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                                 <div class="form-group key-text">
                                    <label for="fullname" style="font-weight:bold;">Profile website</label>
                                    <small class="form-text text-muted">Select the website</small>
                                    <select id="profileweb1" name="type1" class="form-control">
                                       <option value=""></option>
										<?php
                						if(is_array($link_type_list))
                                        {
                                        	foreach($link_type_list as $link_type_lis)
                                        	{
                                        	?>
                                        	<option value="<?php echo $link_type_lis->link_type_id;?>"><?php echo $link_type_lis->link_type_name;?></option>
                                        	<?php
                                        	}
                                        }
                                        ?>
                                    </select>
                                  </div>
                                  </div>
                                  <div class="col-lg-1 col-md-1 col-sm-1 col-12"></div>
                             </div></div>
                             <div class="text-center"><p id="AddMoreImageBox2" style="font-size: 18px;color: #187db7;font-weight: 600;cursor:pointer;">+ Add More</p> <button class=" btn btn-primary1 input-lg input-lg1" type="button" id="social-btn">Save</button></div>
                        </from>
                        </div>
                    </div>
                </div></div>
				</div>
			</div>	
			<div class="clear"></div>
			<section class="container-fluid coin_group cd_pad_mob grad_bg bg-light ">
				<div class="row cd_value_diff">
					<div class="col-12 col-md-4 cd_box">
						<div class="row padge_row">
							<div class="col-2 col-md-2"><img class="img-fluid" src="<?php echo COIN_URL.$details[0]['coin_image_name']; ?>"></div>
							<div class="col-10 col-md-10 no-gutters">
									<div class="col-12 col-md-12 no-gutters">
										<h5><?php echo $details[0]['coinname']; ?> <span class="coin_txtsize"><?php echo $details[0]['coin_name']; ?></span>
										<br>
										<span style="font-size:11px;position: absolute;margin: -2px 0 0 2px;">Rank <?php echo $details[0]['sortorder']; ?></span>
										</h5>
										<br>
										<p>
                                            <?php  if(isset($coin->Application) AND $coin->Application!='' AND $coin->Application=="1") {?><span class="badge btn-primary">Application</span> <? } ?>
                                    		<?php  if(isset($coin->Coin) AND $coin->Coin!='' AND $coin->Coin=="1") {?><span class="badge btn-success">Coin</span><?php } ?>
                                    		<?php  if(isset($coin->currency) AND $coin->currency!='' AND $coin->currency=="1") {?><span class="badge btn-info">Currency</span><?php } ?>
                                    		<?php  if(isset($coin->erc20) AND $coin->erc20!='' AND $coin->erc20=="1") {?><span class="badge btn-warning">ERC20</span> <? } ?>
                                    		<?php  if(isset($coin->Exchange) AND $coin->Exchange!='' AND $coin->Exchange=="1") {?><span class="badge btn-danger">Exchange</span><?php } ?>
                                    		<?php  if(isset($coin->Hybrid) AND $coin->Hybrid!='' AND $coin->Hybrid=="1") {?><span class="badge btn-primary">Hybrid</span><?php } ?>
                                    		<?php  if(isset($coin->Mineable) AND $coin->Mineable!='' AND $coin->Mineable=="1") {?><span class="badge btn-success">Mineable</span> <? } ?>
                                    		<?php  if(isset($coin->Platform) AND $coin->Platform!='' AND $coin->Platform=="1") {?><span class="badge btn-info">Platform</span><?php } ?>
                                    		<?php  if(isset($coin->Privacy) AND $coin->Privacy!='' AND $coin->Privacy=="1") {?><span class="badge btn-warning">Privacy</span><?php } ?>
                                    		<?php  if(isset($coin->Token) AND $coin->Token!='' AND $coin->Token=="1") {?><span class="badge btn-danger">Token</span><?php } ?>
										</p>	
									</div>
							</div>
						</div>
						<div class="row cd_twit_box">
							<div class="col-4 col-md-2"></div>
							<div class="col-8 col-md-10 no-gutters">
								<div class="row">
								<?php if(is_array($links_lists))
								{
									$total = count($links_lists);
									$totalHalf = ceil($total/2);
									?>
									<div class="col-6 col-md-6 no-gutters">
										<ul class="dc_links">
										<?php 
										$i=1;
										foreach($links_lists as $links_list)
										{ 
										   if($i==$totalHalf+1)
										   {
											   echo '</ul>
									</div>	
									<div class="col-6 col-md-6 no-gutters">
										<ul class="dc_links">';
										   }
										if($links_list->link_type_name == 'Email'){
										    ?>
										    	<li><a target="_top" href="mailto:<?php echo $links_list->links_url;?>">
 <i class="<?php echo $links_list->link_type_font_awesome;?>"target="_blank" aria-hidden="true"></i> <?php echo $links_list->links_url; ?></a></li>
										<?php 
										}else{?>
										    	<li><a target="_blank" href="<?php echo $links_list->links_url;?>"><i class="<?php echo $links_list->link_type_font_awesome;?>"target="_blank" aria-hidden="true"></i> <?php echo $links_list->link_type_name;?></a></li>
										<?php $i++; }
										}
										?>
										</ul>
									</div>	
									<?php 
								} ?>
								</div>
							</div>
						</div>
					</div>
					<style>
								.scrollbar {
    background-color: #292f33;
    float: left;
    height: 650px;
    margin-bottom: 10px;
  /*  margin-left: 22px;
    margin-top: 27px;*/
    width: 65px;
       overflow-y: scroll;
}
#style-1::-webkit-scrollbar {
    width: 8px;
    background-color: #292f33;
} 
#style-1::-webkit-scrollbar-thumb {
    background-color: #753AC9;
	 height: 100px !important;
}
#style-1::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px #292f33;
    background-color: #292f33;
}
.twits::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px #292f33;
	background-color: #000;
}
.twits::-webkit-scrollbar
{
	width: 8px;
	background-color: #292f33;
}
.twits::-webkit-scrollbar-thumb
{
background-color: #753AC9;
	 height: 100px !important;
}
.file {
  visibility: hidden;
  position: absolute;
}
.btn.btn-primary1 {
  background-color: #753ac8;
  border-color: #753ac8;
  outline: none;color:#fff;
}
.btn.btn-primary1:hover {
  background-color: #442f62;
  border-color: #442f62;color:#fff;
}
.btn-primary1.focus, .btn-primary1:focus {box-shadow: 0 0 0 0;color:#fff;}
.btn-primary1:not([disabled]):not(.disabled).active, .btn-primary1:not([disabled]):not(.disabled):active, .show>.btn-primary1.dropdown-toggle {
    color: #fff;
    background-color: #000;
    border-color: #000;
    box-shadow: 0 0 0 0;color:#fff;
}
								</style>
					<div class="clear"></div>
					<div class="col-12 col-md-8 cd_box ">
						<div class="row cd_twit_box">
							<div class="col-12 col-md-12 no-gutters">
								<div class="row">
									<h5 class="follow_link col-md-6" ><!--<i class="fa  fa-user-circle" aria-hidden="true"></i>-->Key People</h5>
									<div class="col-md-6 text-right" style="padding:0">
									 <span class="" style="padding: 0 15px 0 40px;"><i class="fa fa-check-circle" aria-hidden="true"></i> VERIFIED PROFILE </span><span onclick="location.href = '<?php echo base_url().'home/help'; ?>';"  style="cursor:pointer;text-decoration:underline;">What is Verified profile?<!--<i class="fa fa-question"></i>--></span>
								</div></div>
											<div class="row">
											<div class="twits twits_horiz black_box">
										<div class="row cd_twit_box"  id="keypeoplelist">
										    <?php
										        if(is_array($keydata)) { 
										        foreach($keydata as $rowkeypeople) {
										    ?>
											<div class="col-12 col-md-12 no-gutters">
                                                <div class="row">
                                                	<div class="role-topic_twit"><span class="role-txt"><?php 
                                                	    echo $rowkeypeople->keypeople_roletype;
                                                	?></span></div>
                                                </div>
                                                </div>
                                                <?php
                                                $roletype = $rowkeypeople->keypeople_roletype;
                                                $coinid = $rowkeypeople->keypeople_coinid;
                                                $keydatagrup = $this->coin_page_model->getKeypeoplegroup($roletype, $coinid);
                                                foreach($keydatagrup as $rowtype) {
                                                ?>
                                                <div class="col-4 col-md-4 no-gutters">
                                                 <div class="col-12 col-md-12 no-gutters">
                                                <?php 
                            				     $linkid = $rowtype->keypeople_id;
                            				     $getdatagrup = $this->coin_page_model->getKeypeoplegrouplink($linkid);
                                                if(count($getdatagrup)>0) { ?>
                                                 <div class="containg-text"></div>
                                                 <?php } ?>
                                                <a target="_blank" href="<?php echo $rowtype->keypeople_id.'/'.$rowtype->keypeople_fullname; ?>">
                                                	<div class="row link_box" style="min-height:75px;">
                                                		<div class="col-3 col-md-3 no-gutters">
                                                		<img class="img-fluid c_rad" src="<?php echo base_url().'img/'.$rowtype->keypeople_filename; ?>">
                                                		</div>
                                                		<div class="col9 col-md-9 no-gutters">
                                                			<div class="row">
                                                				<div class="col-12 col-md-12 no-gutters">
                                                				    
                                                				<h6><?php echo $rowtype->keypeople_fullname; if(count($getdatagrup)>0) { ?><i class="fa fa-check" aria-hidden="true" style="position:absolute;right:-17px;"></i><?php } ?></h6>
                                                					<p><?php echo $rowtype->keypeople_roletitle; ?></p>
                                                				</div>	
                                                			</div>
                                                		</div>
                                                	</div>	
                                                	</a>
                                                	</div>
                                                </div>
                                                <?php
                                                }
										        }
										        }
										        ?>
										</div>
</div>	
</div>
							</div>	
						</div>	
					</div>
				</div>
			</section>
			<div class="clear"></div>
			<section class="container-fluid coin_group cd_pad_mob">
				<div class="row">	
					<div class="col-lg-8 coin_tab cd_bg">
						<ul class="nav nav-tabs">
							<li><a class="active" data-toggle="tab" href="#home" aria-expanded="false"><!--<i class="fa fa-bar-chart" aria-hidden="true"></i>--> <?php echo $details[0]['coinname']; ?> Markets</a></li>
							<li><a data-toggle="tab" href="#menu2" class="" aria-expanded="true"><!--<i class="fa fa-calendar" aria-hidden="true"></i>--> Historical data</a></li>
						</ul>
						<div class="tab-content bit-txt">
							<div id="home" class="tab-pane in active" aria-expanded="false">
								  <div id="cd_market"> 
						 <div class="card-body">
									<table class="table">
										<thead>
										    <tr>
										        <th scope="col"</th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
										    </tr>
											<tr>
												<th scope="col">S.No</th>
												<th scope="col">Source</th>
												<th scope="col">Pair</th>
												<th scope="col">Volume (24h)</th>
												<th scope="col">Price</th>
												<th scope="col">Volume (%)</th>
												<th scope="col">Updated</th>
											</tr>
										</thead>
										 <tbody id="pageData2" style="height:492px">
                                         </tbody>
										 <tr><td onclick="loadmore();" align="center" style="color:#fff;background-color:#753AC9;cursor:pointer;" colspan="7"><span >Load More</span></td></tr>
									</table>
								   <input type="hidden" id="market_count" value="<?php echo $count; ?>">
								   <input type="hidden" id="row_nos" value="0">
								   <input type="hidden" id="page_id" value="0">
								   <input type="hidden" id="currency_value" value="">
                          <!--- for pagination -->
								</div>
							</div>
							</div>
							<div id="menu2" class="tab-pane" aria-expanded="true">
								<h5>Historical data for <?php echo $details[0]['coinname']; ?></h5>
						       <div class="pull-right col-xs-7 col-sm-3 col-md-3 col-lg-3 alg sect dsk_align mar-top2 desktop" >
								<div  class="flash2"></div>
									
									<style>
									.caret {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px solid;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
}
									</style>
									
									<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span> <b class="caret"></b>
</div>
								</div>
						  
						  
						      <div class="card-body">
									<table class="table">
										<thead>
										    <tr>
										        <th scope="col"</th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
												<th scope="col"></th>
										    </tr>
											<tr>
												<th scope="col">Date</th>
												<th scope="col">Open</th>
												<th scope="col">High</th>
												<th scope="col">Low</th>
												<th scope="col">Close</th>
												<th scope="col">Volume</th>
												<th scope="col">Market Cap</th>
											</tr>
										</thead>
										
										<tbody id="pageData" >
                                        </tbody>
										
										
										
									</table>
									<div align="center" class="flash1"></div>
							 </div>
							 
							</div>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="cd_box">
					    <div class="row cd_twit_box">
							<div class="col-12 col-md-12 no-gutters">
								<div class="row">
									<div class="col-12 col-md-12 no-gutters">
										<h5 class="follow_link"><i class="fa fa-twitter" aria-hidden="true"></i> Follow <a target="_blank" href="<?php echo $details[0]['twitter_url']; ?>">@<?php echo $details[0]['coinname']; ?></a> on twitter</h5>
									</div>
								</div>
								
							</div>
						</div>
						<?php
						if($details[0]['twitter_url']) {
						?>
						<div  class="row cd_twit_box ">
							<div class="col-12 col-md-12 no-gutters ">
								<div class="row">
								
								
									<div  class="col-12 col-md-12 no-gutters scrollbar twits">
										<a  class="twitter-timeline" data-theme="dark" data-link-color="#F5F8FA"  href="<?php echo $details[0]['twitter_url']; ?>"></a> 

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
									</div>
								</div>
								
							</div>
						</div>
						<?php } ?>
						
					</div>
					    
					</div>
				</div>
			</section>
			
		</section>
		<script>

        var arr = <?php echo json_encode($Data['Data']); ?>;
        function getMax(arr, prop) {
            var max
            for (var i=0 ; i<arr.length ; i++) {
                if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
                    max = arr[i];
            }
            return max;
        }
         function getMin(arr, prop) {
            var min
            for (var i=0 ; i<arr.length ; i++) {
                if (!min || parseInt(arr[i][prop]) < parseInt(min[prop]))
                    min = arr[i];
            }
            return min;
        }
        var maxPpg = getMax(arr, "high");
        var minPpg = getMin(arr, "low");
        
        var maxValue = parseFloat(maxPpg.high),minValue = parseFloat(minPpg.low);
        document.getElementById('changeprice_high').innerHTML = maxValue.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
        document.getElementById('changeprice_low').innerHTML = minValue.toLocaleString('en-US', { style: 'currency', currency: 'USD' });;
        document.getElementById('changepricetxt_high').innerHTML = "1 week high";
        document.getElementById('changepricetxt_low').innerHTML = "1 week low";
        
</script>