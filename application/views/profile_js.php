<script>

function change_currency(symbol,name)
{
	 $("#change_currency").html(symbol+" "+name);
}

function currency(per_page)
{
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html('<div id="loader"></div>');
     var dataString = 'pageId='+ pageId+'&currency='+per_page;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 $("#change_title").html(per_page);
           }
      });

}

function tops(type,val)
{
	if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
		$("#bal_al").html("Loading...");
		
	}
	else if(type=="sorting")
	{
		<?php if($this->detect->isMobile())
		{ ?>
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val+"_mobile" ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
		}
		<?php }else
		{			?>
	
	$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "active" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('active');
			 $( "#sortby_volume").removeClass('active');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('active');
			 $( "#sortby_volume").removeClass('active');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('active');
			 $( "#sortby_marketcap").removeClass('active');
		}
	
	    <?php } ?>
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#hidden_search").val();
	
	<?php  }else { ?>
	search = $("#hidden_search").val();
	<?php } ?>
	sorting = $("#hidden_sorting").val();
	
      
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html('<div id="loader"></div>');
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&sorting='+sorting+'&search='+search;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Ajax/load_profile');?>',
           data: dataString,
           cache: false,
           success: function(result){
			 //  alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 document.getElementById("row_no").value = 1;
				 document.getElementById("pageids").value = 0;
				 if(type=="tops")
				 {
					$("#change_title").html(per_page);
				 }
				 displaybalance();
           }
      });

}
function watch_heart(){ 
 var data= 'coinId=1360';
  $.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>ajax_profile/favdata',
           data: data,
           cache: false,
           dataType : 'json',
           success: function(response){
           }
      });
}
watch_heart();

  function changePagination(pageId,liId){
	  search = $("#hidden_search").val();
	  currency = $("#hidden_currency").val();
	  $("#pageData").html('<span class="flash"></span>');
     $(".flash").show();
     $(".flash").fadeIn(400).html('<div id="loader"></div>');
      var watchname = '<?php echo $watchname; ?>';
         var dataString = 'pageId='+ pageId+'&search='+ search+'&currency='+ currency+'&watchname='+ watchname;
     
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_profile/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			 
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
				 if(result!='--No records found--')
				 {
                 $("#pageData").html(result);
                 
                 
                 
                 function watchmouseout(id) {
                    $('.watch'+id).removeClass('fa-heart');
                    $('.watch'+id).addClass('fa-heart-o');
                 }
                 $(".watch_heart").hover(function() {
                     var data= 'coinId='+ this.id;
                      $.ajax({
                               type: "POST",
                               url: '<?php echo base_url(); ?>ajax_profile/favdata',
                               data: data,
                               cache: false,
                               dataType : 'json',
                               success: function(response){
                                   if(response){
                                       $('.watch'+response[0].watch_nameid).removeClass('fa-heart-o');
                                       $('.watch'+response[0].watch_nameid).addClass('fa-heart');
                                   }
                                   $(".watch_heart").mouseout(function() { 
                                       watchmouseout(response[0].watch_nameid);
                                   });
                    			 //console.info(response[0].watch_nameid);
                                    
                               }
                          });
                 });
                 
                     
                    $('.watchlist').click(function() {
                         var data= 'coinId='+ this.id+'&watchId='+ this.rev;
                          $.ajax({
                               type: "POST",
                               url: '<?php echo base_url(); ?>ajax_profile/watchlist_data',
                               data: data,
                               cache: false,
                               success: function(response){
                    			 if(response == 'true'){
                    			     $.toast({
                                            heading: 'Success',
                                            text: 'Added your crypto asset watch list.',
                                            showHideTransition: 'slide',
                                             position: 'bottom-left',
                                            icon: 'success'
                                        });
                    			 }else{
                    			    	$.toast({
                                        heading: 'Error',
                                        text: 'Already added your crypto asset watch list.',
                                        showHideTransition: 'fade',
                                         position: 'bottom-left',
                                        icon: 'error'
                                    }); 
                    			 }
                                    
                               }
                          });
                    });
				 }
           }
      });
}

changePagination("0","0");


setInterval(function(){ 

$('.coin_list').each(function(){

var id=$(this).attr('id');
   
var visible = $(this).visible( "partial" );
		 
	
		if(visible)
		{	

currency = $("#hidden_currency").val();
 prev_price = $("#beat"+id+"price").html();
  var dataString = 'id='+ id+'&currency='+currency+'&prev_price='+prev_price;
	$.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/updaterow');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  var obj = JSON.parse(result); 
               $("#beat"+id+"price").html(obj.price);
			   $("#beat"+id+"market").html(obj.mktcap);
			   $("#beat"+id+"hour").html(obj.volume24hour);
			   $("#beat"+id+"change").html(obj.change24hour);
			   $("#beat"+id+"price_mobile").html(obj.price);
			   $("#beat"+id+"market_mobile").html(obj.mktcap);
			   $("#beat"+id+"volume24hour_mobile").html(obj.volume24hour);
			   $("#beat"+id+"change_mobile").html(obj.change24hour);
			   if(obj.price_change==="Yes")
			   {
				   
				   if(obj.change24hour<0)
				   {
					     $( "#currency"+id).removeClass('green');
						 $( "#beat"+id+"hour").removeClass('green');
						 
						  $("#beat"+id+"change_mobile").removeClass('green');
						  $( "#currency"+id).addClass('red');
						 $( "#beat"+id+"hour").addClass('red');
						  $("#beat"+id+"change_mobile").addClass('red');
			    $("#"+id).addClass( "row_background" );
				setTimeout(function(){
            $( "#"+id).removeClass('row_background');
          
    }, 500);
				   }
				   if(obj.change24hour>0)
				   {
					      $( "#currency"+id).removeClass('red');
						 $( "#beat"+id+"hour").removeClass('red');
						 
						  $("#beat"+id+"change_mobile").removeClass('red');
				     $( "#currency"+id).addClass('green');
						 $( "#beat"+id+"hour").addClass('green');
						  $("#beat"+id+"change_mobile").addClass('green');
						 
	 $( "#"+id).addClass( "row_background2" );
				setTimeout(function(){
            $( "#"+id).removeClass('row_background2');
          
				   }, 500);    
				   }
			   }
			 
           }
      });
		}
});

}, 5000);

$(document).on('scroll', function(){ 
	  if($(document).height()-400 <= ($(document).scrollTop() + $(document).height()))
	  {
		  totalcoins = document.getElementById('coins_count').value;
		  totalcoins = Number(totalcoins);
		  var val = document.getElementById("row_no").value;
		  pagedisplays = val * <?php echo $this->settings->settings_records_per_page_front;?>;
		  if(totalcoins>pagedisplays)
		  {
			  //alert(val);
			 var pageids = document.getElementById("pageids").value;
			 var pageidsarray = pageids.split(",");
			  var i = pageidsarray.indexOf(val);
             if(i > -1){ 
			 //alert('inside if condition'+i);
			     //loadmore();
			 }
			 else
			 {
				 //alert();
			  loadmore();
			  document.getElementById('pageids').value=pageids+','+val;
			 }
			  
		  }
	  }
    });
function loadmore()
{  
	type = "";
	 var val = document.getElementById("row_no").value;
	 
	 if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
	}
	else if(type=="sorting")
	{
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_marketcap").removeClass('selecte');
		}
	}
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	<?php if($this->detect->isMobile())
	{ ?>
	search = $("#hidden_search").val();
	<?php  }else { ?>
	search = $("#hidden_search").val();
	<?php } ?>
	sorting = $("#hidden_sorting").val();
	pageId = val;
	liId = "0";
	 var content = document.getElementById("pageData");
     content.innerHTML = content.innerHTML+'<span class="flash"></span>';
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&sorting='+sorting+'&search='+search;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_profile/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 //$("#pageData").html(result);
				 var content = document.getElementById("pageData");
				 //alert(result);
				 if(result!="--No records found--")
				 {
				    
					 content.innerHTML = content.innerHTML+result;
					 document.getElementById("row_no").value = Number(val)+1;
					 if(type=="tops")
					 {
						$("#change_title").html(per_page);
					 }
				 }
				 
           }
      });
}
function addportfolio(val)
{
	<?php if($this->session->userdata('user_id'))
	{ ?>

   var dataString = 'cid='+ val;
 	$("#heart_"+val).fadeIn(400).html('<div id="loader"></div>');
	 $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_addportfolio/add');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   
               if(result=="success")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else if(result=="exit")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart-o heart_cor\" aria-hidden=\"true\"></i>");

			   }
                 //$("#pageData").html(result);
				
           }
      });
	<?php }
	else
	{
		
		?>
		$('#myModal').modal('show');
    <?php 
    } 
	?>
}
function removeFavouriteid(val)
{
	<?php if($this->session->userdata('user_id'))
	{ 
	  
	                	
	    ?>

   var dataString = 'cid='+ val;
 	$("#heart_"+val).fadeIn(400).html('<div id="loader"></div>');
	 $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_addportfolio/removeFavourite');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   
               if(result!="exit")
			   {
				   //$("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
				 document.getElementById("coins_count1").innerHTML  = result;
				   $("#"+val).css('display','none');
			   }
			   else if(result=="exit")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart-o heart_cor\" aria-hidden=\"true\"></i>");

			   }
                 //$("#pageData").html(result);
				
           }
      });
	<?php }
	else
	{
		
		?>
		$('#myModal').modal('show');
    <?php 
    } 
	?>
}

// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#coin_name").css("background","#FFF");
		}
		});
	});
});


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name_mobile").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});
//To select country name
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val;
	<?php if($this->detect->isMobile())
	{ ?>
$("#coin_name_mobile").val(val);
$("#suggesstion-box_mobile").hide();

	<?php }else{ ?>
	$("#coin_name").val(val);
$("#suggesstion-box").hide();
	<?php } ?>
//tops('search','');
}

/**
$(window).on('load', function() {
  displaybalance();
}) **/


function displaybalance(){
	
	
	 var currency = $("#hidden_currency").val();
     var dataString = 'currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=home');?>',
           data: dataString,
           cache: false,
           success: function(result){
			
                 $("#pageData3").html(result);
           }
      });
}

function dropValueAtID(id,value,id2,value2)
{
	if(id)
	{
		$("#"+id).html(value);
	}
	if(id2)
	{
		$("#"+id2).val(value2);
	}
	
	if(id2=="sorting_price")
	{
		$("#hidden_sorting").val("price");
		$("#sorting_market_html").html("<span style='margin-left:5px;'>---</span>");
	}
	else if(id2=="sorting_market")
	{
		$("#hidden_sorting").val("marketcap");
		$("#sorting_price_html").html("<span style='margin-left:5px;'>---</span>");
	}
	else if(id2=="sorting_change")
	{
		$("#hidden_sorting").val("volume");
	}
	$("#pageids").val('0');
	$("#row_no").val('1');
}
function removeFromlist(id){
     var watchname = '<?php echo $watchname; ?>';
     var userid    = '<?php echo $this->session->userdata('user_id'); ?>';
     var dataString = 'coinId='+ id+'&watchName='+ watchname+'&userId='+ userid;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_profile/removefromlist');?>',
           data: dataString,
           cache: false,
           success: function(response){
			    $('#'+response).fadeOut(400);
                $.toast({
                heading: 'Success',
                text: 'Removed from the watch list.',
                showHideTransition: 'slide',
                 position: 'bottom-left',
                icon: 'success'
            }); 
           }
      });
}function removeprofileFromlist(id){
     var userid    = '<?php echo $this->session->userdata('user_id'); ?>';
     var dataString = 'coinId='+ id+'&userId='+ userid;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_profile/removefromhomelist');?>',
           data: dataString,
           cache: false,
           success: function(response){
			    $('#'+response).fadeOut(400);
                $.toast({
                heading: 'Success',
                text: 'Removed from the watch list.',
                showHideTransition: 'slide',
                 position: 'bottom-left',
                icon: 'success'
            }); 
           }
      });
;
 } 
function dropValueAtID(id,value,id2,value2,id3)
{
   
   if(id3 == 'f1'){
        $('#f1').css('color','#FFEB3B');
   }else{
       $('#f1').removeAttr("style")
   }
    if(id3 == 'f2'){
        $('#f2').css('color','#FFEB3B');
   }else{
       $('#f2').removeAttr("style")
   }
    if(id3 == 'f3'){
        $('#f3').css('color','#FFEB3B');
   }else{
       $('#f3').removeAttr("style")
   }
    if(id3 == 'f4'){
        $('#f4').css('color','#FFEB3B');
   }else{
       $('#f4').removeAttr("style")
   }
    if(id3 == 'f5'){
        $('#f5').css('color','#FFEB3B');
        $('#filter_txt1').text('1 Hour high');
        $('#filter_txt2').text('1 Hour low');
   }else{
       $('#f5').removeAttr("style")
   }
    if(id3 == 'f6'){
        $('#f6').css('color','#FFEB3B');
        $('#filter_txt1').text('1 Day high');
        $('#filter_txt2').text('1 Day low');
   }else{
       $('#f6').removeAttr("style")
   }
    if(id3 == 'f7'){
        $('#f7').css('color','#FFEB3B');
        $('#filter_txt1').text('1 Week high');
        $('#filter_txt2').text('1 Week low');
   }else{
       $('#f7').removeAttr("style")
   }
    if(id3 == 'f8'){
        $('#f8').css('color','#FFEB3B');
        $('#filter_txt1').text('1 Month high');
        $('#filter_txt2').text('1 Month low');
   }else{
       $('#f8').removeAttr("style")
   }
    if(id3 == 'f9'){
        $('#f9').css('color','#FFEB3B');
        $('#filter_txt1').text('3 Months high');
        $('#filter_txt2').text('3 Months low');
   }else{
       $('#f9').removeAttr("style")
   }
    if(id3 == 'f10'){
        $('#f10').css('color','#FFEB3B');
        $('#filter_txt1').text('6 Months high');
        $('#filter_txt2').text('6 Months low');
   }else{
       $('#f10').removeAttr("style")
   }
   if(id3 == 'f11'){
        $('#f11').css('color','#FFEB3B');
        $('#filter_txt1').text('1 One high');
        $('#filter_txt2').text('3 One low');
   }else{
       $('#f11').removeAttr("style")
   }
  if(id3 == 'f12'){
        $('#f12').css('color','#FFEB3B');
        $('#filter_txt1').text('All high');
        $('#filter_txt2').text('All low');
   }else{
       $('#f12').removeAttr("style")
   }
    
  
	if(id)
	{
		$("#"+id).html(value);
	}
	if(id2)
	{
		$("#"+id2).val(value2);
	}
	
	if(id2=="sorting_price")
	{
		$("#hidden_sorting").val("price");
		$("#sorting_market_html").html("<span style='margin-left:5px;'>---</span>");
	}
	else if(id2=="sorting_market")
	{
		$("#hidden_sorting").val("marketcap");
		$("#sorting_price_html").html("<span style='margin-left:5px;'>---</span>");
	}
	else if(id2=="sorting_change")
	{
		$("#hidden_sorting").val("volume");
	}
	$("#pageids").val('0');
	$("#row_no").val('1');
	
	
}
</script>