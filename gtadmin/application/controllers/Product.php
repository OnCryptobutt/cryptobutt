<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "product-list";
		$data['title'] = "Product List";
		
		/** Start Activate Records **/
		$search = "";
		if($this->input->get('act')=="search")
		{
			$search = " WHERE product_name like '%".$this->input->get('search')."%' ";
		}
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->product_model->productActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->product_model->productSuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->product_model->productDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('product/index');
        $config["total_rows"] = $this->product_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->product_model->product_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('product/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "product-add";
		$data['title'] = "Add product";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("product_name","Name","required|is_unique[".PRODUCT.".product_name]");
			$this->form_validation->set_rules("product_category_id","Category","required");
			$this->form_validation->set_rules("product_sub_category_id","Sub Category","required");
			$this->form_validation->set_rules("product_price","Price","required");
		    $this->form_validation->set_rules("product_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->product_model->productInsert();
				
				if($_FILES['product_photo']['name']!="")
				{
					
					$ext_array = explode(".",$_FILES['product_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "product_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = PRODUCT_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('product_photo'))
					{
						// case - failure
						
					
						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->product_model->productInsertUpdate($file_name,$photo_name,$ext,$pID);	
					}
					
					$source_path = PRODUCT_PATH.$file_name;
					$target_path = PRODUCT_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."product/add/");
			}
			
				
		}
		$data['getCategoryList']    = $this->product_model->getCategoryList($this->input->post('product_category_id'));
        $data['getSubCategoryList'] = $this->product_model->getSubCategoryList($this->input->post('product_sub_category_id'),$this->input->post('product_category_id'));		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('product/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "product-edit";
		$data['title'] = "Edit product";
		

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("product_name","Name","required|edit_unique_common[".PRODUCT.".product_name.".$pID.".product_id]");
			$this->form_validation->set_rules("product_category_id","Category","required");
			$this->form_validation->set_rules("product_sub_category_id","Sub Category","required");
			$this->form_validation->set_rules("product_price","Price","required");
			$this->form_validation->set_rules("product_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->product_model->productEdit($pID);
				
				if($_FILES['product_photo']['name']!="")
				{
				
				    $this->product_model->productImageDelete($pID);
				
					$ext_array = explode(".",$_FILES['product_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "product_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = PRODUCT_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

				$this->upload->do_upload('product_photo');
				
						
						$this->product_model->productInsertUpdate($file_name,$photo_name,$ext,$pID);	
					
					
					 $source_path = PRODUCT_PATH.$file_name;
					$target_path = PRODUCT_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
					
					
				}
				
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."product/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->product_model->getproductDetails($pID);
		$data['getCategoryList']    = $this->product_model->getCategoryList($data['records']->product_category_id);
        $data['getSubCategoryList'] = $this->product_model->getSubCategoryList($data['records']->product_sub_category_id,$data['records']->product_category_id);
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('product/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "product-view";
		$data['title'] = "View product";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->product_model->getproductDetails($pID);
		$data['records']->category_name = $this->boost_model->getValue(CATEGORY,"category_name","category_id='".$data['records']->product_category_id."'");
		$data['records']->sub_category_name = $this->boost_model->getValue(SUB_CATEGORY,"sub_category_name","sub_category_id='".$data['records']->product_sub_category_id."'");
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('product/view',$data);
		$this->load->view('template/footer',$data);
		
	}
}
