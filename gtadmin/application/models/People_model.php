<?php
class People_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function people_list($limit, $start)
	{
		$sql = "SELECT * FROM ".PEOPLE." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    
    
    
	public function getkeypeoplename() {
	    $sql = "SELECT * FROM ci_addkeypeople ORDER BY keypeople_id DESC";
	    $query = $this->db->query($sql);
	    if ($query->num_rows() > 0)
	   {
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
       }
	}
	function getRows($params = array())
    {
        $this->db->select('*');
        $this->db->from(people);
        $this->db->order_by('people_id','desc');
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }
        
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
	
	public function getpeopleDetails($id)
    {
	   $sql = "SELECT * FROM ".PEOPLE." WHERE people_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      return $this->db->count_all(PEOPLE);
    }
   
    public function peopleActivate($id)
    {
	   $sql = "UPDATE ".PEOPLE." SET people_status='1' WHERE people_id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function peopleSuspend($id)
	{
		$sql = "UPDATE ".PEOPLE." SET people_status='0' WHERE people_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function peopleDelete($id)
	{
		$photoname = $this->boost_model->getValue(PEOPLE,"people_photo_name","people_id='".$id."'");
		$photoext = $this->boost_model->getValue(PEOPLE,"people_photo_ext","people_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(PEOPLE_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(PEOPLE_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(PEOPLE_PATH.$photoname.".".$photoext))
			{
				unlink(PEOPLE_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".PEOPLE." WHERE people_id='$id'";
		$query = $this->db->query($sql);
	}
	public function keypeopleDelete($id)
	{
		 $photoname = $this->boost_model->getValue('ci_addkeypeople',"keypeople_filename","keypeople_id='".$id."'");
		//$photoext = $this->boost_model->getValue(PEOPLE,"people_photo_ext","people_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists("https://www.cryptobutt.com/img/".$photoname))
			{
				unlink("https://www.cryptobutt.com/img/".$photoname);
			}
			
		//	if(file_exists(PEOPLE_PATH.$photoname.".".$photoext))
		//	{
			//	unlink(PEOPLE_PATH.$photoname.".".$photoext);
		//	}
		}
		$sql = "DELETE FROM ci_addkeypeople WHERE keypeople_id='$id'";
		$query = $this->db->query($sql);
		$sql1 = "DELETE FROM ci_keypeoplesociallink WHERE social_people_id='$id'";
		$query1 = $this->db->query($sql1);
	}
	
	public function peopleImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(PEOPLE,"people_photo_name","people_id='".$id."'");
		$photoext = $this->boost_model->getValue(PEOPLE,"people_photo_ext","people_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(PEOPLE_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(PEOPLE_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(PEOPLE_PATH.$photoname.".".$photoext))
			{
				unlink(PEOPLE_PATH.$photoname.".".$photoext);
			}
		}
	}
		
	public function peopleInsert()
	{
		$slug = url_title($this->input->post('people_name'), 'dash', true);
		
		$sql = "INSERT INTO ".PEOPLE." SET 
		
		people_name         = '".$this->input->post('people_name')."',
		people_first_name         = '".$this->input->post('people_first_name')."',
		people_middle_name         = '".$this->input->post('people_middle_name')."',
		people_last_name         = '".$this->input->post('people_last_name')."',
		people_role_type         = '".$this->input->post('people_role_type')."',
		people_linked_in_url         = '".$this->input->post('people_linked_in_url')."',
		people_verified         = '".$this->input->post('people_verified')."',
		people_content      = '".$this->input->post('people_content')."',
		people_status       = '".$this->input->post('people_status')."',
		people_order        = '".$this->input->post('people_order')."',
		people_seo          = '".$slug."',
		people_created_time = '".NOW."',
		people_updated_time = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function peopleEdit($id)
	{
		$slug = url_title($this->input->post('people_name'), 'dash', true);
			
		$sql = "UPDATE ".PEOPLE." SET 
		
		people_name         = '".$this->input->post('people_name')."',
		people_first_name         = '".$this->input->post('people_first_name')."',
		people_middle_name         = '".$this->input->post('people_middle_name')."',
		people_last_name         = '".$this->input->post('people_last_name')."',
		people_role_type         = '".$this->input->post('people_role_type')."',
		people_linked_in_url         = '".$this->input->post('people_linked_in_url')."',
		people_verified         = '".$this->input->post('people_verified')."',
		people_content      = '".$this->input->post('people_content')."',
		people_status       = '".$this->input->post('people_status')."',
		people_order        = '".$this->input->post('people_order')."',
		people_seo          = '".$slug."',
		people_updated_time = '".NOW."' WHERE people_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function peopleInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".PEOPLE." SET 
		
		people_photo      = '".$file_name."',
		people_photo_name = '".$photo_name."',
		people_photo_ext  = '".$ext."' WHERE people_id='".$id."'";
		
		$this->db->query($sql);
	}
	 
	public function getLink_typeList()
	{
		$query = $this->db->select('*')->from(LINK_TYPE)->where("link_type_status","1")->order_by("link_type_name","ASC")->get();
	
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}

	public function getLinks($id)
	{
		$query = $this->db->select('*')->from(SOCIAL)->where("social_people_id",$id)->get();
		
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$type_array[] = $row->social_links_type;
				if(in_array($row->social_links_type,$type_array))
				{
					
					$counts = array_count_values($type_array);
					if($counts[$row->social_links_type]>1)
					{
						$row->links_type_is = $row->social_links_type;
						$row->links_type_count = $counts[$row->social_links_type];
					}
					else
					{
						$row->links_type_is = "";
						$row->links_type_count = "";
					}
				}
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	
	public function deletesocial($id)
	{
		
		$this->db->where('social_id', $id);
        $this->db->delete(SOCIAL);
	}
	public function linksInsertdata($data)
	{
		$this->db->insert('ci_keypeoplesociallink',$data);
		return $this->db->insert_id();
	}
	public function linksInsert($data)
	{
		$this->db->insert(SOCIAL,$data);
		
		return $this->db->insert_id();
	}
	
	public function linksUpdate($data,$id)
	{
		$this->db->where('social_id', $id);
		$this->db->update(SOCIAL,$data);
	
	}
	public function updatepeopledata($id)
	{
	    $data=array('verify_link'=> 'true');
		$this->db->where('keypeople_id', $id);
		$this->db->update('ci_addkeypeople',$data);
	
	}
	
		
}
?>