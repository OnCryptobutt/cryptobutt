<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('portfolio_model');
		$this->detect = new Mobile_Detect();
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url());
		}
    }
	
	public function index()
	{
		$data = "";
	    $data['basename'] = "portfolio";
		/** $data['banners'] = $this->portfolio_model->getBanner(); **/
		
		$user_id = $this->session->userdata('user_id');
		
		 $data['get_portfolio_lists'] = $this->portfolio_model->get_portfolio_lists($user_id);
		
		$data['coin_names'] = $this->portfolio_model->get_coin_names(); 

		$this->load->view('template/header',$data);
		$this->load->view('portfolio_page',$data);
	}
}
