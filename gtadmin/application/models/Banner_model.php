<?php
class Banner_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function banner_list($limit, $start)
	{
		$sql = "SELECT * FROM ".BANNER." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    
	
	function getRows($params = array())
    {
        $this->db->select('*');
        $this->db->from(BANNER);
        $this->db->order_by('banner_id','desc');
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }
        
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
	
	public function getbannerDetails($id)
    {
	   $sql = "SELECT * FROM ".BANNER." WHERE banner_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      return $this->db->count_all(BANNER);
    }
   
    public function bannerActivate($id)
    {
	   $sql = "UPDATE ".BANNER." SET banner_status='1' WHERE banner_id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function bannerSuspend($id)
	{
		$sql = "UPDATE ".BANNER." SET banner_status='0' WHERE banner_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function bannerDelete($id)
	{
		$photoname = $this->boost_model->getValue(BANNER,"banner_photo_name","banner_id='".$id."'");
		$photoext = $this->boost_model->getValue(BANNER,"banner_photo_ext","banner_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(BANNER_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(BANNER_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(BANNER_PATH.$photoname.".".$photoext))
			{
				unlink(BANNER_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".BANNER." WHERE banner_id='$id'";
		$query = $this->db->query($sql);
	}
	
	public function bannerImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(BANNER,"banner_photo_name","banner_id='".$id."'");
		$photoext = $this->boost_model->getValue(BANNER,"banner_photo_ext","banner_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(BANNER_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(BANNER_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(BANNER_PATH.$photoname.".".$photoext))
			{
				unlink(BANNER_PATH.$photoname.".".$photoext);
			}
		}
	}
		
	public function bannerInsert()
	{
		$slug = url_title($this->input->post('banner_name'), 'dash', true);
		
		$sql = "INSERT INTO ".BANNER." SET 
		
		banner_name         = '".$this->input->post('banner_name')."',
		banner_content      = '".$this->input->post('banner_content')."',
		banner_status       = '".$this->input->post('banner_status')."',
		banner_order        = '".$this->input->post('banner_order')."',
		banner_seo          = '".$slug."',
		banner_created_time = '".NOW."',
		banner_updated_time = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function bannerEdit($id)
	{
		$slug = url_title($this->input->post('banner_name'), 'dash', true);
			
		$sql = "UPDATE ".BANNER." SET 
		
		banner_name         = '".$this->input->post('banner_name')."',
		banner_content      = '".$this->input->post('banner_content')."',
		banner_status       = '".$this->input->post('banner_status')."',
		banner_order        = '".$this->input->post('banner_order')."',
		banner_seo          = '".$slug."',
		banner_updated_time = '".NOW."' WHERE banner_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function bannerInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".BANNER." SET 
		
		banner_photo      = '".$file_name."',
		banner_photo_name = '".$photo_name."',
		banner_photo_ext  = '".$ext."' WHERE banner_id='".$id."'";
		
		$this->db->query($sql);
	}
		
}
?>