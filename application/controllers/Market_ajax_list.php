<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Market_ajax_list extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->load->model('coin_page_model');
		$this->settings = $this->boost_model->loadSettings();
		$this->db2 = $this->boost_model->getHistoryDb();
    }
	
	function load_market_lists()
	{
		
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $pid=$_POST['pageId'];
         }
		 else
		 {
		   $pid='0';
		 }
		 
		if(isset($_POST['currency']) && !empty($_POST['currency']))
		{
           $currency=$_POST['currency'];
        }
		 else
		 {
		   if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "";
			}
			
			if($currency=="")
			{
			$currency = "USD";
			}
		 }
		 
		 

        //$start=$this->settings->settings_records_per_page_front*$pid;
		//$limit=$this->settings->settings_records_per_page_front;
		//$limit='1';
		$start=10*$pid;
		$limit=10;
		
		
		
		$id='';
        if(isset($_POST['cid']) && !empty($_POST['cid']))
		{
           $id=$_POST['cid'];
		 
        }
		
		
		
		
		
		
		$market_lists = $this->coin_page_model->market_data2($id,$currency,$limit,$start);	
				
			?>	
				
				<?php if(is_array($market_lists)) {
                    					
					$sno=$start+1;				    
										 foreach($market_lists as $list) {
									//print_r($list);
									?>	
										
										<tr>
											<th scope="row"><?php echo $sno; ?></th>
											<td><?php echo $list->lastmarket;  ?></td>
											<td><?php echo $list->coins;  ?>/<?php echo $currency;  ?></td>
											<td><?php if($list->volume24hour!='') { echo $list->currency_sym.' '; } ?><?php echo $list->volume24hour;  ?></td>
											<td><?php if($list->price!='') { echo $list->currency_sym.' '; } ?><?php echo $list->price;  ?></td>
											<td><?php echo $list->volume24hourto;  ?><?php if($list->volume24hourto!='') { echo ' %'; } ?>	</td>
											<td><?php echo date('d M y H:i:s',strtotime($list->created_time));  ?></td>
											
										</tr>
										
									<?php $sno++;  } } 
									else
									{  ?>  
								
								 <tr><td align="center" colspan="7"><font color="red">--No records found!--</font></td></tr>
								   
								<?php  }?>	
			<?php 	
				
				exit;
	}	
	
	/* historic data */
	
	
	
	
	
	function load_historic_lists()
	{
		
	//echo $_POST['currency'];exit;
		
		//echo $_POST['currency'];exit;
		
		$count2="";
		$paginationCount2="";
		$count2=$this->coin_page_model->historic_record_count();
		
		if($count2>0)
		{
			$paginationCount2=$this->getPagination2($count2);
		}
		
		$data['count2'] = $count2;
		
		 $data['paginationCount2'] = $paginationCount2;
		//exit;
		
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $pid=$_POST['pageId'];
        }
		 else
		 {
		   $pid='0';
		 }
		
		
		if(isset($_POST['currency']) && !empty($_POST['currency']))
		{
           $curency=$_POST['currency'];
        }
		else
		{
			if($this->session->userdata('user_id'))
			{
				$curency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$curency = "";
			}
			
			if($curency=="")
			{
			$curency = "USD";
			}
		}

        $start=$this->settings->settings_records_per_page_front*$pid;
		$limit=$this->settings->settings_records_per_page_front;
		
		
		$id='';
        if(isset($_POST['cid']) && !empty($_POST['cid']))
		{
           $id=$_POST['cid'];
		  
        }
		
		 $from_date='';
		 
		 
		
		 
		if(isset($_POST['from']) && !empty($_POST['from']))
		{
			
		 $from_date=$_POST['from'];
		 
         $to=''; 
         if(isset($_POST['to']) && !empty($_POST['to']))
		 {
			 $to=$_POST['to'];
		 }
		 
		 $historic_lists = $this->coin_page_model->getHistoricList2($id,$from_date,$to,$curency);
		 //print_r($historic_lists);exit;
		 
		 
		 
        }
		
		else
		{
		
		   $historic_lists = $this->coin_page_model->getHistoricList($limit, $start,$id,$from_date,$curency);
		  
		}
		
	
			?>	
				
				<?php if(is_array($historic_lists)) {
                    					
					$sno=$start+1;				    
										 foreach($historic_lists as $list) {
									
									?>	
										
										<tr>
											<th scope="row"><?php echo date('d M y H:i:s',strtotime($list->created_time)); ?></th>
											
											<td><?php echo $list->open24hour; ?><?php if($list->open24hour!='') { echo ' '.$list->currency_sym; } ?> </td>
											
											<td><?php echo $list->high24hour; ?><?php if($list->high24hour!='') { echo ' '.$list->currency_sym; } ?></td>
											
											<td><?php echo $list->low24hour;  ?><?php if($list->low24hour!='') { echo ' '.$list->currency_sym; } ?></td>
											
											<td><?php echo $list->price;  ?><?php if($list->price!='') { echo ' '.$list->currency_sym; } ?></td>
											
											<td><?php echo $list->lastvolume;  ?><?php if($list->lastvolume!=''){ echo " %"; } ?></td>
											
											<td><?php echo $list->mktcap;  ?><?php if($list->mktcap!='') { echo ' '.$list->currency_sym; } ?></td>
											
										</tr>
										
									<?php $sno++;  } 
									
									
									 if($count2 > 0){ 
											if($paginationCount2 > 1){ 
										  
							 $content2 ="";
							$content2 .='<tr id="pages"><td colspan="7"><div class="pagelist"><div align="right" class="paging"><ul class="tsc_pagination tsc_paginationC tsc_paginationC01">
								<li class="firsts links" id="firsts">
									<a  href="javascript:void(0)" onclick="UserPagination2(\'0\',\'firsts\')">F i r s t</a>
								</li>';
								
								$j=1;
								
								if($pid!='0')
								{
								
								$content2 .='<li class="lasts links" id="'.$j.'_nos">
									 <a href="javascript:void(0)" onclick="UserPagination2(\''.($pid-1).'\',\'lasts\')"> Previous</a>
								</li>';
								
								}
								
								if($pid!=$paginationCount2-1)
								{
								$content2 .='<li class="lasts links" id="'.$j.'_nos">
									 <a href="javascript:void(0)" onclick="UserPagination2(\''.($pid+1).'\',\'lasts\')"> Next</a>
								</li>';
								
								}
								
								
								
								
								
								
								
							 
								$content2 .='<li class="lasts links" id="lasts">
									 <a href="javascript:void(0)" onclick="UserPagination2(\''.($paginationCount2-1).'\',\'lasts\')">L a s t</a>
								</li>
							   
							</ul></div></div></td></tr>';

							echo $content2;
										  } } 
										  
										  
									
									} 
									else
									{  ?>  
								
								 <tr><td align="center" colspan="7"><font color="red">--No records found!--</font></td></tr>
								   
								<?php  }?>	
			<?php 	
				
				exit;
	}	
	
	
	
	
	
	function getPagination2($count)
	{
		
	 $paginationCount= ceil($count / $this->settings->settings_records_per_page_front);
	 
      return $paginationCount;	
		
	}
	
	
	
	
	
	
}