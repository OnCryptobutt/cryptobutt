<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feature extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('feature_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "feature-list";
		$data['title'] = "feature List";
		
		/** Start Activate Records **/
		$search = "";
		if($this->input->get('act')=="search")
		{
			$search = " WHERE feature_name like '%".$this->input->get('search')."%' ";
		}
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->feature_model->featureActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->feature_model->featureSuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->feature_model->featureDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('feature/index');
        $config["total_rows"] = $this->feature_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->feature_model->feature_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('feature/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "feature-add";
		$data['title'] = "Add feature";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("feature_name","Name","required|is_unique[".FEATURE.".feature_name]");
			//$this->form_validation->set_rules("feature_category_id","Category","required");
			//$this->form_validation->set_rules("feature_sub_category_id","Sub Category","required");
			//$this->form_validation->set_rules("feature_price","Price","required");
//$this->form_validation->set_rules("feature_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->feature_model->featureInsert();
				
				
				
				if($this->input->post('filenameOrginal')!="")
				{
					
					$ext_array = explode(".",$this->input->post('filenameOrginal'));
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "feature_".$pID."_".$photoname;
					
					
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					$file_name_crop    = $photo_name."_crop.".$ext;
					
					copy(UPLOAD_PATH."crop/".$this->input->post('filenameOrginal'),FEATURE_PATH.$file_name);
					copy(UPLOAD_PATH."crop/temp/".$this->input->post('filenameCrop'),FEATURE_PATH."thumb/".$file_name_crop);
					
					unlink(UPLOAD_PATH."crop/".$this->input->post('filenameOrginal'));
					unlink(UPLOAD_PATH."crop/temp/".$this->input->post('filenameCrop'));
					
					
						
				   $this->feature_model->featureInsertUpdate($file_name,$photo_name,$ext,$pID);	
				
					
					$source_path = FEATURE_PATH.$file_name;
					$target_path = FEATURE_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."feature/add/");
			}
			
				
		}
		$data['getCategoryList']    = $this->feature_model->getCategoryList($this->input->post('feature_category_id'));
        $data['getSubCategoryList'] = $this->feature_model->getSubCategoryList($this->input->post('feature_sub_category_id'),$this->input->post('feature_category_id'));		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('feature/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "feature-edit";
		$data['title'] = "Edit feature";
		

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("feature_name","Name","required|edit_unique_common[".FEATURE.".feature_name.".$pID.".feature_id]");
			//$this->form_validation->set_rules("feature_category_id","Category","required");
			//$this->form_validation->set_rules("feature_sub_category_id","Sub Category","required");
			//$this->form_validation->set_rules("feature_price","Price","required");
			//$this->form_validation->set_rules("feature_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->feature_model->featureEdit($pID);
				
				if($this->input->post('filenameOrginal')!="")
				{
				
				    $this->feature_model->featureImageDelete($pID);
				
					$ext_array = explode(".",$this->input->post('filenameOrginal'));
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "feature_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					$file_name_crop    = $photo_name."_crop.".$ext;
					
					copy(UPLOAD_PATH."crop/".$this->input->post('filenameOrginal'),FEATURE_PATH.$file_name);
					copy(UPLOAD_PATH."crop/temp/".$this->input->post('filenameCrop'),FEATURE_PATH."thumb/".$file_name_crop);
					
					unlink(UPLOAD_PATH."crop/".$this->input->post('filenameOrginal'));
					unlink(UPLOAD_PATH."crop/temp/".$this->input->post('filenameCrop'));
						
						$this->feature_model->featureInsertUpdate($file_name,$photo_name,$ext,$pID);	
					
					
					 $source_path = FEATURE_PATH.$file_name;
					$target_path = FEATURE_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
					
					
				}
				
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."feature/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->feature_model->getfeatureDetails($pID);
		$data['getCategoryList']    = $this->feature_model->getCategoryList($data['records']->feature_category_id);
        $data['getSubCategoryList'] = $this->feature_model->getSubCategoryList($data['records']->feature_sub_category_id,$data['records']->feature_category_id);
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('feature/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "feature-view";
		$data['title'] = "View feature";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->feature_model->getfeatureDetails($pID);
		$data['records']->category_name = $this->boost_model->getValue(CATEGORY,"category_name","category_id='".$data['records']->feature_category_id."'");
		$data['records']->sub_category_name = $this->boost_model->getValue(SUB_CATEGORY,"sub_category_name","sub_category_id='".$data['records']->feature_sub_category_id."'");
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('feature/view',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function upload()
	{
		 $imagePath = UPLOAD_PATH."crop/";
		 $imageURL = UPLOAD_URL."crop/";

	$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
	$temp = explode(".", $_FILES["img"]["name"]);
	$extension = end($temp);
	
	//Check write Access to Directory

	if(!is_writable($imagePath)){
		$response = Array(
			"status" => 'error',
			"message" => 'Can`t upload File; no write Access'
		);
		print json_encode($response);
		return;
	}
	
	if ( in_array($extension, $allowedExts))
	  {
	  if ($_FILES["img"]["error"] > 0)
		{
			 $response = array(
				"status" => 'error',
				"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
			);			
		}
	  else
		{
			
	      $filename = $_FILES["img"]["tmp_name"];
		  list($width, $height) = getimagesize( $filename );

		  move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

		  $response = array(
			"status" => 'success',
			"url" => $imageURL.$_FILES["img"]["name"],
			"width" => $width,
			"height" => $height,
			"filenameOrginal"=>$_FILES["img"]["name"]
		  );
		  
		}
	  }
	else
	  {
	   $response = array(
			"status" => 'error',
			"message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
		);
	  }
	  
	  print json_encode($response);

		
	}
	
	public function crop()
	{
		$imgUrl = $_POST['imgUrl'];
		 
// original sizes
$imgInitW = $_POST['imgInitW'];
$imgInitH = $_POST['imgInitH'];
// resized sizes
$imgW = $_POST['imgW'];
$imgH = $_POST['imgH'];
// offsets
$imgY1 = $_POST['imgY1'];
$imgX1 = $_POST['imgX1'];
// crop box
$cropW = $_POST['cropW'];
$cropH = $_POST['cropH'];
// rotation angle
$angle = $_POST['rotation'];

$jpeg_quality = 100;

$rand = rand();
$output_filename = UPLOAD_PATH."crop/temp/croppedImg_".$rand;
$imageURL = UPLOAD_URL."crop/temp/croppedImg_".$rand;

// uncomment line below to save the cropped image in the same location as the original image.
//$output_filename = dirname($imgUrl). "/croppedImg_".rand();

$what = getimagesize($imgUrl);

switch(strtolower($what['mime']))
{
    case 'image/png':
        $img_r = imagecreatefrompng($imgUrl);
		$source_image = imagecreatefrompng($imgUrl);
		$type = '.png';
        break;
    case 'image/jpeg':
        $img_r = imagecreatefromjpeg($imgUrl);
		$source_image = imagecreatefromjpeg($imgUrl);
		error_log("jpg");
		$type = '.jpeg';
        break;
    case 'image/gif':
        $img_r = imagecreatefromgif($imgUrl);
		$source_image = imagecreatefromgif($imgUrl);
		$type = '.gif';
        break;
    default: die('image type not supported');
}


//Check write Access to Directory

if(!is_writable(dirname($output_filename))){
	$response = Array(
	    "status" => 'error',
	    "message" => 'Can`t write cropped File'
    );	
}else{
	
	
	  // resize the original image to size of editor
    $resizedImage = imagecreatetruecolor($imgW, $imgH);
	imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
    // rotate the rezized image
    $rotated_image = imagerotate($resizedImage, -$angle, 0);
    // find new width & height of rotated image
    $rotated_width = imagesx($rotated_image);
    $rotated_height = imagesy($rotated_image);
    // diff between rotated & original sizes
    $dx = $rotated_width - $imgW;
    $dy = $rotated_height - $imgH;
    // crop rotated image to fit into original rezized rectangle
	$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
	imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
	imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
	// crop image into selected area
	$final_image = imagecreatetruecolor($cropW, $cropH);
	imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
	imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
	// finally output png image
	//imagepng($final_image, $output_filename.$type, $png_quality);
	imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
    
	$response = Array(
	    "status" => 'success',
	    "url" => $imageURL.$type,
		"filenameCrop"=>"croppedImg_".$rand.$type
    );
}
print json_encode($response);
	}
	
	
}
