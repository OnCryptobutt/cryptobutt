<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Feature
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <table class="table table-bordered">
				
				   <tr><td><b>Category</b></td><td><?php echo $records->category_name;?></td></tr>
				   <tr><td><b>Sub Category</b></td><td><?php echo $records->sub_category_name;?></td></tr>
				  
					<tr><td><b>Name</b></td><td><?php echo $records->feature_name;?></td></tr>
					 <tr><td><b>Price</b></td><td><?php echo $records->feature_price;?></td></tr>
					<!--<tr><td><b>Content</b></td><td><?php echo $records->feature_content;?></td></tr>-->
					<tr><td><b>Image</b></td><td><?php if($records->feature_photo){ echo "<img src='".FEATURE_URL."thumb/".$records->feature_photo_name."_150.".$records->feature_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?></td></tr>
					<tr><td><b>Status</b></td><td> <?php if($records->feature_status=="1"){?>Active<?php } ?><?php if($records->feature_status=="0"){?>Suspend<?php } ?></td></tr>
				</table>
				
              </div>
			   <div class="box-footer">
                
				<a href="<?php echo base_url();?>feature/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  