<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        coin
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <table class="table table-bordered">
					<tr><td><b>Symbol</b></td><td><?php echo $records->name;?></td></tr>
					<tr><td><b>Name</b></td><td><?php echo $records->coinname;?></td></tr>
					<tr><td><b>Full Name</b></td><td><?php echo $records->fullname;?></td></tr>
					<tr><td><b>Total Coin Supply</b></td><td><?php echo $records->totalcoinsupply;?></td></tr>
					<tr><td><b>Fully Premined</b></td><td><?php echo $records->fullypremined;?></td></tr>
					<tr><td><b>Proof Type</b></td><td><?php echo $records->prooftype;?></td></tr>
					<tr><td><b>Rank</b></td><td><?php echo $records->sortorder;?></td></tr>
					<tr><td><b>Algorithm</b></td><td><?php echo $records->algorithm;?></td></tr>
					<tr><td><b>Premined Value</b></td><td><?php echo $records->preminedvalue;?></td></tr>
					<tr><td><b>Total Coins Free Float</b></td><td><?php echo $records->totalcoinsfreefloat;?></td></tr>
					<!--<tr><td><b>Content</b></td><td><?php echo $records->coin_content;?></td></tr>-->
					<tr><td><b>Image</b></td><td><?php if($records->image){ echo "<img width=\"100\" src='".COIN_URL.$records->image."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?></td></tr>
					<tr><td><b>Status</b></td><td> <?php if($records->status=="1"){?>Active<?php } ?><?php if($records->status=="0"){?>Suspend<?php } ?></td></tr>
				</table>
				
              </div>
			   <div class="box-footer">
                
				<a href="<?php echo base_url();?>coin/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
		  
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
		
		
		
		<section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">History</h3>
            </div>
			
			
			
			
			<form id="form1" name="form1" method="post" action="">
			 
				  
            <!-- /.box-header -->
            <div class="box-body">
			
			<?php if($success) echo $success;?>
			
			     <!--<div align="right">
                  <a href="<?php echo base_url();?>coin/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>
				  
				  <a onclick="return confirm('Are you sure want to run cron file?');" class="btn btn-sm btn-danger" href="<?php echo base_url('coin_list?run_from=manual');?>">Run Cron File</a>
				  
                  <input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>
                  <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>-->
				  <br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <!--<th><input type="checkbox" id="selecctall"/></th>-->
				  <th>Sno</th>
				  <th>Date</th>
                  <th>Price (USD)</th>
				  <th>Action</th>
                </tr>
				
                </thead>
                <tbody>
				
					  <?php 
					 
					  if(is_array($lists)){
					  foreach($lists as $list){
						
						  ?>
					  <tr>
					  
					    <!--<td><input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $list->id;?>"> </td>-->
						<td><?php echo $list->sno;?></td>
						<td><?php echo date('d M Y h:i:s',strtotime($list->updated_time));?></td>
						<td><?php if(isset($list->history->price)) { echo $list->currency.' '.$list->history->price; } ?></td>
						<td><a href="<?php echo base_url();?>coin/details/<?php echo $cid; ?>/<?php echo $list->id;?>/"><span class="fa fa-eye"></span></a></td>
						
					  </tr>
					  <?php } }

              else{					  ?>
			  
			  <tr><td align="center" colspan="4"><font color="red">--No records found!--</font></td></tr>
			  <?php } ?>
				
               
                </tbody>
                
              </table>
			  
			  <div align="right"><?php echo $pagination;?></div>
			  
            </div>
			</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
		  
		
		
		
		
		
		
		
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  