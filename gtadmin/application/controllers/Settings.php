<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('boost_model');
		$this->load->model('settings_model');
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
	}
	
	public function index()
	{
		$data['basename'] = "settings";
		$data['success']  = "";
		$data['title']    = "Settings";
		if($this->session->flashdata('success'))
		{
			$data['success']  = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="edit")
		{
			$this->form_validation->set_rules("settings_site_name","Site Name","required");
			$this->form_validation->set_rules("settings_email","Email","required|valid_email");
			$this->form_validation->set_rules("settings_records_per_page","Records per page","required|numeric|max_length[11]");
			if($this->form_validation->run()==TRUE)
			{
				$this->settings_model->settingsEdit();
				
				$pID = '1';
				
				if($_FILES['settings_logo_photo']['name']!="")
				{
					
					$photoname = $this->boost_model->getValue(SETTINGS,"settings_logo_photo_name","settings_id='1'");
					$photoext = $this->boost_model->getValue(SETTINGS,"settings_logo_photo_ext","settings_id='1'");
					
					if($photoname)
					{
						if(file_exists(SETTINGS_PATH."thumb/".$photoname."_150.".$photoext))
						{
							unlink(SETTINGS_PATH."thumb/".$photoname."_150.".$photoext);
						}
						
						if(file_exists(SETTINGS_PATH.$photoname.".".$photoext))
						{
							unlink(SETTINGS_PATH.$photoname.".".$photoext);
						}
					}
					
					$ext_array = explode(".",$_FILES['settings_logo_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "logo_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = SETTINGS_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					$this->upload->do_upload('settings_logo_photo');
				
					$this->settings_model->settingsInsertUpdate($file_name,$photo_name,$ext,$pID);	

					$source_path = SETTINGS_PATH.$file_name;
					$target_path = SETTINGS_PATH . 'thumb/'.$file_name_150;
					
					$thumb_marker = "_150";

					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
				$this->session->set_flashdata("success","Records are updated successfully");
				redirect(base_url()."settings/");
			}
		}
		
		$data['records'] = $this->settings_model->settingsDetails();
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('settings',$data);
		$this->load->view('template/footer',$data);
	}
	
	
}
