<?php
class Home_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	function getBanner()
	{
		$this->db->select("*");
		$this->db->from(BANNER);
		$this->db->where("banner_status","1");
		$this->db->order_by("banner_order","ASC");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function coin_list($limit, $start)
	{
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","COMPLETED");
		$this->db->order_by("id","DESC");
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			$rowc = $query->row();
		}
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				$row_columns[] = $row2;
			}
		}
	
		
		$this->db->select('*');
		$this->db->from(COIN);
		
		$this->db->where("status","1");
	$this->db->order_by("sortorder","asc");
	$this->db->limit($limit,$start);
		if($this->input->get('coin_name'))
		{
			$this->db->like("coinname",$this->input->get('coin_name'));
		}
			$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
            foreach ($query->result_array() as $row)
			{
				foreach($row_columns as $row_column)
				{
					$row[$row_column->name] = $this->boost_model->getValue(COIN_HISTORY,$currency,"coin_id='".$row['id']."' AND column_id='".$row_column->id."' order by id desc");
				}
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	 public function record_count()
    {
		$this->db->select('*');
		$this->db->from(COIN);
		
		$this->db->where("status","1");
		$this->db->order_by("sortorder","asc");
	
		if($this->input->get('coin_name'))
		{
			$this->db->like("coinname",$this->input->get('coin_name'));
		}
			$query = $this->db->get();

		return $query->num_rows();
       
    }
	
	public function total_records()
	{
		$this->db->select('*');
		$this->db->from(COIN);
		
		$this->db->where("status","1");
		
		$this->db->order_by("sortorder","asc");
	
		if($this->input->post('search'))
		{
			$this->db->like("coinname",$this->input->post('search'));
		}
			$query = $this->db->get();
			
			return $query->num_rows();
	}
	public function total_coin(){
	    if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
	     $sql = "SELECT *,c.id as coinid FROM ci_coin c JOIN ci_coin_history_temp_".$currency." h ON c.id = h.coin_id where c.status='1'";
	     $query =$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
			return $data;
        }
	}
	public function insertWatchlistname($data)
    {
        $query = null; 
        $id   = $data['user_id']; 
        $name = $data['watchlist_name'];
        $query = $this->db->get_where('ci_watchlist', array('watchlist_name' => $name));
        $count = $query->num_rows(); //counting result from query
        if ($count === 0) {
            $this->db->insert('ci_watchlist', $data);
    	    return 'true';
        } else{
            return 'false';
        }
    }
    public function insertwatchlist_data($data)
    {
        $query = null; 
        $coinid     = $data['watch_coinid']; 
        $watchid    = $data['watch_nameid'];
        /*$query = $this->db->get_where('ci_watchlistassets', array('watch_nameid' => $watchid, 'watch_coinid' => $coinid));*/
         $query = $this->db->get_where('ci_watchlistassets', array('watch_coinid' => $coinid));
        $count = $query->num_rows(); //counting result from query
        if ($count === 0) {
            $this->db->insert('ci_watchlistassets', $data);
    	    return 'true';
        } else{
            return 'false';
        }
    }
    public function getfavdata($coinId,$userId)
    {
        $query = null; 
        $query = $this->db->get_where('ci_watchlistassets', array('watch_userid' => $userId, 'watch_coinid' => $coinId));
        $count = $query->num_rows(); //counting result from query
        if ($count === 0) {
    	    return 'false';
        } else{
            foreach ($query->result() as $row)
			{ 
                $data[] = $row;
            }
            return $data;
        }
    }
    public function getWatchlist($userid){
	     $sql = "SELECT * FROM ci_watchlist WHERE user_id='".$userid."' ORDER BY watchlist_id DESC";
	     $query =$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{ 
                $data[] = $row;
            }
			return $data;
        }
	}
	public function deletewatchlist($coinId,$userId,$watchname){
	     $sql = "SELECT * FROM ci_watchlist WHERE user_id='".$userId."' AND watchlist_name='".$watchname."'";
	     $query =$this->db->query($sql);
	     $row = $query->result();
	    $sql = "DELETE FROM ci_watchlistassets WHERE watch_coinid='$coinId' AND watch_userid='$userId' AND watch_nameid='".$row[0]->watchlist_id."'";
		$query = $this->db->query($sql);
		return $coinId;
	}
	public function deletehomewatchlist($coinId,$userId){
	    $sql = "DELETE FROM ci_watchlistassets WHERE watch_coinid='$coinId' AND watch_userid='$userId'";
		$query = $this->db->query($sql);
		return $coinId;
	}
	public function insertWalletaddress($data)
    {
        $query = null; 
        //$coinid     = $data['watch_coinid']; 
       // $watchid    = $data['watch_nameid'];
        $query = $this->db->get_where('ci_wallet', array('ethereum_address' => $data['ethereum_address'], 'wallet_userid' => $data['wallet_userid']));
        $count = $query->num_rows(); //counting result from query
        if ($count === 0) {
            $this->db->insert('ci_wallet', $data);
    	    return 'true';
        } else{
            return 'false';
        }
    }
}
?>