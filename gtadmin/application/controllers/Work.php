<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Work extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('work_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->user = $this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }
	
	

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "Work-list";
		$data['title'] = ucfirst("Work List");
		
		$data["settings"] = $this->settings;
		
		/*
		$data["privileges"] = $this->user;
		$privileges=explode(',',$this->user->admin_privileges);
		if($this->user->admin_type !="SADMIN")
		{
			if(in_array('Work_list',$privileges))
			{ } 
			else
			{ 
				$this->session->set_flashdata('error','You don\'t have rights to access this data!');
				redirect(base_url()); }
		} */
			
			if($this->session->flashdata('error'))
			{
				$data['success'] = $this->boost_model->showNotify("error",$this->session->flashdata('error'));
			}
		

			
		 
				/** Start Activate Records **/
				
				if($this->input->get('act')=="Activate")
				{
					
						if(isset($_GET['check']))
						{
							$check      = $_GET['check'];
							$checkCount = count($check);
							
							if($checkCount>0)
							{
								foreach( $check as $key=>$value)
								{
									 $this->work_model->workActivate($value);
								}
								
								$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
								
							}
							else
							{
								$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
							}
						}
						else
						{
							$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
						   
						}
					
				}
				
				/** End Activate Records **/
				
				
				/** Start Suspend Records **/
				
				if($this->input->get('act')=="Suspend")
				{
					
						
						if(isset($_GET['check']))
						{
							$check = $_GET['check'];
							$checkCount = count($check);
							
							if($checkCount>0)
							{
								foreach( $check as $key=>$value)
								{
									 $this->work_model->workSuspend($value);
								}
								
								$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
								
							}
							else
							{
								$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
							}
						}
						else
						{
							$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
						}
					
				}
				/** End Suspend Records **/
				
				/** Start Delete Records **/
				if($this->input->get('act')=="Delete")
				{
								
				
						if(isset($_GET['check']))
						{
							$check      = $_GET['check'];
							$checkCount = count($check);
							
							if($checkCount>0)
							{
								foreach( $check as $key=>$value)
								{
									 $this->work_model->workDelete($value);
								}
								
								$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
								
							}
							else
							{
								$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
							}
						}
						else
						{
							$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
						}
					
					
			}
			
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('work/index');
        $config["total_rows"] = $this->work_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->work_model->work_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('work/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "work-add";
		$data['title'] = "Add work";
		
		$data["settings"] = $this->settings;
		
		/*$data["privileges"] = $this->user;
		if($this->user->admin_type !="SADMIN")
		{
			$privileges=explode(',',$this->user->admin_privileges);
			
			if(in_array('work-add',$privileges))
			{ }
			else
			{
				$this->session->set_flashdata('error','You don\'t have rights to access this data!');
				redirect(base_url('work/')); 
			}		
		}*/
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			 $this->form_validation->set_rules("work_name","Name","required|is_unique[".WORK.".work_name]");
			  $this->form_validation->set_rules("work_order","Order","required"); 
			/** $this->form_validation->set_rules("service_content","Content","required"); **/
		    //$this->form_validation->set_rules("service_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->work_model->workInsert();
				
				
				
				
				if(isset($_POST['work_slider_name']))
				{
			    $service_sliders = $_POST['work_slider_name'];
				
				foreach($service_sliders as $key=>$value)
				{
					if($value)
					{
						$insert_data = "";
						//$insert_data = array('work_slider_name'=>addslashes(trim($value)));
						
						$insert_data = array('work_slider_name'=>addslashes(trim($value)),
						'work_slider_work_id'=>$pID);
						$insertid = $this->work_model->workSliderInsert($insert_data);
						
						
						if($_FILES['work_slider_photo']['name'][$key]!="")
							{
								
								$_FILES['service_slider_photo_newTemp']['name'] = $_FILES['work_slider_photo']['name'][$key];
								$_FILES['service_slider_photo_newTemp']['type'] = $_FILES['work_slider_photo']['type'][$key];
								$_FILES['service_slider_photo_newTemp']['tmp_name'] = $_FILES['work_slider_photo']['tmp_name'][$key];
								$_FILES['service_slider_photo_newTemp']['error'] = $_FILES['work_slider_photo']['error'][$key];
								$_FILES['service_slider_photo_newTemp']['size'] = $_FILES['work_slider_photo']['size'][$key];
							
								$ext_array = explode(".",$_FILES['work_slider_photo']['name'][$key]);
								
								$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
								$photoname = implode("-",explode(" ",$photoname));
							   
								$ext = $ext_array[sizeof($ext_array)-1];
								
								$photo_name = "service_slider_".$insertid."_".$photoname;
								
								$file_name    = $photo_name.".".$ext;
								$file_name_150    = $photo_name.".".$ext;
								
								$config['upload_path'] = WORK_PATH;
								$config['allowed_types'] = 'jpg|gif|png';
								$config['file_name'] = $file_name ;

								//load upload class library
								
								
								$this->load->library('upload', $config);
							  
							  $this->upload->initialize($config); 

							if(!$this->upload->do_upload('service_slider_photo_newTemp'))
							{
								echo $this->upload->display_errors();
							}
							else
							{
							
							     $fileData = $this->upload->data();
									
									$this->work_model->workSliderInsertUpdate($file_name,$photo_name,$ext,$insertid);
							}						
								
								
								 $source_path = WORK_PATH.$file_name;
								$target_path = WORK_PATH . 'thumb/'.$file_name_150;
								
							
								$thumb_marker = "_150";
								
								
								
								
								$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
								
								
							}
					}
				}
				
				}
				
				//$this->do_upload($pID);
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."work/add/");
			}
			
				
		}
		//$data['service_list'] = $this->work_model->getServiceList("Nothing");
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('work/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "work-edit";
		$data['title'] = "Edit work";
		
		$data["settings"] = $this->settings;
		
		/*$data["privileges"] = $this->user;
		if($this->user->admin_type !="SADMIN")
		{
			$privileges=explode(',',$this->user->admin_privileges);
			
			if(in_array('work-edit',$privileges))
			{ }
			else
			{
				$this->session->set_flashdata('error','You don\'t have rights to access this data!');
				redirect(base_url('service/')); 
			}	
		}*/
		
		$pID = $this->uri->segment(3);
		
		
		if($this->input->get('workSliderDelId')!="")
		{
			$this->work_model->deleteWorkSlider($this->input->get('workSliderDelId'));
			$this->session->set_flashdata('success',"Your requested record has been deleted");
			redirect(base_url()."work/edit/".$pID."/");
		}

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("work_name","Name","required|edit_unique_common[".WORK.".work_name.".$pID.".work_id]");
			 $this->form_validation->set_rules("work_order","Order","required"); 
			//$this->form_validation->set_rules("service_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->work_model->workEdit($pID);
				
				
				
				if(isset($_POST['work_slider_name']))
				{
				$service_sliders = $_POST['work_slider_name'];
				
				foreach($service_sliders as $key=>$value)
				{
					if($value)
					{
						$insert_data = "";
						//$insert_data = array('work_slider_name'=>addslashes(trim($value)));
						$insert_data = array('work_slider_name'=>addslashes(trim($value)),
						'work_slider_work_id'=>$pID);
						$insertid = $this->work_model->workSliderInsert($insert_data);
						
						
						if($_FILES['work_slider_photo']['name'][$key]!="")
							{
								
								$_FILES['service_slider_photo_newTemp']['name'] = $_FILES['work_slider_photo']['name'][$key];
								$_FILES['service_slider_photo_newTemp']['type'] = $_FILES['work_slider_photo']['type'][$key];
								$_FILES['service_slider_photo_newTemp']['tmp_name'] = $_FILES['work_slider_photo']['tmp_name'][$key];
								$_FILES['service_slider_photo_newTemp']['error'] = $_FILES['work_slider_photo']['error'][$key];
								$_FILES['service_slider_photo_newTemp']['size'] = $_FILES['work_slider_photo']['size'][$key];
							
								$ext_array = explode(".",$_FILES['work_slider_photo']['name'][$key]);
								
								$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
								$photoname = implode("-",explode(" ",$photoname));
							   
								$ext = $ext_array[sizeof($ext_array)-1];
								
								$photo_name = "service_slider_".$insertid."_".$photoname;
								
								$file_name    = $photo_name.".".$ext;
								$file_name_150    = $photo_name.".".$ext;
								
								$config['upload_path'] = WORK_PATH;
								$config['allowed_types'] = 'jpg|gif|png';
								$config['file_name'] = $file_name ;

								//load upload class library
								
								
								$this->load->library('upload', $config);
							  
							  $this->upload->initialize($config); 

							if(!$this->upload->do_upload('service_slider_photo_newTemp'))
							{
								echo $this->upload->display_errors();
							}
							else
							{
							
							     $fileData = $this->upload->data();
									
									$this->work_model->workSliderInsertUpdate($file_name,$photo_name,$ext,$insertid);
							}						
								
								
								 $source_path = WORK_PATH.$file_name;
								$target_path = WORK_PATH . 'thumb/'.$file_name_150;
								
							
								$thumb_marker = "_150";
								
								
								
								
								$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
								
								
							}
					}
				}
				
				}
				
				
				

				$service_sliders_update = $_POST['work_slider_name_update'];
				
				foreach($service_sliders_update as $key=>$value)
				{
					if($value)
					{
						$insert_data = "";
						$insert_data = array('work_slider_name'=>addslashes(trim($value)),
						'work_slider_work_id'=>$pID);
					   $this->work_model->workSliderUpdate($insert_data,$key);
						
						
						
						if($_FILES['work_slider_photo_update']['name'][$key]!="")
							{
								
								$_FILES['service_slider_photo_updateTemp']['name'] = $_FILES['work_slider_photo_update']['name'][$key];
								$_FILES['service_slider_photo_updateTemp']['type'] = $_FILES['work_slider_photo_update']['type'][$key];
								$_FILES['service_slider_photo_updateTemp']['tmp_name'] = $_FILES['work_slider_photo_update']['tmp_name'][$key];
								$_FILES['service_slider_photo_updateTemp']['error'] = $_FILES['work_slider_photo_update']['error'][$key];
								$_FILES['service_slider_photo_updateTemp']['size'] = $_FILES['work_slider_photo_update']['size'][$key];
							
								$ext_array = explode(".",$_FILES['work_slider_photo_update']['name'][$key]);
								
								$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
								$photoname = implode("-",explode(" ",$photoname));
							   
								$ext = $ext_array[sizeof($ext_array)-1];
								
								$photo_name = "service_slider_".$key."_".$photoname;
								
								$file_name    = $photo_name.".".$ext;
								$file_name_150    = $photo_name.".".$ext;
								
								$config['upload_path'] = WORK_PATH;
								$config['allowed_types'] = 'jpg|gif|png';
								$config['file_name'] = $file_name ;

								//load upload class library
								
								
								$this->load->library('upload', $config);
							  
							  $this->upload->initialize($config); 

							if(!$this->upload->do_upload('service_slider_photo_updateTemp'))
							{
								echo $this->upload->display_errors();
							}
							else
							{
							
							     $fileData = $this->upload->data();
									
									$this->work_model->workSliderInsertUpdate($file_name,$photo_name,$ext,$key);
									
									
									 $source_path = WORK_PATH.$file_name;
								$target_path = WORK_PATH . 'thumb/'.$file_name_150;
								
							
								$thumb_marker = "_150";
								
								
								
								
								$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
							}						
								
								
								
								
								
							}
					}
				}
				 
				
				//$this->do_upload($pID);
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."work/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->work_model->getWorkDetails($pID);
		//$data['banner_points'] = $this->work_model->bannerPointsList($pID);
		$data['service_sliders'] = $this->work_model->workSliderList($pID);
		//$data['service_features1'] = $this->work_model->service_features1List($pID);
		//$data['service_features2'] = $this->work_model->service_features2List($pID);
		
		//$data['service_mores'] = $this->work_model->service_moresList($pID);
		
		
		//$data['service_list'] = $this->work_model->getServiceList("Nothing");
		//print_r($data['banner_points']);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('work/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "work-view";
		$data['title'] = "View work";
		
		$data["settings"] = $this->settings;
		
		/*$data["privileges"] = $this->user;
		if($this->user->admin_type !="SADMIN")
		{
			$privileges=explode(',',$this->user->admin_privileges);
			
			if(in_array('work-view',$privileges))
			{ }
			else
			{
				$this->session->set_flashdata('error','You don\'t have rights to access this data!');
				redirect(base_url('service/')); 
			}	
		}*/
		
		
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		//$data['records'] = $this->work_model->getserviceDetails($pID);
		$data['records'] = $this->work_model->getWorkDetails($pID);
		$data['service_sliders'] = $this->work_model->workSliderList($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('work/view',$data);
		$this->load->view('template/footer',$data);
		
	} 
}
