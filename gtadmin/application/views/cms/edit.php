<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cms
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="cms_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo $records->cms_name;?>">
					<span class="red"><?php echo form_error('cms_name');?></span>
                  </div>
                </div>
				
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="cms_content"><?php echo $records->cms_content;?></textarea>
					<span class="red"><?php echo form_error('cms_content');?></span>
                  </div>
                </div>
				
				
			<!--<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                   <input type="file" name="cms_photo" value=""/>
				   <?php if($records->cms_photo){ echo "<img src='".CMS_URL."thumb/".$records->cms_photo_name."_150.".$records->cms_photo_ext."'>";}?>
                  </div>
                </div>-->
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="cms_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo $records->cms_order;?>">
					<span class="red"><?php echo form_error('cms_order');?></span>
                  </div>
                </div>
				
				
					<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Site Title</label>

                  <div class="col-sm-10">
                   <textarea name="cms_site_title" class="form-control"><?php echo $records->cms_site_title;?></textarea>
					<span class="red"><?php echo form_error('cms_site_title');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label>

                  <div class="col-sm-10">
                   <textarea name="cms_meta_description" class="form-control"><?php echo $records->cms_meta_description;?></textarea>
					<span class="red"><?php echo form_error('cms_meta_description');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Meta Keyword</label>

                  <div class="col-sm-10">
                   <textarea name="cms_meta_keyword" class="form-control"><?php echo $records->cms_meta_keyword;?></textarea>
					<span class="red"><?php echo form_error('cms_meta_keyword');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" <?php if($records->cms_status=="1"){?> checked='true'<?php } ?> name="cms_status" value="1"> Enable<br/>
					<input type="radio" <?php if($records->cms_status=="0"){?> checked='true'<?php } ?> name="cms_status" value="0"> Disable
                  </div>
                </div>
                
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>cms/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  