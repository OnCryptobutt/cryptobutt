<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social_list extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
    }

	function index()
	{
		
		header('Content-type: application/json');
		$success ="";
		
		$query =  $this->db->select('*')->from(COIN)->where(array("status"=>"1"))->order_by("sortorder","ASC")->get();
	
		
		if($query->num_rows()>0)
		{
			foreach($query->result_array() as $row)
			{
		$Data = json_decode(file_get_contents('https://www.cryptocompare.com/api/data/socialstats/?id='.$row['api_id']),true);
		
				if($Data['Response']=="Success")
				{
					if($row['api_id'])
					{
						
					$update_data = array("twitter_url"=>$Data['Data']['Twitter']['link'],
						                 "facebook_url"=>$Data['Data']['Reddit']['link'],
						                 "reddit_url"=>$Data['Data']['Facebook']['link']);
										 
					$this->db->set($update_data)->where("api_id",$row['api_id'])->update(COIN);
					
					}
				}
			
			
			}
		
		}
		
		
	}
}
