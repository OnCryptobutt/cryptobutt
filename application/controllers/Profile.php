<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('home_model');
		//$this->load->library('curl');
		$this->detect = new Mobile_Detect();
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url());
		}
    }
	public function index()
	{
		$data = "";
		$time_start = microtime(true);
		$data['basename'] = "profile";
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		if($this->session->userdata('user_id')){
		  $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
			 if($query_user->num_rows()>0)
			 {
				 $query_user_row = $query_user->row();
			 }
			 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$data['userid'] = $this->session->userdata('user_id');
		$data['watchname'] = "";
		$data["getWatchlist"] = $this->home_model->getWatchlist($this->session->userdata('user_id'));
		$data['js'] = $this->load->view('profile_js',$data,TRUE);
		$this->load->view('template/header',$data);
		$this->load->view('profile_page',$data);
		$this->load->view('template/footer',$data);
	}
	function getPagination($count){
      $paginationCount= floor($count / $this->settings->settings_records_per_page_front);
      $paginationModCount= $count % $this->settings->settings_records_per_page_front;
      if(!empty($paginationModCount)){
               $paginationCount++;
      }
      return $paginationCount;
     }
    public function watchlist()
	{
	    
	    $data = "";
		$data['basename'] = "watchlist";
		if($this->input->get('currency'))
		{
		    $currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		if($this->session->userdata('user_id')){
		 $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
		 if($query_user->num_rows()>0)
		 {
			 $query_user_row = $query_user->row();
		 }
		 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$this->load->view('watchlist_page',$data);
	}
	public function addwallet()
	{
	    
	    $data = "";
		$data['basename'] = "addwallet";
		if($this->input->get('currency'))
		{
		    $currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		if($this->session->userdata('user_id')){
		 $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
		 if($query_user->num_rows()>0)
		 {
			 $query_user_row = $query_user->row();
		 }
		 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$this->load->view('addwallet_page',$data);
	}
	public function watch_list(){
	    
		$data = "";
		$time_start = microtime(true);
		$data['basename'] = "profile";
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		if($this->session->userdata('user_id')){
		  $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
			 if($query_user->num_rows()>0)
			 {
				 $query_user_row = $query_user->row();
			 }
			 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$data['userid'] = $this->session->userdata('user_id');
		$data['watchname'] = urldecode($this->uri->segment(3));
		$data["getWatchlist"] = $this->home_model->getWatchlist($this->session->userdata('user_id'));
		$data['js'] = $this->load->view('profile_js',$data,TRUE);
		$this->load->view('template/header',$data);
		$this->load->view('profile_page',$data);
		$this->load->view('template/footer',$data);
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		$totalSecs   = ($execution_time * 60); 
	}
}