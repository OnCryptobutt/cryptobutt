<?php
class Login_model extends CI_Model
{
	public function authentication($data)
	{
		
		$query = $this->db->select('*')->from(USER)->where(array("email"=>$data->email,"identifier"=>$data->identifier))->get();
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			$query = $this->db->select('*')->from(USER)->where(array("email"=>$data->email,"identifier"=>$data->identifier,"status"=>"1"))->get();
			if($query->num_rows()>0)
			{ 
		        $row = $query->row();
				//print_r($row);
				//exit;
				$this->session->set_userdata("user_id",$row->id);
				//redirect(base_url());
				redirect(base_url('profile'));
			}
			else
			{
				$this->session->set_flashdata("error","Your account was suspended. Please contact support team.");
				redirect(base_url('Login'));
			}
		}
		else
		{
			$insert_data = array("email"       => $data->email,
						    "identifier"       => $data->identifier,
			                 "profileURL"      => $data->profileURL,
							  "photoURL"       => $data->photoURL,
								 "gender"      => $data->gender,
								"firstName"    => $data->firstName,
								"lastName"     => $data->lastName,
								"status"       => "1",
								"created_time" => NOW,
								"updated_time" => NOW);
								
			$this->db->set($insert_data)->insert(USER);
			$last_id = $this->db->insert_id();
			$this->session->set_userdata("user_id",$last_id);
			redirect(base_url());
		}
	}
	public function insertSignupdata($data)
	{
	    $query = $this->db->select('*')->from(USER)->where(array("email"=>$data['email']))->get();
	    if($query->num_rows()>0)
		{
		    return;
		}else{
	        $insert_data = array("email"            => $data['email'],
	                        "password"          => $data['password'],
						    "identifier"       => '',
			                 "profileURL"      => '',
							  "photoURL"       => '',
								 "gender"      => '',
								"firstName"    => '',
								"lastName"     => '',
								"status"       => "1",
								"created_time" => NOW,
								"updated_time" => NOW);
								
			$this->db->set($insert_data)->insert(USER);
			$last_id = $this->db->insert_id();
			$this->session->set_userdata("user_id",$last_id);
			return $this->db->insert_id();
		}
	}
	public function siginData($data)
	{
	    $query = $this->db->select('*')->from(USER)->where(array("email"=>$data['email']))->get();
	    if($query->num_rows()>0)
		{
		    	$query = $this->db->select('*')->from(USER)->where(array("email"=>$data['email'],"password"=>$data['password'],"status"=>"1"))->get();
			if($query->num_rows()>0)
			{ 
		        $row = $query->row();
			  $this->session->set_userdata("user_id",$row->id);
			 return true;
				//redirect(base_url('profile'));
				
			}
			else
			{
				$this->session->set_flashdata("error","Your account was suspended. Please contact support team.");
				//redirect(base_url('Login'));
			}
		}
	}
	public function changepasswordData($data){
	  $datas =array('password'=>$data['password']);
      $email = $data['username'];
     // print_r($data);
      if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if($datas){
            $this->db->where('email', $email);
            $this->db->update('ci_user', $datas);  
            return TRUE;
        }else{
           return FALSE;
        }
      }
	}
	  
}
