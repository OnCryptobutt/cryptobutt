<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('boost_model');
		$this->load->model('profile_model');
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
	}
	
	public function index()
	{
		$data['basename'] = "profile";
		$data['success']  = "";
		$data['title']    = "Profile";
		if($this->session->flashdata('success'))
		{
			$data['success']  = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="edit")
		{
			$this->form_validation->set_rules("admin_email","Email","required|valid_email");
			if($this->form_validation->run()==TRUE)
			{
				$this->profile_model->profileEdit();
				$this->session->set_flashdata("success","Records are updated successfully");
				redirect(base_url()."profile/");
			}
		}
		
		$data['records'] = $this->profile_model->profileDetails();
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('profile',$data);
		$this->load->view('template/footer',$data);
	}
	
	
}
