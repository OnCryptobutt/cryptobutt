<?php
class Settings_model extends CI_model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function settingsDetails()
	{
		$sql   = "SELECT * FROM ".SETTINGS." WHERE settings_id='1'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			return $query->row();
		}
	}
	
	public function settingsEdit()
	{
		$sql = "UPDATE ".SETTINGS." SET
		
		settings_site_name            	= '".$this->input->post('settings_site_name')."',
		settings_email                	= '".$this->input->post('settings_email')."',
        settings_records_per_page     	= '".$this->input->post('settings_records_per_page')."',
		settings_records_per_page_front = '".$this->input->post('settings_records_per_page_front')."',
        settings_copyright            	= '".$this->input->post('settings_copyright')."',
		settings_facebook_url         	= '".$this->input->post('settings_facebook_url')."',
		settings_twitter_url          	= '".$this->input->post('settings_twitter_url')."',
		settings_google_url           	= '".$this->input->post('settings_google_url')."',
		settings_linkedin_url         	= '".$this->input->post('settings_linkedin_url')."',
		settings_youtube_url          	= '".$this->input->post('settings_youtube_url')."',
		settings_site_title           	= '".$this->input->post('settings_site_title')."',
		settings_meta_keywords        	= '".$this->input->post('settings_meta_keywords')."',
		settings_meta_description     	= '".$this->input->post('settings_meta_description')."',
		settings_website_name         	= '".$this->input->post('settings_website_name')."',
		settings_address              	= '".$this->input->post('settings_address')."',
		settings_phone                	= '".$this->input->post('settings_phone')."',
		settings_fax                  	= '".$this->input->post('settings_fax')."'
 		WHERE settings_id='1'";
		
		$this->db->query($sql);
	}
	
	public function settingsInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".SETTINGS." SET 
		
		settings_logo_photo      = '".$file_name."',
		settings_logo_photo_name = '".$photo_name."',
		settings_logo_photo_ext  = '".$ext."' WHERE settings_id='".$id."'";
		
		$this->db->query($sql);
	}
}
?>