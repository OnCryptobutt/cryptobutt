<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Coin_page extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('coin_page_model');
		$this->detect = new Mobile_Detect();
		$this->db2 = $this->boost_model->getHistoryDb();
		$cname = $this->uri->segment(1);
    }
	public function index()
	{
		$data = "";
		$data['basename'] = "coin_page";
		if($this->uri->segment(1)!='')
		{
			$cname=$this->uri->segment(1);
			$data['cname']=$cname;
			$cid=$this->boost_model->getValue(COIN,'id',"name='".$cname."'");
			$data['cid']=$cid;
		}	
		else
		{
			redirect(base_url());
		}
		if($this->input->get('currency'))
		{
		    $data['currency'] = $this->input->get('currency');
		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$data['currency'] = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				if($this->input->get('source')=="search" && $this->input->get('time'))
				{
					$source = $this->input->get('source');
					$time   = $this->input->get('time');
					$existSearchValues = $this->boost_model->getValue(USER_SEARCH,"count('*')","user_search_values='".$cname."' AND user_search_time='".$time."' AND user_id='".$this->session->userdata('user_id')."'");
					if($existSearchValues<1)
					{
						$insert_array = array("user_search_values" => $cname,
									          "user_id"            => $this->session->userdata('user_id'),
						                      "user_search_time"   => $time,
											  "user_created_time"   => NOW);
											  
						$this->db->set($insert_array)->insert(USER_SEARCH);
					}
				}
			}
			else
			{
				$data['currency'] = "";
			}
			
			if($data['currency']=="")
			{
			  $data['currency'] = "USD";
			}
		}
		$count="";
		$paginationCount="";
	    //$count=$this->coin_page_model->market_record_count();
		$count=$this->coin_page_model->market_record_count2();
		if($count>0)
		{
			$paginationCount=$this->getPagination($count);
		}
		$data['count'] = $count;
		$data['paginationCount'] = $paginationCount;
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		$data['details']=$this->coin_page_model->coin_details($cid);
		$data['coin']=$this->coin_page_model->coin_below_details($cid);
		$data['js']=$this->load->view('details_js',$data,TRUE);
		if($this->session->userdata('user_id')){
		  $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
			 if($query_user->num_rows()>0)
			 {
				 $query_user_row = $query_user->row();
			 }
			 
			 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$data['links_lists'] = $this->coin_page_model->getLinksList($cid);
		$data['role_lists'] = $this->coin_page_model->getRoleList();
	    $data['keydata'] = $this->coin_page_model->getKeypeople($this->uri->segment(1));
	    $data['link_type_list'] = $this->coin_page_model->getLink_typeList();
		$this->load->view('template/header',$data);
		$this->load->view('main_coin_page2',$data);
		$this->load->view('template/footer',$data);
	}
	public function saveSocialpeoplelink(){
	    $profileid    = $this->input->post('profileid');
	    $soialData    = $this->input->post('soialData');
	    $profilelink1 = $this->input->post('profilelink1');
	    $profileweb1  = $this->input->post('profileweb1');
    	if(isset($_POST['type']))
			{
			    $type[] = '';
			    $url[] = '';
    		    $type = $_POST['type'];
    			$url = $_POST['url'];
    			array_push($url, $profilelink1);
    			array_push($type, $profileweb1);
    			foreach($type as $key=>$value)
    			{
    				if($value)
    				{
    					$insert_data = "";
    					$insert_data = array('social_links_type'=>addslashes(trim($value)),
    					'social_people_id'=>$profileid,
    					'social_links_url'=>$url[$key]);
    					$insertid = $this->coin_page_model->linksInsert($insert_data);
						if($insertid) {
        				    echo "success";
        				} else{
        				    echo "error";
        				}
    				}
    			}
			}else{
			    $insert_data = "";
				$insert_data = array('social_links_type'=>addslashes(trim($profileweb1)),
				'social_people_id'=>$profileid,
				'social_links_url'=>$profilelink1);
				$insertid = $this->coin_page_model->linksInsert($insert_data);
				if($insertid) {
				    echo "success";
				} else{
				    echo "error";
				}
			}
	}
	public function saveMatchingpeople()
        {
            $fullname    = $this->input->post('name');
            $profileimg  = $this->input->post('profileimg');
        	$roletype    = $this->input->post('roletype');
        	$roletitle   = $this->input->post('roletitle');
        	$coin_id     = $this->input->post('coincode');
        	if($roletype == 'Board of Directors'){
            	    $roltypeorder = 1;
            	}elseif($roletype == 'Management'){
            	   $roltypeorder = 2; 
            	}elseif($roletype == 'Advisor'){
            	   $roltypeorder = 3; 
            	}elseif($roletype == 'Team'){
            	   $roltypeorder = 4; 
            	}else{
            	    $roltypeorder = 3;
            	}
            $data = array(
                'keypeople_coinid' => $coin_id,
                'keypeople_fullname' => $fullname,
                'keypeople_roletype' => $roletype,
                'keypeople_roletitle' => $roletitle,
                'keypeople_filename'   => $profileimg,
                'keypeople_date' => date("Y-m-d"),
                'roletype_order' => $roltypeorder
            );
            $id = $this->coin_page_model->insertKeypeople($data);
            $keydata = $this->coin_page_model->getKeypeople($coin_id);
           echo json_encode($keydata);
        }
		public function save_key_people()
        {
                $fullname   = $this->input->post('fullname');
            	$roletype = $this->input->post('roletype');
            	$roletitle = $this->input->post('roletitle');
            	$coin_id   = $this->input->post('coin_id');
            	if($roletype == 'Board of Directors'){
            	    $roltypeorder = 1;
            	}elseif($roletype == 'Management'){
            	   $roltypeorder = 2; 
            	}elseif($roletype == 'Advisor'){
            	   $roltypeorder = 3; 
            	}elseif($roletype == 'Team'){
            	   $roltypeorder = 4; 
            	}else{
            	    $roltypeorder = 3;
            	}
               $this->load->helper('url');
                $config['upload_path'] = './img/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                 $this->load->library('upload', $config);
                 $upload = $this->upload->data();
                if (!$this->upload->do_upload('image')) {
                     $data = array(
                'keypeople_coinid' => $coin_id,
                'keypeople_fullname' => $fullname,
                'keypeople_roletype' => $roletype,
                'keypeople_roletitle' => $roletitle,
                'keypeople_filename'   => 'noimg.jpg',
                'keypeople_date' => date("Y-m-d"),
                'roletype_order' => $roltypeorder
            );
            
          $id = $this->coin_page_model->insertKeypeople($data);
          $keydata = $this->coin_page_model->getKeypeoplelist($id);
           
            echo json_encode($keydata);

               }else {
            
            $upload = $this->upload->data();

            $data = array(
                'keypeople_coinid' => $coin_id,
                'keypeople_fullname' => $fullname,
                'keypeople_roletype' => $roletype,
                'keypeople_roletitle' => $roletitle,
                'keypeople_filename'   => $upload['file_name'],
                'keypeople_date' => date("Y-m-d")
            );
            
          $id = $this->coin_page_model->insertKeypeople($data);
          $keydata = $this->coin_page_model->getKeypeoplelist($id);
           
            echo json_encode($keydata);
 
        } 
                    
        }
public function peoplename()
{
        
         $fullname   = $this->input->post('fullname');
         $datapeople = $this->coin_page_model->matchingPeople($fullname);
         	
         if (is_array($datapeople))
    		{
    		    foreach($datapeople as $rowpeople) {

    		        echo '<div class="row eleborder">
                        	<div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        		<img class="img-rounded" src="'.base_url().'img/'.$rowpeople->keypeople_filename.'" alt="'.$rowpeople->keypeople_fullname.'">
                        	</div>
                        	<div class="col-lg-5 col-md-5 col-sm-5 col-12 name-txt">
                        		<p><span class="txt_wht">'.$rowpeople->keypeople_fullname.'</span></p>';
                        		$coinname = $this->coin_page_model->getcoin($rowpeople->keypeople_fullname);
                        		foreach($coinname as $rowcoin) {
                        		    //print_r($rowcoin);
                        		echo '<span class="coin_img"><a href="javascript:;"><img src="'.base_url().'uploads/coin/'.$rowcoin->image.'" alt="'.$rowcoin->keypeople_coinid.'"></a>
                        		</span>';
                        		}
                        	echo '</div><div class="col-lg-4 col-md-4 col-sm-4 col-12 add-txt">
                        		<button class="browse btn btn-primary1 input-lg input-lg1" type="button" data="'.$rowpeople->keypeople_fullname.','.$rowpeople->keypeople_filename.','.$rowpeople->keypeople_roletype.','.$rowpeople->keypeople_roletitle.','.$rowpeople->keypeople_coinid.','.$rowpeople->keypeople_id.'" id="addpeoplekey" >Add</button>
                        	</div>
                        </div>';
              
                }
    		}else{
    		    echo "No Matching profile";
    		}
          

	
}
	
	function getPagination($count)
	{
		
	 $paginationCount= ceil($count / $this->settings->settings_records_per_page_front);
	 
      return $paginationCount;	
		
	}
	
	public function json_chart()
	{
		
		$data='';
		
		//$cid="1360";
		
		
		$data=$this->coin_page_model->chart_data2();
		
	//	print_r($data);
		
       	$encode1 = str_replace('"','',json_encode($data));
	
	   $encode=str_replace(',,',',',$encode1);
		
		
        header('Content-Type: application/json');
	echo $encode;
		
	}
	public function json_charttest()
	{
		
		$data='';
		
		//$cid="1360";
		
		
		$data=$this->coin_page_model->chart_data3();
		
	//	print_r($data);
		
       	$encode = json_encode($data);
	
	  
		
		
       // header('Content-Type: application/json');
//	echo $data;
		
	}
	
	public function chart()
	{
		
		
		
		$data='';
		if($this->uri->segment(3)!='')
		{
			
			$cname=$this->uri->segment(3);
			
			$data['cname']=$cname;
			
			$cid=$this->boost_model->getValue(COIN,'id',"name='".$cname."'");
			
			$data['cid']=$cid;
		}	
		else
		{
			redirect(base_url());
		}
		
		if($this->input->get('currency'))	
		{
		  $data['currency'] = $this->input->get('currency');	

		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$data['currency'] = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$data['currency'] = "";
			}
			
			if($data['currency']=="")
			{
			  $data['currency'] = "USD";
			}
			
		}
		
		$this->load->view('template/header',$data);
		$this->load->view('chart',$data);
		
	}
   
}
