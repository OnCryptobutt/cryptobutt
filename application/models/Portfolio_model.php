<?php
class Portfolio_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	function getBanner()
	{
		$this->db->select("*");
		$this->db->from(BANNER);
		$this->db->where("banner_status","1");
		$this->db->order_by("banner_order","ASC");
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_portfolio_lists($id)
	{
		$this->db->select('*');
		$this->db->from(PORTFOLIO);
		$this->db->where('portfolio_user_id', $id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0 )
		{
			foreach($query->result() as $row)
			{
				$this->db->select('*');
				$this->db->from(COIN);
				$this->db->where('id', $row->portfolio_coin_id);
				$query2 = $this->db->get();
				
				if($query2->num_rows() > 0 )
				{
					foreach($query2->result() as $row2)
					{
						$data[] = $row2;
					} 
					return $data;
				}
			}
		}
	}
	
	function get_coin_names()
	{
		$query = $this->db->select('*')->from(CURRENCY)->where(array("status"=>"1","is_coin"=>"1"))->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = '"'.$row->currency_code.'"'; 
			}
			
			return implode(",",$row1);
		}
	}

}
?>