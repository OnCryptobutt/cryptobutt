<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_category extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('sub_category_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "sub_category-list";
		$data['title'] = ucfirst("sub category List");
		
		/** Start Activate Records **/
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->sub_category_model->sub_categoryActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->sub_category_model->sub_categorySuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->sub_category_model->sub_categoryDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('sub_category/index');
        $config["total_rows"] = $this->sub_category_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->sub_category_model->sub_category_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('sub_category/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "sub_category-add";
		$data['title'] = "Add sub_category";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("sub_category_name","Name","required|is_unique[".SUB_CATEGORY.".sub_category_name]");
			/** $this->form_validation->set_rules("sub_category_content","Content","required"); **/
		    $this->form_validation->set_rules("sub_category_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->sub_category_model->sub_categoryInsert();
				
				if($_FILES['sub_category_photo']['name']!="")
				{
					
					$ext_array = explode(".",$_FILES['sub_category_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "sub_category_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = SUB_CATEGORY_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('sub_category_photo'))
					{
						// case - failure
						
					
						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->sub_category_model->sub_categoryInsertUpdate($file_name,$photo_name,$ext,$pID);	
					}
					
					$source_path = SUB_CATEGORY_PATH.$file_name;
					$target_path = SUB_CATEGORY_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."sub_category/add/");
			}
			
				
		}
		
		$data['getCategoryList'] = $this->sub_category_model->getCategoryList($this->input->post('sub_category_parent'));
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('sub_category/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "sub_category-edit";
		$data['title'] = "Edit sub_category";
		

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("sub_category_name","Name","required|edit_unique_common[".SUB_CATEGORY.".sub_category_name.".$pID.".sub_category_id]");
			/** $this->form_validation->set_rules("sub_category_content","Content","required"); **/
			$this->form_validation->set_rules("sub_category_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->sub_category_model->sub_categoryEdit($pID);
				
				if($_FILES['sub_category_photo']['name']!="")
				{
				    $this->sub_category_model->sub_categoryImageDelete($pID);
					
					$ext_array = explode(".",$_FILES['sub_category_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "sub_category_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = SUB_CATEGORY_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

				$this->upload->do_upload('sub_category_photo');
				
						
						$this->sub_category_model->sub_categoryInsertUpdate($file_name,$photo_name,$ext,$pID);	
					
					
					 $source_path = SUB_CATEGORY_PATH.$file_name;
					$target_path = SUB_CATEGORY_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
					
					
				}
				
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."sub_category/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->sub_category_model->getsub_categoryDetails($pID);
		
		
		$data['getCategoryList'] = $this->sub_category_model->getCategoryList($data['records']->sub_category_parent);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('sub_category/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "sub_category-view";
		$data['title'] = "View sub_category";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->sub_category_model->getsub_categoryDetails($pID);
		
		$data['records']->category_name = $this->boost_model->getValue(CATEGORY,"category_name","category_id='".$data['records']->sub_category_parent."'"); 
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('sub_category/view',$data);
		$this->load->view('template/footer',$data);
		
	}
}
