<?php
class Category_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function category_list($limit, $start)
	{
		$sql = "SELECT * FROM ".CATEGORY." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getcategoryDetails($id)
    {
	   $sql = "SELECT * FROM ".CATEGORY." WHERE category_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      return $this->db->count_all(CATEGORY);
    }
   
    public function categoryActivate($id)
    {
	   $sql = "UPDATE ".CATEGORY." SET category_status='1' WHERE category_id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function categorySuspend($id)
	{
		$sql = "UPDATE ".CATEGORY." SET category_status='0' WHERE category_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function categoryDelete($id)
	{
		$photoname = $this->boost_model->getValue(CATEGORY,"category_photo_name","category_id='".$id."'");
		$photoext = $this->boost_model->getValue(CATEGORY,"category_photo_ext","category_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(CATEGORY_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(CATEGORY_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(CATEGORY_PATH.$photoname.".".$photoext))
			{
				unlink(CATEGORY_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".CATEGORY." WHERE category_id='$id'";
		$query = $this->db->query($sql);
	}
	
	
	public function categoryImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(CATEGORY,"category_photo_name","category_id='".$id."'");
		$photoext = $this->boost_model->getValue(CATEGORY,"category_photo_ext","category_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(CATEGORY_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(CATEGORY_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(CATEGORY_PATH.$photoname.".".$photoext))
			{
				unlink(CATEGORY_PATH.$photoname.".".$photoext);
			}
		}
		
	}
		
	public function categoryInsert()
	{
		$slug = url_title($this->input->post('category_name'), 'dash', true);
		
		$sql = "INSERT INTO ".CATEGORY." SET 
		
		category_name              = '".$this->input->post('category_name')."',
		category_content           = '".$this->input->post('category_content')."',
		category_status            = '".$this->input->post('category_status')."',
		category_order        	   = '".$this->input->post('category_order')."',
		category_site_title        = '".$this->input->post('category_site_title')."',
		category_meta_description  = '".$this->input->post('category_meta_description')."',
		category_meta_keyword      = '".$this->input->post('category_meta_keyword')."',
		category_seo               = '".$slug."',
		category_created_time      = '".NOW."',
		category_updated_time      = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function categoryEdit($id)
	{
		$slug = url_title($this->input->post('category_name'), 'dash', true);
			
		$sql = "UPDATE ".CATEGORY." SET 
		
		category_name              = '".$this->input->post('category_name')."',
		category_content           = '".$this->input->post('category_content')."',
		category_status            = '".$this->input->post('category_status')."',
		category_order             = '".$this->input->post('category_order')."',
		category_site_title        = '".$this->input->post('category_site_title')."',
		category_meta_description  = '".$this->input->post('category_meta_description')."',
		category_meta_keyword      = '".$this->input->post('category_meta_keyword')."',
		category_seo          	   = '".$slug."',
		category_updated_time      = '".NOW."' WHERE category_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function categoryInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".CATEGORY." SET 
		
		category_photo      = '".$file_name."',
		category_photo_name = '".$photo_name."',
		category_photo_ext  = '".$ext."' WHERE category_id='".$id."'";
		
		$this->db->query($sql);
	}
		
}
?>