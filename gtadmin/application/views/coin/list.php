 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coin
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url()?>coin/">coin</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
			
				<div align="left">
			    <?php 
			$attributes = array('method' => 'get');	
				echo form_open('',$attributes);?>
					<input type="hidden" name="act" value="search"/>
				<div class="box-body">
			<div class="row">
					<div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label">Sort By :</label>

                  <div class="col-sm-5">
                  	<select name="currency" class="form-control">
					<option <?php if($this->input->get('currency')=="USD"){ echo "selected='true'"; } ?> value="USD">USD</option>
					<option <?php if($this->input->get('currency')=="GBP"){ echo "selected='true'"; } ?> value="GBP">GBP</option>
					<option <?php if($this->input->get('currency')=="AUD"){ echo "selected='true'"; } ?> value="AUD">AUD</option>
					<option <?php if($this->input->get('currency')=="CAD"){ echo "selected='true'"; } ?> value="CAD">CAD</option>
					<option <?php if($this->input->get('currency')=="INR"){ echo "selected='true'"; } ?> value="INR">INR</option>
					<option <?php if($this->input->get('currency')=="EUR"){ echo "selected='true'"; } ?> value="EUR">EUR</option>
				</select>
                  </div>
                </div>
				
				
				
				    
			
						<div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label">Sort By :</label>

                  <div class="col-sm-5">
				
					<select name="sortby" class="form-control">
						<option <?php if($this->input->get('sortby')=="marketcap"){ echo "selected='true'"; } ?> value="marketcap">Market Cap</option>
						<option <?php if($this->input->get('sortby')=="price"){ echo "selected='true'"; } ?> value="price">Price</option>
						<option <?php if($this->input->get('sortby')=="volume24hr"){ echo "selected='true'"; } ?> value="volume24hr">Volume 24Hr</option>
						
					</select>
					</div>
                </div></div>
                <div class="row">
						<div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label">Sort By :</label>

                  <div class="col-sm-5">
					<select name="sortby1" class="form-control">
						<option <?php if($this->input->get('sortby1')=="hightolow"){ echo "selected='true'"; } ?> value="hightolow">High to Low</option>
						<option <?php if($this->input->get('sortby1')=="lowtohigh"){ echo "selected='true'"; } ?> value="lowtohigh">Low to High</option>
						
					</select>
					</div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label">Status :</label>

                  <div class="col-sm-5">
				
					<select name="status" class="form-control">
						<option value="">--Select Status--</option>
						<option <?php if($this->input->get('status')=="1"){ echo "selected='true'"; } ?> value="1">Active</option>
						<option <?php if($this->input->get('status')=="0"){ echo "selected='true'"; } ?> value="0">Suspend</option>
					</select>
					</div>
                </div></div>  <div class="row">
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label"></label>

                  <div class="col-sm-5">
				
					<input type="text"  class="form-control" name="search" value="<?php echo $this->input->get('search');?>"/>
					
					</div>	
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-1 control-label"></label>

                  <div class="col-sm-5">
					<input class="btn btn-primary" type="submit" value="Search"/>
						</div>	
                </div>
					</div>
				<?php echo form_close();?>
			</div>
			
			
			</div>
			
			
			
			<form id="form1" name="form1" method="post" action="">
			 
				  
            <!-- /.box-header -->
            <div class="box-body">
			
			<?php if($success) echo $success;?>
			<div align="left">
			    <a class="btn btn-sm btn-warning" href="javascript:;">Total Coin(<?php echo count($coinlist_count); ?>)</a>
			    <a class="btn btn-sm btn-warning" href="javascript:;">Activate Coin(<?php echo count($active_coin); ?>)</a>
			    <a class="btn btn-sm btn-warning" href="javascript:;">Suspend Coin(<?php echo count($suspend_coin); ?>)</a>
			</div>
			 <div align="right">
                  <!--<a href="<?php echo base_url();?>coin/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>-->
				  
				  
				  <a onclick="return confirm('Are you sure want to run cron file?');" class="btn btn-sm btn-danger" href="<?php echo base_url('coin_list?run_from=manual');?>">Run Cron File</a>
				  
                  <input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>
                  <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
				  <br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th><input type="checkbox" id="selecctall"/></th>
				  <th>Image</th>
				  <th>Symbol</th>
				  <th>Coin Name</th>
				  <th>Coin Price</th>
				  <th>Market Cap</th>
				  <th></th>
				  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				
					  <?php 
					  
					 
					 
					  if(is_array($lists)){
					  foreach($lists as $list){
						
						  ?>
					  <tr>
					  
					    <td><a href="javascript:;" style="color:#333">
					        <input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $list->coinid;?>">
					        </a> </td>
						  <td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/" style="color:#333">  <?php if($list->image){ echo "<img width='50' height='50' src='".COIN_URL.$list->image."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?></a></td>
						 
						<td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/" style="color:#333"><?php echo $list->name;?></a></td>
						<td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/" style="color:#333"><?php echo $list->coinname;?></a></td>
						<td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/" style="color:#333"><?php echo number_format($list->price,2);?></a></td>
						<td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/" style="color:#333"><?php echo number_format($list->mktcap,2);?></a></td>
						<td><a href="javascript:;" style="color:#333">
					        <input class="checkbox1" type="checkbox" name="checkstatus[]" onclick="checkstatus(this)" <?php if($list->checkstatus == 'true') echo "checked";?> value="<?php echo $list->coinid;?>">
					        </a> </td>
						<td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/" style="color:#333">
						<?php if($list->status=='1'){?><span class="label label-success">Active</span><?php } ?>
						<?php if($list->status=='0'){?><span class="label label-danger">Suspend</span><?php } ?></a>
						</td>
						<td><a target="_blank" href="<?php echo base_url();?>coin/edit/<?php echo $list->coinid;?>/"><span class="fa fa-pencil"></span></a> |
						
						<a target="_blank" href="<?php echo base_url();?>coin/view/<?php echo $list->coinid;?>/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>
					  <?php } }

              else{					  ?>
			  
			  <tr><td align="center" colspan="5"><font color="red">--No records found!--</font></td></tr>
			  <?php } ?>
				
               
                </tbody>
                
              </table>
			 
			  <div align="right"><?php echo $pagination;?></div>
			  
            </div>
			</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 