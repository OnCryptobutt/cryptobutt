 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cryptobutt People
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url()?>manage_people/">Manage People</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!--
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
			<div class="first_sec">
				<div class="row">
					<div class="col-sm-1">
						<img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive">
					</div>
					<div class="col-sm-9">
						<p class="fir_name">First name Middle name Last name</p>
						<p class="sec_st"><i class="fa fa-bars"></i> Linked in</p>
					</div>
				</div>
				<div>
					<p class="pj_link">Projects Linked</p>
				</div>
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-sm-1">
						<img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive">
					</div>
					<div class="col-sm-2 adv_st">
						<p class="advsr">Advisor</p>
					</div>
					<div class="col-sm-2 ver_st">
						<p class="advsr"><i class="fa fa-check-circle" style="color: #05ceef;"></i>&nbsp;&nbsp;Verified</p>
					</div>
				</div>
				<div class="row margin_bot_10">
					<div class="col-sm-1">
						<img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive">
					</div>
					<div class="col-sm-2 adv_st">
						<p class="advsr">Advisor</p>
					</div>
					<div class="col-sm-2 ver_st">
						<p class="advsr"><i class="fa fa-times-circle" style="color: red;"></i>&nbsp;&nbsp;Not Verified</p>
					</div>
				</div>
				<div class="row margin_bot_10">
					<div class="col-sm-1">
						<img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive">
					</div>
					<div class="col-sm-2 adv_st">
						<p class="advsr">Management</p>
					</div>
					<div class="col-sm-2 ver_st">
						<p class="advsr"><i class="fa fa-check-circle" style="color: #05ceef;"></i>&nbsp;&nbsp; Verified</p>
					</div>
				</div>
			</div>	
			<div class="first_sec">
				<div class="row">
					<div class="col-sm-1">
						<img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive">
					</div>
					<div class="col-sm-9">
						<p class="fir_name">First name Middle name Last name</p>
						<p class="sec_st"><i class="fa fa-bars"></i> Linked in</p>
					</div>
				</div>
				<div>
					<p class="pj_link">Projects Linked</p>
				</div>
				<div class="row margin_bot_10">
					<div class="col-sm-1">
						<img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive">
					</div>
					<div class="col-sm-2 adv_st">
						<p class="advsr">Advisor</p>
					</div>
					<div class="col-sm-2 ver_st">
						<p class="advsr"><i class="fa fa-check-circle" style="color: #05ceef;"></i>&nbsp;&nbsp;Verified</p>
					</div>
				</div>
				
			</div>
        </div>
        
      </div>
      
    </section>-->
	
	
	
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
		
		   <?php if(is_array($lists))
		   {
			   foreach($lists as $list)
			   {
				   ?>
			<div class="first_sec">
				<div class="row">
					<div class="col-sm-1">
					<?php if($list->people_photo!='') {?>
					
						<img style="height:50px" src="<?php echo PEOPLE_URL.$list->people_photo;?>" class="img-responsive">
					<?php } else { ?>	
					
						<img style="height:50px" src="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" class="img-responsive">
						
					<?php } ?>	
					</div>
					<div class="col-sm-9">
						<p class="fir_name"><?php echo $list->people_first_name." ".$list->people_middle_name." ".$list->people_last_name;?></p>
						<p class="sec_st"><i class="fa fa-bars"></i> <?php echo $list->people_linked_in_url;?></p>
					</div>
				</div>
				
				<?php 
					$query = $this->db->select('*')->from(KEY_PEOPLE)->join(PEOPLE,PEOPLE.".people_id=".KEY_PEOPLE.".key_people_people_id")->join(ROLE,ROLE.".role_id=".KEY_PEOPLE.".key_people_role_id")->join(COIN,COIN.".id=".KEY_PEOPLE.".key_people_coin_id")->where(KEY_PEOPLE.".key_people_people_id",$list->people_id)->get();
					if($query->num_rows()>0)
					{
						
					
				?>
				<div>
					<p class="pj_link">Projects Linked</p>
				</div>
				
				<?php foreach($query->result() as $row)
					{ ?>
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-sm-1">
						<img style="height:50px" src="<?php echo COIN_URL.$row->image;?>" class="img-responsive">
					</div>
					<div class="col-sm-2 adv_st">
						<p class="advsr"><?php echo $row->role_name;?></p>
					</div>
					<div class="col-sm-2 ver_st">
						<p class="advsr">
						<?php if($row->key_people_verified=="Y")
								{?><i class="fa fa-check-circle" style="color: #05ceef;"></i> Verified
								<?php } ?>
								<?php if($row->key_people_verified=="N")
								{?><i class="fa fa-check-circle" style="color: red;"></i> Not Verified
								<?php } ?>
								
								<?php if($row->key_people_verified=="P")
								{?><i class="fa fa-check-circle" style="color: orange;"></i> Pending
								<?php } ?>
						</p>
					</div>
				</div>
					<?php } } ?>
				
				
			</div>	
			
		   <?php } } ?>
		   
		   <?php echo $pagination;?>
			
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 