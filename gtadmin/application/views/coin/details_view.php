<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coin Details
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <table class="table table-bordered">
				
				
					<tr><td><b>price</b></td><td><?php echo $records->price; ?></td></tr>
					<tr><td><b>lastupdate</b></td><td><?php echo $records->lastupdate; ?></td></tr>
					<tr><td><b>lastvolume</b></td><td><?php echo $records->lastvolume; ?></td></tr>
					<tr><td><b>lastvolumeto</b></td><td><?php echo $records->lastvolumeto; ?></td></tr>
					<tr><td><b>lasttradeid</b></td><td><?php echo $records->lasttradeid; ?></td></tr>
					<tr><td><b>volume24hour</b></td><td><?php echo $records->volume24hour; ?></td></tr>
					<tr><td><b>volume24hourto</b></td><td><?php echo $records->volume24hourto; ?></td></tr>
					<tr><td><b>open24hour</b></td><td><?php echo $records->open24hour; ?></td></tr>
					<tr><td><b>high24hour</b></td><td><?php echo $records->high24hour; ?></td></tr>
					<tr><td><b>low24hour</b></td><td><?php echo $records->low24hour; ?></td></tr>
					<tr><td><b>lastmarket</b></td><td><?php echo $records->lastmarket; ?></td></tr>
					<tr><td><b>change24hour</b></td><td><?php echo $records->change24hour; ?></td></tr>
					<tr><td><b>changepct24hour</b></td><td><?php echo $records->changepct24hour; ?></td></tr>
					<tr><td><b>supply</b></td><td><?php echo $records->supply; ?></td></tr>
					<tr><td><b>mktcap</b></td><td><?php echo $records->mktcap; ?></td></tr>
					
					
					
				
					
				</table>
				
              </div>
			   <div class="box-footer">
                
				<a href="<?php echo base_url();?>coin/view/<?php echo $cid; ?>"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
		  
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
		

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  