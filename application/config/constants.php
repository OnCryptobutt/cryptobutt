<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


define('TABLE_PREFIX',"ci_");
define('ADMIN',TABLE_PREFIX.'admin');
define('CMS',TABLE_PREFIX.'cms');
define('BANNER',TABLE_PREFIX.'banner');
define('CATEGORY',TABLE_PREFIX.'category');
define('SUB_CATEGORY',TABLE_PREFIX.'sub_category');
define('GALLERY',TABLE_PREFIX.'gallery');
define('GALLERY_PHOTOS',TABLE_PREFIX.'gallery_photos');
define('SETTINGS',TABLE_PREFIX.'settings');
define('PRODUCT',TABLE_PREFIX.'product');
define('COIN',TABLE_PREFIX.'coin');
define('USER',TABLE_PREFIX.'user');
define('CRON',TABLE_PREFIX.'cron');
define('CRON_NEW',TABLE_PREFIX.'cron_new');
define('COLUMN',TABLE_PREFIX.'column');
define('CURRENCY',TABLE_PREFIX.'currency');
define('COIN_DETAILS',TABLE_PREFIX.'coin_details');
define('COIN_HISTORY',TABLE_PREFIX.'coin_history');
define('COIN_HISTORY_TEMP',TABLE_PREFIX.'coin_history_temp');
define('PORTFOLIO',TABLE_PREFIX.'portfolio');
define('PORTFOLIO_DETAILS',TABLE_PREFIX.'portfolio_details');
define('USER_SEARCH',TABLE_PREFIX.'user_search');
define('LINKS',TABLE_PREFIX.'links');
define('LINK_TYPE',TABLE_PREFIX.'link_type');
define('ROLE',TABLE_PREFIX.'role');
define('KEY_PEOPLE',TABLE_PREFIX.'key_people');
define('PEOPLE',TABLE_PREFIX.'people');
define('SOCIAL',TABLE_PREFIX.'social');


define('BASE_URL',"https://www.dev.cryptobutt.com/");
define('BASE_PATH',"/home/cryptobutt/public_html/dev/");
define('CLIENT_BASE_URL',"https://www.dev.cryptobutt.com/");
define('CLIENT_BASE_PATH',"/home/cryptobutt/public_html/dev/");
define('UPLOAD_PATH',CLIENT_BASE_PATH . 'uploads/');
define('UPLOAD_URL',CLIENT_BASE_URL."uploads/");
define('CMS_PATH',UPLOAD_PATH . 'cms/');
define('GALLERY_URL',UPLOAD_URL . 'gallery/');
define('GALLERY_PATH',UPLOAD_PATH . 'gallery/');
define('GALLERY_PHOTOS_URL',UPLOAD_URL . 'gallery_photos/');
define('GALLERY_PHOTOS_PATH',UPLOAD_PATH . 'gallery_photos/');
define('CMS_URL',UPLOAD_URL . 'cms/');
define('BANNER_PATH',UPLOAD_PATH . 'banner/');
define('BANNER_URL',UPLOAD_URL . 'banner/');
define('CATEGORY_PATH',UPLOAD_PATH . 'category/');
define('CATEGORY_URL',UPLOAD_URL . 'category/');
define('SUB_CATEGORY_PATH',UPLOAD_PATH . 'sub_category/');
define('SUB_CATEGORY_URL',UPLOAD_URL . 'sub_category/');
define('PRODUCT_PATH',UPLOAD_PATH . 'product/');
define('PRODUCT_URL',UPLOAD_URL . 'product/');
define('COIN_PATH',UPLOAD_PATH . 'coin/');
define('COIN_URL',UPLOAD_URL . 'coin/');

define('SETTINGS_PATH',UPLOAD_PATH . 'settings/');
define('SETTINGS_URL',UPLOAD_URL . 'settings/');

define('PEOPLE_PATH',UPLOAD_PATH . 'people/');
define('PEOPLE_URL',UPLOAD_URL . 'people/');

define('NOW',date("Y-m-d H:i:s"));

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
