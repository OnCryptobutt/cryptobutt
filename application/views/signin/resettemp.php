<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <!-- So that mobile webkit will display zoomed in  -->


    <title>Cryptobutt.com</title>
    <style type="text/css">
        .ReadMsgBody {
            width: 100%;
            background-color: #f1f1f1;
        }
        
        .ExternalClass {
            width: 100%;
            background-color: #f1f1f1;
        }
        
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 90%;
        }
        
        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            font-family: Helvetica, arial, sans-serif;
        }
        
        body {
            margin: 0;
            padding: 0;
        }
        
        .columns-container {
            width: 100%;
        }
        
        table {
            border-spacing: 0;
        }
        
        table td {
            border-collapse: collapse;
        }
        
        .yshortcuts a {
            border-bottom: none !important;
        }
        
        a {
            text-decoration: none;
        }
        
        .center {
            text-align: center;
        }
        
        .bullet-width {
            width: 10px!important;
        }
        
        .corners-large {
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
        }
        
        .corners-top-large {
            -webkit-border-radius: 8px 8px 0 0;
            -moz-border-radius: 8px 8px 0 0;
            border-radius: 8px 8px 0 0;
        }
        
        .corners-medium {
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
        }
        
        .corners-small {
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        
        div.desktop-header {
            color: #f1f1f1;
        }
        
        .title {
            font-size: 30px !important;
            color: #ffffff;
        }
        
        .title-light {
            font-weight: normal;
            font-size: 30px !important;
            color: #ffffff;
        }
        
        .title-bold {
            font-weight: bold;
            font-size: 26px !important;
            color: #ffffff;
        }
        
        .pad-v-10 {
            padding-top: 10px;
            padding-bottom: 10px;
        }
        
        .pad-v-15 {
            padding-top: 15px;
            padding-bottom: 15px;
        }
        
        .pad-v-20 {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        
        .pad-v-20-30 {
            padding-top: 20px;
            padding-bottom: 30px;
        }
        
        .header-wrapper {
            padding-bottom: 0px !important;
        }
        
        .btn {
            text-align: center;
            padding-top: 6px;
            padding-bottom: 6px;
            padding-left: 12px;
            padding-right: 12px;
        }
        
        .cta-btn {
            padding-top: 12px;
            padding-bottom: 12px;
            padding-left: 24px;
            padding-right: 24px;
            /*font-weight: bold;*/
        }
        
        .btn-color {
            background-color: #c939aa;
            color: #ffffff;
        }
        
        table.container {
            width: 600px !important;
        }
        
        img.col-2-img {
            width: 260px;
        }
        
        img.col-3-img {
            width: 180px;
        }
        
        @media only screen and (max-width: 600px) {
            body[yahoo] .mobile-br {
                display: block!important;
            }
            body[yahoo] span[class=date-color] a {
                color: #565656!important;
                pointer-events: none!important
            }
            body[yahoo] .text-block {
                font-size: 110% !important;
                line-height: 130%;
            }
            body[yahoo] .header-wrapper {
                padding-bottom: 0px !important;
            }
            body[yahoo] .btn {
                display: inline-block;
                text-align: center;
                width: 215px;
                padding: 8px;
                margin: 4px 0;
            }
            body[yahoo] .cta-btn {
                padding-top: 12px;
                padding-bottom: 12px;
                /*font-weight: bold;*/
                display: block;
                width: 60%!important;
                margin: 0 auto;
            }
            body[yahoo] .sub-title {
                font-size: 20px !important
            }
            body[yahoo] .title-bold,
            .title-light {
                font-size: 18px !important;
            }
            body[yahoo] .invisible-text,
            body[yahoo] .invisible-text a {
                color: #f2f2f2 !important;
            }
            body[yahoo] .force-col {
                display: block;
                padding-right: 0 !important;
            }
            body[yahoo] .col-2,
            body[yahoo] .col-3 {
                float: none !important;
                width: 100% !important;
                margin-bottom: 12px;
                padding-bottom: 12px;
                border-bottom: 1px solid #eee !important;
            }
            body[yahoo] .last-col-2,
            body[yahoo] .last-col-3 {
                border-bottom: none !important;
                margin-bottom: 0;
            }
            body[yahoo] .col2-img,
            body[yahoo] img.col-2-img {
                width: 100%;
            }
            body[yahoo] .container-padding {
                -webkit-border-radius: 0px !important;
                padding-left: 3% !important;
                padding-right: 3% !important;
            }
            body[yahoo] .header-wrapper {
                padding-bottom: 0px;
            }
            body[yahoo] img.divider-img {
                width: 100%;
                max-width: 570px;
            }
            body[yahoo] img.footer-img {
                max-width: 100%;
            }
            body[yahoo] .desktop-header {
                display: none;
            }
            body[yahoo] .mobile-header {
                display: block !important;
                width: 100%;
            }
            body[yahoo] .header-anchor {
                height: auto !important;
            }
            body[yahoo] table.container {
                width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }
            body[yahoo] .footer-img {
                display: none;
            }
            body[yahoo] .full-width {
                width: 100% !important;
                padding: 0 !important;
                vertical-align: top;
            }
            body[yahoo] .hidden {
                width: 1px !important;
            }
            body[yahoo] .logo-space {
                width: 20px;
            }
            body[yahoo] .desktop-image {
                display: none;
            }
            body[yahoo] .mobile-image {
                display: block !important;
                width: 100%;
            }
            body[yahoo] .center {
                text-align: center !important;
            }
            body[yahoo] .image-size {
                height: 184px !important;
                width: 100% !important;
            }
            body[yahoo] .invisible {
                display: none !important;
            }
            body[yahoo] .desktop-block {
                display: none !important;
            }
            body[yahoo] .desktop-cell {
                height: 0px !important;
                padding-bottom: 0px !important;
            }
            body[yahoo] .mobile-header-image {
                width: 100%;
            }
            body[yahoo] .mobile-block-holder {
                width: 100% !important;
                height: auto !important;
                max-width: none !important;
                max-height: none !important;
                display: block !important;
            }
            body[yahoo] .mobile-block {
                display: block !important;
            }
            body[yahoo] .mobile-centered {
                text-align: center !important;
                vertical-align: middle !important;
            }
            body[yahoo] .mobile-centered img {
                display: inline;
            }
            body[yahoo] .social-icons img {
                width: 10%;
            }
            body[yahoo] .fcs {
                min-width: 0 !important;
            }
        }
        
        @media only screen and (min-width: 540px) and (max-width: 600px) {
            body[yahoo] .text-block {
                font-size: 160% !important;
                line-height: 140% !important;
            }
            body[yahoo] .extra-padding {
                padding-top: 10px !important;
            }
            body[yahoo] .padding-bottom-SD {
                padding-bottom: 30px !important;
            }
            body[yahoo] .text-block-2 {
                font-size: 180% !important;
                line-height: 140% !important;
            }
            body[yahoo] .mobile-bullet-padding {
                padding-top: 10px !important;
            }
        }
        
        @media only screen and (min-device-width: 322px) and (max-width: 539px) {
            body[yahoo] .text-block {
                font-size: 95% !important;
                line-height: 140% !important;
            }
            body[yahoo] .samsung-br {
                display: block!important;
            }
            /*body[yahoo] .text-block-2 {*/
            /*font-size: 100% !important;*/
            /*line-height: 140% !important;*/
            /*}*/
            body[yahoo] .mobile-bullet-padding {
                /*padding-top: 2px !important;*/
            }
        }
        
        @media only screen and (min-width: 320px) and (max-width: 320px) {
            body[yahoo] .mobile-bullet-padding {
                padding-top: 2px !important;
            }
            body[yahoo] .text-block {
                font-size: 100% !important;
                line-height: 140% !important;
            }
        }
        
        .mobile-block-holder {
            width: 0;
            height: 0;
            max-width: 0;
            max-height: 0;
            overflow: hidden;
        }
        
        .mobile-block-holder,
        .mobile-block,
        .mobile-header {
            display: none;
        }
        
        .desktop-block,
        .desktop-header,
        .desktop-image {
            display: block;
        }
        
        table.col-2 {
            width: 260px;
            border-bottom: none !important;
        }
        
        table.col-3 {
            width: 200px;
            border-bottom: none !important;
        }
        
        .disclaimer {
            font-size: 10px !important;
        }
    </style>

</head>

<body yahoo="fix" style="margin:0; padding:0px; background-color: #f1f1f1;" bgcolor="#f1f1f1; font-family: Arial, 'Helvetica', sans-serif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
    <!--Pre-header text-->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1" class="fcs" style="min-width: 600px; width: 100%; background-color: #f1f1f1;">
        <tr>
            <td align="center" style="text-align: center;"><div style="font-size: 15px; line-height: 24px; color: #333333; text-align:center " align="center" class="desktop-block"><table border="0" cellpadding="0" cellspacing="0" class="desktop-block" width="100%" style="display: table!important;"><tr><td style="line-height: 24px; color: #565656;font-size: 16px;font-weight: normal; padding-bottom: 20px;text-align: center" align="center"><span style="line-height: 24px; color: #565656;font-size: 18px;display: none;">You requested to reset your password for your Cryptobutt account. </span></td></tr></table></div></td>
        </tr>
        <tr>
            <td align="center" valign="top" bgcolor="#f1f1f1" style="background-color: #f1f1f1;width: 1440px">
                <!-- 600px container (white background) -->
                <table border="0" width="600" cellpadding="0" cellspacing="0" class="container fcs" bgcolor="#f2f2f2" style="min-width: 600px; width: 600px; table-layout: fixed;">
                    <tr>
                        <td class="fcs" style="min-width: 600px;">
                            <table cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
                                <tr valign="bottom">
                                    <td>
                                        <div class="desktop-block">
                                            <table cellpadding="0" cellspacing="0" width="100%" style="width: 100%; vertical-align: bottom;">
                                                <tr>
                                                    <td style="width: 600px;padding: 10px 0 10px 30px; background-color: #f1f1f1;text-align: center;" bgcolor="#f1f1f1" align="left"><img alt="" src="https://www.cryptobutt.com/img/logo.png"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" class="" width="100%">
                                            <tr>
                                                <td width="100%">
                                                    <div class="mobile-block-holder" style="width: 0px; height: 0px; max-width: 0px; max-height: 0px; overflow: hidden; text-align: center;">
                                                        <div class="mobile-block center" style="text-align: center; padding-bottom: 10px; float: left; width: 100%; background-color: #f1f1f1">
                                                            <div style="float: left; padding-right: 10px; padding-left: 10px; width: 40%; padding-top: 10px"><img src="https://www.cryptobutt.com/img/logo.png" alt="" class="mobile-logo" style="text-align:center; width: 100%;"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!------------------------------------------------------------------header Block------------------------------------------------------------------------------->
                    <tr>
                        <td class="header-wrapper">
                            <div class="desktop-block">
                                <div class="desktop-header corners-large center">
                                    <table cellpadding="0" cellspacing="0" border="0" width="600" style="background-color: #f1f1f1;color: #333333;text-align: center;">
                                        <tr>
                                            <td style=" width: 100%; text-align: center;">
                                                <a href="javascript:;"><img src="https://www.cryptobutt.com/img/new_forgot_password_desktop.png" style="display: block" border="0" alt="Reset Password"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:36px;font-weight: bold;color:#565656;text-align: center; padding: 10px 0">Reset Your Password</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mobile-block">
                                <tr>
                                    <td width="100%">
                                        <div class="mobile-block-holder" style="overflow:hidden;max-height:0px;max-width:0px;height:0px;width:0px;">
                                            <div class="mobile-block" style="background-color: #f1f1f1;color: #333333">
                                                <div style="text-align: center; background-color: #f1f1f1;width: 100%">
                                                    <a href="javascript:;"><img src="https://www.cryptobutt.com/img/new_forgot_password_mobile.png" style="width: 100%;" width="100%" alt="Reset Password"></a>
                                                </div>
                                            </div>
                                            <div class="mobile-header center" style="background-color:#f1f1f1;color:#565656;font-weight:bold;">
                                                <div style="font-size: 220%;padding-bottom: 2%;padding-top:3%; "><span class="text-block">Reset Your 
Password</span></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!------------------------------------------------------------------End of Header Block------------------------------------------------------------------------------->
                    <!------------------------------------------------------------------Bullet Block------------------------------------------------------------------------------->
                    <tr>
                        <td class="container-padding" style="background-color: #f1f1f1; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">
                            <div style="font-size: 15px; line-height: 24px; color: #333333; text-align:center " align="center" class="desktop-block">
                                <table border="0" cellpadding="0" cellspacing="0" class="desktop-block" width="100%" style="display: table!important;">
                                    <tr>
                                        <td style="line-height: 24px; color: #565656;font-size: 16px;font-weight: normal; padding-bottom: 20px;text-align: center" align="center"><span style="line-height: 24px; color: #565656;font-size: 18px;display: block">You requested to reset your password for your Cryptobutt account. </span></td>
                                    </tr>
                                </table>
                            </div>
                            <table class="mobile-block" cellpadding="0" cellspacing="0" border="0" style="display: table!important; width: 100%">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" class="desktop-block" width="100%" style="display: table!important;">
                                            <tr>
                                                <td width="100%">
                                                    <div class="mobile-block-holder" style="overflow:hidden;max-height:0px;max-width:0px;height:0px;width:0px; display: none;">
                                                        <div class="mobile-block" style="padding-bottom: 10px; width: 100%; text-align: center">
                                                            <table border="0" cellpadding="0" cellspacing="0" class="desktop-block" width="100%" style="display: table!important;">
                                                                <tr>
                                                                    <td style="border-collapse:collapse; font-size: 16px" width="100%" valign="top">
                                                                        <div class="mobile-block-holder" style="overflow:hidden;max-height:0px;max-width:0px;height:0px;width:0px; display: none;">
                                                                            <div class="mobile-block">
                                                                                <div class="mobile-block" style="padding-bottom: 15px;width: 100%;">
                                                                                    <div style="text-align: center;line-height: 20px; font-size: 18px;color: #565656;font-weight: normal"> <span class="text-block"> You requested to reset your<br/> password for your Cryptobutt account. </span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="mobile-block" style="padding-bottom: 20px">
                                                                                <a href="javascript:;" class="btn cta-btn corners-small" style="background-color: #f1f1f1; color: #ffffff!important; ;font-size: 16px; padding-bottom: 12px;padding-top: 12px;"> <span class="text-block"> Reset Password </span></a>
                                                                            </div>
                                                                            <div class="mobile-block" style="padding-bottom: 5px;width: 100%; text-align: center">
                                                                                <div style="text-align: center;line-height: 20px; font-size: 14px;color: #565656"> <span class="text-block"> Questions? Contact <a href="javascript:;" style="text-decoration: none;color: #3999ed;">Cryptobutt Support</a>. </span></div>
                                                                                <div style="text-align: center;line-height: 20px; font-size: 14px;color: #565656"> <span class="text-block"> If you did not request to reset your password,<br/> please ignore this email. </span></div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" class=" desktop-block" width="100%" style="display: table">
                                <tr>
                                    <td class="desktop-block" style="padding: 0 0 20px 0;text-align: center">
                                        <!--Desktop button-->
                                        <table cellspacing="0" cellpadding="0" style="display: inline-block">
                                            <tr>
                                                <td align="center" bgcolor="#3999ed" style=" background-color: #3999ed; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; display: block; text-align: center; margin: 0 auto; color: #ffffff; padding: 14px 30px">
                                                    <a id="outlook-color-fix" href="<?php echo base_url(); ?>signin/resetPassword?emailToken=<?php echo $userName; ?>" style=" color: #ffffff; font-size: 20px; font-weight: normal; font-family: Helvetica, Arial, sans-serif; text-decoration: none; width:100%; display:inline-block">Reset Password</a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div style="font-size: 
15px; line-height: 24px; color: #333333; text-align: center" align="center" class="desktop-block">
                                <table border="0" cellpadding="0" cellspacing="0" class="desktop-block" width="100%" style="display: table!important;">
                                    <tr>
                                        <td style="line-height: 24px; color: #565656;font-size: 15px;font-weight: normal;text-align: center" align="center"><span style="line-height: 24px; color: #565656;font-size: 15px">Questions? Contact <a href="javascript:;" style="text-decoration: none;color: #3999ed;">Cryptobutt Support</a>.</span></td>
                                    </tr>
                                    <tr>
                                        <td style="line-height: 24px; color: #565656;font-size: 15px;font-weight: normal;padding-bottom: 30px;text-align: center" align="center"><span style="line-height: 24px; color: #565656;font-size: 15px">If you did not request to reset your password, please ignore this email.</span></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <!------------------------------------------------------------------End of Bullet Block------------------------------------------------------------------------------->
                    <tr>
                        <td align="center" style="padding-left: 10px; padding-right: 10px; background-color: #f1f1f1; text-align: center; font-size:9px!important;" class="disclaimer">
                            <br>Please do not reply to this email
                            
                            <br>
                            <br>
                        </td>
                    </tr>
                </table>
                <!--/600px container -->
            </td>
        </tr>
    </table>
    <!--/100% wrapper-->
    <div style="display:none; white-space:nowrap; font:15px courier; color:#f2f2f2;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</div>
</body>

</html>