<script>

function change_currency(symbol,name)
{
	 $("#change_currency").html(symbol+" "+name);
}

function currency(per_page)
{
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&currency='+per_page;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 $("#change_title").html(per_page);
           }
      });

}

function tops(type,val)
{
	if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
		$("#bal_al").html("Loading...");
		
	}
	else if(type=="sorting")
	{
		<?php if($this->detect->isMobile())
		{ ?>
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val+"_mobile" ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_volume_mobile").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price_mobile").removeClass('selecte');
			 $( "#sortby_marketcap_mobile").removeClass('selecte');
		}
		<?php }else
		{			?>
	
	$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_marketcap").removeClass('selecte');
		}
	
	    <?php } ?>
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#coin_name_mobile").val();
	
	<?php  }else { ?>
	search = $("#coin_name").val();
	<?php } ?>
	sorting = $("#hidden_sorting").val();
	
      
	
	pageId = "0";
	liId = "0";
	 $("#pageData").html('<span class="flash"></span>');
	$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&search='+search+'&sorting='+sorting;
    
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			 //  alert(result);
			  //alert(dataString);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
				 document.getElementById("row_no").value = 1;
				 document.getElementById("pageids").value = 0;
				 if(type=="tops")
				 {
					$("#change_title").html(per_page);
				 }
				 displaybalance();
           }
      });

}


  function changePagination(pageId,liId){
	  $("#pageData").html('<span class="flash"></span>');
     $(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });
}

changePagination("0","0");


setInterval(function(){ 

$('.well').each(function(){
var id=$(this).attr('id');

currency = $("#hidden_currency").val();
 prev_price = $("#beat"+id+"price").html();
  var dataString = 'id='+ id+'&currency='+currency+'&prev_price='+prev_price;
	$.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/updaterow');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
			  var obj = JSON.parse(result); 
               $("#beat"+id+"price").html(obj.price);
			   $("#beat"+id+"market").html(obj.mktcap);
			   $("#beat"+id+"hour").html(obj.volume24hour);
			   $("#beat"+id+"change").html(obj.change24hour);
			   $("#beat"+id+"price_mobile").html(obj.price);
			   $("#beat"+id+"market_mobile").html(obj.mktcap);
			   $("#beat"+id+"volume24hour_mobile").html(obj.volume24hour);
			   $("#beat"+id+"change_mobile").html(obj.change24hour);
			   if(obj.price_change==="Yes")
			   {
				   if(obj.change24hour<0)
				   {
			    $( "#"+id ).addClass( "row_background" );
				setTimeout(function(){
            $( "#"+id ).removeClass('row_background');
          
    }, 500);
				   }
				   if(obj.change24hour>0)
				   {
				   
	 $( "#"+id ).addClass( "row_background2" );
				setTimeout(function(){
            $( "#"+id ).removeClass('row_background2');
          
				   }, 500);    
				   }
			   }
			   else
			   {
				    $( "#"+id ).addClass( "row_background3" );
				setTimeout(function(){
            $( "#"+id ).removeClass('row_background3');
          
    }, 500);
			   }
           }
      });
});

}, 5000);



//$(window).scroll(function ()
$(document).on('scroll', function(){ 
    
	//alert($(document).height());
	//alert($(document).scrollTop());
		
	  if($(document).height()-400 <= ($(document).scrollTop() + $(document).height()))
	  {
		  totalcoins = document.getElementById('coins_count').innerHTML;
		  totalcoins = Number(totalcoins);
		  var val = document.getElementById("row_no").value;
		  pagedisplays = val * <?php echo $this->settings->settings_records_per_page_front;?>;
		  if(totalcoins>pagedisplays)
		  {
			  //alert(val);
			 var pageids = document.getElementById("pageids").value;
			 var pageidsarray = pageids.split(",");
			  var i = pageidsarray.indexOf(val);
             if(i > -1){ 
			 }
			 else
			 {
			  loadmore();
			  document.getElementById('pageids').value=pageids+','+val;
			 }
			  
		  }
		 
		
		  
	  }
    });
	
	
	
	
	
	
function loadmore()
{
	type = "";
	 var val = document.getElementById("row_no").value;
	 
	 if(type=="tops")
	{
		$("#hidden_per_page").val(val);
	}
	else if(type=="currency")
	{
		$("#hidden_currency").val(val);
	}
	else if(type=="sorting")
	{
		$("#hidden_sorting").val(val);
		
		$( "#sortby_"+val ).addClass( "selecte" );
		
		if(val=="price")
		{
			 $( "#sortby_marketcap").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="marketcap")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_volume").removeClass('selecte');
		}
		else if(val=="volume")
		{
			 $( "#sortby_price").removeClass('selecte');
			 $( "#sortby_marketcap").removeClass('selecte');
		}
	}
	
	
	
	per_page = $("#hidden_per_page").val();
	currency = $("#hidden_currency").val();
	<?php if($this->detect->isMobile())
	{ ?>

	search = $("#coin_name_mobile").val();
	
	<?php  }else { ?>
	search = $("#coin_name").val();
	<?php } ?>
	
	sorting = $("#hidden_sorting").val();
	

	
	pageId = val;
	liId = "0";
	 var content = document.getElementById("pageData");
     content.innerHTML = content.innerHTML+'<span class="flash"></span>';
	 //$("#pageData").html('<span class="flash"></span>');
	//$(".flash").show();
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&per_page='+per_page+'&currency='+currency+'&search='+search+'&sorting='+sorting;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").remove();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 //$("#pageData").html(result);
				 var content = document.getElementById("pageData");
                 content.innerHTML = content.innerHTML+result;
				 document.getElementById("row_no").value = Number(val)+1;
				 if(type=="tops")
				 {
					$("#change_title").html(per_page);
				 }
           }
      });

	 
	
	 
}


function addportfolio(val)
{
	<?php if($this->session->userdata('user_id'))
	{ ?>

   var dataString = 'cid='+ val;
 	$("#heart_"+val).fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader20.gif'>");
	 $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_addportfolio/add');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   
               if(result=="success")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else if(result=="exit")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>");
			   }
			   else
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart-o heart_cor\" aria-hidden=\"true\"></i>");

			   }
                 //$("#pageData").html(result);
				
           }
      });
	<?php }
	else
	{
		
		?>
		$('#myModal').modal('show');
    <?php 
    } 
	?>
}


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#coin_name").css("background","#FFF");
		}
		});
	});
});
//To select country name
/** function selectCountry(val) {
$("#coin_name").val(val);
$("#suggesstion-box").hide();
tops('search','');
} **/


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name_mobile").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});
//To select country name
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val+"?source=search&time="+<?php echo time();?>;
	<?php if($this->detect->isMobile())
	{ ?>
$("#coin_name_mobile").val(val);
$("#suggesstion-box_mobile").hide();

	<?php }else{ ?>
	$("#coin_name").val(val);
$("#suggesstion-box").hide();
	<?php } ?>
//tops('search','');
}

$(window).on('load', function() {
  displaybalance();
})


function displaybalance(){
	
	
	 var currency = $("#hidden_currency").val();
     var dataString = 'currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=home');?>',
           data: dataString,
           cache: false,
           success: function(result){
			
                 $("#pageData3").html(result);
           }
      });
}

</script>