<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $basename; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

  </head>
<!------ Include the above in your HEAD tag ---------->
<style>
p{margin-bottom:5px !important;}
/*******************************
* Does not work properly if "in" is added after "collapse".
* Get free snippets on bootpen.com
*******************************/
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        
    }
	.logo-img{
		margin: 0px 0 16px 0;
    border-bottom: 1px solid #ddd;
    padding: 0 0 0 20px;
    box-shadow: 0 8px 6px -6px #ccc;
	-webkit-box-shadow: 0 8px 6px -6px #ccc;
       -moz-box-shadow: 0 8px 6px -6px #ccc;
    background: #fff;
	}
	.panel-default {
    border-bottom-color: rgb(10, 0, 182,0.1) !important;
	border-top-color: #fff !important;
	border-left-color: #fff !important;
	border-right-color: #fff !important;
	
}
	.panel-group .panel+.panel{margin-top:0 !important;}
    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FFF;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 34px 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

/* ----- v CAN BE DELETED v ----- */

body{background-color:#EDE7F6}
.demo {
    padding-top: 60px;
    padding-bottom: 60px;
}
.wix-logo{height:65px;width:25%}
.closeicon{width:18px;}
.panel-title > a {font-size: 34px;}

@media (min-width:320px) and (max-width:767px){
	.wix-logo{width:60%}
	.panel-title > a {font-size: 24px;}
	.more-less{font-size:36px;position: relative;top: -6px;}
	.panel-body{font-size:16px;}
}
.panel-body p{font-size:16px}
</style>
<body>
<div class="container-fluid desktop" style="padding:0;">
	<div class="logo-img">
		<img class="wix-logo" src="<?php echo base_url();?>img/logo.png" alt="title">
		<span class="logo-text hidden-xs" style="color:#ccc;font-size:20px">IMDB for Crypto assets</span>
		<a class="x-close" href="javascript:history.back()" style="text-decoration:none;float:right;margin:20px">
		<img src="<?php echo base_url();?>img/close_352915.svg" alt="close" class="closeicon" /></a>
	</div>
		<div class="container">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#About-us" aria-expanded="true" aria-controls="About-us">
                        <i class="more-less glyphicon-plus"></i>
                        About us
                    </a>
                </h4>
            </div>
            <div id="About-us" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
					<p>As a blockchain enthusiast, we are building this tool to enable and guide the beginner and pros alike to perform your due diligence in a constructive manner before their investment.</p>
					<p>&nbsp;</p>
					<p>On Cryptobutt we put forward (a plan or suggestion) in 3 simple steps for your consideration;</p>
					<p>&nbsp;</p>
					<p>1.Understand their market behaviour and their volatility</p>
					<p>2.Understand the background of the crypto company and people behind the project</p>
					<p>3.Know their product and competitor</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<p><strong>About me</strong></p><br>
					<p>I Dinesh Jayachandran, active learner, contributor to the overall UX and vision of this projects. I’m managing this
					project by hiring freelancers online.</p>
					<p>&nbsp;</p>
					<p>The reason why I’m setting up this website is to help people like me in the wild, wild west cryptocurrency community. I realised some ICO’s, Crypto companies and market participants don’t play fair and they profit at the expense of others. Early 2017 when I started investing in crypto assets, finding a right crypto asset to invest in was a full-time job. I’ve introduced a few of my friends and colleagues in this industry what I have noticed is everyone was researching methodology is different.</p>
					<p>&nbsp;</p>
					<p>Here I’m trying to set up the framework for you guys to get your diligence right. Wish you all good luck. Long live satoshi!</p>
					<p>&nbsp;</p>
					<p><a href="javascript:;">View my Portfolio</a></p>
					<p><a href="javascript:;">Contact me</a></p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Our-timeline" aria-expanded="false" aria-controls="Our-timeline">
                        <i class="more-less glyphicon-plus"></i>
                        Our timeline
                    </a>
                </h4>
            </div>
            <div id="Our-timeline" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <p><img src="<?php echo base_url(); ?>images/timeline.png" alt="Timeline"/></p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#About-verified-accounts" aria-expanded="false" aria-controls="About-verified-accounts">
                        <i class="more-less glyphicon-plus"></i>
                        About verified accounts
                    </a>
                </h4>
            </div>
            <div id="About-verified-accounts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                   <p>The blue verified badge on Cryptobutt acknowledges that a key person working on a project has publicly announced it on his/her social media profile such as Twitter, Facebook or LinkedIn.</p>
					<p>&nbsp;</p>
					<p>The badge shows up next to his/her name on the Crypto Asset's team profile page and also on their individual profile page. The colour and position of the badge is always the same regardless to the Crypto Asset's profile or theme/colour customisations.</p>
					<p>&nbsp;</p>
					<p>Accounts that are not verified do not receive a badge next their name.</p>
					<p>&nbsp;</p>
					<p><strong>How an account becomes verified?</strong></p>
					<p>The content manager</p>
					<p>- checks the Crypto Asset's official website and navigates to the 'team' page or the 'about us' page for team member information</p>
					<p>- checks the team member's name and role </p>
					<p>- verifies it against the team member' official social media links like Twitter, Facebook, LinkedIn, etc.</p>
					<p>- controls if the the team member has publicly announced about his/her work on that particular Crypto Asset on social media.</p>
					<p>- then confers the verified badge as a part of the profile.</p>
					<p>&nbsp;</p>
					<p><strong>What does 'account verified' mean?</strong></p>
					<p>A person can be verified if he/she is determined to be an account of Cryptobutt's interest. Typically this includes accounts 
maintained by Cryptobutt and a verified badge does imply an endorsement by Cryptobutt.com</p>
					<p>&nbsp;</p>
					<p>The good example (Jed McCaleb, Thank you for allowing us to share your profile page)</p>
					<p><img src="<?php echo base_url(); ?>images/verifiedprofile.PNG" alt="verified profile"/></p>
                </div>
            </div>
			
        </div>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Privacy-policy" aria-expanded="false" aria-controls="Privacy-policy">
                        <i class="more-less glyphicon-plus"></i>
                        Privacy policy
                    </a>
                </h4>
            </div>
            <div id="Privacy-policy" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                <div class="panel-body">
                    <p>At Cryptobutt.com, we’re committed to protecting and respecting your privacy. This Policy explains when and why we collect personal information about people who visit our website, how we use it, the conditions under which we may disclose it to others, and how we keep it secure. This Policy also explains how we use cookies and similar technology on our website.</p>
					<p>We may change this Policy from time to time so please check this page occasionally to ensure that you’re happy with any changes. By using our website, you’re agreeing to be bound by this Policy.What Information Do We Collect?</p>
					<p>Cryptobutt.com ("Cryptobutt", "we" or "us") collects (a) the email addresses of those who communicate with us via e-mail, (b) email address of those who register with us and (c) information volunteered by the user (such as survey information and/or site registrations)</p>
					<p>&nbsp;</p>
					<p><strong>How Do We Use the Information?</strong><br>Cryptobutt uses collected information for the following purposes:<br>
					To fulfill a contract or take steps linked to a contract such as processing your registration on our website or sending you information about changes to our terms or policies;<br>Where it is necessary for purposes which are in Cryptobutt’s legitimate interests such (a) to provide the information or content you have requested, (b) to contact you about our programs, products, features or services, (c) for internal business purposes such as identification and authentication or customer service, (d) to ensure the security of our website, by trying to prevent unauthorized or malicious activities, (e) to enforce compliance with our terms of use and other policies, (f) to help other organizations (such as copyright owners) to enforce their rights, and (g) to tailor content.<br>If you do not wish to receive information about our programs, products, features or services, you may send an email to us at <a href="mailto:legal@cryptobutt.com">legal@cryptobutt.com</a>.</p>
					<p>&nbsp;</p>
					<p><strong>How Do We Share Your Information?</strong><br>We do not share or sell your personal data to other organizations for commercial purposes, except to provide products or services you’ve requested, when we have your permission, or under the following circumstances:It is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of Terms of Service, or as otherwise required by law.<br/>We transfer information about you if cryptobutt.com is acquired by or merged with another company. In this event, Cryptobutt will notify you before information about you is transferred and becomes subject to a different privacy policy.</p>
					<p>&nbsp;</p>
					<p><strong>Data Storage</strong><br>Cryptobutt uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run Cryptobutt. Cryptobutt.com owns the code, databases, and all rights to the Cryptobutt application.</p>
					<p>&nbsp;</p>
					<p><strong>Security</strong><br>We take precautions to ensure the security of your personal information. However, we cannot guarantee that hackers or unauthorized personnel may gain access to your personal information despite our efforts. You should note that in using the Cryptobutt service, your information will travel over the Internet and through third party infrastructures which are not under our control.<br>We cannot protect, nor does this Privacy Policy apply to, any information that you transmit to other users. You should never transmit personal or identifying information to other users.Retention of Your Personal Information<br>We retain information as long as it is necessary to provide the services requested by you and others, subject to any legal obligations to further retain such information. Information associated with your account will generally be kept until it is no longer necessary to provide the services or until you ask us to delete it or your account is deleted whichever comes first. Additionally, we may retain information from deleted accounts to comply with the law, prevent fraud, resolve disputes, troubleshoot problems, assist with investigations, enforce the Terms of Use, and take other actions permitted by law. The information we retain will be handled in accordance with this Privacy Policy.</p>
					<p>&nbsp;</p>
					<p><strong>Children</strong><br>The Cryptobutt service is not intended for children under the age of 16, and we do not knowingly collect information from children under the age of 16.<br>Children aged 16 or younger should not submit any personal information without the permission of their parents or guardians. By using the Cryptobutt service, you are representing that you are at least 18 years old, or that you are at least 16 years old and have your parents’ or guardians’ permission to use the service.Consent to Worldwide Transfer and Processing of Personal Information</p>
					<p>&nbsp;</p>
					<p><strong>Complaints</strong><br>Should you wish to raise a concern about our use of your information (and without prejudice to any other rights you may have), you have the right to do so with your local supervisory authority, although we hope that we can assist with any queries or concerns you may have about our use of your personal data.<br>Cryptobutt may periodically update this policy. We may notify you about significant changes in the way we treat personal information by placing a prominent notice on our website so please check back occasionally to ensure that you agree with the changes. If you do not, do not use the website or its services.</p>
					<p>&nbsp;</p>
					<p><strong>Questions</strong><br>Any questions about this Privacy Policy should be addressed to this address: <a href="mailto:legal@cryptobutt.com">legal@cryptobutt.com</a>.</p>
                </div>
            </div>
			
        </div>
    </div>
		
		
		
	</div>
</div>
<script>
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>