<?php
class Market_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	function getBanner()
	{
		$this->db->select("*");
		$this->db->from(BANNER);
		$this->db->where("banner_status","1");
		$this->db->order_by("banner_order","ASC");
		$query = $this->db->get();
		return $query->result();
	}
	
	
	public function coin_details($id)
	{
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","COMPLETED");
		$this->db->order_by("id","DESC");
		$this->db->limit('1');
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			$rowc = $query->row();
		}
		
		
		//$row->coin_name = $this->boost_model->getValue(COIN,"fullname","id='".$id."'");
		
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				//$row_columns[] = $row2;
				
				$row[$row2->name] = $this->boost_model->getValue(COIN_HISTORY,$currency,"coin_id='".$id."' AND column_id='".$row2->id."'");
				
				$row['coin_name'] = $this->boost_model->getValue(COIN,"fullname","id='".$id."'");
				$row['coin_image_name'] = $this->boost_model->getValue(COIN,"image","id='".$id."'");
				$row['rank'] = $this->boost_model->getValue(COIN,"sortorder","id='".$id."'");
				$row['total_coin'] = $this->boost_model->getValue(COIN,"totalcoinsupply","id='".$id."'");
				
				$row['curency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
				
				
			}
			$data[] = $row;
			return $data;
		}
		
		
		/*
		$sql = "SELECT * FROM ".COIN." order by sortorder asc limit ".$start.",".$limit."";
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result_array() as $row)
			{
				foreach($row_columns as $row_column)
				{
					$row[$row_column->name] = $this->boost_model->getValue(COIN_HISTORY,$currency,"cron_id='".$rowc->id."' AND coin_id='".$row['id']."' AND column_id='".$row_column->id."'");
				}
                $data[] = $row;
            }
            return $data;
        }*/
		
		
		
        return false;
    }
	
	
	public function  chart_data($id)
	{
		
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		
		/*
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","COMPLETED");
		$this->db->order_by("id","DESC");
		$query1 = $this->db->get();
		
		if($query1->num_rows()>0)
		{
			//$rowc = $query1->row();
		   foreach($query1->result() as $row)
		   {
			   
		   
	       }
			
			
		}*/
		        $data='';
			 
				$this->db->select('*');
				$this->db->from(COLUMN);
				$this->db->where("status","1");
				$query = $this->db->get();
				if($query->num_rows()>0)
				{
					foreach($query->result() as $row2)
					{
						//$row3[$row2->name] = $this->boost_model->getValue(COIN_HISTORY,$currency,"coin_id='".$id."' AND column_id='".$row2->id."'");
						
						$this->db->select('*');
						$this->db->from(COIN_HISTORY);
						$this->db->where("coin_id",$id);
						$this->db->where("column_id",$row2->id);
						$query1 = $this->db->get();
						if($query1->num_rows()>0)
				        {
							
							$data[$row2->name]=$query1->result();
							
						}
						
					}
					  
					  
					
					//$data[] = $row;
					return $data;
				}
		
		  
		
	}
	
}
?>