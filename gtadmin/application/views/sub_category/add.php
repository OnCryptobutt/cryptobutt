<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sub Category
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="add">
              <div class="box-body">
			  
			   <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Select Category</label>

                  <div class="col-sm-10">
                    <select name="sub_category_parent" class="form-control">
					    <option value="">--Select Category--</option>
						<?php echo $getCategoryList;?>
					</select>
					<span class="red"><?php echo form_error('sub_category_parent');?></span>
                  </div>
                </div>
			  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="sub_category_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo set_value('sub_category_name');?>">
					<span class="red"><?php echo form_error('sub_category_name');?></span>
                  </div>
                </div>
				
               <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="sub_category_content"><?php echo set_value('sub_category_content');?></textarea>
					<span class="red"><?php echo form_error('sub_category_content');?></span>
                  </div>
                </div>-->
				
				
			 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                   <input type="file" name="sub_category_photo" value=""/>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="sub_category_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo set_value('sub_category_order');?>">
					<span class="red"><?php echo form_error('sub_category_order');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Site Title</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="sub_category_site_title"><?php echo set_value('sub_category_site_title');?></textarea>
					<span class="red"><?php echo form_error('sub_category_site_title');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Meta Description</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="sub_category_meta_description"><?php echo set_value('sub_category_meta_description')?></textarea>
					<span class="red"><?php echo form_error('sub_category_meta_description');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Meta Keywords</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="sub_category_meta_keyword"><?php echo set_value('sub_category_meta_keyword');?></textarea>
					<span class="red"><?php echo form_error('sub_category_meta_keyword');?></span>
                  </div>
                </div>
				
				
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" checked='true' name="sub_category_status" value="1"> Enable<br/>
					<input type="radio" name="sub_category_status" value="0"> Disable
                  </div>
                </div>
                
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>sub_category/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  