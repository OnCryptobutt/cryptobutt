
<div class="">
       <div class="page-content">
        <div class="page-header">
			
        </div>
        
        <section class="no-padding-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 rooter">
						<p><a href="index.html">Home</a> / <span>Coin Name</span></p>
					</div>	
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="public-user-block block">
						  <div class="row d-flex align-items-center">                   
							<div class="col-lg-4 d-flex align-items-center">
							  <div class="order_list">Rank1</div>
							  <div class="avatar"> <img src="<?php echo base_url(); ?>img/btc.png" alt="..." class="img-fluid list_coin_img"></div>
							  <p class="name"><strong class="d-block">BitCoin (BTC)</strong><span class="d-block">1BTC</span></p>
							</div>
							<div class="col-lg-4 text-center">
							  <div class="contributions"><p class="coin_details"><span class="cnt_1">$7391.7</span><br/> <span class="ctn_2">2263.130 % Change (24 hr)</span></p></div>
							</div>
							<div class="col-lg-4">
							  <div class="details d-flex">
								<div class="item"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i> Add to Portfoilio</a></div>
							  </div>
							</div>
						  </div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="block margin-bottom-sm">
						  <table class="table">
							
							<tbody>
							  <tr>
								<th scope="row">Market Cap</th>
								<td>$ 123313381566</td>
								<td>16682637 BTC</td>
							  </tr>
							  <tr>
								<th scope="row">Volume (24 h)</th>
								<td>$ 116168.152986</td>
								<td>15.716 BTC</td>
							  </tr>
							  <tr>
								<th scope="row">Circulating Supply</th>
								<td>16682637 BTC</td>
								<td></td>
							  </tr>
							  <tr>
								<th scope="row">Max Supply</th>
								<td>21000000 BTC</td>
								<td></td>
							  </tr>
							</tbody>
						  </table>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="container coin_tab">
				<div class="row">	
					<div class="col-lg-12">
					  <ul class="nav nav-tabs">
						<li><a class="active" data-toggle="tab" href="#home">Chart</a></li>
						<li><a data-toggle="tab" href="#menu1">Market</a></li>
						<li><a data-toggle="tab" href="#menu2">Historical data</a></li>
					  </ul>

					  <div class="tab-content">
						<div id="home" class="tab-pane in active">
						  <h3>Bitcoin Chart</h3>
						  
						  
						  <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
						   <div id="container1" > </div>
						   
						   <div align="center" class="flash2"></div>
						  
						  
						</div>
						
						
						
						
						
						<?php
                            if($this->session->userdata('user_id'))
							{
								$currency3 = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
								
								 $sym=$this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency3."'");
							}
							else
							{
								$currency3 = "";
							}
							
							if($currency3=="")
							{
							  $currency3 = "USD";
							  $sym="$";
							}
					?>
						
						
						
						<div id="menu1" class="tab-pane fade">
						  <h3>Market</h3>
						  
						  <!--<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
						  
						  
						  <div class="pull-right col-xs-5 col-sm-3 col-md-3 col-lg-2 alg sect dsk_align mar-top">
									<div class="btn-group">
										<!--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">$ USD <span class=caret"></span></button>-->
												<div align="center" class="flash3"></div>
									
										<button type="button" class="btn btn-default btn-md dropdown-toggle" data-toggle="dropdown">
										   <span class="flagstrap-selected-g6ZELali" id="currency_val"><?php echo $sym; ?> <?php echo $currency3; ?></span> <span class="caret"></span>
									    </button>
										
										<ul class="dropdown-menu" role="menu">
											
									<?php if(is_array($this->settings->currency_list)) {
									   foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										<!--<li><a href="#"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>-->
										
					                   <li><a onclick="currency_change('<?php echo $currency_list->currency_code; ?>','<?php echo $currency_list->symbol;?>')" href="javascript:void(0);"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>
										
									  
										
									   <?php } } ?>
										
										</ul>
									</div>
						  </div></br></br></br></br>
						  
						  
						  
						  <div class="row col-md-12 custyle table-responsive">
									<table class="table table-striped custab">
										<thead>
											<tr>
												<th>#</th>
												<th>Source</th>
												<th>Pair</th>
												<th>Volume (24h)</th>
												<th>Price</th>
												<th>Volume (%)</th>
												<th >Updated</th>
											</tr>
										</thead>
										
										 <tbody id="pageData2">
				
                                         </tbody>
										
										
										
									</table>
									
						            <!--<div align="center" class="flash"></div>-->
									
						           <!--<span id="market_count"><?php if(isset($count)) { echo $count; }?></span>-->
								   <input type="hidden" id="market_count" value="<?php echo $count; ?>">
								   <input type="hidden" id="row_nos" value="0">
								   <input type="hidden" id="page_id" value="0">
								   <input type="hidden" id="currency_value" value="">
                          <!--- for pagination -->
						
								</div>
						  
						  
						  
						</div>
						
						
						<div id="menu2" class="tab-pane fade">
						  <h3>Historical data</h3>
						  
						       <div class="pull-right col-xs-7 col-sm-3 col-md-3 col-lg-3 alg sect dsk_align mar-top2 desktop" style="background-color: #d4e6e9;">
								<div  class="flash2"></div>
									<div class="btn-group pull-right" id="reportrange" >
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="dateshow"><span></span>
											<div class="caret"></div>
										</button>
									</div>
								</div><br><br><br><br>
						  
						  
						      <div class="row col-md-12 custyle table-responsive">
									<table class="table table-striped custab">
										<thead>
											<tr>
												<th>Date</th>
												<th>Open</th>
												<th>High</th>
												<th>Low</th>
												<th>Close</th>
												<th>Volume</th>
												<th>Market Cap</th>
											</tr>
										</thead>
										
										<tbody id="pageData">
                                        </tbody>
										
									</table>
									<div align="center" class="flash1"></div>
							 </div>
							 
						</div>
						
						
						
					  </div>
					</div>
				</div>		
			</div>
			
            <div class="container"><div class="row">
				<div class="col-lg-12">
					<div class="coin_badge" data-example-id="">
					
					    <?php  if(isset($coin->website) AND $coin->website!='') {?>
						<span class="badge badge-primary"><a href="#"><?php echo $coin->website; ?></a></span>
						
						
						<?php } if(isset($coin->website2) AND $coin->website2!='') {?>
						<span class="badge badge-primary"><a href="#"><?php echo $coin->website2; ?></a></span>
						
						<?php } if(isset($coin->explore) AND $coin->explore!='') {?>
						<span class="badge badge-success"><a href="#"><?php echo $coin->explore; ?></a></span>
						
						<?php } if(isset($coin->explore2) AND $coin->explore2!='') {?>
						<span class="badge badge-success"><a href="#"><?php echo $coin->explore2; ?></a></span>
						
						<?php } if(isset($coin->message_board1) AND $coin->message_board1!='') {?>
						<span class="badge badge-info"><a href="#"><?php echo $coin->message_board1; ?></a></span>
						
						
						<?php } if(isset($coin->message_board2) AND $coin->message_board2!='') {?>
						<span class="badge badge-info"><a href="#"><?php echo $coin->message_board2; ?></a></span>
						
						<?php } if(isset($coin->Mineable) AND $coin->Mineable!='' AND $coin->Mineable=='1') {?>
						<span class="badge badge-warning"><?php if($coin->Mineable=='1'){ echo "Minneable"; } ?></span>
						
						<?php } if(isset($coin->currency) AND $coin->currency!=''  AND $coin->currency=='1') {?>
						<span class="badge badge-danger"><?php if($coin->currency=='1'){ echo "Coin"; } ?></span>
						<?php } ?>
						
						
					</div>
				</div>
			</div></div>

			
          </div>
        </section>
  


  