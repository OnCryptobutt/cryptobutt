<?php
class Coin_page_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	public function coin_details($id)
	{
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "";
			}
			
			if($currency=="")
			{
			  $currency = "USD";
			}
		}
		
		$this->db->select('*');
		$this->db->from(CRON_NEW);
		$this->db->where("status","COMPLETED");
		$this->db->order_by("id","DESC");
		$this->db->limit('1');
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			$rowc = $query->row();
			
			
		}
		
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				//$row_columns[] = $row2;
				
				$btc_id=$this->boost_model->getValue(COIN,"id","name='BTC'");
				
				$row['btc_price'] = $this->boost_model->getValue("ci_coin_history_temp_".$currency,"price","coin_id='".$btc_id."' order by id desc ");
				
				
				$row[$row2->name] = $this->boost_model->getValue("ci_coin_history_temp_".$currency,$row2->name,"coin_id='".$id."' order by id desc ");
					$row['sortorder'] = $this->boost_model->getValue(COIN,"sortorder","id='".$id."'");
				
				
				$row['coin_name'] = $this->boost_model->getValue(COIN,"name","id='".$id."'");
				$row['coinname'] = $this->boost_model->getValue(COIN,"coinname","id='".$id."'");
				$row['coin_image_name'] = $this->boost_model->getValue(COIN,"image","id='".$id."'");
				$row['rank'] = $this->boost_model->getValue(COIN,"sortorder","id='".$id."'");
				$row['total_coin'] = $this->boost_model->getValue(COIN,"totalcoinsupply","id='".$id."'");
				
				$row['twitter_url'] = $this->boost_model->getValue(COIN,"twitter_url","id='".$id."'");
				
				
				$row['curency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
				
				
			}
			$data[] = $row;
			//print_r($data);
			return $data;
		}
		
	
        return false;
    }
    public function linksInsert($data)
	{
		$this->db->insert('ci_keypeoplesociallink',$data);
		return $this->db->insert_id();
	}
    public function getLink_typeList()
	{
		$query = $this->db->select('*')->from(LINK_TYPE)->where("link_type_status","1")->order_by("link_type_name","ASC")->get();
	
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;	
		
		}
	}
	 public function insertKeypeople($data)
    {
    	$this->db->insert('ci_addkeypeople', $data);
    	return $this->db->insert_id();
    }
     public function matchingPeople($fullname)
	{


    $sql = "SELECT * FROM ci_addkeypeople WHERE keypeople_fullname LIKE '".$fullname."%' GROUP BY keypeople_fullname";
	$query =$this->db->query($sql);
     	if($query->num_rows()>0)
		{
			$row = $query->result();
				return $row;
		}
			
		

    }
    public function getKeypeople($id)
	{
	     $array = array('keypeople_coinid' => $id, 'verify_link'=> 'true');
		$this->db->select('*');
		$this->db->from('ci_addkeypeople');
		$this->db->where($array);
		$this->db->group_by('keypeople_roletype');
		$this->db->order_by("roletype_order","ASC");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->result();
			return $row;
			
		}
    }
    public function getKeypeoplelist($id)
	{
		$this->db->select('*');
		$this->db->from('ci_addkeypeople');
		$this->db->where("keypeople_id",$id);
		$this->db->order_by("keypeople_id","DESC");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->result();
			return $row;
			
		}
    }
    
    public function getcoin($name)
	{
	   

         $sql = "SELECT ci_addkeypeople.keypeople_coinid,ci_coin.name,ci_coin.image FROM ci_addkeypeople INNER JOIN ci_coin ON ci_addkeypeople.keypeople_coinid=ci_coin.name WHERE keypeople_fullname='$name'";
         $query =$this->db->query($sql);
     	if($query->num_rows()>0)
		{
			$row = $query->result();
				return $row;
		}
	  
    }
    
    public function getKeypeoplegroup($roletype,$coinid)
	{
	    
	    $array = array('keypeople_roletype' => $roletype, 'keypeople_coinid' =>$coinid, 'verify_link'=> 'true');
		$this->db->select('*');
		$this->db->from('ci_addkeypeople');
		$this->db->where($array);
		//$this->db->group_by('keypeople_roletype');
		$this->db->order_by("keypeople_id","DESC");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->result();
			return $row;
			
		}
    }
     public function getKeypeoplegrouplink($linkid)
	{
	     $array = array('social_people_id' => $linkid, 'social_verify_link' => 'true');
		$this->db->select('*');
		$this->db->from('ci_keypeoplesociallink');
		$this->db->where($array);
		$this->db->order_by("social_id","DESC");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->result();
			return $row;
			
		}
	    
	
    }
     public function getKeypeoplegroupverifylink($linkid)
	{
	     $array = array('social_people_id' => $linkid,'social_verify_link' => 'true');
		$this->db->select('*');
		$this->db->from('ci_keypeoplesociallink');
		$this->db->where($array);
		$this->db->order_by("social_id","DESC");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->result();
			return $row;
			
		}
	    
	
    }
	

	public function  market_data2($id,$currency,$limit,$start)
	{
		
		if($currency!='')
		{
		 $currency = $currency;
		}
		else
		{
			$currency = "USD";
		}
		
		
		$cur_value=$this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		$coins_name=$this->boost_model->getValue(COIN,"name","id='".$id."'");
		
		       $this->db->select('*');
				$this->db->from(COLUMN);
				$this->db->where("status","1");
				$query1 = $this->db->get();
		
		$to=date('Y-m-d');

	  $sql = "SELECT * FROM ci_coin_history_".$currency." WHERE coin_id=".$id." GROUP BY CAST(created_time AS DATE) ORDER BY id DESC LIMIT ".$start.", ".$limit."";
	    $query =$this->db2->query($sql);

		if ($query->num_rows() > 0)
		{ $i=1;
            foreach ($query->result() as $row)
			{

				
				$row->currency_sym = $cur_value;
						 
			 $row->coins=$coins_name;
				
				$data[]=$row;
				
            }
			
         return $data;
			
        }
        return false;
				
				
	}
	
	
	public function coin_below_details($id)
	{
		
		        $this->db->select("*");
				$this->db->from(COIN);
				$this->db->where("id",$id);
				$this->db->where("status",'1');
				$query = $this->db->get();
				
				if($query->num_rows()>0)
				{
				  return $query->row();
				}
				else
				{
					return false;
				}
			
	}
	
	public function market_record_count()
	{
		
		$sql = "SELECT * FROM ".CURRENCY." where status='1'";
        $query = $this->db->query($sql);
		
		return $query->num_rows();
		
	}
	public function market_record_count2()
	{
		
		$sql = "SELECT * FROM ".CRON_NEW;
        $query = $this->db->query($sql);
		
		return $query->num_rows();
		
	}
	
	public function getHistoricList($limit,$start,$id,$from_date,$currency)
	{   
	    
		if($currency!='')
		{
		 $currency = $currency;
		}
		else
		{
			$currency = "USD";
		}
		
		$to=date('Y-m-d');
	
		
		
		$currency_sym = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
				

		 $sql = "SELECT * FROM ci_coin_history_".$currency." WHERE coin_id=".$id." GROUP BY CAST(created_time AS DATE) ORDER BY id DESC LIMIT ".$start.", ".$limit."";
	    $query =$this->db2->query($sql);
		
		
		
		if ($query->num_rows() > 0)
		{ $i=1;
            foreach ($query->result() as $row)
			{
				
				
				 $row->currency_sym = $currency_sym;
				
				$data[]=$row;
				
            }
			
         return $data;
			
        }
        return false;
		
    }
	
	
	
	public function getHistoricList2($id,$from_date,$to,$currency)
	{   
	  
		if($currency!='')
		{
		 $currency = $currency;
		}
		else
		{
			$currency = "USD";
		}
		
		
		if($to!='')
		{
			$to=$to;
		}
		else
		{
			$to=date('Y-m-d H:i:s');
		}
		
	
		 $currency_sym = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
				
		
		$this->db2->select("*");
		$this->db2->from("ci_coin_history_".$currency);
		if($from_date!='')
		{
			$this->db2->where('created_time  >=',$from_date);
			$this->db2->where('created_time  <=',$to);
		}
		$this->db2->where("coin_id",$id);
		$this->db2->order_by("id","DESC");
		
		$query = $this->db2->get();
		
    	if ($query->num_rows() > 0)
		{ $i=1;
            foreach ($query->result() as $row)
			{
			
				
				  $row->currency_sym =  $currency_sym;
				
				$data[]=$row;
				
            }
			
			
         return $data;
			
			
        }
        return false;
    }
	
	
	public function historic_record_count()
	{
		$sql = "SELECT * FROM ".CRON_NEW;
        $query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	public function  chart_data2()
	{
		
		
	    if($this->input->get('currency'))	
		{
		  $currency = $this->input->get('currency');

		}
		else
		{
			$currency = "USD";
			
		}
		
		
		$id='';
		if($this->input->get('cid')!='')
		{
			$id=$this->input->get('cid');
			
		}	
		
		$coumn='';
		if($this->input->get('coumn'))
		{
		   $coumn = $this->input->get('coumn');
		   $coumn1=explode('.',$coumn);
		
		
		   $data='';
			 
						$this->db2->select('*');
						$this->db2->from("ci_coin_history_".$currency);
						$this->db2->where("coin_id",$id);
						
						$query1 = $this->db2->get();
						if($query1->num_rows()>0)
				        {
							
							//$data[$coumn]=$query1->result();
							$i=1;
							foreach($query1->result() as $list)
							{
								if($i!=$query1->num_rows()){
								$coma=",";
								}
								else{ $coma=''; }
								
								$data[]='['.(strtotime($list->api_updated_date_time)*1000).','.$list->$coumn1[0].']';
								
								$i++;
							}
							
						}
						
						return $data;
		}
					
		
	}
	public function  chart_data3()
	{
	    if($this->input->get('currency'))	
		{
		  $currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";	
		}
		$id='';
		if($this->input->get('cid')!='')
		{
			$id=$this->input->get('cid');	
		}	
		$coumn='';
		if($this->input->get('coumn'))
		{
            $coumn = $this->input->get('coumn');
            $coumn1=explode('.',$coumn);
            $data='';	 
    		$this->db2->select('*');
    		$this->db2->from("ci_coin_history_".$currency);
    		$this->db2->where("coin_id",$id);
    		
    		$query1 = $this->db2->get();
    		if($query1->num_rows()>0)
            {	
    		$json_response = array();  
    			foreach($query1->result() as $list)
    			{
    			$var = date('Y-m-d', strtotime($list->api_updated_date_time));
    			$row_array[$var] = $list->$coumn1[0]; 	
    			}
    			array_push($json_response,$row_array); 
    			echo json_encode(array('bpi' => $json_response[0]));
    		}
    		return $json_response;
		}	
	}
	
	public function getLinksList($cid)
	{
		
		$query = $this->db->select('*')->from(LINKS)->join(LINK_TYPE,LINK_TYPE.".link_type_id=".LINKS.".links_type")->where(LINKS.".links_coin_id='".$cid."'")->order_by(LINK_TYPE.".link_type_id","ASC")->get();
		
		if($query->num_rows()>0)
		{
			$type_array = array();
			foreach($query->result() as $row)
			{
				$type_array[] = $row->link_type_id;
				if(in_array($row->link_type_id,$type_array))
				{
					
					$counts = array_count_values($type_array);
					if($counts[$row->link_type_id]>1)
					{
						$row->link_type_name = $row->link_type_name." ".$counts[$row->link_type_id];
					}
				}
				$row1[] = $row;
				
			}
			
			return $row1;
		}
	}
	
	
	public function getRoleList()
	{
		$query = $this->db->select('*')->from(ROLE)->where("role_status","1")->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			return $row1;
		}
	}

	
}
?>