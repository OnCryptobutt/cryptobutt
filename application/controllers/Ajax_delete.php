<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_delete extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
    }
	
	function delete_portfolio_details()
	{
		$id = (int) $this->input->post('pdid');
		if($id)
		{
			$this->db->where("portfolio_details_id",$id)->delete(PORTFOLIO_DETAILS);
			echo "success";
		}
		exit;
	}
	
	function delete_portfolio()
	{
		$id = (int) $this->input->post('pid');
		if($id)
		{
			$query = $this->db->select('*')->from(PORTFOLIO_DETAILS)->where("portfolio_details_portfolio_id",$id)->get();
			if($query->num_rows()>0)
			{
				foreach($query->result() as $row)
				{
					$this->db->where("portfolio_details_id",$row->portfolio_details_id)->delete(PORTFOLIO_DETAILS);
				}
			}
			$this->db->where("portfolio_id",$id)->delete(PORTFOLIO);
			echo "success";
		}
		exit;
	}
}


