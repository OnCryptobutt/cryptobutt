<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Cryp to currency</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/mystyle.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway" rel="stylesheet">
    <style type="text/css">
.glyphicon { margin-right:5px;}
.section-box h2 { margin-top:0px;}
.section-box h2 a { font-size:15px; }
.glyphicon-heart { color:#e74c3c;}
.glyphicon-comment { color:#27ae60;}
.separator { padding-right:5px;padding-left:5px; }
.section-box hr {margin-top: 0;margin-bottom: 5px;border: 0;border-top: 1px solid rgb(199, 199, 199);}
    </style>
	<style type="text/css">
ul.tsc_pagination { padding:0px; height:100%; overflow:hidden; font:12px \'Tahoma\'; list-style-type:none; }
ul.tsc_pagination li { float:left; margin:0px; padding:0px; margin-left:5px; }
ul.tsc_pagination li:first-child { margin-left:0px; }
ul.tsc_pagination li a { color:black; display:block; text-decoration:none; padding:7px 10px 7px 10px; }
ul.tsc_pagination li a img { border:none; }
ul.tsc_paginationC li a { color:#707070; background:#FFFFFF; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; border:solid 1px #DCDCDC; padding:6px 9px 6px 9px; }
ul.tsc_paginationC li { padding-bottom:1px; }
ul.tsc_paginationC li a:hover,
ul.tsc_paginationC li a.current { color:#FFFFFF; box-shadow:0px 1px #EDEDED; -moz-box-shadow:0px 1px #EDEDED; -webkit-box-shadow:0px 1px #EDEDED; }
ul.tsc_paginationC01 li a:hover,
ul.tsc_paginationC01 li a.current { color:#893A00; text-shadow:0px 1px #FFEF42; border-color:#FFA200; background:#FFC800; background:-moz-linear-gradient(top, #FFFFFF 1px, #FFEA01 1px, #FFC800); background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #FFFFFF), color-stop(0.02, #FFEA01), color-stop(1, #FFC800)); }
ul.tsc_paginationC li a.In-active {
   pointer-events: none;
   cursor: default;
}
</style> 
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	
	
</head>
<body>
<div class="container">
    <div class="row">
		<div class="col-md-12 col-sm-12">
            <div class="row">
				<div class="pull-right2">
					<div class="head-t">
						<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 desktop">
							<a href="<?php echo base_url();?>"><img class="img-responsive" src="<?php echo base_url();?>images/logo.png" alt="processing"></a>
						</div>
						<div class="col-xs-4 col-sm-3 col-md-3 col-lg-4 desktop">
						</div>
						<div class="col-xs-4 col-sm-3 col-md-3 col-lg-6 mobile">
							<a href="<?php echo base_url();?>"><img class="img-responsive" src="<?php echo base_url();?>images/logo.png" alt="processing"></a>
						</div>
						<div class="header_alg">
							<div class="col-xs-5 col-sm-3 col-md-3 col-lg-4 alg alg2 desktop"><?php if($this->session->userdata('user_id')){?><span id="bal_al" class="bal_al">Loading...</span><a href="<?php echo base_url('Logout');?>"><span class="label label-danger">Logout</span></a><?php }else{?><a href="<?php echo base_url('hauth/login/Google');?>"><span class="label label-success">Login</span></a><?php } ?></div>
							
							<!-- mobile view start -->
							
							<div class="col-xs-5 col-sm-3 col-md-3 col-lg-4 alg alg2 mobile nowrap"><?php if($this->session->userdata('user_id')){?><span id="bal_al_mobile" class="bal_al">Loading...</span><a href="<?php echo base_url('Logout');?>"><span class="log-alg"><span class="label label-danger">Logout</span></span></a><?php }else{?><a href="<?php echo base_url('hauth/login/Google');?>"><span class="label label-success">Login</span></a><?php } ?></div>
							
							<!-- mobile view end -->
							
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 alg sect dsk_align"><span class="dis">Currency</span>
								<div class="btn-group">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span id="change_currency"><?php echo $this->settings->currency_symbol;?> <?php echo $this->settings->currency;?> </span><span class="caret"></span>
									</button>
									<?php if(isset($basename)) { $basename= $basename; } else { $basename=''; } ?>
									<?php if($basename=='home') { ?>
									<ul class="dropdown-menu" role="menu">
									<?php if(is_array($this->settings->currency_list))
									 { 
								      if($this->input->get('per_page'))
									  {
										  $per_page = $this->input->get('per_page');
									  }
									  else
									  {
										  $per_page = '10';
									  }
								       foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										<li><a onclick="tops('currency','<?php echo $currency_list->currency_code;?>');change_currency('<?php echo $currency_list->symbol;?>','<?php echo $currency_list->currency_code;?>');" href="javascript:void(0);"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>
									   <?php } } ?>
										
									</ul>
									
									<?php }
									elseif($basename=='portfolio') { ?>
									<ul class="dropdown-menu" role="menu">
									<?php if(is_array($this->settings->currency_list))
									 { 
								      if($this->input->get('per_page'))
									  {
										  $per_page = $this->input->get('per_page');
									  }
									  else
									  {
										  $per_page = '10';
									  }
								       foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										<li><a onclick="common('currency','<?php echo $currency_list->currency_code;?>');change_currency('<?php echo $currency_list->symbol;?>','<?php echo $currency_list->currency_code;?>');" href="javascript:void(0);"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>
									   <?php } } ?>
										
									</ul>
									
									<?php } 									else { ?>
									
									
									<ul class="dropdown-menu" role="menu">
									<?php if(is_array($this->settings->currency_list))
									 { 
								      if(isset($cname))
									  {
										  $cname = $cname;
									  }
									  else
									  {
										  $cname = '';
									  }
									  
									  if($basename=="coin_page")
									  {
										  $page_name = "coin_page";
									  }
									  elseif($basename=="market")
									  {
										  $page_name = "market";
									  }
									  else{
										  $page_name = "home";
									  }
								       foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										<li><a href="<?php echo base_url($cname.'?currency='.$currency_list->currency_code);?>"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>
									   <?php } } ?>
										
									</ul>
									
									<?php } ?>
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
		</div>
		<div class="col-md-12 col-sm-12 main-menu wid desktop">
            <div class="row">
				<div class="pull-right2">
					<div class="head-t">
					    <form method="get">
						<div class="col-xs-4 col-sm-3 col-md-3 col-lg-9">
							<div class="input-group">
								<div class="input-group-btn">
									<!--<button  class="btn btn-default2 adjust" <?php if($basename=="home"){?>type="button" onclick="tops('search','');"<?php } else { ?>type="submit"<?php } ?>><i class="glyphicon glyphicon-search"></i></button>-->
									
									<button  class="btn btn-default2 adjust" type="button"><i class="glyphicon glyphicon-search"></i></button>
								</div>
									<!--<input onkeyup="tops('search','');" id="coin_name" name="coin_name" type="text" class="form-control text-box1" placeholder="Search Currencies" value="<?php echo $this->input->get('coin_name');?>">-->
									
									<input id="coin_name" name="coin_name" type="text" class="form-control text-box1" placeholder="Search Currencies" value="<?php echo $this->input->get('coin_name');?>">
									<div id="suggesstion-box"></div>
							</div>
						</div>
						</form>
						<div class="header_alg">
							<?php 
							$current_page_path = 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
							?>
							<div class="<?php if($current_page_path==base_url()) { ?> active <?php } ?> col-xs-2 col-sm-3 col-md-3 col-lg-1 alg home_s l_r_2"><a class="<?php if($current_page_path==base_url()) { ?> active_col <?php } ?>" href="<?php echo base_url();?>"> <i class="fa fa-home ho_ic_size_new" aria-hidden="true"></i> <span class="menu_header">Home</span></a></div>
							<div class="<?php if($current_page_path==base_url('portfolio')) { ?> active <?php } ?> col-xs-3 col-sm-3 col-md-3 col-lg-1 alg m_port wid_13 l_r_2"><a class="<?php if($current_page_path==base_url('portfolio')) { ?> active_col <?php } ?>" href="<?php echo base_url('portfolio'); ?>"> <i class="<?php if($current_page_path==base_url('portfolio')) { ?>fa fa-heart icon_size_new <?php } else { ?> fa fa-heart-o icon_size_new <?php } ?>" aria-hidden="true"></i> <span class="menu_header">My Portfolio</span></a></div>
							<!--<div class="<?php if($current_page_path==base_url('feeds')) { ?> active <?php } ?> col-xs-3 col-sm-3 col-md-3 col-lg-1 alg m_port l_r_2"><a class="<?php if($current_page_path==base_url('feeds')) { ?> active_col <?php } ?>" href="<?php echo base_url('feeds');?>"><i class="fa fa-bars ho_ic_size_new" aria-hidden="true"></i> <span class="menu_header">Feeds</span></a></div>-->
						</div>
					</div>
				</div>
            </div>
		</div>
		
		<!-- Mobile View Start -->
		
		<div class="col-md-12 col-sm-12 main-menu wid mobile sear_alg new_alg">
            <div class="row">
				<div class="pull-right2">
					<div class="head-t">
						<form method="get">
							<div class="col-xs-7 col-sm-3 col-md-3 col-lg-8">
								<div class="input-group">
									<div class="input-group-btn">
										<button  class="btn btn-default2 adjust" type="button"><i class="glyphicon glyphicon-search"></i></button>
									</div>
									<input id="coin_name_mobile" name="coin_name" type="text" class="form-control text-box1 mobie" placeholder="Search Currencies" value="<?php echo $this->input->get('coin_name');?>">
								</div>
							</div>
						
							<div class="header_alg">
								<div class="<?php if($current_page_path==base_url()) { ?> active <?php } ?> col-xs-2 col-sm-3 col-md-3 col-lg-2 alg text-center home_s"><a class="<?php if($current_page_path==base_url()) { ?> active_col <?php } ?>" href="<?php echo base_url();?>"> <i class="fa fa-home ho_ic_size" aria-hidden="true"></i><br> Home</a></div>
								<div class="<?php if($current_page_path==base_url('portfolio')) { ?> active <?php } ?> col-xs-3 col-sm-3 col-md-3 col-lg-2 alg m_port"> <a class="<?php if($current_page_path==base_url('portfolio')) { ?> active_col <?php } ?>" href="<?php echo base_url('portfolio'); ?>"> <i class="<?php if($current_page_path==base_url('portfolio')) { ?>fa fa-heart icon_size <?php } else { ?> fa fa-heart-o icon_size <?php } ?>" aria-hidden="true"></i><br> My Portfolio</a></div>	
							</div>
							
							<div class="col-xs-12">
								<div id="suggesstion-box_mobile"></div>
							</div>
						</form>
					</div>
				</div>
            </div>
		</div>
		
		<!-- Mobile View End -->
		
