<input type="hidden" id="hidden_search" value="<?php echo $this->input->get('search');?>"/>
<input type="hidden" id="hidden_per_page" value=""/>
<input type="hidden" id="hidden_currency" value="<?php echo $this->input->get('currency');?>"/>
<input type="hidden" id="hidden_sorting" value=""/>
<input type="hidden" id="row_no" value="1">
<input type="hidden" id="pageids" value="0">
<input type="hidden" id="coins_count" value="<?php if(isset($total_list)) { echo $total_list; }?>">

<input type="hidden" id="sorting_price" value="DESC"/>
<input type="hidden" id="sorting_market" value="DESC"/>
<input type="hidden" id="sorting_change" value="24H"/>

<section class="purple">
	<?php if($this->session->userdata('user_id')){ ?>
		<div class="container" >
			<div class="page-header text-center">
				<img class="img-rounded" src="<?php echo $query_user_row->photoURL;?>" alt="..." >
			</div>
			<div class="row grid-divider txt_wht">
				<div class="col-sm-6 wid_50">
					<div class="col-padding">
						<h3 class="txt_alg_rit"><span id="coins_count1"></span></h3>
						<p class="txt_alg_rit">Favourite</p>
					</div>
				</div>
				<div class="col-sm-6 border_left wid_50">
					<div class="col-padding">
						<h3>+</h3>
						<p> Favourite</p>
					</div>
				</div>
			<!--	<div class="col-sm-12 text-center">
					<p class="log_sty"><a href="<?php echo base_url('Logout');?>" style="color:white;">Logout</a></p>
				</div>-->
			</div>
		</div>
	<?php } ?>					
</section>

 <section  class="bg-light sticky-top purple" id="navbar">

			<section class="container desktop-marg">

			  <nav class="navbar navbar-expand-lg navbar-light">

				<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">

					<a href="javascript:;">
                        <div class="col-1 col-md-1 tog"></div>
						<div class="tog col-3 col-md-3">

							<!--<p class="collapse_nav_price">Crypto Currency</p>-->
							<p class="collapse_nav_price">Crypto assets</p>

							<p class="collapse_nav_htl">All</p>

						</div>

						<div class="tog col-4 col-md-2">

							<p class="collapse_nav_price">Price</p>

							<p id="sorting_price_html" class="collapse_nav_htl">---</p>

						</div>

						<div class="tog coin_cap col-3 col-md-3">

							<p class="collapse_nav_price">Market Cap</p>

							<p id="sorting_market_html" class="collapse_nav_htl">High to Low</p>

						</div>

						<div class="tog col-2 col-md-2">

							<!--<p class="collapse_nav_price">1 Hour,1 Day,1 Week,1 Month</p>-->
							<p class="collapse_nav_price">Change</p>

							<p id="sorting_change_html" class="collapse_nav_htl">1 Day</p>

						</div>

					</a>

				</div>

			  </nav>

			  <div class="collapse" id="collapse1">

				  <div class="card card-block">

					<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        <div class="col-1 col-md-1 tog"></div>
						<div class="tog col-2 col-md-3">All</div>

						<div class="tog col-4 col-md-2">

							<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_price_html','High to Low','sorting_price','DESC');changePagination();">High to Low</a></p>

							 
							<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_price_html','Low to High','sorting_price','ASC');changePagination();">Low to High</a></p>
						
						</div>

						<div class="tog coin_cap col-4 col-md-3">

							<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_market_html','High to Low','sorting_market','DESC');changePagination();">High to Low</a></p>

							 
							<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_market_html','Low to High','sorting_market','ASC');changePagination();">Low to High</a></p>
						

						</div>

						<div class="tog col-4 col-md-2">
						
					

                        <p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 hour','sorting_change','1H');changePagination();">1 hour</a></p>
						
						<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Day','sorting_change','24H');changePagination();">1 Day</a></p>
						
						<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Week','sorting_change','1W');changePagination();">1 Week</a></p>
						
						<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Month','sorting_change','1M');changePagination();">1 Month</a></p>
					
                        <!--<p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','3 Months','sorting_change','3M');changePagination();">3 Months</a></p>
						 
                         <p><a href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','All Time','sorting_change','ALL');changePagination();">All Time</a></p>-->
						

						</div>

					</div>

				  </div>

				</div>
				
				

			</section>

		</section>
		
		<!--<div id="popupsearch1" class="text-right heade_alg" style="display:none;">
			<form id="searchForm" action="#">
			  <div class="form-group">
			   <input type="search" id="coin_name" name="search" placeholder="Search crypto asset" style="width:100%;padding: 3px 25px;border-bottom: 2px solid #753ac8;border-left: 2px solid #eeeeee;border-top: 2px solid #eeeeee;height: 50px;">
				<span class="input-group-btn">
					<button class="btn btn-danger" type="button" style="padding: 0;margin-top: -39px;margin-left: 4px;height: 0px;">
						<i class="fa fa-search"></i>
					</button>
				</span>
				<span class="input-group-btn" style="float: right;">
					<button class="btn btn-danger" type="button" style="padding: 0;margin-top: -39px;margin-left: -23px;height: 0px;">
						<div onclick="searchToggle();" class="close-btn"><i style="color: black;" class="fa fa-window-close" aria-hidden="true"></i></div>
					</button>
				</span>
				<div id="suggesstion-box"></div>
		
			  </div>
			</form>	
		</div>-->
		
		<!--<div id="popupsearch1" class="search-panel" style="display:none;">
          <div class="search-inner d-flex align-items-center justify-content-center new_alg_set">
            
            <form id="searchForm" action="#">
              <div class="form-group">
               <input type="search" id="coin_name" name="search" placeholder="Search crypto asset" style="width:100%;padding: 3px 25px;border-bottom: 2px solid #753ac8;border-left: 2px solid #eeeeee;border-top: 2px solid #eeeeee;">
				<span class="input-group-btn">
					<button class="btn btn-danger" type="button" style="padding: 0;margin-top: -31px;margin-left: 4px;height: 0px;">
						<i class="fa fa-search"></i>
					</button>
				</span>
				<span class="input-group-btn" style="float: right;">
					<button class="btn btn-danger" type="button" style="padding: 0;margin-top: -31px;margin-left: -23px;height: 0px;">
						<div onclick="searchToggle();" class="close-btn"><i style="color: black;" class="fa fa-window-close" aria-hidden="true"></i></div>
					</button>
				</span>
				<div id="suggesstion-box"></div>
		
              </div>
            </form>			
          </div>
        </div>-->

		<section id="pageData" style="min-height:400px">

			

		</section>	

		
		
<style>
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
    border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.dropdown-toggle::after {
    display: none;
}
.btn-danger.focus, .btn-danger:focus {
    box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.5);
}

.btn-danger:not([disabled]):not(.disabled):active, .show>.btn-danger.dropdown-toggle {
    color: #753ac8;
    background-color: #ffffff;
    border-color: #ffffff;
    box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.5);
}

.btn-danger {
    color: #753ac8;
    background-color: #ffffff;
    border-color: #ffffff;
}
.btn-danger:hover {
    color: #753ac8;
    background-color: #ffffff;
    border-color: #ffffff;
}	
.row_background
{
	
background-color : #FFCDD2 !important;

}

.row_background3
{
	
background-color : #ccdefc !important;

}
.row_background2
{
	
background-color : #B9F6CA !important;

}
.flash
{
	margin-left: 45%;
}

.green
{
	color:green !important;
}

.red
{
	color:red !important;
}

.black
{
	color:black !important;
	font-weight:bold !important;
	padding:5px;
}

img.img-rounded {
    border-radius: 50%;
    margin-top: 15px;
	margin-bottom: 15px;
	width: 80px;
}

.border_left {
	border-left: 2px solid white;
}

.txt_wht {
	color: white;
}

.txt_alg_rit {
	text-align: right;
}

.log_sty {
	margin-top: 10px;
    margin-bottom: 0px;
	color: white !important;
}

.wid_50 {
	width: 50% !important;
}
</style>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alert!</h4>
      </div>
      <div class="modal-body">
        <p>Please login to continue. <a href="<?php echo base_url("hauth/Login/Google");?>">Click Here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
