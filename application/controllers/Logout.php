<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		
    }
	
	public function index()
	{
		$data = "";
		$this->session->unset_userdata('user_id');
		redirect(base_url('signin/login'));
		
	}
}
