<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Market extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('market_model');
    }
	
	public function index()
	{
		$data = "";
		$data['banners'] = $this->market_model->getBanner();
		$data['basename'] = "market";
		
		//$cid=$this->uri->segment(2);
		
		if($this->input->get('cid')!='')
		{
			$cid=$this->input->get('cid');
			$data['cid']=$cid;
		}	
		else
		{
			redirect(base_url());
		}
		
		
		
		
		$data['details']=$this->market_model->coin_details($cid);
		
		
		$data['chart_lists']=$this->market_model->chart_data($cid);
		
		
		
		//print_r($data['chart_lists']['mktcap']);exit;
		
		$this->load->view('template/header',$data);
		//$this->load->view('template/slider',$data);
		$this->load->view('market_page',$data);
		//$this->load->view('template/footer',$data);
	}
}
