<div id="pageData3"></div> 
<input type="hidden" id="hidden_currency" value=""/> 
 <div class="row alg-con">
		<div class="col-md-9 pad-right">
            <div class="well well-sm">
			
                <div class="row desktop">
					<div class="col-xs-3 col-md-2 mob_res">
                        <img src="<?php echo COIN_URL.$details[0]['coin_image_name']; ?>" alt="image" class="img-rounded img-responsive img_width"/>
						<!--<div class="text-col dis">BTC</div>-->
                    </div>
					<div class="col-xs-5 col-md-7 pad_top_25">
						<div class="text-col3 detail"><a href="" ><?php echo $details[0]['coin_name']; ?></a></div>
						<div class="text-col2"><i class="material-icons float_left">school</i> <div class="ic_c_a3">Rank <?php echo $details[0]['rank']; ?></div></div>
						<div class="mobile-second"><span class="mobile-second1"><?php echo $details[0]['curency_symbol']; ?><?php echo $details[0]['price']; ?></span><span <?php if($details[0]['change24hour']>0) { ?> class="mobile-second3" <?php }else { ?> class="mobile-second2" <?php } ?> ><?php echo number_format($details[0]['change24hour'],3); ?> %</span><span class="mobile-second4">Change (24 hr)</span></div>
						<div class="text-col7 mg-bot"><?php echo round(($details[0]['price']/$details[0]['btc_price']),4); ?> BTC</div>
					</div>
					
					<?php

               $exist = $this->boost_model->getValue(PORTFOLIO,"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$cid."'");
				if($exist>0)
				{
					$heartcls = "";
				}
				else
				{
					$heartcls = "-o";
				}
					?>
					
					
					<div class="col-xs-4 col-md-3 pad_top_25">
                       <div class="text-col4 det" onclick="addportfolio('<?php echo $cid; ?>')" id="heart_<?php echo $cid; ?>"><i class="fa fa-heart<?php echo $heartcls;  ?> heart_cor" aria-hidden="true"></i>&nbsp;Add to portfolio</div>
                    </div>
					
					
					
				</div>
				
				<div class="row mobile">	
					<!-- Mobile View Start-->
					<div class="col-xs-3 col-md-2 mob_res img-g">
                        <img src="<?php echo COIN_URL.$details[0]['coin_image_name']; ?>" alt="image" class="img-rounded img-responsive img_width"/>
						<!--<div class="text-col dis">BTC</div>-->
                    </div>
					<div class="col-xs-5 col-md-7 set mag-let">
						<div class="text-col3 detail"><a href="#" target="_blank"><?php echo $details[0]['coin_name']; ?></a></div>
						<div class="text-col2"><i class="material-icons float_left">school</i> <div class="ic_c_a2">Rank <?php echo $details[0]['rank']; ?></div></div>
						<div class="mobile-second nowrap float_left"><span class="mobile-second1"><?php echo $details[0]['curency_symbol']; ?><?php echo $details[0]['price']; ?></span><span <?php if($details[0]['change24hour']>0) { ?> class="mobile-second3" <?php }else { ?> class="mobile-second2" <?php } ?> ><?php echo $details[0]['change24hour']; ?> %</span></div>
						<div class="text-col7 float_left"> <?php echo round(($details[0]['price']/$details[0]['btc_price']),4); ?>  BTC</div>
					</div>
					
					
					<div class="col-xs-2 col-md-3 set">
                        <div class="text-col4 det" onclick="addportfolio('<?php echo $cid; ?>')" id="heart_<?php echo $cid; ?>"><i class="fa fa-heart<?php echo $heartcls;  ?> heart_cor" aria-hidden="true"></i></div>
                    </div>
					
					
					<!-- Mobile View End-->
                </div>
				
				<div class="container top-bott_bor desktop">
					<div class="row">
						<div class="col-xs-3 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Market Cap</span></div>
						</div>
						<div class="col-xs-4 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['curency_symbol'].' '; ?><?php echo $details[0]['mktcap']; ?></span></div>
						</div>
						<div class="col-xs-5 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second6"><?php echo round(($details[0]['mktcap']/$details[0]['btc_price']),4); ?> BTC</span></div>
						</div>
						
					</div>
                </div>
				
				<div class="container top-bott_bor desktop">
					<div class="row">
						<div class="col-xs-3 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Volume (24 h)</span></div>
						</div>
						<div class="col-xs-4 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['curency_symbol'].' '; ?><?php echo $details[0]['volume24hour']; ?></span></div>
						</div>
						<div class="col-xs-5 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second6"><?php echo round(($details[0]['volume24hour']/$details[0]['btc_price']),4); ?> BTC</span></div>
						</div>
					</div>
				</div>
				
				<div class="container top-bott_bor desktop">
					<div class="row">
						<div class="col-xs-3 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Circulating Supply</span></div>
						</div>
						<div class="col-xs-4 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['supply']; ?>  BTC   </span></div>
						</div>
					</div>
				</div>
				
				<div class="container top-bott_bor desktop">
					<div class="row">
						<div class="col-xs-3 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Max Supply</span></div>
						</div>
						<div class="col-xs-4 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['total_coin']; ?>   BTC</span></div>
						</div>
					</div>
				</div>
				
				<!-- mobile view start --->
					<div class="container top-bott_bor mobile">
					<div class="row">
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Market Cap</span></div>
						</div>
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['curency_symbol'].' '; ?><?php echo $details[0]['mktcap']; ?></span></div>
							<div class="mobile-second"><span class="mobile-second6"><?php echo round(($details[0]['mktcap']/$details[0]['btc_price']),4); ?> BTC</span></div>
						</div>
						
					</div>
                </div>
				
				<div class="container top-bott_bor mobile">
					<div class="row">
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Volume (24 h)</span></div>
						</div>
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['curency_symbol'].' '; ?><?php echo $details[0]['volume24hour']; ?></span></div>
							<div class="mobile-second"><span class="mobile-second6"><?php echo round(($details[0]['volume24hour']/$details[0]['btc_price']),4); ?> BTC</span></div>
						</div>
					</div>
				</div>
				
				<div class="container top-bott_bor mobile">
					<div class="row">
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Circulating Supply</span></div>
						</div>
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['supply']; ?>  BTC</span></div>
						</div>
					</div>
				</div>
				
				<div class="container top-bott_bor mobile">
					<div class="row">
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second1">Max Supply</span></div>
						</div>
						<div class="col-xs-6 col-md-4 mob_res mob_res2">
							<div class="mobile-second"><span class="mobile-second5"><?php echo $details[0]['total_coin']; ?>   BTC</span></div>
						</div>
					</div>
				</div>
				<!-- mobile view end -->
				
            </div>
        </div>
		
		<div class="col-md-3 pad-left desktop">
			<div class="well well-sm min_hei_408">
				
				<div class="container">
					<div class="row">
						<?php  if(isset($coin->website) AND $coin->website!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE7FA;</i>&nbsp;<div class="ic_c_a"><?php echo $coin->website; ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->website2) AND $coin->website2!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE7FA;</i>&nbsp;<div class="ic_c_a"><?php echo $coin->website2; ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->explore) AND $coin->explore!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE867;</i>&nbsp;<div class="ic_c_a"><?php echo $coin->explore; ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->explore2) AND $coin->explore2!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE867;</i>&nbsp;<div class="ic_c_a"><?php echo $coin->explore2; ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->message_board1) AND $coin->message_board1!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE312;</i>&nbsp;<div class="ic_c_a"><?php echo $coin->message_board1; ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->message_board2) AND $coin->message_board2!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE312;</i>&nbsp;<div class="ic_c_a"><?php echo $coin->message_board2; ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->Mineable) AND $coin->Mineable!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE8D3;</i>&nbsp;<div class="ic_c_a"><?php if($coin->Mineable=='1'){ echo "Minneable"; } ?></div></div>
						</div>
						<?php } ?>
						<?php  if(isset($coin->currency) AND $coin->currency!='') {?>
						<div class="col-xs-3 col-md-12 mob_res def_hei">
							 <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE8D3;</i>&nbsp;<div class="ic_c_a"><?php if($coin->currency=='1'){ echo "Coin"; } ?></div></div>
						</div>
						<?php } ?>
					</div>
                </div>
            </div>
        </div>
		
		</div>
		
		
		<div class="row alg-con mobile">
			<div class="col-md-12">
				<div class="well well-sm">
					<div class="row nowrap">
					
					   <?php  if(isset($coin->website) AND $coin->website!='') {?>
						<div class="col-xs-6 col-md-3 set2">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE7FA;</i>&nbsp;<div class="ic_c_a2"><?php echo $coin->website; ?></div></div>
						</div>
						
						<?php } if(isset($coin->website2) AND $coin->website2!='') {?>
						<div class="col-xs-6 col-md-2 set2">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE7FA;</i>&nbsp;<div class="ic_c_a2"><?php echo $coin->website2; ?></div></div>
						</div>
						
						<?php } if(isset($coin->explore) AND $coin->explore!='') {?>
						<div class="col-xs-6 col-md-2 set2">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE867;</i>&nbsp;<div class="ic_c_a2"><?php echo $coin->explore; ?></div></div>
						</div>
						
						<?php } if(isset($coin->explore2) AND $coin->explore2!='') {?>
						<div class="col-xs-6 col-md-2 set2">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE867;</i>&nbsp;<div class="ic_c_a2"><?php echo $coin->explore2; ?></div></div>
						</div>
						
						<?php } if(isset($coin->message_board1) AND $coin->message_board1!='') {?>
						<div class="col-xs-6 col-md-3 set2">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE312;</i>&nbsp;<div class="ic_c_a2"><?php echo $coin->message_board1; ?></div></div>
						</div>
						
						<?php } if(isset($coin->message_board2) AND $coin->message_board2!='') {?>
						<div class="col-xs-6 col-md-3 set2 mag-to">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE312;</i>&nbsp;<div class="ic_c_a2"><?php echo $coin->message_board2; ?></div></div>
						</div>
						
						<?php } if(isset($coin->Mineable) AND $coin->Mineable!='' AND $coin->Mineable=='1') {?>
						<div class="col-xs-6 col-md-2 set2 mag-to">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE8D3;</i>&nbsp;<div class="ic_c_a2"><?php if($coin->Mineable=='1'){ echo "Minneable"; } ?></div></div>
						</div>
						
						
						<?php } if(isset($coin->currency) AND $coin->currency!=''  AND $coin->currency=='1') {?>
						<div class="col-xs-6 col-md-2 set2 mag-to">
						   <div class="text-col4 det cor1 bold"><i class="material-icons cor1 float_left">&#xE8D3;</i>&nbsp;<div class="ic_c_a2"><?php if($coin->currency=='1'){ echo "Coin"; } ?></div></div>
						</div>
						<?php } ?>
						
					</div>
				</div>
			</div>
		</div>
		
		<script>
			function addportfolio(val)
{
	
	<?php if($this->session->userdata('user_id'))
	{ ?>

   var dataString = 'cid='+ val;
 	$("#heart_"+val).fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader20.gif'>");
	 $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_addportfolio/add');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   
               if(result=="success")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>&nbsp;Add to portfolio");
			   }
			   else if(result=="exit")
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart heart_cor\" aria-hidden=\"true\"></i>&nbsp;Add to portfolio");
			   }
			   else
			   {
				   $("#heart_"+val).html("<i class=\"fa fa-heart-o heart_cor\" aria-hidden=\"true\"></i>&nbsp;Add to portfolio");

			   }
                 //$("#pageData").html(result);
				
           }
      });
	<?php }
	else
	{
		
		?>
		$('#myModal').modal('show');
    <?php 
    } 
	?>
}


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/common_search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#coin_name").css("background","#FFF");
		}
		});
	});
});

// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/common_search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name_mobile").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});
//To select country name
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val+"?source=search&time="+<?php echo time();?>;
$("#coin_name").val(val);
$("#suggesstion-box").hide();

}







function displaybalance(){
	

	 var currency = $("#hidden_currency").val();
     var dataString = 'currency='+ currency;
	 $("#bal_al").html("Loading...");
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			//alert(result);
                 $("#pageData3").html(result);
           }
      });
}
 displaybalance();


		</script>
		
		<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alert!</h4>
      </div>
      <div class="modal-body">
        <p>Please login to continue. <a href="<?php echo base_url("hauth/Login/Google");?>">Click Here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>