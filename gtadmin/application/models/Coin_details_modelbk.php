<?php
class Coin_details_model extends CI_Model
{
	public function getParameter()
	{
		$this->db->select('*');
		$this->db->from(CURRENCY);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row->currency_code;
			}
			
			$chunks = array_chunk($row1,"25");
			foreach($chunks as $chunk)
			{
				$parameter[] = implode(",",$chunk);
			}
			
			return $parameter;
		}
	}
	
	public function getCoinList($data)
	{
		
		$this->db->select('*');
		$this->db->from(COIN);
		$this->db->where("status","1");
		if($data->coin_id)
		{
			$this->db->where("id >=",$data->coin_id);
		}
		$this->db->order_by("sortorder","ASC");
		if($this->input->get('limit'))
		{
		$this->db->limit($this->input->get('limit'));
		}
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
	
	public function getCronDetails()
	{
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","PROCESSING");
		$this->db->limit("1","0");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
		}
		else
		{
			$insert_data =  array("start_time"=>NOW,
			                      "end_time"=>NOW,
								  "status"=>"PROCESSING");
			$this->db->set($insert_data)->insert(CRON);
			$insert_id = $this->db->insert_id();
			$this->db->select('*');
			$this->db->from(CRON);
			$this->db->where("status","PROCESSING");
			$this->db->limit("1","0");
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				$row = $query->row();
			}
		}
		
		return $row;
		
	}
	
	public function getColumns()
	{
		
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}		
			return $row1;
		}
	}
}
?>