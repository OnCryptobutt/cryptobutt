<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
	
			<?php if($success) echo $success;?>
			   <form method="post" class="form-horizontal" enctype="multipart/form-data">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
         
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
			  
			  
			  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Site Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_site_name" class="form-control" id="inputEmail3" placeholder="Site Name" value="<?php echo $records->settings_site_name;?>">
					<span class="red"><?php echo form_error('settings_site_name');?></span>
                  </div>
                </div>
				
				
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Admin Email</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_email" class="form-control" id="inputEmail3" placeholder="Email" value="<?php echo $records->settings_email;?>">
					<span class="red"><?php echo form_error('settings_email');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Records per page</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_records_per_page" class="form-control" id="inputEmail3" placeholder="Records Per Page" value="<?php echo $records->settings_records_per_page;?>">
					<span class="red"><?php echo form_error('settings_records_per_page');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Site Logo</label>

                  <div class="col-sm-10">
                    <input type="file" name="settings_logo_photo"/>
					<?php if($records->settings_logo_photo){ ?><img src="<?php echo SETTINGS_URL;?>thumb/<?php echo $records->settings_logo_photo_name;?>_150.<?php echo $records->settings_logo_photo_ext;?>"> <?php } ?>
					<span class="red"><?php echo form_error('settings_logo_photo');?></span>
                  </div>
                </div>

				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Frond End Records per page</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_records_per_page_front" class="form-control" id="inputEmail3" placeholder="Frond End Records Per Page" value="<?php echo $records->settings_records_per_page_front;?>">
					<span class="red"><?php echo form_error('settings_records_per_page_front');?></span>
                  </div>
                </div>
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
				
              </div>
            
              <!-- /.box-footer -->
           
          </div>
		  
		  
		  	
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Footer</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
			  
			  
			   <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_address" class="form-control" id="inputEmail3" placeholder="Address" value="<?php echo $records->settings_address;?>">
					<span class="red"><?php echo form_error('settings_address');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_phone" class="form-control" id="inputEmail3" placeholder="Phone" value="<?php echo $records->settings_phone;?>">
					<span class="red"><?php echo form_error('settings_phone');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Fax</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_fax" class="form-control" id="inputEmail3" placeholder="Fax" value="<?php echo $records->settings_fax;?>">
					<span class="red"><?php echo form_error('settings_fax');?></span>
                  </div>
                </div>
				
			 
				
				 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Copyright</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_copyright" class="form-control" id="inputEmail3" placeholder="Copyright" value="<?php echo $records->settings_copyright;?>">
					<span class="red"><?php echo form_error('settings_copyright');?></span>
                  </div>
                </div>
				
				 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Website Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_website_name" class="form-control" id="inputEmail3" placeholder="Website" value="<?php echo $records->settings_website_name;?>">
					<span class="red"><?php echo form_error('settings_website_name');?></span>
                  </div>
                </div>
				
				
				
			
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
				
              </div>
            
              <!-- /.box-footer -->
           
          </div>
		  
		  
		  
		   <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Social Links</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
			 
				
				 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Facebook Url</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_facebook_url" class="form-control" id="inputEmail3" placeholder="Facebook Url" value="<?php echo $records->settings_facebook_url;?>">
					<span class="red"><?php echo form_error('settings_facebook_url');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Twitter Url</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_twitter_url" class="form-control" id="inputEmail3" placeholder="Twitter Url" value="<?php echo $records->settings_twitter_url;?>">
					<span class="red"><?php echo form_error('settings_twitter_url');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Google Url</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_google_url" class="form-control" id="inputEmail3" placeholder="Google Url" value="<?php echo $records->settings_google_url;?>">
					<span class="red"><?php echo form_error('settings_google_url');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Linkedin Url</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_linkedin_url" class="form-control" id="inputEmail3" placeholder="Linkedin Url" value="<?php echo $records->settings_linkedin_url;?>">
					<span class="red"><?php echo form_error('settings_linkedin_url');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Youtube Url</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_youtube_url" class="form-control" id="inputEmail3" placeholder="Youtube Url" value="<?php echo $records->settings_youtube_url;?>">
					<span class="red"><?php echo form_error('settings_youtube_url');?></span>
                  </div>
                </div>
								
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
				
              </div>
            
              <!-- /.box-footer -->
           
          </div>
		  
		  <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Meta</h3>
            </div>
           
              <div class="box-body">
			 
				
				 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Site Title</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_site_title" class="form-control" id="inputEmail3" placeholder="Site Title" value="<?php echo $records->settings_site_title;?>">
					<span class="red"><?php echo form_error('settings_site_title');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Meta Keyword</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_meta_keywords" class="form-control" id="inputEmail3" placeholder="Meta Keyword" value="<?php echo $records->settings_meta_keywords;?>">
					<span class="red"><?php echo form_error('settings_meta_keywords');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label>

                  <div class="col-sm-10">
                    <input type="text" name="settings_meta_description" class="form-control" id="inputEmail3" placeholder="Meta Description" value="<?php echo $records->settings_meta_description;?>">
					<span class="red"><?php echo form_error('settings_meta_description');?></span>
                  </div>
                </div>
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
				
              </div>

          </div>
		  
		  
		  
		   </form>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  