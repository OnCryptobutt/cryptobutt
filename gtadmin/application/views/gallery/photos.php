<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $gallery_name;?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
		
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
	
              <div class="box-body">
                
				
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Image</label>

                  <div class="col-sm-10">
                   
					<div id="mulitplefileuploader">Upload</div>

                     
					 <div id="status"></div>
                  </div>
                </div>
				
				
				
                
				
				
              </div>
			   <div class="box-footer">
				<a href="<?php echo base_url('gallery/');?>"><button type="button" class="btn btn-primary">List</button></a>
				<a href=""><button type="button" class="btn btn-primary">Refresh</button></a>
				
				
				 
              </div>
			  
            
              <!-- /.box-footer -->
           
			
			
			
			
          </div>
          <!-- /.box -->
		  
		    <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
		  
		  
		 <section class="content">
      <div class="row">
			 <div class="col-md-12">
			 
			
			<?php if($success) echo $success;?>
			 
			    <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Photos</h3>
			  
			 <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger pull-right"/>
			  
			  <span class="pull-right"> <input type="checkbox" id="selecctall"/>&nbsp;Select All&nbsp;&nbsp;&nbsp;</span>
			    
			   
				 
				  
				  
            </div>
			
			<div class="box-body">	
			 
				 <div class="row">
			<?php if(is_array($lists)){
				
				$i=0;
				foreach($lists as $list){
					
					 if($i%6=='0')
					 {
						echo '</div><div class="row">';
					 }
				?>
				   
				 
				 
				 
					<div class="col-sm-2">
					<p align="center">
					<img src="<?php echo GALLERY_PHOTOS_URL."thumb/".$list->gallery_photos_photo_name."_150.".$list->gallery_photos_photo_ext;?>"></p>
					<p align="center"><input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $list->gallery_photos_id;?>"></p>
					</div>
				
					
				
				
			<?php $i++;  }  }?>
				
				</div>
				</div>
				
				<?php echo $pagination;?>
				
				
				</div>
				</div>
				</div>
				
				
				
				
				
		  
		   </form>
		 
          
        
    <!-- /.content -->
  </div>
  