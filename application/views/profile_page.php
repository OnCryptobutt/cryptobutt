<style>
.dropdown:hover .dropdown-content {
   visibility: visible;
}
.dropdown-content a::after{background-color: #000;}
/*.dropdown-content a:last-child:hover a:last-child::after{background-color: #FFEB3B;} */
.dropdown-content {
    visibility: hidden;
    position: absolute;
    background-color: #000;
    min-width: 225px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    top:60px;
}
.dropdown-content a {
    float: none;
    color:#FFEB3B;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
    font-size:13px;
}
.dropdown-content a:hover {
    background-color: #FFEB3B;
    color:#000;
}
.row_background{background-color : #FFCDD2 !important;}
.row_background3{background-color : #ccdefc !important;}
.row_background2{background-color : #B9F6CA !important;}
</style>
<input type="hidden" id="hidden_search" value="<?php echo $this->input->get('search');?>"/>
<input type="hidden" id="hidden_per_page" value=""/>
<input type="hidden" id="hidden_currency" value="<?php echo $this->input->get('currency');?>"/>
<input type="hidden" id="hidden_sorting" value=""/>
<input type="hidden" id="row_no" value="1">
<input type="hidden" id="pageids" value="0">
<input type="hidden" id="coins_count" value="<?php if(isset($total_list)) { echo $total_list; }?>">
<input type="hidden" id="sorting_price" value="DESC"/>
<input type="hidden" id="sorting_market" value="DESC"/>
<input type="hidden" id="sorting_change" value="24H"/>
<section class="wallet_txtbg col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="container-bk">
    	<div class="page-header row">
    	    <div class="col-lg-1 col-md-2 col-sm-2 col-4">
    	         <?php
				 if (file_exists($query_user_row->photoURL)) {
				    ?>
				    <img class="img-rounded" src="<?php echo $query_user_row->photoURL;?>" alt="<?php echo $query_user_row->email;?>"  style="width: 40px;height: 40px;margin:10px 0;color:#FFEB3B;" >
				    <?php
				        } else{
			            ?>
			             <i class="fa fa-user-circle"  style="font-size:40px;margin:10px 0;color:#FFEB3B;"></i>
			            <?php
				        }
				        ?>
    		</div>
    		<div class="col-lg-5 col-md-5 col-sm-5 col-5 wallet_b wallet_bl dropdown"  style="display: flex;align-items: center;max-width: 17%;padding:0;cursor:pointer">
    		    <a href="javascript:;" class="dropbtn" id="yourWatchlist"><span class="txt_wallet">Your watch list:</span>
    		    <span class="txt_wallet" style="font-size:12px;">&nbsp;&nbsp;On hold&nbsp;&nbsp;(12)</span>&nbsp;&nbsp;<i class="fa fa-caret-down" style="color:#FFEB3B;margin-left: 6px;"></i></span></a>
    		    <div class="dropdown-content" id="myDropdown">
    		      <?php 
    		      foreach($getWatchlist as $row) {
		            $watch_sql = "SELECT * FROM ci_watchlistassets WHERE watch_nameid ='".$row->watchlist_id."' AND watch_userid='".$userid."'";
                    $watch_query =$this->db->query($watch_sql);
    		      ?>
                  <a href="<?php echo base_url(); ?>profile/yourwatchlist/<?php echo $row->watchlist_name; ?>"><?php echo $row->watchlist_name; ?> (<?php echo $watch_query->num_rows(); ?> Crypto assets)</a>
                <?php
    		      }
    		      ?>
                </div>
    		</div>
    		  <div class="col-lg-2 col-md-2 col-sm-2 col-5 wallet_bl">
    		    <!--<a href="javascript:;" onclick="walletFun()"><span class="txt_wallet">Your wallet balance</span></a>-->
    		</div>
    		 <div class="addwallet col-lg-6 col-md-4 col-sm-4 col-3">
        		<a href="<?php echo base_url();?>profile/addwallet" class="wallet_following">Add wallet</a>	
            </div>
    	</div>
	</div>
</section>
<section id="urwatchlist">
<!--<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 1.5% 0; background: #F3E5F5;">
	<div class="container-bk">
    	<div class="page-header row" >
    	    <input type="text" id="coin_name" name="search" class="search" autocomplete="off" placeholder="Search and add coin to your watch list" style="width: 100%;margin: 0 20%;">
    	</div>
	</div>
</section>-->
<section class="desktop-marg purple" style="padding: 0 10%;">
    <nav class="navbar navbar-expand-lg navbar-light">
    <div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
    	<a href="javascript:;">
            <div class="col-1 col-md-1 tog"></div>
    		<div class="tog col-2 col-md-3 coin-tag">
    			<p class="collapse_nav_price">My watch list</p>
    			<p class="collapse_nav_htl">On hold (<span id="coins_count1"></span>)</p>
    		</div>
            <div class="tog coin_cap col-2 col-md-3" style="padding-left:0;">
    			<p class="collapse_nav_price">Market cap</p>
    			<p id="sorting_market_html" class="collapse_nav_htl">High to Low</p>
    		</div>
    		<div class="tog coin_cap col-2 col-md-2">
    			<p class="collapse_nav_price">Price</p>
    			<p id="sorting_market_html" class="collapse_nav_htl">--</p>
    		</div>
    		<div class="tog col-2 col-md-2" style="width: 12%;">
    			<p class="collapse_nav_price">Change</p>
    			<p id="sorting_change_html" class="collapse_nav_htl">1 Day</p>
    
    		</div>
            <div class="tog col-2 col-md-2" style="width:11%">
                <p class="collapse_nav_price" style="visibility:hidden">1 Day high</p>
                <p class="collapse_nav_htl" id="filter_txt1">1 Day high</p>
    		</div>
    		<div class="tog col-2 col-md-2" style="width:12%">
    		    <p class="collapse_nav_price" style="visibility:hidden">1 Day low</p>
    		    <p class="collapse_nav_htl" id="filter_txt2">1 Day low</p>
    		</div>
    	</a>
    </div>
    </nav>
    <div class="collapse" id="collapse1">
      <div class="card card-block">
    	<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
            <div class="col-1 col-md-1 tog"></div>
    		<div class="tog col-2 col-md-3" style="color:#FFEB3B;left: 25px;">All</div>
    		<div class="tog coin_cap col-3 col-md-2 select-txt" style="padding-left:0">
    			<p><a id="f3" href="javascript:void(0);" style="color:#FFEB3B;" onclick="dropValueAtID('sorting_market_html','High to Low','sorting_market','DESC','f3');changePagination();">High to Low</a></p>
    			<p><a id="f4" href="javascript:void(0);" onclick="dropValueAtID('sorting_market_html','Low to High','sorting_market','ASC','f4');changePagination();">Low to High</a></p>
    		</div>
    		<div class="tog col-3 col-md-3 select-txt">
    			<p><a id="f1" href="javascript:void(0);"></a></p>
    			<p><a id="f2" href="javascript:void(0);"></a></p>
    		</div>
    		<div class="tog col-3 col-md-2 select-txt" style="width:12%">
            <p><a id="f5" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 hour','sorting_change','1H','f5');changePagination();">1 hour</a></p>
			<p><a id="f6" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Day','sorting_change','24H','f6');changePagination();" style="color:#FFEB3B;">1 Day</a></p>
			<p><a id="f7" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Week','sorting_change','1W','f7');changef7Pagination();">1 Week</a></p>
			<p><a id="f8" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Month','sorting_change','1M','f8');changePagination();">1 Month</a></p>
            <p><a id="f9" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','3 Months','sorting_change','3M','f9');changePagination();">3 Months</a></p>
            <p><a id="f10" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','6 Months','sorting_change','6M','f10');changePagination();">6 Months</a></p>
            <p><a id="f11" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Year','sorting_change','12M','f11');changePagination();">1 Year</a></p>
            <p><a id="f12" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','All','sorting_change','ALL','f12');changePagination();">All</a></p>
    		</div>
    	</div>
      </div>
    </div>
</section>
<section id="pageData" style="min-height:400px;margin-bottom:100px;"></section>
</section>
<section id="walletdata" style="display:none">
    <style>
    .wallet_b a::after {
    content: "";
    background: #FFEB3B;
    height: 5px;
    position: absolute;
    width: 100%;
    left: 0px;
    bottom: -1px;
    transition: all 250ms ease 0s;
    transform: scale(1);
}
.wallet_b{display: flex;align-items: center;max-width:14%; margin-right: 20px;}
.txt_wallet{color:#FFEB3B;}
.wallet_txtbg{background:#000;}
.wallet_following{background: #FFEB3B;color:#000;font-weight: bold;padding: 2px 10px;}
.addwallet{display: flex;align-items: center;}
</style>

<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 3% 0; background: #F3E5F5;">
    
    <?php
    	$Data = json_decode(file_get_contents('http://api.ethplorer.io/getAddressInfo/0xDfFFfae857cE8D26b3C76D179Ad6FceA97D67F9C?apiKey=freekey'),true);
    	//print_r($Data['ETH']['balance']);
    	//print_r($Data);
    ?>
    
	<div class="container-bk">
    	<div class="page-header row" >
    	    <div class="col-lg-1 col-md-2 col-sm-2 col-4"></div>
    		<div class="col-lg-2 col-md-2 col-sm-2 col-5" style="max-width:13%"></div>
    		<div class="col-lg-5 col-md-5 col-sm-5 col-5">
    		    <a href="javascript:;" style="font-size: 22px;color:#753ac8">$ <?php echo $Data['ETH']['balance']; ?></a><br/><a href="javascript:;" style="font-size: 14px;color:#753ac8">Your nano ledger balance</a>
    		</div>
    		 <div class="addwallet col-lg-4 col-md-4 col-sm-4 col-3">
        		<h2>No wallet found</h2>
            </div>
    	</div>
	</div>
</section>
<section class="desktop-marg purple" style="padding: 0 15%;">
    <nav class="navbar navbar-expand-lg navbar-light">
    <div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
    	<a href="javascript:;">
            <div class="col-1 col-md-1 tog"></div>
    		<div class="tog col-3 col-md-3 coin-tag">
    			<p class="collapse_nav_price">Crypto assets name</p>
    			<p class="collapse_nav_htl">All (820)</p>
    		</div>
            <div class="tog coin_cap col-3 col-md-3">
    			<p class="collapse_nav_price">Balnce</p>
    			<p id="sorting_market_html" class="collapse_nav_htl">High to Low</p>
    		</div>
    		<div class="tog col-2 col-md-2" style="width: 12%;">
    			<p class="collapse_nav_price">Change</p>
    			<p id="sorting_change_html" class="collapse_nav_htl">1 Day</p>
    
    		</div>
            <div class="tog col-2 col-md-2" style="width:11%">
                <p class="collapse_nav_price" style="visibility:hidden">1 Day high</p>
                <p class="collapse_nav_htl" id="filter_txt1">1 Day high</p>
    		</div>
    		<div class="tog col-2 col-md-2" style="width:12%">
    		    <p class="collapse_nav_price" style="visibility:hidden">1 Day low</p>
    		    <p class="collapse_nav_htl" id="filter_txt2">1 Day low</p>
    		</div>
    	</a>
    </div>
    </nav>
    <div class="collapse" id="collapse1">
      <div class="card card-block">
    	<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
            <div class="col-1 col-md-1 tog"></div>
    		<div class="tog col-2 col-md-3" style="color:#FFEB3B;left: 25px;">All</div>
    		<div class="tog coin_cap col-4 col-md-3 select-txt">
    			<p><a id="f3" href="javascript:void(0);" style="color:#FFEB3B;">High to Low</a></p>
    			<p><a id="f4" href="javascript:void(0);">Low to High</a></p>
    		</div>
    		<div class="tog col-4 col-md-2 select-txt">
    			<p><a id="f1" href="javascript:void(0);">High to Low</a></p>
    			<p><a id="f2" href="javascript:void(0);">Low to High</a></p>
    		</div>
    		<div class="tog col-4 col-md-2 select-txt" style="width:12%">
            <p><a id="f5" href="javascript:void(0);">1 hour</a></p>
    		<p><a id="f6" href="javascript:void(0);"  style="color:#FFEB3B;">1 Day</a></p>
    		<p><a id="f7" href="javascript:void(0);">1 Week</a></p>
    		<p><a id="f8" href="javascript:void(0);">1 Month</a></p>
            <p><a id="f9" href="javascript:void(0);">3 Months</a></p>
            <p><a id="f10" href="javascript:void(0);">6 Months</a></p>
            <p><a id="f11" href="javascript:void(0);">1 Year</a></p>
            <p><a id="f12" href="javascript:void(0);">All</a></p>
    		</div>
    	</div>
      </div>
    </div>
</section>
<section class="coin_group col-lg-12 col-md-12 col-sm-12 col-12 coin_list" style="height:800px">
	<div>
       
	</div>
</section>
</section>
<script>
  function walletFun() {
      document.getElementById('walletdata').style.display="block";
      document.getElementById('urwatchlist').style.display="none";
  }  
</script>