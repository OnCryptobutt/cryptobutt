<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 199px;">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Work
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <table class="table table-bordered">
					<tr><td><b>Name</b></td><td><?php echo $records->work_name;?></td></tr>
					
					<tr>
					
					<td><b>Sliders</b></td><td>
					<?php if($service_sliders) { 
					 
					 foreach($service_sliders as $list)
					 {
					
					?>
					<?php if($list->work_slider_photo){ echo "<img src='".WORK_URL."thumb/".$list->work_slider_photo_name."_150.".$list->work_slider_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?></br>
					<?php } } ?>
					
					</td></tr>
					
					<tr><td><b>Status</b></td><td> <?php if($records->work_status=="1"){?>Active<?php } ?><?php if($records->work_status=="0"){?>Suspend<?php } ?></td></tr>
				</table>
				
              </div>
			   <div class="box-footer">
                
				<a href="<?php echo base_url();?>service/"><button type="button" class="btn btn-warning">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  
  </div>