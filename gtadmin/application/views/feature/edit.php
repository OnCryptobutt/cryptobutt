<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        feature
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
			  
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Select Category</label>

                  <div class="col-sm-10">
                    <select id="feature_category_id" onchange="getSubCategory()" name="feature_category_id" class="form-control">
					<option value="">--Select Category--</option>
					 <?php echo $getCategoryList;?>
					</select>
					<span class="red"><?php echo form_error('feature_category_id');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Select Sub Category</label>

                  <div class="col-sm-10">
				    
					<span id="span_feature_sub_category_id">
                    <select id="feature_sub_category_id" name="feature_sub_category_id" class="form-control">
					<option value="">--Select Sub Category--</option>
					<?php echo $getSubCategoryList;?>
					</select>
					</span>
					<span class="red"><?php echo form_error('feature_sub_category_id');?></span>
                  </div>
                </div>
			  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="feature_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo $records->feature_name;?>">
					<span class="red"><?php echo form_error('feature_name');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Price</label>

                  <div class="col-sm-10">
                    <input type="text" name="feature_price" class="form-control" id="inputEmail3" placeholder="Price" value="<?php echo $records->feature_price;?>">
					<span class="red"><?php echo form_error('feature_price');?></span>
                  </div>
                </div>
				
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="feature_content"><?php echo $records->feature_content;?></textarea>
					<span class="red"><?php echo form_error('feature_content');?></span>
                  </div>
                </div>
				
				
			 <!--<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                   <input type="file" name="feature_photo" value=""/>
				   <?php if($records->feature_photo){ echo "<img src='".feature_URL."thumb/".$records->feature_photo_name."_15.".$records->feature_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?>
                  </div>
                </div>-->
				
				 <div class="form-group">
				 <label for="inputEmail3" class="col-sm-2 control-label">
				 Photo</label>

                  <div class="col-sm-10">
				<div id="cropContainerModal" style="width:230px;height:150px;">
				  <?php if($records->feature_photo){ echo "<img src='".FEATURE_URL."thumb/".$records->feature_photo_name."_crop.".$records->feature_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?>
				</div>
				<input type="hidden" name="filenameCrop" id="filenameCrop" value=""/>
				<input type="hidden" name="filenameOrginal" id="filenameOrginal" value=""/>
				</div>
				 </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="feature_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo $records->feature_order;?>">
					<span class="red"><?php echo form_error('feature_order');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Site Title</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="feature_site_title"><?php echo $records->feature_site_title;?></textarea>
					<span class="red"><?php echo form_error('feature_site_title');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Meta Description</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="feature_meta_description"><?php echo $records->feature_meta_description;?></textarea>
					<span class="red"><?php echo form_error('feature_meta_description');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Meta Keywords</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" name="feature_meta_keyword"><?php echo $records->feature_meta_keyword;?></textarea>
					<span class="red"><?php echo form_error('feature_meta_keyword');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" <?php if($records->feature_status=="1"){?> checked='true'<?php } ?> name="feature_status" value="1"> Enable<br/>
					<input type="radio" <?php if($records->feature_status=="0"){?> checked='true'<?php } ?> name="feature_status" value="0"> Disable
                  </div>
                </div>
                
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>feature/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
  <script>
	function getSubCategory()
	{
		var val = $("#feature_category_id").val();
		$("#span_feature_sub_category_id").html("<img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
		$.ajax({
			url:"<?php echo base_url('ajax/getSubCategory');?>",
			data: "act=getSubCategory&feature_category_id="+val,
			method:"GET",
			success:function(data)
			{
				$("#span_feature_sub_category_id").html(data);
			}
		});
	}
</script>