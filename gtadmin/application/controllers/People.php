<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('people_model');
		$this->load->model('coin_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->helper('url');
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "people-list";
		$data['title'] = "people List";
		
		/** Start Activate Records **/
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->people_model->peopleActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->people_model->peopleSuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		/*if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->people_model->peopleDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}*/
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->people_model->keypeopleDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('people/index');
        $config["total_rows"] = $this->people_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->people_model->people_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();
        $data['keypeople_name'] = $this->people_model->getkeypeoplename();
		$data['link_type_list'] = $this->people_model->getLink_typeList();
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('people/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "people-add";
		$data['title'] = "Add people";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			/**$this->form_validation->set_rules("people_first_name","First Name","required");**/
			/**if($this->input->post('people_linked_in_url'))
			{
			$this->form_validation->set_rules("people_linked_in_url","Linkedin URL","required|is_unique[".PEOPLE.".people_linked_in_url]");
			}**/
			/** $this->form_validation->set_rules("people_content","Content","required"); **/
		    /**$this->form_validation->set_rules("people_order","Order","required|max_length[11]|numeric");**/
			
				$pID = $this->people_model->peopleInsert();
				
				if($_FILES['people_photo']['name']!="")
				{
				  
							
					$ext_array = explode(".",$_FILES['people_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "people_photo_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					
					 if (move_uploaded_file($_FILES["people_photo"]["tmp_name"], PEOPLE_PATH.$file_name)) {
						 
						 
						$sql = "UPDATE ".PEOPLE." SET 
		
		people_photo      = '".$file_name."'
		WHERE people_id='".$pID."'";
		
		$this->db->query($sql);
      
    } else {
       
    }
				
				
					
					
				
				
				
				
			
					
					
				}
				
				
				
				if(isset($_POST['type']))
				{
			    $type = $_POST['type'];
				$url = $_POST['url'];
				
				
				
				
				foreach($type as $key=>$value)
				{
					if($value)
					{
						$insert_data = "";
						//$insert_data = array('work_slider_name'=>addslashes(trim($value)));
						
						$insert_data = array('social_links_type'=>addslashes(trim($value)),
						'social_people_id'=>$pID,
						'social_links_url'=>$url[$key]);
						$insertid = $this->people_model->linksInsert($insert_data);
					}
				}
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."people/add/");
			
			
				
		}
		
		$data['link_type_list'] = $this->people_model->getLink_typeList();
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('people/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "people-edit";
		$data['title'] = "Edit people";
		
		
		if($this->input->get('socialid')!="")
		{
			$this->people_model->deletesocial($this->input->get('socialid'));
			$this->session->set_flashdata('success',"Your requested record has been deleted");
			redirect(base_url()."people/edit/".$this->uri->segment(3)."/");
		}

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
			/**$this->form_validation->set_rules("people_first_name","First Name","required");	**/
			
			/**if($this->input->post('people_linked_in_url'))
			{
			$this->form_validation->set_rules("people_linked_in_url","Linkedin Url","required|edit_unique_common[".PEOPLE.".people_linked_in_url.".$pID.".people_id]");
			} **/
			/** $this->form_validation->set_rules("people_content","Content","required"); **/
			/**$this->form_validation->set_rules("people_order","Order","required|max_length[11]|numeric");**/
			
		
				$this->people_model->peopleEdit($pID);
				
				if($_FILES['people_photo']['name']!="")
				{
				   
				    $this->people_model->peopleImageDelete($pID);
					
							
					$ext_array = explode(".",$_FILES['people_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "people_photo_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					
					 if (move_uploaded_file($_FILES["people_photo"]["tmp_name"], PEOPLE_PATH.$file_name)) {
						 
						 
						$sql = "UPDATE ".PEOPLE." SET 
		
		people_photo      = '".$file_name."'
		WHERE people_id='".$pID."'";
		
		$this->db->query($sql);
      
    } else {
       
    }
				
				
					
					
					
				}
				
				
			
				
				if(isset($_POST['primary_update']))
				{
					$insert_data = array('social_primary'=>"1");
					$this->db->where('social_id', $_POST['primary_update'][0]);
		            $this->db->set($insert_data)->update(SOCIAL);
				}
				
				
				if(isset($_POST['type']))
				{
			    $type = $_POST['type'];
				$url = $_POST['url'];
				
				
				
				
				foreach($type as $key=>$value)
				{
					if($value)
					{
						$insert_data = "";
						//$insert_data = array('work_slider_name'=>addslashes(trim($value)));
						
						$insert_data = array('social_links_type'=>addslashes(trim($value)),
						'social_people_id'=>$pID,
						'social_links_url'=>$url[$key]);
						$insertid = $this->people_model->linksInsert($insert_data);
					}
				}
				}
				
				
				if(isset($_POST['type_update']))
				{
					 $type_update = $_POST['type_update'];
				     $url_update = $_POST['url_update'];
				
					
					foreach($type_update as $key=>$value)
					{
						if($value)
						{
							$insert_data = "";
							$insert_data = array('social_links_type'=>addslashes(trim($value)),
							'social_links_url'=>$url_update[$key]);
						   $this->people_model->linksUpdate($insert_data,$key);
						}
					}
				}
				
				
				
			
				
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."people/edit/".$pID."/");
				
			
			
				
		}
		
		$data['records'] = $this->people_model->getpeopleDetails($pID);
		$data['link_type_list'] = $this->people_model->getLink_typeList();
		$data['links'] = $this->people_model->getLinks($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('people/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "people-view";
		$data['title'] = "View people";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->people_model->getpeopleDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('people/view',$data);
		$this->load->view('template/footer',$data);
		
	}
	public function updatepeople() {
	    $id    = $this->input->post('id');
	    $updateid = $this->people_model->updatepeopledata($id);
	    echo json_encode(array('status' => 'true'));
	}
	
	public function addSocialLink()
	{
	   $profileid    = $this->input->post('peopleid');
	    $profilelink1 = $this->input->post('url1');
	    $profileweb1  = $this->input->post('type1');
    	if(isset($_POST['type']))
			{
			    $type[] = '';
			    $url[] = '';
    		    $type = $_POST['type'];
    			$url = $_POST['url'];
    			array_push($url, $profilelink1);
    			array_push($type, $profileweb1);
    			
    			
    			
    			foreach($type as $key=>$value)
    			{
    				if($value)
    				{
    					$insert_data = "";
    					$insert_data = array('social_links_type'=>addslashes(trim($value)),'social_people_id'=>$profileid,'social_links_url'=>$url[$key]);
    					$insertid = $this->people_model->linksInsertdata($insert_data);
						
    				}
    			}
    			 redirect('/people');
			}else{
			    $insert_data = "";
				$insert_data = array('social_links_type'=>addslashes(trim($profileweb1)),
				'social_people_id'=>$profileid,
				'social_links_url'=>$profilelink1);
				$insertid = $this->people_model->linksInsertdata($insert_data);
				
				 redirect('/people');
			}
	}
}
