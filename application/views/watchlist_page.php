
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Crypto asset watch list log in | CryptoButt</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/watchlist.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.toast.css">
  </head>
<!------ Include the above in your HEAD tag ---------->
<style>

.container.desktop{overflow:auto;}
.btn.btn-primary1 {
    background-color: #753ac8;
    border-color: #753ac8;
    outline: none;
    color: #fff !important;

}
.input-lg1{margin-right:10px;}
.back_btn{position: relative;top: 10px;text-decoration: underline;}
.container, body, html{max-width:100% !important;}
.container.desktop login .login-button, .container.desktop signup .login-button{padding-left:25px;}

/* The container */
.container-check {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 16px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container-check input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}
input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {background-color: #fff;}
/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 16px;
    width: 16px;
    background-color: #eee;
	border:1px solid #753ac8;
}

/* On mouse-over, add a grey background color */
.container-check:hover input ~ .checkmark {
    background-color: #ccc;
}

input[type=text]:focus {
    border: 1px solid #753ac8;
}

.form-control:focus {
    border-color: #753ac8 !important;
    box-shadow: 0 0 0 1px #753ac8 !important;
}
#loademail,#loading,#resetloading{float: right;position: relative;top: -35px;left: 25px;}
.watch_alert-success{color: #fff;background-color: #753ac8;border-color: #753ac8;margin:20px 0}
.text_hold{display: flex;align-items: center;top: 10px;color:gray}
/* Center the loader */
#loader {
    position: absolute;
    left: 50%;
    top: 40%;
    z-index: 1;
    border: 5px solid #f3f3f3;
    border-radius: 50%;
    border-top: 5px solid #753ac8;
    width: 40px;
    height: 40px;
    -webkit-animation: spin 1s linear infinite;
    animation: spin 1s linear infinite;
    margin: 0;
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>
<body>
<div class="container desktop">
	<div class="header" style="float:left;position:relative">
		<img class="wix-logo" src="<?php echo base_url();?>img/logo.png" alt="title" style="height:65px;margin: 60px 0 20px 0;">
		<span class="logo-text" style="position: absolute;bottom: 0;left: 85px;color:gray;">IMDB for Crypto assets</span>
		<a class="x-close" href="<?php echo base_url();?>" style="text-decoration:none">
		<img src="<?php echo base_url();?>img/close_352915.svg" alt="close" style="width:40px" /></a>
	</div>
	<div id="loader" style="display:none"></div>
	<login id="login_temp" style="height:auto">
			<div class="container">
			<div class="row">
			<div class="col-sm-2 col-md-2"></div>
			<div class="col-sm-10 col-md-10">
				<div name="loginForm">
				    <div id="signin-1">
    					<div class="log-in-title title" style="font-size: 32px;text-align:center;">Create your crypto asset watch list</div>
					</div>
					<div class="col-sm-12 col-md-12" id="watch_vis" style="visibility:hidden">
					<div class="row">
					<div class="col-sm-11 col-md-11" style="height:100px">
					<div id="watchscuccess" class="alert watch_alert-success">
					  <div id="check" style="display:none">
					    <strong><img src="/img/check-mark_511949.svg" style="height:32px" /></strong> Created your crypto asset watch list.
					  </div>
					   <div id="cross" style="display:none">
					    <strong><img src="/img/close_watch_352915.svg" style="height:18px" /></strong> Already exists your crypto asset watch list.
					  </div> 
					</div></div>
					</div>
					</div>
					<div class="signin-section col-sm-12 col-md-12">
						<div class="signin-with-email" id="signin-2">
						<form>
						<div class="row">
							<div class="col-sm-3 col-md-3">
							<div class="form-group" autocomplete="off">
							  <label for="nameyourlist" style="font-size:12px;margin:0">Name your list:</label>
							  <input type="text" class="form-control" id="nameyourlist" name="nameyourlist" value="" style="border:1px solid #753ac8">
							</div>
							</div>
							<div class="col-sm-9 col-md-9 text_hold"><label for="">E.g. On hold, Thinking to sell, Next shopping, Alert list</label></div>
							</div>
						<div>						
						<a class="btn btn-primary1 input-lg input-lg1"   href="javascript:;">Save and Add more</a>
						<a class="btn btn-primary1 input-lg input-lg1"   href="javascript:;" id="watchsave-btn">Save</a>
						<a href="javascript:history.back()" class="back_btn">Back</a>
						</div>
						</form>
						</div>
					</div>
			</div></div>
			</div>
		</div>
	</login>
	
    
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>

$("#watchsave-btn").on("click", function (event) { 
    $('#loader').fadeIn(400);
    var name = $('#nameyourlist').val();
    var dataString = 'name='+ name;
    $.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>/ajax_profile/saveWatchlistname',
           data: dataString,
           cache: false,
           success: function(response){
             
			  if(response == 'true'){
			      $('#cross').hide();
			      $('#loader').fadeOut(400);
			      $('#watch_vis').css('visibility','visible');
			      $('#nameyourlist').val('');
			      $('#check').fadeIn('slow');
			      $('#watchscuccess').fadeIn("3000");
			      setTimeout(function(){
			          $('#watchscuccess').fadeOut("3000");
			          $('#watch_vis').css('visibility','visible');
			      }, 3000);
			  }else{
			      $('#check').hide();
			      $('#loader').fadeOut(400);
			      $('#watch_vis').css('visibility','visible');
			      $('#nameyourlist').val('');
			      $('#cross').fadeIn('slow');
			      $('#watchscuccess').fadeIn("3000");
			      setTimeout(function(){
			          $('#watchscuccess').fadeOut("3000");
			          $('#watch_vis').css('visibility','visible');
			      }, 3000);
			  }
           }
    
    });
});
</script>

</body>