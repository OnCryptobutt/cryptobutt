   
<input type="hidden" id="hidden_currency" value=""/>

   <div id="pageData2">
		<span class="flash2"></span>
	   </div>

		<!-- Mobile View End-->
		<!--<div class="col-md-12 desktop">
			<div class="row">
				<div class="col-xs-2 col-lg-4"></div>
				<div class="col-xs-8 col-lg-4">
					<div id="custom1-search-input">
						<div class="input-group col-md-12">
							<input type="text" class="  search-query form-control" placeholder="Search my Portfolio" />
							<span class="input-group-btn">
								<button class="btn btn-danger" type="button">
									<span class=" glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</div>
				</div>
				<div class="col-xs-2 col-lg-4"></div>
			</div>

		</div>-->
	
	   <div id="pageData">
		<span class="flash"></span>
	   </div>
	   
	    <div id="pageData3">
		
	   </div>
	
				
    </div>
</div>

<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div  class="modal-body">
	    <div id="edit_details">
        <span id="flash_details"></span>
		</div>
		<span id="flash_details_edit"></span>
		<span id="edit_details_edit"></span>
      </div>
	  
      <div class="modal-footer">
	     <button onclick="save_edit();" type="button" class="btn btn-danger">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

<style>
.table-info{
background-color: #d4e6e9;
}

.control-label {
    font-size: 14px;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #301c1e;
    margin-bottom: 10px;
}
</style>

<script>

function change_currency(symbol,name)
{
	 $("#change_currency").html(symbol+" "+name);
	
}

function common(type,val)
{
	if(type=="currency")
	{
		$("#hidden_currency").val(val);
	}
	changePagination("0","0");
	changePagination2("0","0");
	
}
<!-- Portfolio list -->
function changePagination(pageId,liId){
	$("#pageData").html('<span class="flash"></span>');
     $(".flash").show();
     $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
	 var currency = $("#hidden_currency").val();
     var dataString = 'pageId='+ pageId+'&currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });
}

changePagination("0","0");

<!-- Heading-->
function changePagination2(pageId,liId){
	$("#pageData2").html('<span class="flash"></span>');
     $(".flash2").show();
     $(".flash2").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
	  var currency = $("#hidden_currency").val();
     var dataString = 'pageId='+ pageId+'&currency='+ currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_heading');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash2").hide();
                 $("#pageData2").html(result);
				 displaybalance();
				 
           }
      });
}

changePagination2("0","0");


<!-- Save coin -->
function savecoin(coin_id,pid,refresh)
{
	var bought   = $("#bought_"+coin_id).val();
	var exchange = $("#exchange_"+coin_id).val();
	var amount   = $("#amount_"+coin_id).val();
	var date     = $("#date_"+coin_id).val();
	var month    = $("#month_"+coin_id).val();
	var year     = $("#year_"+coin_id).val();
	var amount2 = "";
   var currency2 = "";
	var amount2 = document.getElementById("amount2_"+coin_id).value;
	var currency2 = document.getElementById("currency2_"+coin_id).value;
	
	var err = "";
	if(bought=="")
	{
		err = 'AMOUNT OF COIN BOUGHT should not be empty \n';
	}
	if(exchange=="")
	{
		err = err + 'Please select EXCHANGE TYPE \n';
	}
	if(amount=="")
	{
		err = err +'AMOUNT SPENT should not be empty \n';
	}
	if(date=="" || month=="" || year=="")
	{
		err = err +'DATE OF PURCHASE should not be empty \n';
	}
	
	
	var currency = $("#currency_"+coin_id).val();
	
	

	var currency_array = [<?php echo $coin_names;?>];
if (currency_array.indexOf(currency) === -1) {
			// does exist
		
		}
		else {
			
			

		if(amount2=="")
	{
		err = err +'1 '+currency+' IS APPROXIMATELY should not be empty \n';
	}
			
		}
	
	if(err=="")
	{
		 $(".flash_"+coin_id).show();
     $(".flash_"+coin_id).fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'coin_id='+ coin_id+'&bought='+ bought+'&exchange='+ exchange+'&amount='+ amount+'&pid='+ pid+'&date='+ date+'&month='+ month+'&year='+ year+'&amount2='+ amount2+'&currency='+ currency+'&currency2='+ currency2;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_details/load_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_"+coin_id).hide();
                 $("#details_"+coin_id).html(result);
				 $("#bought_"+coin_id).val('');
				 $("#amount_"+coin_id).val('');
				 displaybalance(pid);
				 if(refresh=="close")
				 {
					 hide('form_'+pid);
				 }
				 
           }
      });
		
	}
	else
	{
		if(refresh=="yes")
		{
			 $(".flash_"+coin_id).show();
     $(".flash_"+coin_id).fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'coin_id='+ coin_id+'&bought='+ bought+'&exchange='+ exchange+'&amount='+ amount+'&pid='+ pid;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_details/load_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_"+coin_id).hide();
                 $("#details_"+coin_id).html(result);
				 $("#bought_"+coin_id).val('');
				 $("#amount_"+coin_id).val('');
				 
           }
      });
		}
		else
		{
			alert(err);
		}
	}
}

<!-- load edit model-->
function get_edit_section(did)
{
	 $(".flash_details").show();
     $(".flash_details").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'did='+ did;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_get_details/edit_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_details").hide();
                 $("#edit_details").html(result);
				
				 
           }
      });
}

<!-- update edit details -->
function save_edit()
{
	
	var id             = $("#edit_id").val();
	var coin_id        = $("#coin_id").val();
	var portfolio_id   = $("#portfolio_id").val();
	var date           = $("#date").val();
	var coin_brought   = $("#coin_brought").val();
	var exchange_type  = $("#exchange_type").val();
	var details_amount = $("#details_amount").val();
	var currency = $("#currency").val();
	var amount2 = $("#amount2").val();
	var currency2 = $("#currency2").val();
	var date = $("#date").val();
	var month = $("#month").val();
	var year = $("#year").val();
	var err="";
	if(coin_brought=="")
	{
		err = "AMOUNT OF COIN BOUGHT should not be empty\n";
	}
	if(exchange_type=="")
	{
		err = err + "Please select Exchange Type\n";
	}
	if(details_amount=="")
	{
		err = err + "AMOUNT SPENT should not be empty\n";
	}
	if(date=="" || month=="" || year=="")
	{
		err = err + "DATE OF PURCHASE should not be empty\n";
	}
	
	
		var currency_array = [<?php echo $coin_names;?>];
if (currency_array.indexOf(currency) === -1) {
			// does exist
		
		}
		else {
			
			

		if(amount2=="")
	{
		err = err +'1 '+currency+' IS APPROXIMATELY should not be empty \n';
	}
			
		}
	
	if(err=="")
	{
	
		 $(".flash_details_edit").show();
		 $("#edit_details_edit").show()
     $(".flash_details_edit").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'id='+ id+'&coin_brought='+coin_brought+'&exchange_type='+exchange_type+'&details_amount='+details_amount+'&date='+date+'&month='+month+'&year='+year+'&currency='+currency+'&currency2='+currency2+'&amount2='+amount2;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_get_details/save_edit_details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $(".flash_details_edit").hide();
                 $("#edit_details_edit").html(result);
				setTimeout(myfunction, 3000);
				<!-- Refresh list-->
				displaybalance(portfolio_id);
				savecoin(coin_id,portfolio_id,"yes");
				
				 
           }
      });
	}
	else
	{
		alert(err);
	}
	
	function myfunction()
	{
		$("#edit_details_edit").hide()
	}
	
}


// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/common_search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#coin_name").css("background","#FFF");
		}
		});
	});
});

// AJAX call for autocomplete 
$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/common_search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#coin_name_mobile").css("background","#FFF url(<?php echo UPLOAD_URL;?>loader20.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});

//To select country name
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val+"?source=search&time="+<?php echo time();?>;
$("#coin_name").val(val);
$("#suggesstion-box").hide();

}

function delete_portfolio_details(pdid,pid)
{
	if(confirm('Are you sure want to delete this record?'))
	{
	$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_delete/delete_portfolio_details');?>',
		data:'pdid='+pdid,
		success: function(data){
			if(data=="success")
			{
				$("#portdetaildel"+pdid).remove();
				//changePagination("0","0");
	            //changePagination2("0","0");
				displaybalance(pid);
			}
		}
		});
	}
}

function  delete_portfolio(pid)
{
	if(confirm('Are you sure want to delete this record?'))
	{
	$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_delete/delete_portfolio');?>',
		data:'pid='+pid,
		success: function(data){
			if(data=="success")
			{
				$("#headingTop"+pid).remove();
				//changePagination("0","0");
	            //changePagination2("0","0");
				displaybalance();
			}
		}
		});
	}
}


function displaybalance(changes_id){
	
	if(changes_id=="undefined")
	{
		changes_id = "";
	}
	$("#bal_al").html("Loading...");
	 var currency = $("#hidden_currency").val();
     var dataString = 'currency='+ currency+'&changes_id='+changes_id;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=portfolio');?>',
           data: dataString,
           cache: false,
           success: function(result){
			
                 $("#pageData3").html(result);
           }
      });
}

function show(id)
{
	$("#"+id).show();
}
function hide(id)
{
	$("#"+id).hide();
}

function show_approx(id)
{
	
	var currency = $("#currency_"+id).val();

	var currency_array = [<?php echo $coin_names;?>];
if (currency_array.indexOf(currency) === -1) {
			// does exist
			
			$("#amount2titleshow_"+id).hide();
			$("#amount2show_"+id).hide();
		}
		else {

			// does exist
			$("#currencydisplay_"+id).html(currency);
			$("#amount2titleshow_"+id).show();
			$("#amount2show_"+id).show();
			
		}
		
		//alert(currency);
		//alert(id);
		
	var bought   = $("#bought_"+id).val();
	var amount   = $("#amount_"+id).val();
	var currency = $("#currency_"+id).val();
		
		  /** var dataString = 'currency='+currency+'&coinid='+id;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_get_currency/get_currency');?>',
           data: dataString,
           cache: false,
           success: function(result){
			//alert(result);
                 $("#currencychange"+id).html(result);
           } 
      }); **/
	  if(bought!="" && amount!="")
	  {
	  var newamount = amount/bought;
	    $("#currencychange"+id).html(currency+" "+newamount);
	  }
}

function editamount_display(id)
{
	
	var currency = $("#currency").val();

	var currency_array = [<?php echo $coin_names;?>];
if (currency_array.indexOf(currency) === -1) {
			// does exist
			
			$("#editamount_display").hide();
			
		}
		else {

			// does exist
		
			$("#editamount_display").show();
		
			
		}
}
</script>

