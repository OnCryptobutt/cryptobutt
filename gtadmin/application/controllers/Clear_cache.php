<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Clear_cache extends CI_Controller
{
	
	
	function index()
	{
		
		$this->clear_all_cache();
		
}

public function clear_all_cache()
{
    $CI =& get_instance();
$path = $CI->config->item('cache_path');

    $cache_path = ($path == '') ? APPPATH.'cache/' : $path;

    $handle = opendir($cache_path);
    while (($file = readdir($handle))!== FALSE) 
    {
        //Leave the directory protection alone
        if ($file != '.htaccess' && $file != 'index.html')
        {
           @unlink($cache_path.'/'.$file);
        }
    }
    closedir($handle);
	
	if(file_exists(BASE_PATH."error_log"))
		{
			unlink(BASE_PATH."error_log");
			echo "error log remove from frontend";
		}
	
	
}
}
