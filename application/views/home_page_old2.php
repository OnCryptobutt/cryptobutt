<input type="hidden" id="hidden_search" value="<?php echo $this->input->get('search');?>"/>
<input type="hidden" id="hidden_per_page" value=""/>
<input type="hidden" id="hidden_currency" value="<?php echo $this->input->get('currency');?>"/>
<input type="hidden" id="hidden_sorting" value=""/>
<input type="hidden" id="row_no" value="1">
<input type="hidden" id="pageids" value="0">
<input type="hidden" id="coins_count" value="<?php if(isset($total_list)) { echo $total_list; }?>">

 <div class="">
 
 

       <div class="page-content">
        <div class="page-header">
			
        </div>
        
        <section class="no-padding-bottom">
			<div class="container">
			
					
				
				<div class="row">
					<div class="col-lg-12 sort">
						<p><span>Sort by :</span> <a  id="sortby_price" href="javascript:void(0)" onclick="tops('sorting','price');" > Coin Price</a>
						<a id="sortby_marketcap" href="javascript:void(0)" onclick="tops('sorting','marketcap');" class="active"> Market Cap</a>
						<a id="sortby_volume" href="javascript:void(0)" onclick="tops('sorting','volume');"> Volume 24hr</a></p>
					</div>	
				</div>
				<div class="row" id="pageData">
					
					
				</div>
			</div>
            <!-- <div class="container">
			  <ul class="pagination">
				<li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#">Next</a></li>
			  </ul>
			</div> -->
          </div>
        </section>
		
<style>
.row_background
{
	
background-color : #FFCDD2 !important;

}

.row_background3
{
	
background-color : #ccdefc !important;

}
.row_background2
{
	
background-color : #B9F6CA !important;

}
.flash
{
	margin-left: 45%;
}

.green
{
	color:green !important;
}

.red
{
	color:red !important;
}

.black
{
	color:black !important;
	font-weight:bold !important;
	padding:5px;
}



</style>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alert!</h4>
      </div>
      <div class="modal-body">
        <p>Please login to continue. <a href="<?php echo base_url("hauth/Login/Google");?>">Click Here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>