<?php
class Boost_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	public function counts2($table, $val)
	{
		if(!empty($val))
		{
			$this->db->where($val);
		}
		$this->db->from($table);
		return $this->db->count_all_results();
	}
	
	public function showNotify($type,$msg)
	{
		if($type=="success")
		{
			$msg = " <div id='SiteNotifyMessage' class=\"alert alert-success alert-dismissible\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                
              ".$msg."
              </div>";
		}
		elseif($type=="error")
		{
			$msg = " <div id='SiteNotifyMessage' class=\"alert alert-danger alert-dismissible\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                
              ".$msg."
              </div>";
		}
		
		return $msg;
	}
	
	function create_thumbnail($fileName,$dest,$width,$height,$thumb_marker) 
    {
		$config['image_library']  = 'gd2';
        $this->load->library('image_lib');
		$config['source_image']   = $fileName;       
        $config['create_thumb']   = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']          = $width;
        $config['height']         = $height;
        $config['new_image']      = $dest; 
        $config['thumb_marker']   = $thumb_marker; 		
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize())
        { 
            echo $this->image_lib->display_errors();
        }        
    }
	
	public function getValue( $table,$field,$where )
	{
    	$sql   = "SELECT ".$field." FROM ".$table." WHERE ".$where;
    	$query = $this->db->query($sql);
		$row   = $query->row();
		if($query->num_rows()>0)
		{
    	return $row->$field;
		}
		
	}
	
	public function getValue2( $table,$field,$where )
	{
		$this->db2 = $this->getHistoryDb();
    	$sql   = "SELECT ".$field." FROM ".$table." WHERE ".$where;
    	$query = $this->db2->query($sql);
		$row   = $query->row();
		if($query->num_rows()>0)
		{
    	return $row->$field;
		}
	}
	
	public function checkAdminAuthentication()
	{
    	if($this->session->userdata('admin_id')=="")
		{
			$continue = base_url(uri_string());
			redirect(base_url()."login/?continue=".$continue);
		}
	}
	
	public function loadSettings()
	{
		$this->db->select("*");
		$this->db->from(SETTINGS);
		$this->db->where("settings_id","1");
		$query = $this->db->get();
		return $query->row();
		
	}
	
	public function loadPaginationConfig()
	{
		$config['full_tag_open'] = '<div align="right" class="paging"><ul class="pagination" id="coin_page_last">';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</li></a>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
        
		return $config;
	}
	
	public function phpMail($from,$to,$message,$subject,$fromName,$cc,$bcc)
	{
		
		    /**
			This script for smtp 
			
			$config['mailtype'] = 'html';
			
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'smtp.gmail.com'; //change this
			$config['smtp_port'] = '587';
			$config['smtp_user'] = ''; //change this
			$config['smtp_pass'] = ''; //change this
			$config['smtp_crypto'] = 'tls'; 
			$config['mailtype'] = 'html';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n";


			$this->email->initialize($config); 
			**/
			
			
		    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
            $this->email->set_header('Content-type', 'text/html');
			$this->email->from($from, $fromName);
			$this->email->to($to);
			if($cc)
			{
				$this->email->cc($cc);
			}
			if($bcc)
			{
				$this->email->bcc($bcc);
			}
			$this->email->subject($subject);
			$this->email->message($message);

			$this->email->send();
			
	}
	
	function ip_details($IPaddress) 
    {
        $json       = file_get_contents("http://ipinfo.io/{$IPaddress}");
        $details    = json_decode($json);
        return $details;
    }
	
	public function getHistoryDb()
	{
		$dsn1 = 'mysqli://cryptobu_coin:cryptobutt@123@localhost/cryptobu_crypto2';
        $this->master_db = $this->load->database($dsn1, true); 

        return 	$this->master_db;	
	}
	
	public function getnormaldb()
	{
		$dsn1 = 'mysqli://cryptobu_coin:cryptobutt@123@localhost/cryptobu_coin';
        $this->master_db = $this->load->database($dsn1, true); 

        return 	$this->master_db;	
	}
	
	public function clear_all_cache()
	{
    $CI =& get_instance();
$path = $CI->config->item('cache_path');

    $cache_path = ($path == '') ? APPPATH.'cache/' : $path;

    $handle = opendir($cache_path);
    while (($file = readdir($handle))!== FALSE) 
    {
        //Leave the directory protection alone
        if ($file != '.htaccess' && $file != 'index.html')
        {
           @unlink($cache_path.'/'.$file);
        }
    }
    closedir($handle);
	
	if(file_exists(BASE_PATH."error_log"))
		{
			unlink(BASE_PATH."error_log");
			//echo "error log remove from frontend";
		}
	
	
		}
}
?>