<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url();?>"><b>Admin</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

	<span class="red"><?php if(isset($error)) echo $error;?></span>
	  <?php if($this->input->get('continue')!=""){ $continue = "?continue=".$this->input->get('continue');}?>
    <form action="<?php echo base_url();?>login/<?php if(isset($continue)){ echo $continue; }?>" method="post">
	<input type="hidden" name="act" value="login"/>
	
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo set_value('username');?>">
		
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		<span class="red"><?php echo form_error('username');?></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo set_value('password');?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		<span class="red"><?php echo form_error('password');?></span>
      </div>
	    <div class="form-group has-feedback">
	     <?php echo $captcha;?>
	  </div>
	  
	   <div class="form-group has-feedback">
    
	     <input type="text" name="captcha" class="form-control" placeholder="Captcha">
		
      
		<span class="red"><?php echo form_error('captcha');?></span>
      </div>
      <div class="row">
       
        <!-- /.col -->
        <div class="col-xs-3">
          <button type="submit" class="btn btn-primary">Sign In</button>
        </div>
		 <div align="left" class="col-xs-2">
          <a href="<?php echo base_url('forgot_password');?>"><button type="button" class="btn btn-primary">Forgot Password</button></a>
        </div>
        <!-- /.col -->
      </div>
    </form>

  

    
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
