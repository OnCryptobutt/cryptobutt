<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Banner
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="add">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="banner_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo set_value('banner_name');?>">
					<span class="red"><?php echo form_error('banner_name');?></span>
                  </div>
                </div>
				
               <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="banner_content"><?php echo set_value('banner_content');?></textarea>
					<span class="red"><?php echo form_error('banner_content');?></span>
                  </div>
                </div>-->
				
				
			 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                   <input type="file" name="banner_photo" value=""/>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="banner_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo set_value('banner_order');?>">
					<span class="red"><?php echo form_error('banner_order');?></span>
                  </div>
                </div>
				
				
				
				
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" checked='true' name="banner_status" value="1"> Enable<br/>
					<input type="radio" name="banner_status" value="0"> Disable
                  </div>
                </div>
                
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>banner/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  