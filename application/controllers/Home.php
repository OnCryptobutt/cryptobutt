<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('home_model');
		//$this->load->library('curl');
		$this->detect = new Mobile_Detect();
		
		
    }
	
	public function index()
	{
		$data = "";
		$data['title'] = $this->settings->settings_site_title;
		$data['meta_keywords'] = $this->settings->settings_meta_keywords;
		$data['meta_description'] = $this->settings->settings_meta_description;
		$time_start = microtime(true);
		$data['basename'] = "home";
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		
		if($this->session->userdata('user_id')){
		  $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
			 if($query_user->num_rows()>0)
			 {
				 $query_user_row = $query_user->row();
			 }
			 
			 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$data["total_coin"] = $this->home_model->total_coin();
		//$data['js'] = $this->load->view('home_js',$data,TRUE);
		$data['js'] = " ";
		$this->load->view('template/header',$data);
		$this->load->view('home_page',$data);
		$this->load->view('template/footer',$data);
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		$totalSecs   = ($execution_time * 60); 
		$this->db->close();
	}
	
	function getPagination($count){
      $paginationCount= floor($count / $this->settings->settings_records_per_page_front);
      $paginationModCount= $count % $this->settings->settings_records_per_page_front;
      if(!empty($paginationModCount)){
               $paginationCount++;
      }
      return $paginationCount;
     }

	public function wallet()
	{
	    $data = "";
		$data['title'] = $this->settings->settings_site_title;
		$data['meta_keywords'] = $this->settings->settings_meta_keywords;
		$data['meta_description'] = $this->settings->settings_meta_description;
		//echo phpinfo();
		$time_start = microtime(true);
		$data['basename'] = "home";
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		if($this->session->userdata('user_id')){
		  $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
			 if($query_user->num_rows()>0)
			 {
				 $query_user_row = $query_user->row();
			 }
			 
			 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		$data["total_coin"] = $this->home_model->total_coin();
		$data['js'] = $this->load->view('home_js',$data,TRUE);
	    $this->load->view('template/header',$data);
        $this->load->view('wallet_page',$data);
		$this->load->view('template/footer',$data);
	}
	
	public function help()
	{
	    
	    $data = "";
		$data['basename'] = "Crypto asset help | CryptoButt";
		if($this->input->get('currency'))
		{
		    $currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$this->load->view('help_page',$data);
	}
     
}
