<!DOCTYPE html>

<html lang="en">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://use.fontawesome.com/c596d80306.js"></script>

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">

	<!-- <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet"> -->

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">

	<title><?php if(isset($title)){ echo $title; }?></title>
	<meta name="keyword" content="<?php if(isset($meta_keywords)){ echo $meta_keywords; }?>"/>
	<meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; }?>"/>
	
	<style type="text/css">
ul.tsc_pagination { padding:0px; height:100%; overflow:hidden; font:12px \'Tahoma\'; list-style-type:none; }
ul.tsc_pagination li { float:left; margin:0px; padding:0px; margin-left:5px; }
ul.tsc_pagination li:first-child { margin-left:0px; }
ul.tsc_pagination li a { color:black; display:block; text-decoration:none; padding:7px 10px 7px 10px; }
ul.tsc_pagination li a img { border:none; }
ul.tsc_paginationC li a { color:#707070; background:#FFFFFF; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; border:solid 1px #DCDCDC; padding:6px 9px 6px 9px; }
ul.tsc_paginationC li { padding-bottom:1px; }
ul.tsc_paginationC li a:hover,
ul.tsc_paginationC li a.current { color:#FFFFFF; box-shadow:0px 1px #EDEDED; -moz-box-shadow:0px 1px #EDEDED; -webkit-box-shadow:0px 1px #EDEDED; }
ul.tsc_paginationC01 li a:hover,
ul.tsc_paginationC01 li a.current { color:#893A00; text-shadow:0px 1px #FFEF42; border-color:#FFA200; background:#FFC800; background:-moz-linear-gradient(top, #FFFFFF 1px, #FFEA01 1px, #FFC800); background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #FFFFFF), color-stop(0.02, #FFEA01), color-stop(1, #FFC800)); }
ul.tsc_paginationC li a.In-active {
   pointer-events: none;
   cursor: default;
}
</style>

  </head>

  <body>
  
  <?php if($this->input->get('currency'))
			{
				$currency = $this->input->get('currency');
			}
			else
			{
					if($this->session->userdata('user_id'))
					{
						$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
					
						
					}
					else
					{
						$currency = "";
					}
					
					if($currency=="")
					{
					$currency = "USD";
					}
			}
			?>

    <section class="container-fluid">

		<header class="top">

			<figure class="figure">

				<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>img/logo.png" class="figure-img img-fluid rounded logo hidden-xs-down" alt="Criptobutt"></a>

				<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>img/m_logo.png" class="figure-img img-fluid rounded logo hidden-sm-up" alt="Criptobutt"></a>

			</figure>

			<section class="float-lg-right main_nav">

				<span><a class="navbar-brand active" href="<?php echo base_url();?>"><i class="fa fa-connectdevelop" aria-hidden="true"></i> Coin</a></span>

				<span><a class="navbar-brand" href="#"><i class="fa fa-search" aria-hidden="true"></i> Coin Search</a></span>

				
				
				<?php if($this->session->userdata('user_id')){
           
			?>
			
            <!--<li class="list-inline-item logout"><a id="logout" href="<?php echo base_url('Logout');?>" class="nav-link">Logout <i class="fa fa-sign-out" aria-hidden="true"></i>	</a></li>-->
			
			 
			 	
				
				<span><a class="navbar-brand active" href="<?php echo base_url('profile');?>"><img class="img-fluid user" src="<?php echo $query_user_row->photoURL;?>"> <span class="coin_cap"><?php echo $query_user_row->firstName;?></span></a></span>
				
			<?php } else { ?>
			
				<span><a class="navbar-brand" href="<?php echo base_url('hauth/login/Google');?>"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Login</a></span>
			<?php } ?>

			</section>

		</header>
