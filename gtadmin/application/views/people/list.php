 <style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {    vertical-align: middle; }
    .btn{margin:10px;}
    /* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        People
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url()?>people/">people</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
			
			
			
			<div class="box-body">
			    	<?php if($success) echo $success;?>
			    <form id="form2" name="form2" method="post" action="">
			     <div align="right">
                 
                 <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
				  <br/>
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
							    <th><input type="checkbox" id="selecctall"/></th>
								<th>COIN</th>
								<th>PICTURE</th>
								<th>FULL NAME</th>
								<th>ROLE TYPE</th>
								<th>ROLE TITLE</th>
								<th>SOCIAL</th>
								<th>VERIFY</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						
						<tbody id="appendContent">
						<?php if(is_array($keypeople_name))
						{ 
					       foreach($keypeople_name as $people)
						   {
					        ?>
							<tr><td><input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $people->keypeople_id;?>"> </td>
								<td><?php echo $people->keypeople_coinid;?></td>
								<td>
								
								<?php if($people->keypeople_filename!='') { ?>
								
								   <img style="height:50px" src="<?php echo 'https://www.cryptobutt.com/img/'.$people->keypeople_filename;?>" class="img-responsive">
								   
								<?php } else { ?>
								
								    <img style="height:50px" src="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" class="img-responsive">
									
								<?php } ?>
								
								</td>
								
								<td><?php echo $people->keypeople_fullname; ?></td>
							
								<td><?php echo $people->keypeople_roletype;?></td>
								<td><?php echo $people->keypeople_roletitle;?></td>
								<td>
								<select id="socialcontent" name="socialcontent" class="form-control" onchange="socialFun(this.value, <?php echo $people->keypeople_id ?>)">
								    <option value="empty">--select--</option>
								    <?php
								            $vLink = array();
								            $sql = "SELECT * FROM ci_keypeoplesociallink WHERE social_people_id='".$people->keypeople_id."'";
                                    	    $query = $this->db->query($sql);
                                    	    if ($query->num_rows() > 0)
                                    	   {
                                    			foreach($query->result() as $row)
                                    			{
                                    			    $sql1 = "SELECT * FROM ci_link_type WHERE link_type_id='".$row->social_links_type."'";
                                    	            $query1 = $this->db->query($sql1);
                                    	            if ($query1->num_rows() > 0)
                                    	            {
                                	                	foreach($query1->result() as $row1)
                                    		        	{
                                    		        	    array_push($vLink,$row1->link_type_id);
                                    				?>
                                    				<option value="<?php echo $row1->link_type_id; ?>" <?php if($row->social_verify_link=='true'){ echo "selected='selected'"; }  ?>><?php echo $row1->link_type_name; ?></option>
                                    				<?php
                                    		        	}
                                    	            }
                                    			}
                                           }
								    ?>
										
									
                                    </select>
								
								</td> 
								<td><div id="<?php echo $people->keypeople_id;?>">
								    <!--<label for="label<?php echo $people->keypeople_id;?>" class="btn <?php if($people->verify_link=="true"){?> btn-success <?php } else{ echo 'btn-danger'; } ?>">VERIFY  
<input type="checkbox" <?php if($people->verify_link=="true"){?> checked='true'<?php } ?> id="label<?php echo $people->keypeople_id;?>" class="badgebox1 badgebox"  name="verify"  onchange="if(this.checked) this.value='true'; else this.value='false';verifyFun(this.value, <?php echo $people->keypeople_id ?>);">
        <span class="badge">✓</span></label>-->
        
        <?php 
         if ($query->num_rows() > 0)
    	   {
    			foreach($query->result() as $row)
    			{
    			    if($row->social_verify_link=='true') {
        ?>
         <?php if($row->social_verify_link=="true"){ ?>
        <span class="badge">✓</span>     <?php
         }
    			} } }
    			?>
    			
    			
    			
        
        </div></td>
        
                            <td>
                                <?php 
                               // print_r($vLink);
                               // if(empty($vLink)) {
                                ?>
                                <button type="button" name="add" onclick="updatePeopleDetails(<?php echo $people->keypeople_id;?>)" class="btn btn-sm btn-info"/>Add</button>
                                <?php
                               // }
                                ?>
                            <span id="badge<?php echo $people->keypeople_id;?>">
                                <?php
                                if($people->verify_link == 'true') {
                                ?>
                                <span class="badge">✓</span>
                                <?php
                                }
                                ?>
                                
                            </span>
                            </td>
                             <td><button type="button" name="social" onclick="GetPeopleDetails(<?php echo $people->keypeople_id;?>)" id="<?php echo $people->keypeople_id;?>" class="btn btn-sm btn-info"/>Social</button></td>
							</tr>
						   <?php } } 
							 else{					  ?>
			  
			  <tr><td align="center" colspan="5"><font color="red">--No records found!--</font></td></tr>
			  <?php } ?>
						</tbody>
					</table>
				    </form>
				</div>
			
			
			
			
			
	<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Profile</h4>
            </div>
            <form method="post" id="sociallinkform" action="<?php echo base_url().'people/addSocialLink' ?> ">
            <div class="modal-body">
                 <div id="InputSlider3">
                <div class="row">
                          
                             <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                 <div class="form-group key-text">
                                    <label for="fullname" style="font-weight:bold;">Add profile link</label><br>
                                    <small class="form-text text-muted">e.g. Linkedin, Twitter, Blog</small>
                                    <input type="text" class="form-control ui-autocomplete-input" id="profilelink1" name="url1" value="" autocomplete="off">
                                  </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                 <div class="form-group key-text">
                                    <label for="fullname" style="font-weight:bold;">Profile website</label><br>
                                    <small class="form-text text-muted">Select the website</small>
                                    <select id="profileweb1" name="type1" class="form-control">
                                       <option value=""></option>
                                    		<?php
                						if(is_array($link_type_list))
                                        {
                                        	foreach($link_type_list as $link_type_lis)
                                        	{
                                        	?>
                                        	<option value="<?php echo $link_type_lis->link_type_id;?>"><?php echo $link_type_lis->link_type_name;?></option>
                                        	<?php
                                        	}
                                        }
                                        ?>
                                    </select>
                                  </div>
                                  </div>
                                 
                             </div>
                             </div>
                             <div class="text-center">
                                 <p id="AddMoreImageBox3" style="font-size: 18px;color: #187db7;font-weight: 600;cursor:pointer;">+ Add More</p> </div>
                             
                             
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary"  >Save Changes</button>
                <input type="hidden" id="hidden_user_id" name="peopleid">
            </div>
        </div>
        </form>
    </div>
</div>
			
			
			
			
			
			
		<!--	<form id="form1" name="form1" method="post" action="">
			 
				  
            <!-- /.box-header -->
           <!-- <div class="box-body">
			
			<?php if($success) echo $success;?>
			
			 <div align="right">
                  <a href="<?php echo base_url();?>people/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>
                  <!--<input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>-->
                  <!--<input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
				  <br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th><input type="checkbox" id="selecctall"/></th>
				  <th>Image</th>
                  <th>People Name</th>
                  <!--<th>Profile Verified</th>-->
                 <!-- <th>Action</th>
                </tr>
                </thead>
                <tbody>
				
					  <?php 
					 
					  if(is_array($lists)){
					  foreach($lists as $list){
						
						  ?>
					  <tr>
					  
					    <td><input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $list->people_id;?>"> </td>
						  <td>  <?php if($list->people_photo){ echo "<img width='50' height='50' src='".PEOPLE_URL."".$list->people_photo."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?></td>
						<td><?php echo $list->people_first_name;?></td>
						<!--<td>
						<?php if($list->people_verified=='Y'){?><span class="label label-success">YES</span><?php } ?>
						<?php if($list->people_verified=='N'){?><span class="label label-danger">NO</span><?php } ?>
						<?php if($list->people_verified=='P'){?><span class="label label-warning">PENDING</span><?php } ?>
						</td>-->
					<!--	<td><a href="<?php echo base_url();?>people/edit/<?php echo $list->people_id;?>/"><span class="fa fa-pencil"></span></a> |
						
						<a href="<?php echo base_url();?>people/view/<?php echo $list->people_id;?>/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>
					  <?php } }

              else{					  ?>
			  
			  <tr><td align="center" colspan="5"><font color="red">--No records found!--</font></td></tr>
			  <?php } ?>
				
               
                </tbody>
                
              </table>
			  
			  <div align="right"><?php echo $pagination;?></div>
			  
            </div>
			</form>-->
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 