<input type="hidden" id="hidden_currency" value="<?php echo $currency;?>"/>

<?php

               $exist = $this->boost_model->getValue(PORTFOLIO,"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$cid."'");
				if($exist>0)
				{
					$heartcls = "";
				}
				else
				{
					$heartcls = "-o";
				}
					?>
					

<section id="coin_bundle" class="cd_main_bg">
			<section class="container coin_group cd_pad_mob">
				<div class="row">	
					<div class="col-lg-12 coin_tab cd_bg">
						<ul class="nav nav-tabs">
							<li><a class="active" data-toggle="tab" href="#home" aria-expanded="false"><i class="fa fa-bar-chart" aria-hidden="true"></i> <?php echo $details[0]['coin_name']; ?></a></li>
							<!-- <li><a data-toggle="tab" href="#menu1" class="" aria-expanded="false"><i class="fa fa-line-chart" aria-hidden="true"></i> Market</a></li> -->
							<li><a data-toggle="tab" href="#menu2" class="" aria-expanded="true"><i class="fa fa-calendar" aria-hidden="true"></i> Historical data</a></li>
						</ul>
						<!--<div class="cd_hours">
							<a href="#">1H</a>
							<a class="cd_hours_active" href="#">1D</a>
							<a href="#">1W</a>
							<a href="#">1M</a>
							<a href="#">1Y</a>
							<a href="#">All</a>
						</div>-->
						<div class="tab-content">
							<div id="home" class="tab-pane in active" aria-expanded="false">
							  	
								<section class="container coin_group">
									<!-- <table class="table table-bordered cd_bit_top">
									  <thead>
										<tr class="cd_main_fn">
											<th scope="col" class="align-middle cd_coin_1r dc_ico_sing">
												
												<span class="coin_img"><img class="cd_ind_coin" src="img/btc.png">
													<span class="coin_sh cd_coin_sh">BTC</span>
												</span>
												
												- <i class="fa fa-usd" aria-hidden="true"></i><span class="coin_name">14,499.00 
												<a class="coin_fav" href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
												<br>
												
												<span class="btc_prize btc_txt">BITCOIN PRICE</span>
											</th>
											<th scope="col" class="align-middle cd_coin_1r">
												<span>- <i class="fa fa-usd" aria-hidden="true"></i> 366.63</span><br>
												<span class="btc_txt">SINCE YESTERDAY (USD)</span>
											</th>
											<th scope="col" class="align-middle cd_coin_1r">
												<span>−2.47%</span><br>
												<span class="btc_txt">SINCE YESTERDAY (%)</span>
											</th>
										</tr>
									  </thead>
									  
									</table> -->
									<div class="row cd_value_diff cd_bit_top">
										<div class="col-4 col-md-4 cd_box cd_align_center cd_coin_1l">
											<span class="coin_img"><img class="cd_ind_coin" src="<?php echo COIN_URL.$details[0]['coin_image_name']; ?>">
													<span class="coin_sh cd_coin_sh"><?php echo $details[0]['coin_name']; ?></span>
												</span>
												
												<span class="coin_name"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i><?php echo $details[0]['price']; ?> 
												<a class="coin_fav" href="#" onclick="addportfolio('<?php echo $cid; ?>')" id="heart_<?php echo $cid; ?>"><i class="fa fa-heart<?php echo $heartcls;  ?> heart_cor" aria-hidden="true"></i></a>
												
												
												
												</span>
												<br>
												
												<span class="btc_prize btc_txt"><?php echo $details[0]['coin_name']; ?> PRICE</span>
										</div>
										<div class="col-4 col-md-4 cd_box cd_align_center">
											<p class="coin_name"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo number_format($details[0]['change24hour'],3); ?></p>
											<p class="btc_txt">SINCE YESTERDAY (USD)</p>
										</div>
										<div class="col-4 col-md-4 cd_box cd_align_center cd_coin_1r">
											<p class="coin_name"><?php echo number_format($details[0]['changepct24hour'],3); ?>%</p>
											<p class="btc_txt">SINCE YESTERDAY (%)</p>
										</div>
										
									</div>
									
									<div id="container" >
	
								  <!-- <div class="highcharts-background" style="background-color: white;font-size: 23px;padding-bottom: 40px;padding-top: 40px;">
									 <span style="padding-left: 20px;">Bitcoin Charts</span> 
									 <span style="padding-left: 760px;padding-top: 40px;">
										<a href="<?php echo base_url('coin_page/chart/'.$cname.'?currency='.$currency); ?>" target="_blank"><button type="button" class="btn btn-warning" style="background-color: #FFD700 !important; color: black !important;"><i class="fa fa-external-link" aria-hidden="true"></i> Open in new window</button></a>
									 </span>
								   </div>-->
								   
								<!-- chart show id --> 
								
								   <div id="container1" > </div>
								   <div align="center" class="flash2"></div>
								   
								<!-- -->   
								   
	   

	                       </div>
									
								</section>
							</div>
							<!-- <div id="menu1" class="tab-pane" aria-expanded="false">
							  <h3>Market</h3>
							  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div> -->
							<div id="menu2" class="tab-pane" aria-expanded="true">
								
						 <h5>Historical data for <?php echo $details[0]['coin_name']; ?></h5>
						  
						       <div class="pull-right col-xs-7 col-sm-3 col-md-3 col-lg-3 alg sect dsk_align mar-top2 desktop" >
								<div  class="flash2"></div>
									<!--<div class="btn-group pull-right" id="reportrange2" >
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="dateshow"><span></span>
											<div class="caret"></div>
										</button>
									</div>-->
									
									<style>
									.caret {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px solid;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
}
									</style>
									
									<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span> <b class="caret"></b>
</div>
								</div>
						  
						  
						      <div class="card-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th scope="col">Date</th>
												<th scope="col">Open</th>
												<th scope="col">High</th>
												<th scope="col">Low</th>
												<th scope="col">Close</th>
												<th scope="col">Volume</th>
												<th scope="col">Market Cap</th>
											</tr>
										</thead>
										
										<tbody id="pageData">
                                        </tbody>
										
									</table>
									<div align="center" class="flash1"></div>
							 </div>
							 
						
							</div>
						</div>
					</div>
				</div>
				
				<div class="row cd_value_diff">
					<div class="col-3 col-md-3 cd_box cd_align_center">
						<p>Marketing Cap</p>
						<p class="cd_amount"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo $details[0]['mktcap']; ?></p>
					</div>
					<div class="col-3 col-md-3 cd_box cd_align_center">
						<p>Volume (24h)</p>
						<p class="cd_amount"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo $details[0]['volume24hour']; ?></p>
					</div>
					<div class="col-3 col-md-3 cd_box cd_align_center">
						<p>Circulating Supply</p>
						<p class="cd_amount"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo $details[0]['supply']; ?> <span>BTC</span> </p>
					</div>
					<div class="col-3 col-md-3 cd_box cd_align_center">
						<p>Max Supply</p>
						<p class="cd_amount"><i class="fa fa-<?php echo strtolower($currency); ?>" aria-hidden="true"></i> <?php echo $details[0]['total_coin']; ?><span>BTC</span> </p>
					</div>
				</div>
				
				<div class="row cd_value_diff">
					<div class="col-4 col-md-4 cd_box">
						<ul class="dc_links">
						<?php  if(isset($coin->website) AND $coin->website!='') {?>
							<li><a href="<?php echo $coin->website; ?>"><i class="fa fa-link" aria-hidden="true"></i> Website</a></li>
							<?php } ?>
							<?php  if(isset($coin->website2) AND $coin->website2!='') {?>
							<li><a href="<?php echo $coin->website2; ?>"><i class="fa fa-link" aria-hidden="true"></i> Website 2</a></li>
							<?php } ?>
							<?php  if(isset($coin->explore) AND $coin->explore!='') {?>
							<li><a href="<?php echo $coin->explore; ?>"><i class="fa fa-search" aria-hidden="true"></i> Explorer</a></li>
							<?php } ?>
							<?php  if(isset($coin->explore2) AND $coin->explore2!='') {?>
							<li><a href="<?php echo $coin->explore2; ?>"><i class="fa fa-search" aria-hidden="true"></i> Explorer 2</a></li>
							<?php } ?>
							<?php  if(isset($coin->message_board1) AND $coin->message_board1!='') {?>
							<li><a href="<?php echo $coin->message_board1; ?>"><i class="fa fa-navicon" aria-hidden="true"></i> Message Board</a></li>
							<?php } ?>
							<?php  if(isset($coin->message_board2) AND $coin->message_board2!='') { ?>
							<li><a href="<?php echo $coin->message_board2; ?>"><i class="fa fa-navicon" aria-hidden="true"></i> Message Board 2</a></li>
							<?php } ?>
							<li><i class="fa fa-star" aria-hidden="true"></i> <span class="badge badge-success">Rank<?php echo $details[0]['rank']; ?></span></li>
							
							<?php  if($coin->Mineable=='1' || $coin->currency=='1') { ?>
							<li><i class="fa fa-tag" aria-hidden="true"></i> <?php  if(isset($coin->Mineable) AND $coin->Mineable!='') {?><span class="badge badge-warning"><?php if($coin->Mineable=='1'){ echo "Minneable"; } ?></span> <?php } ?> <?php  if(isset($coin->currency) AND $coin->currency!='') {?><span class="badge badge-warning"><?php if($coin->currency=='1'){ echo "Coin"; } ?></span><?php } ?></li>
							<?php } ?>
						</ul>
					</div>
					<div class="col-4 col-md-4 cd_box">
						<p class="cd_amount"><?php echo $details[0]['coin_name']; ?> Social Media Feeds</p>
						<div class="row">
							<!--<div class="col-9 col-md-9"><p>Tweets <a href="#">by @ Bitcoin</a></p></div>
							<div class="col-3 col-md-3 text-right"><a href="#"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></a></div>-->
						</div>
						
						<div class="twits">	
<a class="twitter-timeline" data-width="300" data-height="400" href="<?php echo $details[0]['twitter_url']; ?>"></a> 

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

						</div>
						
					</div>
					<div class="col-4 col-md-4 cd_box">
						<!-- <p>Circulating Supply</p>
						<p class="cd_amount"><i class="fa fa-usd" aria-hidden="true"></i> 16,682,637</p> -->
					</div>
					
				</div>
				
				
						<?php
                            if($this->session->userdata('user_id'))
							{
								$currency3 = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
								
								 $sym=$this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency3."'");
							}
							else
							{
								$currency3 = "";
							}
							
							if($currency3=="")
							{
							  $currency3 = "USD";
							  $sym="$";
							}
					?>
				
				<div class="row cd_value_diff cd_bg">
				
				
				<div class="col-8 col-md-8">
						  <h5><?php echo $details[0]['coin_name']; ?> Market</h5>
						  </div>
						  <!--<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
						  
						  
						  <div class="col-4 col-md-4 text-right country">
									<div class="btn-group">
										<!--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">$ USD <span class=caret"></span></button>-->
												<div align="center" class="flash3"></div>
									
										
										
										
										
										<select onchange="currency_change(this.value)" id="select">
											
									<?php if(is_array($this->settings->currency_list)) {
									   foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										<!--<li><a href="#"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>-->
										
					                   <option value="<?php echo $currency_list->currency_code;?>@<?php echo $currency_list->symbol;?> "><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></option>
										
									  
										
									   <?php } } ?>
										
										</select>
									</div>
						  </div>
						  
						 <div class="col-12 col-md-12 cd_market_h">
						
					  <div id="cd_market"> 
						  
						 <div class="card-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th scope="col">#</th>
												<th scope="col">Source</th>
												<th scope="col">Pair</th>
												<th scope="col">Volume (24h)</th>
												<th scope="col">Price</th>
												<th scope="col">Volume (%)</th>
												<th scope="col">Updated</th>
											</tr>
										</thead>
										
										 <tbody id="pageData2">
				
                                         </tbody>
										
										
										
									</table>
									
						            <!--<div align="center" class="flash"></div>-->
									
						           <!--<span id="market_count"><?php if(isset($count)) { echo $count; }?></span>-->
								   <input type="hidden" id="market_count" value="<?php echo $count; ?>">
								   <input type="hidden" id="row_nos" value="0">
								   <input type="hidden" id="page_id" value="0">
								   <input type="hidden" id="currency_value" value="">
                          <!--- for pagination -->
						
								</div>
								
							</div>
							
							</div>
						  
						  
						  
						</div>
				
				</div>
			</section>
		</section>	