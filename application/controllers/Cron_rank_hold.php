<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Cron_rank extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->detect = new Mobile_Detect();
    }
	
	

	
	function index()
	{
		$time_start = microtime(true);
		
		$data = "";
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }

		 if(isset($_POST['per_page']))
		 {
			
			 if($_POST['per_page']>0)
			 {
				  $per_page = $_POST['per_page'];
			 }
			 else
			 {
				 $per_page = $this->settings->settings_records_per_page_front;
			 }
		 }
		 else
		 {
			 $per_page = $this->settings->settings_records_per_page_front;
		 }
        $pageLimit= $per_page*$id;

		$ar = "";
		$ar1="";
		if(isset($_POST['e1']))
		{
			$ar1 = explode(",",$_POST['e1']);
			foreach($ar1 as $key=>$value)
			{
				$ar[$key] =$value;
			}
		}

		
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				$row_columns[] = $row2;
			}
		}
	
	if($this->input->post('currency'))
		{
			$currency = $this->input->post('currency');
		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "";
			}
			
			if($currency=="")
			{
			$currency = "USD";
			}
		}
	
		if($this->session->userdata('user_id'))
			{
				
				$update_array = array("base_currency"=>$currency);
				$this->db->set($update_array)->where("id",$this->session->userdata('user_id'))->update(USER);
			}
			
			
			//$sorting = "order by ci_coin.sortorder asc";
			
			$sorting_option = $this->input->post('sorting');
			
			//$sorting = "order by convert(h.".$currency.",decimal) desc";
			
				if($sorting_option=="price")
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".price AS DECIMAL(38,10)) desc";
			}
			elseif($sorting_option=="marketcap")
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) desc";
			}
			elseif($sorting_option=="volume")
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".volume24hour AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) desc";
			}
			
			
			
			//convert(h.USD, float) desc
			
			if($sorting_option=="price")
			{
				$columnid = '1';
			}
			elseif($sorting_option=="marketcap")
			{
				$columnid = '15';
			}
			elseif($sorting_option=="volume")
			{
				$columnid = '6';
			}
			else
			{
				$columnid = '15';
			}
			
			$search_text = $this->input->post('search');
			
			if(trim($search_text)!="")
			{
				$ser = "AND ".COIN.".coinname LIKE '".$this->input->post('search')."%'";
			}
			else
			{
				$ser ="";
			}
			
	
		
		$sql = "SELECT *
FROM ci_coin
LEFT JOIN ci_coin_history_temp_".$currency." ON ci_coin.id = ci_coin_history_temp_".$currency.".coin_id
 where  ".COIN.".status='1' $ser $sorting";
		
		$query =$this->db->query($sql);
		
		
		//echo $this->db->last_query();
		//exit;
		
		
		/**$this->db->select('*');
		$this->db->select(COIN_HISTORY.".id as hid");
		$this->db->from(COIN);
		$this->db->join(COIN_HISTORY,COIN.".id=".COIN_HISTORY.".coin_id",'left');
		$this->db->group_by(COIN.'.id'); 
		
		$this->db->where(COIN.".status","1");
	//$this->db->order_by(COIN.".sortorder","asc");
	$this->db->order_by("hid","desc");
	$this->db->limit($per_page,$pageLimit);
	
		if($this->input->post('search'))
		{
			$this->db->like("coinname",$this->input->post('search'));
		}
		
		
		
		
		
		
			$query = $this->db->get(); **/
			
			$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
			
			

		if ($query->num_rows() > 0)
		{
            foreach ($query->result_array() as $row)
			{
				
                $data[] = $row;
            }
			
			echo "Rank updated";
          
        }
		
		

		
		
		
		if (is_array($data))
		{
			$i=1;
			$ids =array();
            foreach ($data as $row)
			{
				$list = $row;
				$list['id'] = $list['coin_id'];
				$ids[] = $list['name'];
				$update_data = array("sortorder"=>$i);
				$this->db->set($update_data)->where("id",$list['id'])->update(COIN);
				$i++;
				
                
            }
			
			//print_r($ids);
			//array_chunk($ids,2)
			
			$ids_implode = implode(",",$ids);
			
			echo $ids_implode;
            
        }
		else
		{
			echo "<center><font color='red'>--No records found--</font></center>";
		}
		
		
exit();
}

}
