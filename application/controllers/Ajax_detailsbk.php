<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_details extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url());
		}
    }
	
	function load_details()
	{
		if($this->input->post('pid') && $this->input->post('bought') && $this->input->post('exchange') && $this->input->post('amount') && $this->input->post('pid') && $this->input->post('date') && $this->input->post('month') && $this->input->post('year') && $this->input->post('currency'))
		{
			$date = $this->input->post('year')."-".$this->input->post('month')."-".$this->input->post('date');
			
			$price = $this->boost_model->getValue(COIN_HISTORY,$this->input->post('currency'),"coin_id='".$this->input->post('coin_id')."' AND column_id='1' order by id desc");
			
			
			
		$insert_data = array("portfolio_details_coin_id"       => $this->input->post('coin_id'),
		                     "portfolio_details_coin_brought"  => $this->input->post('bought'),
							 "portfolio_details_exchange_type" => $this->input->post('exchange'),
							 "portfolio_details_amount"        => $this->input->post('amount'),
							 "portfolio_details_portfolio_id"  => $this->input->post('pid'),
							 "portfolio_details_date"          => $date,
							 "portfolio_currency"              => $this->input->post('currency'),
							 "portfolio_currency2"             => $this->input->post('currency2'),
							 "portfolio_amount2"               => $this->input->post('amount2'),
							 "coin_price"                      => $price,
							 "portfolio_details_created_time"  => NOW,
							 "portfolio_details_updated_time"  => NOW
							 );
		$this->db->set($insert_data)->insert(PORTFOLIO_DETAILS);
		}
		
		$this->db->select('*');
		$this->db->from(PORTFOLIO_DETAILS);
		$this->db->where("portfolio_details_portfolio_id",$this->input->post('pid'));
		$query = $this->db->get();
		if($query->num_rows()>0)
					  {
					  
						echo '<table class="table table-striped custab">
							<thead class="table-info">
								<tr>
									<th>#</th>
									<th>Date</th>
									<th>Coin brought</th>
									<th>Exchange type</th>
									<th>Amount spent</th>
									<th></th>
								</tr>
							</thead>';
							
							$i=1;
							foreach($query->result_array() as $row_details)
							{
								$coinname = $this->boost_model->getValue(COIN,"coinname","id='".$row_details['portfolio_details_coin_id']."'");
								
								$name = $this->boost_model->getValue(COIN,"name","id='".$row_details['portfolio_details_coin_id']."'");
							
							$currencysymbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$row_details['portfolio_currency']."'");
							
							if($row_details['portfolio_amount2'])
							{
								$coinp = '<div class="coin_dis">1 '.$row_details['portfolio_currency'].' @ '.$row_details['portfolio_amount2'].' '.$row_details['portfolio_currency2'].'</div>';
							}
							else
							{
								$coinp = "";
							}
								
							echo '<tr id="portdetaildel'.$row_details['portfolio_details_id'].'">
								<td>'.$i.'</td>
								<td>'.$row_details['portfolio_details_date'].'</td>
								<td>'.$row_details['portfolio_details_coin_brought'].' '.$coinname.'
								<div class="coin_dis">1 '.$name.' @'.$currencysymbol.' '.$row_details['coin_price'].'</td>
								<td>'.$coinname.' / '.$row_details['portfolio_details_exchange_type'].' </td>
								<td>'.$row_details['portfolio_details_amount'].' '.$row_details['portfolio_details_exchange_type'].' '.$coinp.'</td>
								<td class="text-center"><a onclick="get_edit_section('.$row_details['portfolio_details_id'].')" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"> Edit &nbsp;<span class="glyphicon glyphicon-edit"></span></a><a onclick="delete_portfolio_details('.$row_details['portfolio_details_id'].','.$this->input->post('pid').')" class="btn btn-danger btn-xs"> Delete &nbsp;<span class="glyphicon glyphicon-close"></span></a></td>
							</tr>';
							$i++;
							}
						
						echo '</table>';
						
					  }
		
		exit;
	}
}