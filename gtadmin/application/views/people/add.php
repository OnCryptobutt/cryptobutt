<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        People
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="add">
              <div class="box-body">
                <!--<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo set_value('people_name');?>">
					<span class="red"><?php echo form_error('people_name');?></span>
                  </div>
                </div>-->
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_first_name" class="form-control" id="inputEmail3" placeholder="First Name" value="<?php echo set_value('people_first_name');?>">
					<span class="red"><?php echo form_error('people_first_name');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Middle Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_middle_name" class="form-control" id="inputEmail3" placeholder="Middle Name" value="<?php echo set_value('people_name');?>">
					<span class="red"><?php echo form_error('people_middle_name');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_last_name" class="form-control" id="inputEmail3" placeholder="Last Name" value="<?php echo set_value('people_name');?>">
					<span class="red"><?php echo form_error('people_last_name');?></span>
                  </div>
                </div>
                	
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Role Type</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_role_type" class="form-control" id="inputEmail3" placeholder="Role Type" value="<?php echo set_value('people_name');?>">
					<span class="red"><?php echo form_error('people_role_type');?></span>
                  </div>
                </div>
				
				<!--<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Linkedin Url</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_linked_in_url" class="form-control" id="inputEmail3" placeholder="Linkedin Url" value="<?php echo set_value('people_linked_in_url');?>">
					<span class="red"><?php echo form_error('people_linked_in_url');?></span>
                  </div>
                </div>-->
				
				
				<!--<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Profile Verified</label>

                  <div class="col-sm-10">
                    <input type="radio" checked='true' name="people_verified" value="Y"> Yes<br/>
					<input type="radio" name="people_verified" value="N"> No
					<input type="radio" name="people_verified" value="P"> Pending
                  </div>
                </div>-->
				
               <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="people_content"><?php echo set_value('people_content');?></textarea>
					<span class="red"><?php echo form_error('people_content');?></span>
                  </div>
                </div>-->
				
				
			 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                   <input type="file" name="people_photo" value=""/>
                  </div>
                </div>
				
				
				<div id="InputSlider2">
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-3">
                    <select class="form-control" name="type[]">
						<option value="">--Select--</option>
						<?php
						if(is_array($link_type_list))
{
	foreach($link_type_list as $link_type_lis)
	{
	?>
	<option value="<?php echo $link_type_lis->link_type_id;?>"><?php echo $link_type_lis->link_type_name;?></option>
	<?php
	}
}
?>
					</select>
                  </div>
				  
				  <div class="col-sm-3">
                   <input type="text" class="form-control" name="url[]" id="inputEmail3" placeholder="Add URL" value="<?php ?>">
					<span class="red"><?php ?></span>
                  </div>
				  
				  
				  
                </div>
				
				
				</div>
				
				
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <p  id="AddMoreImageBox2" style="font-size: 23px;color: #187db7;font-weight: 600;cursor:pointer;">+ Add More</p>
                  </div>

                </div>
				
				
				<!--<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="people_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo set_value('people_order');?>">
					<span class="red"><?php echo form_error('people_order');?></span>
                  </div>
                </div>-->
				
				
				
				
				
				<!--<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" checked='true' name="people_status" value="1"> Enable<br/>
					<input type="radio" name="people_status" value="0"> Disable
                  </div>
                </div>-->
                
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>people/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  