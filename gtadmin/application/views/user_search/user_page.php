 <style type="text/css">
ul.tsc_pagination { margin:4px 0; padding:0px; padding-top:20px;  height:100%; overflow:hidden; font:12px \'Tahoma\'; list-style-type:none; }
ul.tsc_pagination li { float:left; margin:0px; padding:0px; margin-left:5px; }
ul.tsc_pagination li:first-child { margin-left:0px; }
ul.tsc_pagination li a { color:black; display:block; text-decoration:none; padding:7px 10px 7px 10px; }
ul.tsc_pagination li a img { border:none; }
ul.tsc_paginationC li a { color:#707070; background:#FFFFFF; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; border:solid 1px #DCDCDC; padding:6px 9px 6px 9px; }
ul.tsc_paginationC li { padding-bottom:1px; }
ul.tsc_paginationC li a:hover,
ul.tsc_paginationC li a.current { color:#367fa9; box-shadow:0px 1px #EDEDED; -moz-box-shadow:0px 1px #EDEDED; -webkit-box-shadow:0px 1px #EDEDED; }
ul.tsc_paginationC01 li a:hover,
ul.tsc_paginationC01 li a.current { color:#ffffff; text-shadow:0px 1px #ffffff; border-color:#367fa9; background:#367fa9; background:-moz-linear-gradient(top, #367fa9 1px, #367fa9 1px, #367fa9); background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #367fa9), color-stop(0.02, #367fa9), color-stop(1, #367fa9)); }
ul.tsc_paginationC li a.In-active {
   pointer-events: none;
   cursor: default;
}

</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url()?>user/">User</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
			
			
			
			
			<form id="form1" name="form1" method="post" action="">
			 
				  
            <!-- /.box-header -->
            <div class="box-body">
			
			<?php if($success) echo $success;?>
			
			
			<div class="col-sm-12">
			<div align="left" class="col-sm-4">
			
			<input type="text" name="ajax_search" class="form-control" placeholder="Search.." onkeyup="fetch_list(this.value)">
			</div>
			 <div align="right" class="col-sm-8">
                  <a href="<?php echo base_url();?>user/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>
                  <input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>
                  <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
			</div>
				  <br/><br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th><input type="checkbox" id="selecctall"/></th>
				 
                  <th>User Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="pageData2">
				
					 
               
                </tbody>
                
              </table>
			  
			  <!--<div align="right"><?php echo $pagination;?></div>-->
			  
			  <?php if($count > 0){ 
			    if($paginationCount > 1){ 
			  
 $content ="";
$content .='<div class="pagelist"><div align="right" class="paging"><ul class="tsc_pagination tsc_paginationC tsc_paginationC01">
    <li class="first link" id="first">
        <a  href="javascript:void(0)" onclick="UserPagination(\'0\',\'first\')">F i r s t</a>
    </li>';
    for($i=0;$i<$paginationCount;$i++){
 
        $content .='<li id="'.$i.'_no" class="link">
          <a  href="javascript:void(0)" onclick="UserPagination(\''.$i.'\',\''.$i.'_no\')">
              '.($i+1).'
          </a>
    </li>';
    }
 
    $content .='<li class="last link" id="last">
         <a href="javascript:void(0)" onclick="UserPagination(\''.($paginationCount-1).'\',\'last\')">L a s t</a>
    </li>
   
</ul></div><div  class="flash"></div></div>';

echo $content;
			  } } ?>
			  
            </div>
			</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 