var  baseUrl = 'https://www.dev.cryptobutt.com/';
function change_currency(e, t) {
    $("#change_currency").html(e + " " + t)
}

function currency(e) {
    pageId = "0", liId = "0", $("#pageData").html('<span class="flash"></span>'), $(".flash").show(), $(".flash").fadeIn(400).html('<div id="loader"></div>');
    var t = "pageId=" + pageId + "&currency=" + e;
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax/load_data",
        data: t,
        cache: !1,
        success: function(t) {
            $(".flash").remove(), $(".link a").removeClass("In-active current"), $("#" + liId + " a").addClass("In-active current"), $("#pageData").html(t), $("#change_title").html(e)
        }
    })
}

function tops(e, t) {
    "tops" == e ? $("#hidden_per_page").val(t) : "currency" == e ? ($("#hidden_currency").val(t), $("#bal_al").html("Loading...")) : "sorting" == e && ($("#hidden_sorting").val(t), $("#sortby_" + t).addClass("active"), "price" == t ? ($("#sortby_marketcap").removeClass("active"), $("#sortby_volume").removeClass("active")) : "marketcap" == t ? ($("#sortby_price").removeClass("active"), $("#sortby_volume").removeClass("active")) : "volume" == t && ($("#sortby_price").removeClass("active"), $("#sortby_marketcap").removeClass("active"))), per_page = $("#hidden_per_page").val(), currency = $("#hidden_currency").val(), search = $("#hidden_search").val(), sorting = $("#hidden_sorting").val(), pageId = "0", liId = "0", $("#pageData").html('<span class="flash"></span>'), $(".flash").show(), $(".flash").fadeIn(400).html('<div id="loader"></div>');
    var a = "pageId=" + pageId + "&per_page=" + per_page + "&currency=" + currency + "&sorting=" + sorting + "&search=" + search;
    $.ajax({
        type: "POST",
        url: baseUrl+"Ajax/load_data",
        data: a,
        cache: !1,
        success: function(t) {
            $(".flash").remove(), $(".link a").removeClass("In-active current"), $("#" + liId + " a").addClass("In-active current"), $("#pageData").html(t), document.getElementById("row_no").value = 1, document.getElementById("pageids").value = 0, "tops" == e && $("#change_title").html(per_page), displaybalance()
        }
    })
}

function watch_heart() {
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax_profile/favdata",
        data: "coinId=1360",
        cache: !1,
        dataType: "json",
        success: function(e) {}
    })
}

function addwatchlist(e, t) {
    var a = "coinId=" + e + "&watchId=" + t;
    $("#remove_" + e).addClass("fa-heart").removeClass("fa-heart-o"), $("#assests_" + e + t).addClass("fa-heart").removeClass("fa-heart-o"), $.ajax({
        type: "POST",
        url: baseUrl+"ajax_profile/watchlist_data",
        data: a,
        cache: !1,
        success: function(a) {
            "true" == a ? ($("#assests_" + e + t).addClass("fa-heart").removeClass("fa-heart-o"), $.toast({
                heading: "Success",
                text: "Added your crypto asset watch list.",
                showHideTransition: "slide",
                position: "bottom-left",
                icon: "success"
            })) : $.toast({
                heading: "Error",
                text: "Already added your crypto asset watch list.",
                showHideTransition: "fade",
                position: "bottom-left",
                icon: "error"
            })
        }
    })
}

function changePagination(e, t) {
	
    search = $("#hidden_search").val(), currency = $("#hidden_currency").val(), sorting_price = $("#sorting_price").val(), sorting_market = $("#sorting_market").val(), sorting_change = $("#sorting_change").val(), sorting = $("#hidden_sorting").val(), $("#pageData").html('<span class="flash"></span>'), $(".flash").show(), $(".flash").fadeIn(400).html('<div id="loader"></div>');
    var a = "pageId=" + e + "&search=" + search + "&currency=" + currency + "&sorting_price=" + sorting_price + "&sorting_market=" + sorting_market + "&sorting_change=" + sorting_change + "&sorting=" + sorting;
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax/load_data",
        data: a,
        cache: !1,
        success: function(e) {
            $(".flash").remove(), $(".link a").removeClass("In-active current"), $("#" + t + " a").addClass("In-active current"), $("#pageData").html(e), $(".watch_heart").hover(function() {
                var e = "coinId=" + this.id;
                $.ajax({
                    type: "POST",
                    url: baseUrl+"ajax_profile/favdata",
                    data: e,
                    cache: !1,
                    dataType: "json",
                    success: function(e) {
                        e && ($(".watch" + e[0].watch_nameid).removeClass("fa-heart-o"), $(".watch" + e[0].watch_nameid).addClass("fa-heart")), $(".watch_heart").mouseout(function() {
                            var t;
                            t = e[0].watch_nameid, $(".watch" + t).removeClass("fa-heart"), $(".watch" + t).addClass("fa-heart-o")
                        })
                    }
                })
            }), $(".watchlist_home").click(function() {
                console.info("dhsjdhsjdhsjdhs.....");
                var e = "coinId=" + this.id + "&watchId=" + this.rev;
                $.ajax({
                    type: "POST",
                    url: baseUrl+"ajax_profile/watchlist_data",
                    data: e,
                    cache: !1,
                    success: function(e) {
                        "false" == e ? $.toast({
                            heading: "Error",
                            text: "Already added your crypto asset watch list.",
                            showHideTransition: "fade",
                            position: "bottom-left",
                            icon: "error"
                        }) : (console.info("hhhh...", e), $.toast({
                            heading: "Success",
                            text: "Added your crypto asset watch list.",
                            showHideTransition: "slide",
                            position: "bottom-left",
                            icon: "success"
                        }))
                    }
                })
            })
        }
    })
}

function watchmouseout(e) {
    $(".watch" + e).removeClass("fa-heart"), $(".watch" + e).addClass("fa-heart-o")
}

function watchhover(e) {
    if ($("#remove_" + e.id).hasClass("fa-heart-o")) {
        var t = "removeFromlist_" + e.id;
        document.getElementById(t).style.pointerEvents = "none", document.getElementById(t).style.color = "#ccc"
    }
    var a = "coinId=" + e.id;
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax_profile/favdata",
        data: a,
        cache: !1,
        dataType: "json",
        success: function(e) {
            e && ($(".watch" + e[0].watch_nameid).removeClass("fa-heart-o"), $(".watch" + e[0].watch_nameid).addClass("fa-heart")), $(".watch_heart").mouseout(function() {
                watchmouseout(e[0].watch_nameid)
            })
        }
    })
}

function loadmore() {
    type = "";
    var e = document.getElementById("row_no").value;
    "tops" == type ? $("#hidden_per_page").val(e) : "currency" == type ? $("#hidden_currency").val(e) : "sorting" == type && ($("#hidden_sorting").val(e), $("#sortby_" + e).addClass("selecte"), "price" == e ? ($("#sortby_marketcap").removeClass("selecte"), $("#sortby_volume").removeClass("selecte")) : "marketcap" == e ? ($("#sortby_price").removeClass("selecte"), $("#sortby_volume").removeClass("selecte")) : "volume" == e && ($("#sortby_price").removeClass("selecte"), $("#sortby_marketcap").removeClass("selecte"))), per_page = $("#hidden_per_page").val(), currency = $("#hidden_currency").val(), search = $("#hidden_search").val(), sorting = $("#hidden_sorting").val(), sorting_price = $("#sorting_price").val(), sorting_market = $("#sorting_market").val(), sorting_change = $("#sorting_change").val(), pageId = e, liId = "0";
    var t = document.getElementById("pageData");
    t.innerHTML = t.innerHTML + '<span class="flash"></span>', $(".flash").show(), $(".flash").fadeIn(400).html('<div id="loader"></div>');
    var a = "pageId=" + pageId + "&per_page=" + per_page + "&currency=" + currency + "&sorting=" + sorting + "&search=" + search + "&sorting_price=" + sorting_price + "&sorting_market=" + sorting_market + "&sorting_change=" + sorting_change;
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax/load_data",
        data: a,
        cache: !1,
        success: function(t) {
            $(".link a").removeClass("In-active current"), $("#" + liId + " a").addClass("In-active current");
            var a = document.getElementById("pageData");
            a.innerHTML = a.innerHTML + t, document.getElementById("row_no").value = Number(e) + 1, "tops" == type && $("#change_title").html(per_page), $(".flash").remove()
        }
    })
}

function selectCountry(e) {
    window.location = "https://www.cryptobutt.com/" + e, $("#coin_name").val(e), $("#suggesstion-box").hide()
}

function displaybalance() {
    var e = "currency=" + $("#hidden_currency").val();
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax_portfolio/load_portfolio_balance_display?page=home",
        data: e,
        cache: !1,
        success: function(e) {
            $("#pageData3").html(e)
        }
    })
}

function redirect(e) {
    window.location = "https://www.cryptobutt.com/?currency=" + e
}

function dropValueAtID(e, t, a, o, r) {
    "f1" == r ? $("#f1").css("color", "#FFEB3B") : $("#f1").removeAttr("style"), "f2" == r ? $("#f2").css("color", "#FFEB3B") : $("#f2").removeAttr("style"), "f3" == r ? $("#f3").css("color", "#FFEB3B") : $("#f3").removeAttr("style"), "f4" == r ? $("#f4").css("color", "#FFEB3B") : $("#f4").removeAttr("style"), "f5" == r ? ($("#f5").css("color", "#FFEB3B"), $("#filter_txt1").text("1 Hour high"), $("#filter_txt2").text("1 Hour low")) : $("#f5").removeAttr("style"), "f6" == r ? ($("#f6").css("color", "#FFEB3B"), $("#filter_txt1").text("1 Day high"), $("#filter_txt2").text("1 Day low")) : $("#f6").removeAttr("style"), "f7" == r ? ($("#f7").css("color", "#FFEB3B"), $("#filter_txt1").text("1 Week high"), $("#filter_txt2").text("1 Week low")) : $("#f7").removeAttr("style"), "f8" == r ? ($("#f8").css("color", "#FFEB3B"), $("#filter_txt1").text("1 Month high"), $("#filter_txt2").text("1 Month low")) : $("#f8").removeAttr("style"), "f9" == r ? ($("#f9").css("color", "#FFEB3B"), $("#filter_txt1").text("3 Months high"), $("#filter_txt2").text("3 Months low")) : $("#f9").removeAttr("style"), "f10" == r ? ($("#f10").css("color", "#FFEB3B"), $("#filter_txt1").text("6 Months high"), $("#filter_txt2").text("6 Months low")) : $("#f10").removeAttr("style"), "f11" == r ? ($("#f11").css("color", "#FFEB3B"), $("#filter_txt1").text("1 One high"), $("#filter_txt2").text("3 One low")) : $("#f11").removeAttr("style"), "f12" == r ? ($("#f12").css("color", "#FFEB3B"), $("#filter_txt1").text("All high"), $("#filter_txt2").text("All low")) : $("#f12").removeAttr("style"), e && $("#" + e).html(t), a && $("#" + a).val(o), "sorting_price" == a ? ($("#hidden_sorting").val("price"), $("#sorting_market_html").html("<span style='margin-left:5px;'>---</span>")) : "sorting_market" == a ? ($("#hidden_sorting").val("marketcap"), $("#sorting_price_html").html("<span style='margin-left:5px;'>---</span>")) : "sorting_change" == a && $("#hidden_sorting").val("volume"), $("#pageids").val("0"), $("#row_no").val("1")
}

function removehomeFromlist(e) {
    $("#remove_" + e).addClass("fa-heart-o").removeClass("fa-heart");
    var t = "coinId=" + e + "&userId=" + $("#user_id").val();
    $.ajax({
        type: "POST",
        url: baseUrl+"ajax_profile/removefromhomelist",
        data: t,
        cache: !1,
        success: function(e) {
            $("#remove_" + e).addClass("fa-heart-o").removeClass("fa-heart"), $.toast({
                heading: "Success",
                text: "Removed from the watch list.",
                showHideTransition: "slide",
                position: "bottom-left",
                icon: "success"
            })
        }
    })
}
watch_heart(), changePagination("0", "0"), setInterval(function() {
    $(".coin_list").each(function() {
        var e = $(this).attr("id");
        if (sorting_change = $("#sorting_change").val(), $(this).visible("partial")) {
            currency = $("#hidden_currency").val(), prev_price = $("#beat" + e + "price").html();
            var t = "id=" + e + "&currency=" + currency + "&prev_price=" + prev_price + "&sorting_change=" + sorting_change;
            $.ajax({
                type: "POST",
                url: baseUrl+"ajax/updaterow",
                data: t,
                cache: !1,
                success: function(t) {
                    var a = JSON.parse(t);
                    $("#beat" + e + "price").html(a.price), $("#beath" + e + "price").html(a.high24hour), $("#beatl" + e + "price").html(a.low24hour), $("#beat" + e + "market").html(a.mktcap), $("#beat" + e + "hour").html(Math.abs(a.volume24hour)), $("#beat" + e + "change").html(Math.abs(a.change24hour)), $("#beat" + e + "price_mobile").html(a.price), $("#beat" + e + "market_mobile").html(a.mktcap), $("#beat" + e + "volume24hour_mobile").html(a.volume24hour), $("#beat" + e + "change_mobile").html(a.change24hour), "Yes" === a.price_change && (a.change24hour < 0 && ($("#beat" + e + "minus").html("-"), $("#beat" + e + "minus2").html("-"), $("#changecolor" + e).removeClass("c_up"), $("#changecolor1" + e).removeClass("fa fa-angle-double-up"), $("#beat" + e + "change_mobile").removeClass("green"), $("#changecolor" + e).addClass("c_down"), $("#changecolor1" + e).addClass("fa fa-angle-double-down"), $("#beat" + e + "change_mobile").addClass("red"), $("#beat" + e + "change").addClass("price-bold"), $("#" + e).addClass("row_background"), setTimeout(function() {
                        $("#" + e).removeClass("row_background"), $("#beat" + e + "change").removeClass("price-bold")
                    }, 1e3)), a.change24hour > 0 && ($("#beat" + e + "minus").html("+"), $("#beat" + e + "minus2").html("+"), $("#changecolor" + e).removeClass("c_down"), $("#changecolor1" + e).removeClass("fa fa-angle-double-down"), $("#beat" + e + "change_mobile").removeClass("red"), $("#changecolor" + e).addClass("c_up"), $("#changecolor1" + e).addClass("fa fa-angle-double-up"), $("#beat" + e + "change_mobile").addClass("green"), $("#" + e).addClass("row_background2"), setTimeout(function() {
                        $("#" + e).removeClass("row_background2")
                    }, 1e3)))
                }
            })
        }
    })
}, 5e3), $(document).on("scroll", function() {
    if ($(document).height() <= $(document).scrollTop() + $(document).height()) {
        totalcoins = document.getElementById("coins_count").value, totalcoins = Number(totalcoins);
        var e = document.getElementById("row_no").value;
        if (pagedisplays = e * $("#records_per_page_front").val(), totalcoins > pagedisplays) {
            var t = document.getElementById("pageids").value;
            t.split(",").indexOf(e) > -1 || (loadmore(), document.getElementById("pageids").value = t + "," + e)
        }
    }
}), $(document).ready(function() {
    $("#coin_name").keyup(function() {
        $.ajax({
            type: "POST",
            url: baseUrl+"Ajax_search/search",
            data: "keyword=" + $(this).val(),
            beforeSend: function() {},
            success: function(e) {
                $("#suggesstion-box").show(), $("#suggesstion-box").html(e)
            }
        })
    })
}), $(document).ready(function() {
    $("#coin_name_mobile").keyup(function() {
        $.ajax({
            type: "POST",
            url: baseUrl+"Ajax_search/search",
            data: "keyword=" + $(this).val(),
            beforeSend: function() {},
            success: function(e) {
                $("#suggesstion-box_mobile").show(), $("#suggesstion-box_mobile").html(e)
            }
        })
    })
}), $(".form-search").click(function(e) {
    $(".instant-results").fadeIn("slow").css("height", "auto"), e.stopPropagation()
}), $("body").click(function() {
    $(".instant-results").fadeOut("500")
});