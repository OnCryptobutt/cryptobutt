  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b></b> 
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> Cryptobutt</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url();?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>dist/js/demo.js"></script>
<script src="<?php echo base_url();?>bootstrap/js/jquery.fileuploadmulti.min.js"></script>




<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
	
	$("#e1").select2();
$("#checkbox").click(function(){
    if($("#checkbox").is(':checked') ){
        $("#e1 > option").prop("selected","selected");
        $("#e1").trigger("change");
    }else{
        $("#e1 > option").removeAttr("selected");
         $("#e1").trigger("change");
     }
	 var e1 = $("#e1").val();
	 pageId = "0";
	 liId = "0";
	 $(".flash").show();
     $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&e1='+e1;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  // alert(result);
                 $(".flash").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });
	 
});

$("#e1").change(function(){
	 var e1 = $("#e1").val();
	pageId = "0";
	liId = "0";
	$(".flash").show();
     $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId+'&e1='+e1;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			   //alert(result);
                 $(".flash").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });

});

  });
  
  $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
		
		
    });
    
});

setTimeout(function() {
    $('#SiteNotifyMessage').fadeOut('slow');
}, 5000); 

</script>

<script>
  function changePagination(pageId,liId){
     $(".flash").show();
     $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/load_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  // alert(result);
                 $(".flash").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData").html(result);
           }
      });
}

changePagination("0","0");


</script>

<script>
  function UserPagination(pageId,liId){
     $(".flash").show();
     $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/user_data');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  // alert(result);
                 $(".flash").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
                 $("#pageData2").html(result);
           }
      });
}

UserPagination("0","0");


</script>

<?php if($basename=="coin-list" || $basename=="coin-add" || $basename=="coin-edit" || $basename=="people-list" || $basename=="people-add" || $basename=="people-edit")
{ 
$typeList="";
//print_r($link_type_list);
if(isset($link_type_list))
{
	if(is_array($link_type_list))
	{
		
		foreach($link_type_list as $link_type_lis)
		{
			$typeList .="<option value=".$link_type_lis->link_type_id.">".$link_type_lis->link_type_name."</option>";
		}
	}
}
?>
<script>	
		
		
$(document).ready(function() {

var MaxInputs       = 50; //maximum input boxes allowed

var InputSlider   = $("#InputSlider"); //Input boxes wrapper ID

var AddButtons       = $("#AddMoreImageBox"); //Add button ID



var x = InputSlider.length; //initlal text box count

var FieldCount=1; //to keep track of text box added



$(AddButtons).click(function (e)  //on add input button click

{

   

        if(x <= MaxInputs) //max input box allowed

        {

            FieldCount++; //text box added increment

$(InputSlider).append('<div class="form-group"><label for="inputPassword3" class="col-sm-2 control-label"></label><div class="col-sm-4"><select class="form-control" name="type[]"><option value="">--Select--</option><?php echo $typeList;?></select></div><div class="col-sm-4"><input type="text" class="form-control" name="url[]" id="inputEmail3" placeholder="Add URL" value=""><span class="red"></span></div><a href="#" class="removeclass btn btn-danger">Delete</a></div>');

            x++; //text box increment

        }

return false;

});



$("body").on("click",".removeclass", function(e){ //user click on remove text

        if( x > 1 ) {

                $(this).parent('div').remove(); //remove text box

                x--; //decrement textbox

        }

return false;

});


var MaxInputs2       = 50; //maximum input boxes allowed

var InputSlider2   = $("#InputSlider2"); //Input boxes wrapper ID

var AddButtons2       = $("#AddMoreImageBox2"); //Add button ID



var x = InputSlider2.length; //initlal text box count

var FieldCount2=1; //to keep track of text box added



$(AddButtons2).click(function (e)  //on add input button click

{



        if(x <= MaxInputs2) //max input box allowed

        {

            FieldCount2++; //text box added increment

$(InputSlider2).append('<div class="form-group"><label for="inputPassword3" class="col-sm-2 control-label"></label><div class="col-sm-3"><select class="form-control" name="type[]"><option value="">--Select--</option><?php echo $typeList;?></select></div><div class="col-sm-3"><input type="text" class="form-control" name="url[]" id="inputEmail3" placeholder="Add URL" value=""><span class="red"></span></div><a href="#" class="removeclass btn btn-danger">Delete</a></div>');

            x++; //text box increment

        }

return false;

});









});
		

<?php if($basename!="people-add"){ ?>		
		
function AddKeyPeople()
{
	var role = $("#roleid").val();
	var peopleid = $("#peopleid").val();
	var profilestatus = $("#profilestatus").val();
	var roletype = $('#roletype').val();
	var error="";
	if(role=="")
	{
		error = "Please select the role\n";
	}
	if(peopleid=="")
	{
		error = error + "Please select the People\n";
	}
	if(profilestatus=="")
	{
		error = error + "Please select the Profile Verified\n";
	}
	if(roletype == "") {
	    error = error + "Please enter the role type\n";
	}
	if(error=="")
	{
		
		
     var dataString = 'role='+ role+'&peopleid='+peopleid+'&profilestatus='+profilestatus+'&roletype='+roletype+'&coin_id='+<?php echo $this->uri->segment(3);?>;
    // alert(dataString);
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/save_key_people');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
			  $("#appendContent").append(result);
                 //$("#pageData2").html(result);
				 $("#roleid").val("");
				 $("#peopleid").val("");
           }
      });
		
	}
	else
	{
	alert(error);
	}
}
<?php } ?>
	</script>
<?php } ?>

<script>

$(document).ready(function()
{

var settings = {
	url: "<?php echo base_url()?>gallery/upload/<?php echo $id;?>",
	method: "POST",
	allowedTypes:"jpg,png,gif",
	fileName: "myfile",
	multiple: true,

	onSuccess:function(files,data,xhr)
	{

	$("#status").html("<font color='green'>Upload is success</font>");
		
	},
    afterUploadAll:function()
    {
        location.reload('<?php echo base_url()?>gallery/photos/<?php echo $id;?>');
    },
	onError: function(files,status,errMsg)
	{	
   
		$("#status").html("<font color='red'>Upload is Failed</font>");
	}
}
$("#mulitplefileuploader").uploadFile(settings);

});
</script>



<?php if($basename=="cms-add" || $basename=="cms-edit")
{
	$editor_fields = "'cms_content'";
}
elseif($basename=="banner-add" || $basename=="banner-edit")
{
	$editor_fields = "'banner_content'";
}
else{ $editor_fields = "";
      }
	?>

<?php
 if($editor_fields){?>
 <script type="text/javascript" src="<?php echo CLIENT_BASE_URL;?>responsive_filemanager/ckeditor/ckeditor.js"></script>
<script>
 $(function () {
CKEDITOR.replace( <?php echo $editor_fields;?>,{
	filebrowserBrowseUrl : '<?php echo CLIENT_BASE_URL;?>responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
	filebrowserUploadUrl : '<?php echo CLIENT_BASE_URL;?>responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
	filebrowserImageBrowseUrl : '<?php echo CLIENT_BASE_URL;?>responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
});
});
</script>	
<?php } ?>

<script src="<?php echo base_url();?>bootstrap/crop/js/jquery.mousewheel.min.js"></script>
   	<script src="<?php echo base_url();?>bootstrap/crop/js/croppic.min.js"></script>
    <script src="<?php echo base_url();?>bootstrap/crop/js/main.js"></script>
    <script type="text/javascript" src="https://www.cryptobutt.com/js/jquery.toast.js"></script>
    <script>
		
		
		
		var croppicContainerModalOptions = {
				uploadUrl:'<?php echo base_url('Feature/upload/')?>',
				cropUrl:'<?php echo base_url('Feature/crop/')?>',
				
				modal:true,
				imgEyecandyOpacity:0.4,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){  },
				onAfterImgUpload: 
				function(data){                                  
           		var res = window.response;  
                $("#filenameOrginal").val(res.filenameOrginal);
       
				},
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(data){ //var res = JSON.stringify(data);
				 $("#filenameCrop").val(data.filenameCrop);},
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var cropContainerModal = new Croppic('cropContainerModal', croppicContainerModalOptions);
		
	</script>

<script>
  function fetch_list(val){
    // $(".flash").show();
    // $(".flash").fadeIn(400).html("Loading <img src='<?php echo UPLOAD_URL;?>loader1.gif'>");
	//alert(val);
	$(".pagelist").hide();
     var dataString = 'list_value='+ val;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax/user_list_ajax');?>',
           data: dataString,
           cache: false,
           success: function(result){
			  //alert(result);
                 $("#pageData2").html(result);
           }
      });
}
function sociallinkFunction(data,arg2) {
    var str = data.split("=");
    var t = str[1].split("&");
    if(t[0] != 'empty') {
    $.ajax({
           type: "POST",
           url: '<?php echo base_url('coin/socialpeoplelink'); ?>',
           data: data,
           cache: false,
           success: function(response){
            if(response) {
               $('#'+arg2).html(response);
            }
           }
    });
    } else{
        alert("Please choose link.....");location. reload(true);
    }
}
function socialFun(arg1, arg2){
    var dataString = 'socialid='+ arg1+'&peopleid='+arg2;
    
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('coin/socialpeople'); ?>',
           data: dataString,
           cache: false,
           success: function(response){
               sociallinkFunction(dataString,arg2);
               var data = JSON.parse(response);
        		for(var i = 0, l = data.length; i < l; i++) {
                    var result = data[i];
                    window.open(result.social_links_url, '_blank');
                }
           }
    });
}
function verifyFun(arg1, arg2){
   var dataString = 'verifyid='+ arg1+'&peopleid='+arg2;
   $.ajax({
           type: "POST",
           url: '<?php echo base_url('coin/updateverifystatus'); ?>',
           data: dataString,
           dataType: "json",
           cache: false,
           success: function(response){
               if(response.status=='true') {
                    $.toast({
                    heading: 'Success',
                    text: 'KeyPeople profile verifyied successfully.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
               }
           }
    });
}
function verifylinkFun(arg1, arg2, arg3){ 
   var dataString = 'verifyid='+ arg1+'&peopleid='+arg2+'&socialid='+arg3;
   $.ajax({
           type: "POST",
           url: '<?php echo base_url('coin/updateverifystatuslink'); ?>',
           data: dataString,
           dataType: "json",
           cache: false,
           success: function(response){
               if(response.status=='true') {
                    $.toast({
                    heading: 'Success',
                    text: 'KeyPeople profile verifyied successfully.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
                $('#'+response.peopleid+'>label').removeClass('btn-danger');
                $('#'+response.peopleid+'>label').addClass('btn-success');
                
               } else{
                   $('#'+response.peopleid+'>label').removeClass('btn-success');
                    $('#'+response.peopleid+'>label').addClass('btn-danger');
               }
           }
    });
}

function GetPeopleDetails(id) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_user_id").val(id);
   
    // Open modal popup
    $("#update_user_modal").modal("show");
}
function updatePeopleDetails(id) {
 var dataString = 'id='+ id;
   $.ajax({
           type: "POST",
           url: '<?php echo base_url('people/updatepeople'); ?>',
           data: dataString,
           dataType: "json",
           cache: false,
           success: function(response){
               if(response.status=='true') {
                    $.toast({
                    heading: 'Success',
                    text: 'KeyPeople profile verifyied successfully.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
                
                $('#badge'+id).html('<span class="badge">✓</span>');
               } 
           }
    });
}
var MaxInputs3       = 50; //maximum input boxes allowed

var InputSlider3   = $("#InputSlider3"); //Input boxes wrapper ID

var AddButtons3       = $("#AddMoreImageBox3"); //Add button ID



var x = InputSlider3.length; //initlal text box count

var FieldCount3=1; //to keep track of text box added



$(AddButtons3).click(function (e)  //on add input button click

{



        if(x <= MaxInputs3) //max input box allowed

        {

            FieldCount3++; //text box added increment

$(InputSlider3).append('<div class="row"><div class="col-lg-5 col-md-5 col-sm-5 col-12"><div class="form-group key-text"><input type="text" class="form-control ui-autocomplete-input" id="profilelink" name="url[]" value="" autocomplete="off"></div></div><div class="col-lg-5 col-md-5 col-sm-5 col-12"><div class="form-group key-text"><select id="profileweb" name="type[]" class="form-control"><option value=""></option><?php echo $typeList;?></select></div></div><div class="col-lg-1 col-md-1 col-sm-1 col-12"><a href="javascript:;" class="removeclass btn btn-danger">Delete</a></div></div>');

            x++; //text box increment

        }

return false;

});
$("body").on("click",".removeclass", function(e){ //user click on remove text
        if( x > 1 ) {
                $(this).parent('div').parent('div').remove(); //remove text box
                x--; //decrement textbox
        }
return false;
});
function checkstatus(arg){
    var dataString = 'id='+ arg.value+'&value='+arg.checked;
   $.ajax({
           type: "POST",
           url: '<?php echo base_url('coin/checkstatus'); ?>',
           data: dataString,
           dataType: "json",
           cache: false,
           success: function(response){
               if(response.status=='true') {
                    $.toast({
                    heading: 'Success',
                    text: 'Coin has been verified.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
               } 
           }
    });
}
</script>

</body>
</html>
