<?php
class Home_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	function getBanner()
	{
		$this->db->select("*");
		$this->db->from(BANNER);
		$this->db->where("banner_status","1");
		$this->db->order_by("banner_order","ASC");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function coin_list($limit, $start)
	{
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$this->db->select('*');
		$this->db->from(CRON);
		$this->db->where("status","COMPLETED");
		$this->db->order_by("id","DESC");
		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			$rowc = $query->row();
		}
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				$row_columns[] = $row2;
			}
		}
	
		
		$this->db->select('*');
		$this->db->from(COIN);
		
		$this->db->where("status","1");
	$this->db->order_by("sortorder","asc");
	$this->db->limit($limit,$start);
		if($this->input->get('coin_name'))
		{
			$this->db->like("coinname",$this->input->get('coin_name'));
		}
			$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
            foreach ($query->result_array() as $row)
			{
				foreach($row_columns as $row_column)
				{
					$row[$row_column->name] = $this->boost_model->getValue(COIN_HISTORY,$currency,"cron_id='".$rowc->id."' AND coin_id='".$row['id']."' AND column_id='".$row_column->id."'");
				}
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	 public function record_count()
    {
		$this->db->select('*');
		$this->db->from(COIN);
		
		$this->db->where("status","1");
	$this->db->order_by("sortorder","asc");
	
		if($this->input->get('coin_name'))
		{
			$this->db->like("coinname",$this->input->get('coin_name'));
		}
			$query = $this->db->get();

		return $query->num_rows();
       
    }
}
?>