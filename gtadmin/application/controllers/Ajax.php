<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	function getSubCategory()
	{
		$getSubCategoryList = $this->product_model->getSubCategoryList($this->input->get('product_sub_category_id'),$this->input->get('product_category_id'));
		echo "<select id=\"product_sub_category_id\" name=\"product_sub_category_id\" class=\"form-control\">
					<option value=\"\">--Select Sub Category--</option>
					".$getSubCategoryList."
					</select>";
		exit();
	}
	
	function load_data()
	{
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }

        $pageLimit=$this->settings->settings_records_per_page*$id;

		$ar = "";
		$ar1="";
		if(isset($_POST['e1']))
		{
			$ar1 = explode(",",$_POST['e1']);
			foreach($ar1 as $key=>$value)
			{
				$ar[$key] =$value;
			}
		}


        $this->db->select('*')->from(PRODUCT);
		
		if($this->input->get('act')=="search")
		{
			$this->db->like('product_name',$this->input->get('search'));
		}
		$this->db->limit($this->settings->settings_records_per_page,$pageLimit);
		
		if(isset($_POST['e1']))
		{
		$this->db->where_in('product_category_id', $ar);//WHERE author IN ('Bob', 'Geoff')
		}
		
		$query = $this->db->get();
		
		
	///echo $this->db->last_query();
		//exit;
		
		
		
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
				$list = $row;
				 echo '<tr>
					  
					    <td><input class="checkbox1" type="checkbox" name="check[]" value="'.$list->product_id.'"> </td>
						  <td>';

						  if($list->product_photo){ echo "<img width='50' height='50' src='".PRODUCT_URL."thumb/".$list->product_photo_name."_150.".$list->product_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }
						  
						  echo '</td>
						<td>'.$list->product_name.'</td>
						<td>';
						if($list->product_status=='1'){
							echo '<span class="label label-success">Active</span>'; } 
					    if($list->product_status=='0'){
							echo '<span class="label label-danger">Suspend</span>'; } 
						echo '</td>
						<td><a href="'.base_url().'product/edit/'.$list->product_id.'/"><span class="fa fa-pencil"></span></a> |
						
						<a href="'.base_url().'product/view/'.$list->product_id.'/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>';
                
            }
            
        }
		else
		{
			echo "<tr><td colspan='5' align='center'><font color='red'>-No records found-</font></td></tr>";
		}

exit();
}


function user_data()
	{
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }

        $pageLimit=$this->settings->settings_records_per_page*$id;

        $this->db->select('*')->from(ADMIN);
		$this->db->where("admin_type",'ADMIN');
		$this->db->limit($this->settings->settings_records_per_page,$pageLimit);
		
		
		$query = $this->db->get();
		
		
	///echo $this->db->last_query();
		//exit;
		
		
		
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
				$list = $row;
				
					echo '<tr>
					  
					    <td><input class="checkbox1" type="checkbox" name="check[]" value="'.$list->admin_id.'"> </td>
						  
						<td>'.$list->admin_username.'</td>
						
						<td>';
						
						if($list->admin_status=='1')
						{
						   echo'<span class="label label-success">Active</span>'; 
						} 
						 
						if($list->admin_status=='0')
						{
						  echo '<span class="label label-danger">Suspend</span>';
						} 
						
						echo '</td>
						<td><a href="'.base_url().'user/edit/'.$list->admin_id.'/"><span class="fa fa-pencil"></span></a> |
						
						<a href="'.base_url().'user/view/'.$list->admin_id.'/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>';
					  
                
            }
            
        }
		else
		{
			echo "<tr><td colspan='4' align='center'><font color='red'>-No records found-</font></td></tr>";
		}

exit();
}


function user_list_ajax()
	{
		//define('PAGE_PER_NO',8); 
		$client=$this->input->post('list_value');
		
		$pageLimit=$this->settings->settings_records_per_page;
		$start='0';
		
		//echo"<script>alert('hi');</script>";
		/*
        $this->db->select('*')->from(ADMIN);
		$this->db->where('admin_type','ADMIN');
		
		if($this->input->post('list_value')!="")
		{
			
			$id=$this->input->post('list_value');
			echo"<script>alert('$id);</script>";
			
			$this->db->like('admin_username',$this->input->get('list_value'));
			$this->db->or_like('admin_email', $this->input->get('list_value'));
		}*/
		//$query = $this->db->get();
		//$this->db->limit($this->settings->settings_records_per_page,$pageLimit);
		
		
		
		$sql ="SELECT * FROM ".ADMIN." WHERE admin_type='ADMIN' AND (admin_username LIKE '%".$client."%' or admin_email LIKE '%".$client."%')";
		$query = $this->db->query($sql);
		
		
		//echo $sql;
		//exit;
		
		
	///echo $this->db->last_query();
		//exit;
		
		
		
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
				$list = $row;
				 echo '<tr>
					  
					    <td><input class="checkbox1" type="checkbox" name="check[]" value="'.$list->admin_id.'"> </td>
						 ';

						  
						  
						  echo '<td>'.$list->admin_username.'</td>
						<td>';
						if($list->admin_status=='1'){
							echo '<span class="label label-success">Active</span>'; } 
					    if($list->admin_status=='0'){
							echo '<span class="label label-danger">Suspend</span>'; } 
						echo '</td>
						<td><a href="'.base_url().'user/edit/'.$list->admin_id.'/"><span class="fa fa-pencil"></span></a> |
						
						<a href="'.base_url().'user/view/'.$list->admin_id.'/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>';
                
            }
            
        }
		else
		{
			echo "<tr><td colspan='3' align='center'><font color='red'>-No records found-</font></td></tr>";
		}

exit();
}


public function save_key_people()
{
	$role_id   = $this->input->post('role');
	$people_id = $this->input->post('peopleid');
	$coin_id   = $this->input->post('coin_id');
	$profilestatus   = $this->input->post('profilestatus');
	$roletype   = $this->input->post('roletype');
	if($role_id && $people_id && $coin_id && $roletype)
	{
			$insert_data = array("key_people_people_id"=>$people_id,
			                     "key_people_coin_id"=>$coin_id,
								 "key_people_role_id"=>$role_id,
								 "key_people_verified"=>$profilestatus,
								 "key_people_roletype"=>$roletype);
			$this->db->set($insert_data)->insert(KEY_PEOPLE);
			$insert_id = $this->db->insert_id();
			
			
			
			$query = $this->db->select('*')->from(KEY_PEOPLE)->join(PEOPLE,PEOPLE.".people_id=".KEY_PEOPLE.".key_people_people_id")->join(ROLE,ROLE.".role_id=".KEY_PEOPLE.".key_people_role_id")->where(KEY_PEOPLE.".key_people",$insert_id)->get();
			
			
		
		if($query->num_rows()>0)
		{
			$row = $query->row();	
			
			
			$linkedurl ="";
												   $linkedtype ="";
												    $linkedsymbol ="";
												   $primaryQuery = "SELECT * FROM ".SOCIAL." WHERE social_people_id='".$row->people_id."' and social_primary='1' limit 1";
											$resPrimaryQuery = $this->db->query($primaryQuery);
												 if($resPrimaryQuery->num_rows()>0)
												 {
													 $rowPrimaryQuery = $resPrimaryQuery->row();
													 $linkedurl = $rowPrimaryQuery->social_links_url;
													 $linkedtype = $this->boost_model->getValue(LINK_TYPE,"link_type_name","link_type_id='".$rowPrimaryQuery->social_links_type."'");
													  $linkedsymbol = $this->boost_model->getValue(LINK_TYPE,"link_type_font_awesome","link_type_id='".$rowPrimaryQuery->social_links_type."'");
												 }
												 else
												 {
													 $primaryQuery = "SELECT * FROM ".SOCIAL." WHERE social_people_id='".$row->people_id."' limit 1";
													 $resPrimaryQuery = $this->db->query($primaryQuery);
													 if($resPrimaryQuery->num_rows()>0)
													 {
														 $rowPrimaryQuery = $resPrimaryQuery->row();
														  $linkedurl = $rowPrimaryQuery->social_links_url;
													 $linkedtype = $this->boost_model->getValue(LINK_TYPE,"link_type_name","link_type_id='".$rowPrimaryQuery->social_links_type."'");
													  $linkedsymbol = $this->boost_model->getValue(LINK_TYPE,"link_type_font_awesome","link_type_id='".$rowPrimaryQuery->social_links_type."'");
													 }
												 }

			
			if($row->people_photo)
			{
				$img =PEOPLE_URL.$row->people_photo;
			}
			else
			{
		$img ="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg";
			}
			
			echo '<tr>
								<td>'.$row->role_name.'</td>
								<td><img style="height:50px" src="'.$img.'" class="img-responsive"></td>
								<td>'.$row->people_first_name." ".$row->people_middle_name." ".$row->people_last_name.'</td>
								<td>'.$linkedtype.'</td>
								<td>';
								if($row->key_people_verified=="Y")
								{
									echo '<i class="fa fa-check-circle" style="color: #05ceef;"></i> Verified';
								 }
								if($row->key_people_verified=="N")
								{
									echo '<i class="fa fa-check-circle" style="color: red;"></i> Not Verified';
								} 
								
								if($row->key_people_verified=="P")
								{
									echo '<i class="fa fa-check-circle" style="color: orange;"></i> Pending';
								} 
								echo '</td> 
								
								<td><a href="'.base_url('coin/edit/'.$row->key_people_coin_id.'/?keypeople='.$row->key_people).'" onclick="return confirm(\'Are you sure want to delete this record?\')" href="#" class="btn btn-danger">Delete</a></td>
							</tr>';
		
		}
	}
	//echo "Key People has added";
	exit;
}


}
