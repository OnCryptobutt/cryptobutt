<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function validate_captcha()
	{
		if($this->input->post('captcha')!="")
		{
			if($this->input->post('captcha') != $this->session->userdata['captchaWord'])
			{
				$this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
				return false;
			}else{
				return true;
			}

		}
		else
		{
			$this->form_validation->set_message('validate_captcha', 'The Captcha field is required.');
			return false;
		}
	
	}
	
	public function index()
	{
		$data = "";
		$data['title']    = "Login";
		
		if($this->session->userdata('admin_id') == 6)
		{
			redirect(base_url()."coin/");
		}else if($this->session->userdata('admin_id')){
		    redirect(base_url()."dashboard/");
		}
		
		$this->load->model('login_model');
		
		if($this->input->post('act')=="login")
		{
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha');
			
			if($this->form_validation->run()==TRUE)
			{
				if($this->login_model->authentication()==TRUE)
				{
					if($this->input->get('continue'))
					{
						redirect($this->input->get('continue'));
					}
					else
					{
					    if($this->input->post('username') == 'buddy@cryptobutt.com'){
					        redirect(base_url()."coin/", 'refresh'); 
					    }else{
					        redirect(base_url()."dashboard/", 'refresh');
					    }
					}
				}
				else
				{
				   $data['error'] = "Invalid Login";
				}
			}
			
		}
		
		
		$vals = array('img_path'      => UPLOAD_PATH."captcha/",'img_url'       => UPLOAD_URL."captcha/",'expiration' => 7200 , 'word_length'   => 4, 'pool'          => '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ','font_size'     => 32);

		$cap = create_captcha($vals);
		
		$data['captcha'] = $cap['image'];
		
		//print_r($cap['word']);exit;

		$this->session->set_userdata('captchaWord', $cap['word']);
		
		$this->load->view('login_view',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */