 <footer class="footer">
          <div class="footer__block block no-margin-bottom">
            <div class="container">
				<div class="row">
					<div class="col-lg-6">
						<p class="no-margin-bottom"><a class="contact" href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> Contact</a> <span></span></p>
					</div>
					<div class="col-lg-6">
						<p style="float:right;" class="no-margin-bottom">Copyright all right reserved CryptoButt</p>
					</div>					
				</div>	
            </div>
          </div>
        </footer>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"> </script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/front.js"></script>
	<script src="<?php echo base_url();?>js/flag_coin.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/jquery.visible.js"></script>
	<!-- <script src="http://blazeworx.com/jquery.flagstrap.min.js"></script> -->
	<!-- <script>
		jQuery(document).ready(function($){
		  $('.search-box input[type="text"]').focus(function(){
			$('.search-filters').slideToggle();
		  });
		  $('.search-filters input[type="radio"]').on('click', function(){
			var placeholder_text = $(this).closest('label').text();
			$('.search-input').attr('placeholder', 'search: '+placeholder_text);
			$('.search-filters').slideToggle();
		  });
		});
	</script> -->
	<script>
		$(document).ready(function () {
			$('.flagstrap').flagStrap({
    
    scrollable: false,
    scrollableHeight: "350px",
    onSelect: function (value, element) {
       if(value=="US")
	   {
		   val = "USD";
	   }
	   else if(value=="IN")
	   {
		    val = "INR";
	   }
	    else if(value=="GB")
	   {
		    val = "GBP";
	   }
	    else if(value=="AU")
	   {
		    val = "AUD";
	   }
	    else if(value=="CA")
	   {
		    val = "CAD";
	   }
	   else if(value=="EU")
	   {
		    val = "EUR";
	   }
	   
	   window.location = "<?php echo 'https://'. $_SERVER['SERVER_NAME'];?>?currency="+val;
    }
});
});
	</script>
	
	<?php if($js)
	{ echo $js; } ?>
	
  </body>
</html>
<?php $this->db->close();?>