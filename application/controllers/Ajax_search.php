<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_search extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->detect = new Mobile_Detect();
    }
	
	function search()
	{
		if($this->input->post('keyword'))
		{
			$query = $this->db->from(COIN)->like("coinname",$this->input->post('keyword'),'after')->or_like("name",$this->input->post('keyword'),'after')->order_by("sortorder", "asc")->limit(6)->get();
			
			/*$this->db->select('ci_coin.*,ci_coin_history_temp_USD.mktcap');    
            $this->db->from('ci_coin');
            $this->db->join('ci_coin_history_temp_USD', 'ci_coin_history_temp_USD.coin_id = ci_coin.id');
            $this->db->or_like('ci_coin.coinname',$this->input->post('keyword'),'after');
            $this->db->or_like('ci_coin.name',$this->input->post('keyword'),'after');
            $this->db->order_by("ci_coin.sortorder", "ASC");
            $this->db->limit(10);
            $query = $this->db->get();*/

			if($query->num_rows()>0)
			{
			    
			    
			  // print_r($query->result_array());
				echo "<div class='z-index bod-top'><div class='col-xs-12 col-lg-12 coin_lists div_hoer' style='font-size:16px;background:#F3E5F5;color:#000'>Crypto assets</div>";
				foreach($query->result_array() as $row)
				{
					 $exist = $this->boost_model->getValue(PORTFOLIO,"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$row['id']."'");
					 if($exist>0)
					 {
						$heartcls = "";
					 }
					 else
					 {
						$heartcls = "-o";
					 }
					if ( $this->detect->isMobile() || $this->detect->isTablet())
					{
						echo "<div class='col-xs-12 col-lg-12 coin_lists div_hoer' onclick=\"selectCountry('".$row['name']."')\"><img width='15px' src='".COIN_URL.$row['image']."' class='img-rounded img-ser'>'<span class='ser_tit'>".$row['coinname']."&nbsp;&nbsp;(".$row['name'].")</span>'<span class='ser-rig'><i class='fa fa-heart$heartcls heart_cor' aria-hidden='true'></i></span></div>";
					}
					elseif( !$this->detect->isMobile() && !$this->detect->isTablet() )
					{
						echo "<div class='col-xs-12 col-lg-12 coin_lists div_hoer' onclick=\"selectCountry('".$row['name']."')\">
						
						<img  src='".COIN_URL.$row['image']."' class='coin_img'>".$row['coinname']."&nbsp;&nbsp;(".$row['name'].")
						<div class='ser-rig'><i class='fa fa-heart$heartcls heart_cor' aria-hidden='true'></i></div></div>";
					}
				}
				
				
				$query = $this->db->from("ci_addkeypeople")->like("keypeople_fullname",$this->input->post('keyword'),'after')->or_like("keypeople_roletitle",$this->input->post('keyword'),'after')->limit(6)->get();
				if($query->num_rows()>0)
			    {
			        echo "<div class='col-xs-12 col-lg-12 coin_lists div_hoer' style='font-size:16px;background:#F3E5F5;color:#000'>Keypeople</div>";
			        foreach($query->result_array() as $row)
				    {
				        echo "<div class='col-xs-12 col-lg-12 coin_lists div_hoer'onclick=\"selectPeople('".$row['keypeople_id']."/".$row['keypeople_fullname']."')\" >
						<img  src='".base_url()."img/".$row['keypeople_filename']."' class='coin_img'>".$row['keypeople_fullname']."
						</div>";
				    }
			    }
			    
			echo	"</div>";
			}
		}
		exit;
	}
	/*
	function common_search()
	{
		if($this->input->post('keyword'))
		{
			$query = $this->db->from(COIN)->like("coinname",$this->input->post('keyword'))->or_like("name",$this->input->post('keyword'))->limit(6)->get();
			if($query->num_rows()>0)
			{

				echo "<div class='z-index bod-top'>";
				foreach($query->result_array() as $row)
				{
					 $exist = $this->boost_model->getValue(PORTFOLIO,"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$row['id']."'");
					 if($exist>0)
					 {
						$heartcls = "";
					 }
					 else
					 {
						$heartcls = "-o";
					 }
				echo "<div class='col-xs-10 col-lg-12 coin_lists div_hoer' onclick=\"selectCountry('".$row['name']."')\"><img width='30px' src='".COIN_URL.$row['image']."' class='img-rounded img-ser'>".$row['coinname']."&nbsp;&nbsp;(".$row['name'].")<span class='ser-rig'><i class='fa fa-heart$heartcls heart_cor' aria-hidden='true'></i></span></div>";
				}
				echo "</div>";
			}
		}
		exit;
	}
	*/
}