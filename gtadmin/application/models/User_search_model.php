<?php
class User_search_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function user_search_list($limit, $start)
	{
		$sql = "SELECT * FROM ".USER_SEARCH." order by user_search_id desc limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
				
				$query1 = $this->db->select('*')->from(USER)->where('id',$row->user_id)->get();
				$row1 = $query1->row();
				$row->firstName = $row1->firstName;
				$row->lastName = $row1->lastName;
				$row->profileURL = $row1->profileURL;
				$row->photoURL = $row1->photoURL;
				
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getuser_searchDetails($id)
    {
	   $sql = "SELECT * FROM ".USER_SEARCH." WHERE user_search_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			$row = $query->row();
			$row->firstName = $this->boost_model->getValue(USER,"firstName","id='".$row->user_id."'");
				$row->lastName = $this->boost_model->getValue(USER,"lastName","id='".$row->user_id."'");
			return $row;
       }
       
    }
   
    public function record_count()
    {
      //return $this->db->count_all(ADMIN);
	  //$this->db->where('admin_type', 'ADMIN');
      return $this->db->count_all_results(USER_SEARCH);
    }
   
    public function user_searchActivate($id)
    {
	   $sql = "UPDATE ".user_search." SET status='1' WHERE id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function user_searchSuspend($id)
	{
		$sql = "UPDATE ".user_search." SET status='0' WHERE id='$id'";
		$query = $this->db->query($sql);
	}
		
		
	public function user_searchDelete($id)
	{
		
		$this->db->where('user_search_id',$id);
		//$this->db->where('admin_type','ADMIN');
		$this->db->delete(USER_SEARCH);
		
	}
	
	public function user_searchInsert()
	{
		
		$sql = "INSERT INTO ".user_search." SET 
		
		user_search_name    		= '".$this->input->post('user_search_name')."',
		password    		= '".$this->input->post('password')."',
		email       		= '".$this->input->post('email')."',
		gender       		= '".$this->input->post('gender')."',
		firstName       	= '".$this->input->post('firstName')."',
		lastName        	= '".$this->input->post('lastName')."',
		status      		= '".$this->input->post('status')."',
		created_time 		= '".NOW."',
		updated_time 		= '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function user_searchEdit($id)
	{
			
		$sql = "UPDATE ".ADMIN." SET 
		
		user_search_name    		= '".$this->input->post('user_search_name')."',
		password    		= '".$this->input->post('password')."',
		email       		= '".$this->input->post('email')."',
		gender       		= '".$this->input->post('gender')."',
		firstName       	= '".$this->input->post('firstName')."',
		lastName        	= '".$this->input->post('lastName')."',
		status      		= '".$this->input->post('status')."',
		updated_time 		= '".NOW."' WHERE id='".$id."'";
			
		$this->db->query($sql);
	}
	
		
}
?>