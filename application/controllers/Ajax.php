<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Ajax extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->detect = new Mobile_Detect();
		$this->db1 = $this->boost_model->getHistoryDb();
    }
	
	function updaterow()
	{
		$id = $this->input->post('id');
		$name = $this->boost_model->getValue(COIN,"name","id='".$id."'");
		$currency = $this->input->post('currency');
		$prev_price = $this->input->post('prev_price');

		if($currency=="")
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "USD";
			}
		}

		$query = $this->db->select('price,change24hour,volume24hour,mktcap,old_price,changepct24hour,high24hour,low24hour')->from("ci_coin_history_temp_".$currency)->where("coin_id",$id)->get();
		
		if($query->num_rows()>0)
		{
			$row1                = $query->row();
			$row['price']        = $row1->price;
			$row['change24hour'] = $row1->change24hour;
			$row['volume24hour'] = $row1->volume24hour;
			$row['changepct24hour'] = $row1->changepct24hour;
			$row['mktcap']       = $row1->mktcap;
			$row['old_price']       = $row1->old_price;
			$row['high24hour']   = $row1->high24hour;
            $row['low24hour']   = $row1->low24hour;
			
		}
		
		$price = number_format($row['price'],4);
		if($price===$prev_price)
		{
			$row['price_change'] = "No";
		}
		else
		{
			$row['price_change'] = "Yes";
			
		}
		
		$sorting_change = $this->input->post('sorting_change');
		
	
		
		if($sorting_change=="24H")
				{
					$hour = 24;
				}
				elseif($sorting_change=="1H")
				{
					$hour = 1;
				}
				elseif($sorting_change=="1W")
				{
					$hour = 168;
				}
				elseif($sorting_change=="1M")
				{
					$hour = 720;
				}
				elseif($sorting_change=="3M")
				{
					$hour = 2160;
				}
				elseif($sorting_change=="ALL")
				{
					$hour = 8760;
				}
				else
				{
					$hour = 24;
				}
		
		
		$meeting_time1 = date('Y-m-d H:i:s', time() - 60 * 60 * $hour);
		
		$meeting_time1_string = strtotime($meeting_time1);
		
			/**$old_price = $this->boost_model->getValue2("ci_coin_history_".$currency,"price","coin_id='".$id."' and created_time < '$meeting_time1' ORDER by id desc limit 1");
			**/
			
			if($sorting_change!="24H" && $sorting_change!="")
				{
					
			$Data2 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym='.$name.'&tsyms='.$currency.'&ts='.$meeting_time1_string),true);
	
$old_price = $Data2[$name][$currency];
	
	
	 $newprice = $row['price'] - $old_price;
	 
	 $change24 = $newprice / $row['price'];
	 
	 $change24 = number_format($change24 * 100,2);

		$calc = ( $change24 / 100);
		$volume24hour = $calc * $row['price']; 
		
			$row['volume24hour'] = number_format($volume24hour,2);
		$row['change24hour'] = number_format($change24,2);
		
				}
				else
				{
		$row['volume24hour'] = number_format($row['change24hour'],2);
		$row['change24hour'] = number_format($row['changepct24hour'],2);
				}

		$row['price'] = number_format($row['price'],4);
		$row['high24hour'] = number_format($row['high24hour'],2);
        $row['low24hour'] = number_format($row['low24hour'],2);
		

		/*$row['mktcap'] = number_format($row['mktcap'],2);*/
		$row['mktcap'] = number_format($row['mktcap'],0);
		echo json_encode($row);
		exit;
	
	}

	function load_data()
	{
	    
		$time_start = microtime(true);
		$data = "";
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }

		 if(isset($_POST['per_page']))
		 {
			
			 if($_POST['per_page']>0)
			 {
				  $per_page = $_POST['per_page'];
			 }
			 else
			 {
				 $per_page = $this->settings->settings_records_per_page_front;
			 }
		 }
		 else
		 {
			 $per_page = $this->settings->settings_records_per_page_front;
		 }
        $pageLimit= $per_page*$id;

		$ar = "";
		$ar1="";
		if(isset($_POST['e1']))
		{
			$ar1 = explode(",",$_POST['e1']);
			foreach($ar1 as $key=>$value)
			{
				$ar[$key] =$value;
			}
		}

		
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				$row_columns[] = $row2;
			}
		}
	
	if($this->input->post('currency') && $this->input->post('currency')!="undefined")
		{
			$currency = $this->input->post('currency');
		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
				//echo "df";
				
			}
			else
			{
				$currency = "";
			}
			
			if($currency=="")
			{
			$currency = "USD";
			}
		}

		if($this->session->userdata('user_id'))
			{
				
				$update_array = array("base_currency"=>$currency);
				$this->db->set($update_array)->where("id",$this->session->userdata('user_id'))->update(USER);
			}
			
			$sorting_option = $this->input->post('sorting');
			
			
			if($this->input->post('sorting_price'))
			{
				$sorting_price = $this->input->post('sorting_price');
			}
			else
			{
				$sorting_price = "DESC";
			}
			
			
			if($this->input->post('sorting_market'))
			{
				$sorting_market = $this->input->post('sorting_market');
			}
			else
			{
				$sorting_market = "DESC";
			}
			
		
		 $sorting_change = $this->input->post('sorting_change');
		
			if(isset($sorting_change))
			{
				
			}
			else
			{
				 $sorting_change ="24H";
			}
			
			if($sorting_option=="price")
			{
				 $sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".price AS DECIMAL(38,10)) ".$sorting_price;
			}
			elseif($sorting_option=="marketcap")
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) ".$sorting_market;
			}
			elseif($sorting_option=="volume")
			{
				//$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".changepct24hour AS DECIMAL(38,10)) desc";
				
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) desc";
			}
			
			$search_text = $this->input->post('search');
			
			if(trim($search_text)!="")
			{
				$ser = COIN.".coinname LIKE '".$this->input->post('search')."%' or ".COIN.".name LIKE '".$this->input->post('search')."%'";
			}
			else
			{
				$ser = COIN.".status='1'";
			}

		$sql = "SELECT *
FROM ci_coin
LEFT JOIN ci_coin_history_temp_".$currency." ON ci_coin.id = ci_coin_history_temp_".$currency.".coin_id
 where   $ser and ci_coin_history_temp_".$currency.".price!='0.00' $sorting limit $pageLimit,$per_page";
		$query =$this->db->query($sql);
			$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");

				if($sorting_change=="24H")
				{
					$hour = 24;
				}
				elseif($sorting_change=="1H")
				{
					$hour = 1;
				}
				elseif($sorting_change=="1W")
				{
					$hour = 168;
				}
				elseif($sorting_change=="1M")
				{
					$hour = 720;
				}
				elseif($sorting_change=="3M")
				{
					$hour = 2160;
				}elseif($sorting_change=="6M")
				{
					$hour = 4320;
				}
				elseif($sorting_change=="12M")
				{
					$hour = 8640;
				}
				elseif($sorting_change=="ALL")
				{
					$hour = 8760;
				}
				else
				{
					$hour = 24;
				}

	$meeting_time1 = date('Y-m-d H:i:s', time() - 60 * 60 * $hour);
    $meeting_time1_string = strtotime($meeting_time1);
		if ($query->num_rows() > 0)
		{
			$i = $pageLimit+1;
		$b = 0;
            foreach ($query->result_array() as $row)
			{

				if($sorting_option=="marketcap" && $sorting_market=="DESC")
				{
				
				$update_data = array("sortorder"=>$i);
				$this->db->set($update_data)->where("id",$row['coin_id'])->update(COIN);

				 $row['sortorder'] = $i;
				}
				
				
				if($sorting_change!="24H" && $sorting_change!="")
				{
//$old_price = $this->boost_model->getValue2("ci_coin_history_".$currency,"price","coin_id='".$row['coin_id']."' and created_time < '$meeting_time1' ORDER by id desc limit 1");
	 $Data2 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym='.$row['name'].'&tsyms='.$currency.'&ts='.$meeting_time1_string),true);
	 $lowHighData = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym='.$row['name'].'&limit=168&tryConversion=false&tsym='.$currency),true);
	 $arr = $lowHighData['Data'];
     $old_price = $Data2[$row['name']][$currency];
	 $newprice = $row['price'] - $old_price;
	 $change24 = $newprice / $row['price'];
	 $change24 = number_format($change24 * 100,2);
	            $calc = ($change24 / 100);
		        $row['volume24hour'] = $calc * $row['price'];
		        $row['change24hour'] = $change24; 
				}
				else
				{
				 $row['volume24hour'] = $row['change24hour'];
				 $row['change24hour'] = $row['changepct24hour'];
				}
                $data[] = $row; 
				$i++;
            }
        }
		if (is_array($data))
		{
            foreach ($data as $row)
			{
				$list = $row;
				$list['id'] = $list['coin_id'];
				
				$exist = $this->boost_model->getValue("ci_watchlistassets","count(*)","watch_userid='".$this->session->userdata('user_id')."' AND watch_coinid='".$list['id']."'");

				if($exist>0)
				{
					$heartcls = "";
				}
				else
				{
					$heartcls = "-o";
				}
				
				if($list['change24hour']>0)
							{
							$changecolor='up';
							$minus='+';
							}
							else
							{
								$changecolor='down';
								$minus='-';
							}
			
					$url = base_url().$list['name'];
					$url = "'".$url."'";
				    if($sorting_change=="24H")
    				{
    					$highprice = $list['high24hour'];
    					$lowprice = $list['low24hour'];
    				}
    				elseif($sorting_change=="1H")
    				{
    					$highprice = $list['high1hour'];
    					$lowprice = $list['low1hour'];
    				}
    				elseif($sorting_change=="1W")
    				{
    					$highprice = $list['high1week'];
    					$lowprice = $list['low1week'];
    				}
    				elseif($sorting_change=="1M")
    				{
    					$highprice = $list['high1month'];
    					$lowprice = $list['low1month'];
    				}
    				elseif($sorting_change=="3M")
    				{
    					$highprice = $list['high3months'];
    					$lowprice = $list['low3months'];
    					
    				}elseif($sorting_change=="6M")
    				{
    					$highprice = $list['high6months'];
    					$lowprice = $list['low6months'];
    				}
    				elseif($sorting_change=="12M")
    				{
    					$highprice = $list['high1year'];
    					$lowprice = $list['low1year'];
    				}
    				elseif($sorting_change=="ALL")
    				{
    					$highprice = $list['allhigh'];
    					$lowprice = $list['alllow'];
    				}
    				else
    				{
    					$highprice = $list['high24hour'];
    					$lowprice = $list['low24hour'];
    				}
				
					echo '<section class="coin_list" id="'.$list['id'].'">
				<section class="container coin_group desktop-marg" >
					<div id="c_'.$list['id'].'" class="row coin_dist"  >';
					
					if($this->session->userdata('user_id')){
					    echo '<!--<a class="coin_fav" onclick="addportfolio('.$list['id'].')" id="heart_'.$list['id'].'" href="javascript:;">-->
						<div class="col-2 col-md-1 bg_alg4 watch_heart" onmouseover="watchhover(this)" id="'.$list['id'].'" style="position:relative;display:inline-block;cursor:pointer;">
                        	
                        	<i class="fa fa-heart'.$heartcls.' heart_cor" id="remove_'.$list['id'].'" aria-hidden="true"></i>
                        	<div class="watch_tool">
                            	<div class="watch_tooltips">
                            		<span>
                            			<a class="watch_hover" href="javascript:void(0);">
                            				<ul>
                            					<li><i class="fa fa-heart heart_cor" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                            					<li>On hold (15 Crypto assets)</li>
                            				</ul>
                            			</a>';
                            			$watchlist_sql = "SELECT * FROM ci_watchlist WHERE user_id='".$this->session->userdata('user_id')."' ORDER BY watchlist_id DESC";
		                                $watchlist_query =$this->db->query($watchlist_sql);
		                                if ($watchlist_query->num_rows() > 0)
                                    	{
                                	    foreach ($watchlist_query->result_array() as $row)
		                                {
		                                $watch_sql = "SELECT * FROM ci_watchlistassets WHERE watch_nameid ='".$row['watchlist_id']."' AND watch_userid='".$this->session->userdata('user_id')."'";
		                                $watch_query =$this->db->query($watch_sql);
                            			echo '<a class="watch_hover watchlist_home" onclick="addwatchlist('.$list['id'].','.$row['watchlist_id'].')" id="'.$list['id'].'" rev="'.$row['watchlist_id'].'" href="javascript:void(0);">
                            				<ul>
                    <li><i class="fa fa-heart-o heart_cor watch'.$row['watchlist_id'].'" id="assests_'.$list['id'].$row['watchlist_id'].'" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                            					<li>'.$row['watchlist_name'].' ('.$watch_query->num_rows().' Crypto assets)</li>
                            				</ul>
                            			</a>';
		                                }
                                    	}
                            			echo '<a class="watch_hover"  href="profile/watchlist">
                            				<ul>
                            					<li><i class="fa fa-plus" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                            					<li>Create new watch list</li>
                            				</ul>
                            			</a>
                            				<a class="watch_hover" href="javascript:void(0);" id="removeFromlist_'.$list['id'].'" onclick="removehomeFromlist('.$list['id'].');">
                            				<ul style="border:1px solid #ccc;">
                            					<li><i class="fa fa-trash" aria-hidden="true" style="font-weight:bold;"></i></li>
                            					<li>Remove from the list</li>
                            				</ul>
                            			</a>
                            		</span>
                            	</div>
                            </div>
                        
                        	</div>	<!--</a>-->';
					} else{
                        echo '<!--<a class="coin_fav" onclick="addportfolio('.$list['id'].')" id="heart_'.$list['id'].'" href="javascript:;">-->
						<div class="col-2 col-md-1 bg_alg4 watch_heart" style="position:relative;display:inline-block;cursor:pointer;">
                        	<i class="fa fa-heart'.$heartcls.' heart_cor" aria-hidden="true"></i>
                        	<div class="watch_tool">
                            	<div class="watch_tooltips">
                            		<span>
                            			<div style="cursor:default">
                            				<ul>
                            					<li>Sign in to add this coin to a watchlist.</li>
                            				</ul>
                            			</div>
                            			<a class="watch_hover" href="/signin/login">
                            				<ul style="border:1px solid #ccc;">
                            					<li><i class="fa fa-sign-in" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                            					<li style="color:#753ac8;font-weight:bold;">SIGN IN</li>
                            				</ul>
                            			</a>
                            		</span>
                            	</div>
                            </div>
                        	</div>	<!--</a>-->';
					}
						echo '<div style="cursor:pointer;" onclick="gotocoin('.$url.');" class="col-4 col-md-3 coin_merge bg_alg1"><span class="coin_count">'.$list['sortorder'].'</span><span class="coin_img"><a href="'.base_url().$list['name'].'"><img src="'.COIN_URL.$list['image'].'" alt="'.$list['name'].'"></a></span><span class="coin_name">'.$list['coinname'].'<br>
						<a href="'.base_url().$list['name'].'"><span class="coin_sh coin_cap">'.$list['name'].'</span></a></span></div>
                        <div style="cursor:pointer;" onclick="gotocoin('.$url.');" class="col-3 col-md-3 coin_cap bg_alg3"><span><i class="fa fa-'.strtolower($currency).'" aria-hidden="true"></i>&nbsp;<span id="beat'.$list['id'].'market">'.number_format($list['mktcap'],0).'</span></span></div>
						<div style="cursor:pointer;" onclick="gotocoin('.$url.');" class="col-3 col-md-2 bg_alg2"><span><i class="fa fa-'.strtolower($currency).'" aria-hidden="true"></i>&nbsp;<span id="beat'.$list['id'].'price">'.number_format($list['price'],4).'</span></span></div>
						<div class="col-2 col-md-2 bg_alg4" style="max-width:12%">
							<div style="cursor:pointer;" onclick="gotocoin('.$url.');" id="changecolor'.$list['id'].'" class="text-left tooltip1 coin_hr c_'.$changecolor.'">
								<span style="z-index:1018 !important" id="beat'.$list['id'].'minus2">'.$minus.'</span><span style="font-size:14px;z-index:1018 !important;" id="beat'.$list['id'].'change">'.number_format(abs($list['change24hour']),2).'</span><span style="font-size:14px;z-index:1018 !important;">%</span> 
								<span class="toolhover_cls"> <b><span id="beat'.$list['id'].'minus">'.$minus.'</span></b> <b>'.$currency_symbol.'&nbsp;<span id="beat'.$list['id'].'hour"> '.number_format(abs($list['volume24hour']),2).'</span></b> </span>
							</div> 
						<!--<a class="coin_fav" onclick="addportfolio('.$list['id'].')" id="heart_'.$list['id'].'" href="#"><i class="fa fa-heart'.$heartcls.' heart_cor" aria-hidden="true"></i></a>-->
						</div>
						<div style="cursor:pointer;max-width: 11%;" onclick="gotocoin('.$url.');" class="col-2 col-md-2 bg_alg4" style="max-width: 12%;">
							<span><i class="fa fa-usd" aria-hidden="true"></i>&nbsp;<span "beath'.$list['id'].'price">'.number_format($highprice,2).'</span></span>
				    	</div>
				    	<div style="cursor:pointer;max-width: 12%;" onclick="gotocoin('.$url.');" class="col-2 col-md-2 bg_alg4" style="max-width: 12%;">
							<span><i class="fa fa-usd" aria-hidden="true"></i>&nbsp;<span "beatl'.$list['id'].'price">'.number_format($lowprice,2).'</span></span>
				    	</div>
					</div>
				</section>
			</section>';
            }
            
        }
		else
		{
			echo "<center><font color='red'>--No records found--</font></center>";
		}
			
			$sql = "SELECT *
FROM ci_coin
LEFT JOIN ci_coin_history_temp_".$currency." ON ci_coin.id = ci_coin_history_temp_".$currency.".coin_id
 where   $ser";
		$query =$this->db->query($sql);
		$paginationCount = $this->getPagination($query->num_rows(),$per_page);
		$prev_id = $_POST['pageId']-1;
		$next_id = $_POST['pageId']+1;
		
		echo "<script>document.getElementById(\"coins_count\").value = ".$query->num_rows().";</script>";
		$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;
exit();
}
 function getPagination($count,$per_page){
      $paginationCount= floor($count / $per_page);
      $paginationModCount= $count % $per_page;
      if(!empty($paginationModCount)){
               $paginationCount++;
      }
      return $paginationCount;
     }
}