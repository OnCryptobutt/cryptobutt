<style>
    .wallet_b a::after {
    content: "";
    background: #FFEB3B;
    height: 5px;
    position: absolute;
    width: 100%;
    left: 0px;
    bottom: -1px;
    transition: all 250ms ease 0s;
    transform: scale(1);
}
.wallet_b{display: flex;align-items: center;max-width:14%; margin-right: 20px;}
.txt_wallet{color:#FFEB3B;}
.wallet_txtbg{background:#000;}
.wallet_following{background: #FFEB3B;color:#000;font-weight: bold;padding: 2px 10px;}
.addwallet{display: flex;align-items: center;}
</style>
<section class="wallet_txtbg col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="container-bk">
    	<div class="page-header row">
    	    <div class="col-lg-1 col-md-2 col-sm-2 col-4">
    		    <img class="img-rounded" src="https://www.cryptobutt.com/img/nullstyle_1360110274_82.jpg" alt="Scott Fleckenstein" style="width: 50px;height: 50px;margin:10px 0;color:#FFEB3B;">
    		</div>
    		<div class="col-lg-2 col-md-2 col-sm-2 col-5 wallet_b">
    		    <a href="javascript:;"><span class="txt_wallet">Your wallet balance</span></a>
    		</div>
    		<div class="col-lg-5 col-md-5 col-sm-5 col-5" style="display: flex;align-items: center;">
    		    <a href="javascript:;"><span class="txt_wallet">Your watch list</span></a>
    		</div>
    		 <div class="addwallet col-lg-2 col-md-4 col-sm-4 col-3">
        		<a href="javascript:;" class="wallet_following">Add wallet</a>	
            </div>
    	</div>
	</div>
</section>
<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 3% 0; background: #F3E5F5;">
	<div class="container-bk">
    	<div class="page-header row" >
    	    <div class="col-lg-1 col-md-2 col-sm-2 col-4"></div>
    		<div class="col-lg-2 col-md-2 col-sm-2 col-5" style="max-width:13%"></div>
    		<div class="col-lg-5 col-md-5 col-sm-5 col-5">
    		    <a href="javascript:;" style="font-size: 22px;color:#753ac8">$ 0.00</a><br/><a href="javascript:;" style="font-size: 14px;color:#753ac8">Your nano ledger balance</a>
    		</div>
    		 <div class="addwallet col-lg-4 col-md-4 col-sm-4 col-3">
        		<h2>No wallet found</h2>
            </div>
    	</div>
	</div>
</section>
<section class="desktop-marg purple" style="padding: 0 15%;">
    <nav class="navbar navbar-expand-lg navbar-light">
    <div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
    	<a href="javascript:;">
            <div class="col-1 col-md-1 tog"></div>
    		<div class="tog col-3 col-md-3 coin-tag">
    			<p class="collapse_nav_price">Crypto assets name</p>
    			<p class="collapse_nav_htl">All (820)</p>
    		</div>
            <div class="tog coin_cap col-3 col-md-3">
    			<p class="collapse_nav_price">Balnce</p>
    			<p id="sorting_market_html" class="collapse_nav_htl">High to Low</p>
    		</div>
    		<div class="tog col-2 col-md-2" style="width: 12%;">
    			<p class="collapse_nav_price">Change</p>
    			<p id="sorting_change_html" class="collapse_nav_htl">1 Day</p>
    
    		</div>
            <div class="tog col-2 col-md-2" style="width:11%">
                <p class="collapse_nav_price" style="visibility:hidden">1 Day high</p>
                <p class="collapse_nav_htl" id="filter_txt1">1 Day high</p>
    		</div>
    		<div class="tog col-2 col-md-2" style="width:12%">
    		    <p class="collapse_nav_price" style="visibility:hidden">1 Day low</p>
    		    <p class="collapse_nav_htl" id="filter_txt2">1 Day low</p>
    		</div>
    	</a>
    </div>
    </nav>
    <div class="collapse" id="collapse1">
      <div class="card card-block">
    	<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
            <div class="col-1 col-md-1 tog"></div>
    		<div class="tog col-2 col-md-3" style="color:#FFEB3B;left: 25px;">All</div>
    		<div class="tog coin_cap col-4 col-md-3 select-txt">
    			<p><a id="f3" href="javascript:void(0);" style="color:#FFEB3B;">High to Low</a></p>
    			<p><a id="f4" href="javascript:void(0);">Low to High</a></p>
    		</div>
    		<div class="tog col-4 col-md-2 select-txt">
    			<p><a id="f1" href="javascript:void(0);">High to Low</a></p>
    			<p><a id="f2" href="javascript:void(0);">Low to High</a></p>
    		</div>
    		<div class="tog col-4 col-md-2 select-txt" style="width:12%">
            <p><a id="f5" href="javascript:void(0);">1 hour</a></p>
    		<p><a id="f6" href="javascript:void(0);"  style="color:#FFEB3B;">1 Day</a></p>
    		<p><a id="f7" href="javascript:void(0);">1 Week</a></p>
    		<p><a id="f8" href="javascript:void(0);">1 Month</a></p>
            <p><a id="f9" href="javascript:void(0);">3 Months</a></p>
            <p><a id="f10" href="javascript:void(0);">6 Months</a></p>
            <p><a id="f11" href="javascript:void(0);">1 Year</a></p>
            <p><a id="f12" href="javascript:void(0);">All</a></p>
    		</div>
    	</div>
      </div>
    </div>
</section>
<section class="coin_group col-lg-12 col-md-12 col-sm-12 col-12 coin_list" style="height:800px">
	<div>
       
	</div>
</section>