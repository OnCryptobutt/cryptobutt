<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_addportfolio extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
    }
	
	function add()
	{
		if($this->session->userdata('user_id'))
		{
			$cid = (int)$this->input->post('cid');
			if($cid)
			{
				$exist = $this->boost_model->getValue(PORTFOLIO,"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$cid."'");
				if($exist>0)
				{
				    
				    $remove_data = array("portfolio_user_id"=>$this->session->userdata('user_id'),
					                     "portfolio_coin_id"=>$cid);
					//$this->db->set($remove_data)->delete(PORTFOLIO);
					$this->db->delete('ci_portfolio',array("portfolio_user_id"=>$this->session->userdata('user_id'),"portfolio_coin_id"=>$cid));
					echo "exit";
				}
				else
				{
					$insert_data = array("portfolio_user_id"=>$this->session->userdata('user_id'),
					                     "portfolio_coin_id"=>$cid,
										 "created_time"=>NOW
										 );
					$this->db->set($insert_data)->insert(PORTFOLIO);
					
					echo "success";
				}
			}
			else
			{
				echo "error";
			}
		}
		else
		{
			echo "error";
		}
		exit;
		
	}
	function removeFavourite()
	{
		if($this->session->userdata('user_id'))
		{
			$cid = (int)$this->input->post('cid');
			if($cid)
			{
				$exist = $this->boost_model->getValue(PORTFOLIO,"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$cid."'");
				if($exist>0)
				{
					
					$remove_data = array("portfolio_user_id"=>$this->session->userdata('user_id'),
					                     "portfolio_coin_id"=>$cid);
					//$this->db->set($remove_data)->delete(PORTFOLIO);
					$this->db->delete('ci_portfolio',array("portfolio_user_id"=>$this->session->userdata('user_id'),"portfolio_coin_id"=>$cid));
						$sql = "SELECT * FROM ci_portfolio WHERE portfolio_user_id='".$this->session->userdata('user_id')."'";
		
	                	//echo $sql;
	                	$query =$this->db->query($sql);
	                	
					echo $query->num_rows();
				//	echo "success";
				}
				else
				{
					echo "exit";
				}
			}
			else
			{
				echo "error";
			}
		}
		else
		{
			echo "error";
		}
		exit;
		
	}
	
}


