<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_get_details extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url());
		}
    }
	
	function edit_details()
	{
		if($this->input->post('did'))
		{
			$this->db->select('*');
			$this->db->from(PORTFOLIO_DETAILS);
			$this->db->where("portfolio_details_id",$this->input->post('did'));
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				$row = $query->row();
				
				$coinname = $this->boost_model->getValue(COIN,"name","id='".$row->portfolio_details_coin_id."'");
				
				$is_coin = $this->boost_model->getValue(CURRENCY,"is_coin","currency_code='".$row->portfolio_currency."'");
				
				if($is_coin==1)
				{
					$approxmate = "block";
				}
				else
				{
					$approxmate = "none";
				}
				
				if($row->portfolio_details_date)
				{
					$split = explode("-",$row->portfolio_details_date);
					$date  = $split[2];
					$month = $split[1];
					$year  = $split[0];
				}
				
				echo '<form class="form-horizontal">
				<input type="hidden" id="edit_id" value="'.$this->input->post('did').'"/>
				<input type="hidden" id="coin_id" value="'.$row->portfolio_details_coin_id.'"/>
				<input type="hidden" id="portfolio_id" value="'.$row->portfolio_details_portfolio_id.'"/>
  <!--<div class="form-group">
    <label class="control-label col-sm-2" for="Date">Date:</label>
    <div class="col-sm-10">
      <input id="date" required name="date" type="text" class="form-control" placeholder="Date" value="'.$row->portfolio_details_date.'">
	  <br/>
	  DATE FORMAT : YYYY-MM-DD
    </div>
  </div>-->
  <div class="form-group">
    <label class="control-label col-sm-5" for="pwd">Amount of btc bought:</label>
    <div class="col-sm-7"> 
      <input id="coin_brought" required name="coin_brought" type="text" class="form-control" placeholder="" value="'.$row->portfolio_details_coin_brought.'">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-5" for="pwd">Exchange Type:</label>
    <div class="col-sm-7"> 
      <select  id="exchange_type" required class="form-control" name="exchange_type">';
	  
	  if(is_array($this->settings->currency_list))
	  {
		  foreach($this->settings->currency_list as $list)
		  {
			  if($row->portfolio_details_exchange_type==$list->currency_code)
			  {
				  $selected = "selected='true'";
			  }
			  else
			  {
				  $selected = "";
			  }
			  echo '<option '.$selected.' value="'.$list->currency_code.'">'.$coinname.' / '.$list->currency_code.'</option>';
		  }
	  }
	  
	  echo '</select>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-5" for="pwd">Amount Spent:</label>
    <div class="col-sm-7"> 
	  <div class="col-xs-6 col-sm-6 padding-left_0">
      <input required id="details_amount" name="details_amount" type="text" class="form-control" value="'.$row->portfolio_details_amount.'"></div>';
	  
	  echo '<div class="col-xs-6 col-sm-6 padding-left_0"><select class="form-control" onchange="editamount_display('."'".'exchange_type'."'".');" id="currency" required name="currency">';
	  
	  if(is_array($this->settings->currency_list))
	  {
		  foreach($this->settings->currency_list as $list)
		  {
			  if($row->portfolio_currency==$list->currency_code)
			  {
				  $selected = "selected='true'";
			  }
			  else
			  {
				  $selected = "";
			  }
			  echo '<option '.$selected.' value="'.$list->currency_code.'">'.$list->currency_code.'</option>';
		  }
	  }
	  
	  echo '</select></div>';
    echo '</div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-5 nowrap" for="pwd">Date of purchase:</label>
    <div class="col-sm-7"> 
		<div class="col-xs-4 col-sm-4 padding-left_0">
			<input type="text" class="form-control" id="date" value="'.$date.'" placeholder="Date">
		</div>
		<div class="col-xs-4 col-sm-4 padding-left_0">
			<input type="text" class="form-control" id="month" value="'.$month.'" placeholder="Month">
		</div>
		<div class="col-xs-4 col-sm-4 padding-left_0">
			<input type="text" class="form-control" id="year" value="'.$year.'" placeholder="Year">
		</div>
	</div>
  </div>
  
  <div style="display:'.$approxmate.'" id="editamount_display" class="form-group">
    <label class="control-label col-sm-5" for="pwd">1 Coin IS APPROXIMATELY:</label>
    <div class="col-sm-7">
		<div class="col-xs-6 col-sm-6 padding-left_0">
			<input required id="amount2" class="form-control" name="amount2" type="text" value="'.$row->portfolio_amount2.'"></div>';
		
	  echo '<div class="col-xs-6 col-sm-6 padding-left_0"><select id="currency2" class="form-control" required name="currency">';
	  
	  if(is_array($this->settings->currency_only))
	  {
		  foreach($this->settings->currency_only as $list)
		  {
			  if($row->portfolio_currency2==$list->currency_code)
			  {
				  $selected = "selected='true'";
			  }
			  else
			  {
				  $selected = "";
			  }
			  echo '<option '.$selected.' value="'.$list->currency_code.'">'.$list->currency_code.'</option>';
		  }
	  }
	  
	  echo '</select></div>';
    echo '</div>
  </div>
  
  
</form>
';
				
			}
		}
	}
	
	function save_edit_details()
	{
		if($this->input->post('id') && $this->input->post('coin_brought') && $this->input->post('exchange_type') && $this->input->post('details_amount') && $this->input->post('date') && $this->input->post('month') && $this->input->post('year') && $this->input->post('currency'))
		{
			$datesave = $this->input->post('year')."-".$this->input->post('month')."-".$this->input->post('date');
			
			$coin_id = $this->boost_model->getValue(PORTFOLIO_DETAILS,"portfolio_details_coin_id","portfolio_details_id='".$this->input->post('id')."'");
			
			//$price = $this->boost_model->getValue(COIN_HISTORY,$this->input->post('currency'),"coin_id='".$coin_id."' AND column_id='1' order by id desc");
			
			$price = $this->boost_model->getValue('ci_coin_history_temp_'.$this->input->post('currency'),'price',"coin_id='".$coin_id."' order by id desc");
			
			
			$update_data = array("portfolio_details_coin_brought"  => $this->input->post('coin_brought'),
								 "portfolio_details_exchange_type" => $this->input->post('exchange_type'),
								 "portfolio_details_amount"        => $this->input->post('details_amount'),
								 "portfolio_details_date"        => $datesave,
								 "coin_price"                => $price,
								 "portfolio_currency"         => $this->input->post('currency'),
								 "portfolio_currency2"        => $this->input->post('currency2'),
								 "portfolio_amount2"          => $this->input->post('amount2'),
								 "portfolio_details_updated_time"  => NOW);
			$this->db->set($update_data)->where("portfolio_details_id",$this->input->post('id'))->update(PORTFOLIO_DETAILS);
			
			$success = $this->boost_model->showNotify("success","Records updated successfully");
			
			echo $success;
		}
		else
		{
			$success = $this->boost_model->showNotify("error","Something going to wrong");
			
			echo $success;
		}
		exit;
	}
}