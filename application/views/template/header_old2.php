<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CryptoButt</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
    <script src="https://use.fontawesome.com/c596d80306.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" id="theme-stylesheet">
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">
	<link href="<?php echo base_url();?>css/flags.css" rel="stylesheet">
    
  </head>
  <body>
    <header class="header">   
      <nav class="navbar navbar-expand-lg">
		<div class="search-panel">
          <div class="search-inner d-flex align-items-center justify-content-center">
            <div class="close-btn">Close <i class="fa fa-window-close" aria-hidden="true"></i></div>
            <form id="searchForm" action="#">
              <div class="form-group">
                <input type="search" id="searchc" name="search" placeholder="What are you searching for...">
				<div id="suggesstion-box"></div>
                <button type="submit" class="submit">Search</button>
              </div>
            </form>
          </div>
        </div>
        		
		<div class="container-fluid d-flex align-items-center justify-content-between">
			<div class="navbar-header">
				<a href="<?php echo base_url();?>" class="navbar-brand"><img src="<?php echo SETTINGS_URL.$this->settings->settings_logo_photo;?>" class="img-fluid"></a>
			</div>
          <ul class="right-menu list-inline no-margin-bottom">    
			<li class="list-inline-item"><a href="#" class="search-open nav-link"><i class="fa fa-search" aria-hidden="true"></i> Coins</a></li>
			
			<?php if($this->input->get('currency'))
			{
				$currencyF = substr($this->input->get('currency'), 0, 2);
			}
			else
			{
					if($this->session->userdata('user_id'))
					{
						$currencyF = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
						$currencyF = substr($currencyF, 0, 2);
						
					}
					else
					{
						$currencyF = "";
					}
					
					if($currencyF=="")
					{
					$currencyF = "US";
					}
			}
			?>
			
			<li class="list-inline-item">
				<div class="container">
					<form class="form-horizontal">
						<div class="form-group">
							<div class="flagstrap" id="select_country" data-input-name="NewBuyer_country" data-selected-country="<?php echo $currencyF;?>"></div>
						</div>
					</form>
				</div>
			</li>
			<?php if($this->session->userdata('user_id')){
           
			?>
			
            <!--<li class="list-inline-item logout"><a id="logout" href="<?php echo base_url('Logout');?>" class="nav-link">Logout <i class="fa fa-sign-out" aria-hidden="true"></i>	</a></li>-->
			 <li class="list-inline-item logout"><a id="logout" href="<?php echo base_url('profile');?>" class="nav-link"><?php echo $query_user_row->firstName;?><img src="<?php echo $query_user_row->photoURL;?>" alt="..." class="img-fluid"> </a></li>
			<?php } else { ?>
			<li class="list-inline-item logout"><a id="login" href="<?php echo base_url('hauth/login/Google');?>" class="nav-link">Login <i class="fa fa-sign-in" aria-hidden="true"></i>	</a></li>
			<?php } ?>
          </ul>
        </div>
		
      </nav>
    </header>
