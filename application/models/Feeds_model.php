<?php
class Feeds_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	function getBanner()
	{
		$this->db->select("*");
		$this->db->from(BANNER);
		$this->db->where("banner_status","1");
		$this->db->order_by("banner_order","ASC");
		$query = $this->db->get();
		return $query->result();
	}
}
?>