<input type="hidden" id="hidden_search" value="<?php echo $this->input->get('search');?>"/>
<input type="hidden" id="hidden_per_page" value=""/>
<input type="hidden" id="hidden_currency" value="<?php echo $this->input->get('currency');?>"/>
<input type="hidden" id="hidden_sorting" value=""/>
<input type="hidden" id="row_no" value="1">
<input type="hidden" id="pageids" value="0">
<input type="hidden" id="coins_count" value="<?php if(isset($total_list)) { echo $total_list; }?>">

<input type="hidden" id="sorting_price" value="DESC"/>
<input type="hidden" id="sorting_market" value="DESC"/>
<input type="hidden" id="sorting_change" value="24H"/>

<section class="purple col-lg-12  col-md-12 col-sm-12 col-xs-12">
	<?php
	//print_r($query_people_row);
	
	?>
		<div class="container-bk" >
			<div class="page-header row">
			    <div class="col-lg-1  col-md-2 col-sm-2 col-4">
				    <img class="img-rounded" src="<?php echo base_url().'img/'.$query_people_row->keypeople_filename;?>" alt="<?php echo $query_people_row->keypeople_fullname;?>" >
				</div>
				<div class="col-lg-9  col-md-7 col-sm-7 col-5" style="display: flex;align-items: center;">
				    <span class="txt_wht">Keypeople <?php echo $query_people_row->keypeople_fullname;?></span>
				   
				</div>
				 <div class="profile-action col-lg-2  col-md-4 col-sm-4 col-3">
                		<a href="javascript:;" class="coin-follow following" >
                			<span class="fa fa-plus"></span> Following
                		</a>	
                    </div>
			</div>
			</div>
		</div>
				
</section>
<section class="coin_group col-lg-12  col-md-12 col-sm-12 col-12 coin_list ">
	<div>
        <div class="bg_alg2-profile">
    	    <h3 class="coint-txt-bold">Projects involved in</h3>
    	</div>
	</div>
</section>
<?php //print_r($keypeople_row); ?>
        <section  class="row">
		<section id="pageData1" class="col-lg-9 col-md-9 col-sm-9 col-12">
		    <?php
		        if(is_array($keypeople_row)) {
		        foreach($keypeople_row as $row) {
		            if($row->change24hour > 0)
							{
						
							    $minus='+';
							}
							else
							{
								
								$minus='-';
							}
							
						$exist = $this->boost_model->getValue('ci_portfolio',"count(*)","portfolio_user_id='".$this->session->userdata('user_id')."' AND portfolio_coin_id='".$row->id."'");
        				if($exist>0)
        				{
        					$heartcls = "";
        				}
        				else
        				{
        					$heartcls = "-o";
        				}	
							
							
		    ?>
		    <section>

				<section class="container-bk coin_group desktop-marg-bk coin_list ">
				    
					<div id="c_1360" class="row coin_dist">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-2 bg_alg2-profile">
                    	    <a class="coin_fav" onclick="addFavorite(<?php echo $row->id; ?>)" id="heart_<?php echo $row->id; ?>" href="javascript:;">
                    	        <i class="fa fa-heart<?php echo $heartcls; ?> heart_cor" aria-hidden="true"></i>
                    	    </a>
                    	</div>
                    	<div style="cursor:pointer;" onclick="gotocoin('<?php echo base_url().$row->name; ?>');" class="col-lg-1 col-md-1 col-sm-1 col-1 coin_merge bg_alg2-profile">
						    <span class="coin_img"><a href="<?php echo base_url().$row->name; ?>">
						        <img src="<?php echo base_url().'uploads/coin/'.$row->image; ?>" alt="<?php echo $row->name; ?>" style="width:40px;"></a>
						    </span>
						 </div>
						<div style="cursor:pointer;" onclick="gotocoin('<?php echo base_url(); ?>BTC');" class="col-lg-2 col-md-2 col-sm-2 col-2 coin_merge bg_alg2-profile coint-txt-purple">
						    <div class="coin_name coint-txt-bold"><?php echo $row->coinname; ?></div>
						    <div><a href="<?php echo base_url().$row->name; ?>"><span class="coin_cap coint-txt-purple"><?php echo $row->name; ?></span></a></div>
						 </div>

						<div style="cursor:pointer;" onclick="gotocoin('<?php echo base_url().$row->name; ?>');" class="col-lg-2 col-md-2 col-sm-2 col-6  bg_alg2-profile">
						    <div class="coint-txt-bold"><span><i class="fa fa-usd" aria-hidden="true"></i>&nbsp;<span id="beat1360price"><?php echo number_format($row->price,2); ?>
						    <span style="cursor:pointer;" onclick="gotocoin('<?php echo base_url().$row->name; ?>');" id="changecolor1360" class="text-left tooltip1 c_up">
							<span style="z-index:1018 !important" id="beat1360minus2"><?php echo $minus; ?></span>
							<span style="font-size:14px;z-index:1018 !important;" id="beat<?php echo $row->id; ?>change"><?php echo number_format(abs($row->change24hour),2); ?></span>
							<span style="font-size:14px;z-index:1018 !important;">%</span> 
							<!--<span class="toolhover_cls"> <b><span id="beat1360minus">+</span></b> <b>$&nbsp;
							<span id="beat1360hour"> 286.85</span></b> </span>-->
							</span></span></span></div>
						    <div class="coin_merge" style="padding:0 15%;"><span class="coin_cap">24 hours</span></div>
						</div>
                    	<div style="cursor:pointer;" onclick="gotocoin('<?php echo base_url().$row->name; ?>');" class="col-lg-2 col-md-2 col-sm-2 col-0 coin_cap bg_alg2-profile ">
						    <div class="coint-txt-bold founder-txt"><span>
						    <span id="beat1360market">$<?php echo $row->mktcap; ?></span></span></div>
						    <div class="coin_merge" style="padding:0 15%;"><span class="coin_cap">Market Cap</span></div>
                        </div>
						<div style="cursor:pointer;" onclick="gotocoin('<?php echo base_url().$row->name; ?>');" class="col-lg-3 col-md-3 col-sm-3 col-0 coin_cap bg_alg2-profile ">
						    <div class="coint-txt-bold founder-txt"><span>
						    <span id="beat1360market"><?php echo $row->keypeople_roletype; ?></span></span></div>
						    <div class="coin_merge" style="padding:0 15%;"><span class="coin_cap"><?php echo $row->keypeople_roletitle; ?></span></div>
                        </div>
						

					</div>

				</section>

			</section>
			<?php 
		            
		        } 
		        }
			
			?>
			</section>
			<section class="col-lg-3 col-md-3 col-sm-3 col-0">
			    <div class="bg_alg2-profile">
            	    <h3 class="coint-txt-bold">Social links</h3>
        			<ul class="dc_links">
        			    <?php
        			    $a=array('success','primary','info','warning','danger');
                        $random_keys=array_rand($a,5);
        			    if(is_array($keypeople_rowlinks)) {
        			       
		                    foreach($keypeople_rowlinks as $row) {
        			        
        			    ?>
        		    	<li class="badge btn-<?php echo $a[$random_keys[0]]; ?>"><a target="_blank" href="<?php echo $row->social_links_url; ?>"><i class="<?php echo $row->link_type_font_awesome; ?>"  aria-hidden="true"></i> <?php echo $row->link_type_name; ?></a></li>
        		    	<?php
        		    	
		                    }
        			    }
        		    	?>
        		    	
        			</ul>
    			</div>
			</section>
			</section>

<style>
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
    border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.dropdown-toggle::after {
    display: none;
}

.dc_links li a {
    color: #fff;
    font-size: 10px;
    line-height: 10px;
}
.dc_links li a:hover {
    color: #fff;
}


.row_background
{
	
background-color : #FFCDD2 !important;

}

.row_background3
{
	
background-color : #ccdefc !important;

}
.row_background2
{
	
background-color : #B9F6CA !important;

}
.flash
{
	margin-left: 45%;
}

.green
{
	color:green !important;
}

.red
{
	color:red !important;
}

.black
{
	color:black !important;
	font-weight:bold !important;
	padding:5px;
}

img.img-rounded {
    border-radius: 50%;
    margin-top: 15px;
	margin-bottom: 15px;
	width: 80px; height: 80px;    background: #fff;
    padding: 2px;
	
}

.border_left {
	border-left: 2px solid white;
}

.txt_wht {
	color: white;
}

.txt_alg_rit {
	text-align: right;
}

.log_sty {
	margin-top: 10px;
    margin-bottom: 0px;
	color: white !important;
}

.wid_50 {
	width: 50% !important;
}
</style>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border:1px solid #753ac8;">
      <div class="modal-header" style="background:#753ac8;">
        <button type="button" class="close" data-dismiss="modal" style="color:#fff">&times;</button>
        <h4 class="modal-title" ></h4>
      </div>
      <div class="modal-body">
        <p>Please login to continue. <a href="<?php echo base_url("hauth/Login/Google");?>" style="color:#753ac8">Click Here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background:#753ac8;color:#fff">Close</button>
      </div>
    </div>

  </div>
</div>
