<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in | CryptoButt</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="https://static.parastorage.com/services/third-party/fonts/Helvetica/fontFace.css">-->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
	<link rel="stylesheet" href="https://static.parastorage.com/services/login-statics/1.375.0/styles/main.css">
	<link rel="stylesheet" href="https://static.parastorage.com/services/login-statics/1.376.0/styles/main.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.toast.css">
  </head>
<!------ Include the above in your HEAD tag ---------->
<style>
body{font-family: HelveticaNeueW01-Thin,HelveticaNeueW02-Thin,HelveticaNeueW10-35Thin,sans-serif !important;}
.container.desktop .forgot-password-link:hover {
    color: #005EA5;
}
.container.desktop{overflow:auto;}
.btn.btn-primary1 {
    background-color: #753ac8;
    border-color: #753ac8;
    outline: none;
    color: #fff !important;
    padding-top: 8px;
}
.input-lg1 {
    padding: .200rem 3rem;
}
.container, body, html{max-width:100% !important;}
.container.desktop login .login-button, .container.desktop signup .login-button{padding-left:25px;}

/* The container */
.container-check {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 16px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container-check input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}
input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {background-color: #fff;}
/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 16px;
    width: 16px;
    background-color: #eee;
	border:1px solid #753ac8;
}

/* On mouse-over, add a grey background color */
.container-check:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container-check input:checked ~ .checkmark {
    background-color: #753ac8;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container-check input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container-check .checkmark:after {
    left: 5px;
    top: 1px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
input[type=text]:focus {
    border: 1px solid #753ac8;
}

.form-control:focus {
    border-color: #753ac8 !important;
    box-shadow: 0 0 0 1px #753ac8 !important;
}
#circle_tick,#circle_tick1,#circle_tick2{font-size: 24px;color: green;float: right;position: relative;top: -32px;left: 25px;}
#loademail,#loading,#resetloading{float: right;position: relative;top: -35px;left: 25px;}


</style>
<body>
<div class="container desktop">
<div class="header">
<img class="wix-logo" src="<?php echo base_url(); ?>img/logo.png" style="height:65px;">
<a class="x-close login-svg-font-icons-x" href="<?php echo base_url(); ?>"></a>
</div>
<reset-password id="reset-password">
<div><form class="reset-password" name="resetPasswordForm">
    <input type="hidden" name="username" id="username" value="<?php echo $emailToken; ?>" />
<div class="reset-password-title title">Select your new password:</div>
	<div class="form-group" style="width:75%;">
	  <label for="resetpassword">Enter new password:</label>
	  <input type="password" class="form-control" value="" id="resetpassword" autocomplete="nope">
	  <div id="resetmessage"></div>
	</div> 
	<div class="form-group" style="width:75%;">
	  <label for="resetpasswordconf">Retype new password:</label>
	  <input type="password" class="form-control" id="resetpasswordconf" autocomplete="nope">
	  <div id="resetmessage1"></div><div id="resetmessage2"></div>
	</div>   
	<div class="button-spinner">
	<a class="btn btn-primary1 input-lg input-lg1" name="submit" id="resetpwd" href="javascript:;" style="padding-top:5px">Change password</a>
	</div>
</form>
</div></reset-password>
<reset-password-confirmation id="reset-password-confirmation" style="display:none">
    <form class="reset-password-confirmation">
    <div class="title">Congratulations</div>
    <div class="content">Your password has been changed successfully.</div>
    <div class="button">
    <a class="btn btn-primary1 input-lg input-lg1" href="<?php echo base_url();?>">Go to homepage</a>
    </div>
    </form>
</reset-password-confirmation>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.toast.js"></script>
<script>
    $('#resetpwd').click(function(event){
        data  = $('#resetpassword').val();
        data1 = $('#resetpasswordconf').val();
        var len = data.length, len1 = data1.length;
        if(len < 1) {
             $('#resetmessage').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#resetmessage').html('');
        }
        if(len1 < 1) { 
             $('#resetmessage1').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#resetmessage1').html('');
        }
         if($('#resetpassword').val() != $('#resetpasswordconf').val()) {
            $('#resetmessage2').html('').html("<span style='color:#f00;'>Password don't match.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#resetmessage2').html('');
        }
        if($('#resetpassword').val()!='' && $('#resetpasswordconf').val() !='' && $('#resetpassword').val() == $('#resetpasswordconf').val()) {

        var dataString = 'password='+ $('#resetpassword').val()+'&username='+$('#username').val();
        $.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>signin/changePassword',
           data: dataString,
           cache: false,
           success: function(response){ 
			   if(response == 'success') {
			     $('#reset-password').hide();
			     $('#reset-password-confirmation').show();
    			 $.toast({
                    heading: 'Success',
                    text: 'Your password has been changed successfully.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
			} else {						
				$.toast({
                heading: 'Error',
                text: 'error',
                showHideTransition: 'fade',
                 position: 'bottom-right',
                icon: 'error'
            });
			}
                 
           }
    
    });
     } 
    });
</script>
</body>
</html>