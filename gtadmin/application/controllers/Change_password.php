<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_password extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('boost_model');
		$this->load->model('change_password_model');
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
	}
	
	public function index()
	{
		$data['basename'] = "change_password";
		$data['success']  = "";
		$data['title']    = "Change Password";
		
		if($this->session->flashdata('success'))
		{
			$data['success']  = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
	
		if($this->input->post('act')=="edit")
		{
			
			$this->form_validation->set_rules("old_password","Old Password","required");
			$this->form_validation->set_rules("new_password","New Password","required|matches[confirm_password]");
			$this->form_validation->set_rules("confirm_password","Confirm Password","required");
			$this->form_validation->set_message('matches', 'New password does not match with confirm password');
			if($this->form_validation->run()==TRUE)
			{
				if($this->change_password_model->checkOldPassword()==TRUE)
				{
					$this->change_password_model->changePassword();
					redirect(base_url()."logout/");
				}
				else
				{
					$data['success']  = $this->boost_model->showNotify("error","Invalid old password");
				}
			}
		}
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('change_password',$data);
		$this->load->view('template/footer',$data);
	}
	
	
}
