<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
    }
	
	public function index()
	{
		$data['basename'] = "dashboard";
		$data['title']    = "Dashboard";
		//$data['cmsActiveRecords']  = $this->boost_model->getValue(CMS,"count(*)","cms_status='1'");
		//$data['cmsSuspendRecords'] = $this->boost_model->getValue(CMS,"count(*)","cms_status='0'");
		
		$data['coinActiveRecords']  = $this->boost_model->getValue(COIN,"count(*)","status='1'");
		$data['coinSuspendRecords'] = $this->boost_model->getValue(COIN,"count(*)","status='0'");
		
		//$data['bannerActiveRecords']  = $this->boost_model->getValue(BANNER,"count(*)","banner_status='1'");
		//$data['bannerSuspendRecords'] = $this->boost_model->getValue(BANNER,"count(*)","banner_status='0'");
		//$data['galleryActiveRecords']  = $this->boost_model->getValue(GALLERY,"count(*)","gallery_status='1'");
		//$data['gallerySuspendRecords'] = $this->boost_model->getValue(GALLERY,"count(*)","gallery_status='0'");
		$data['userActiveRecords']  = $this->boost_model->getValue(ADMIN,"count(*)","admin_status='1' and admin_type!='SADMIN'");
		$data['userSuspendRecords'] = $this->boost_model->getValue(ADMIN,"count(*)","admin_status='0' and admin_type!='SADMIN'");
		//$data['categoryActiveRecords']  = $this->boost_model->getValue(CATEGORY,"count(*)","category_status='1'");
		//$data['categorySuspendRecords'] = $this->boost_model->getValue(CATEGORY,"count(*)","category_status='0'");
		//$data['subCategoryActiveRecords']  = $this->boost_model->getValue(SUB_CATEGORY,"count(*)","sub_category_status='1'");
		//$data['subCategorySuspendRecords'] = $this->boost_model->getValue(SUB_CATEGORY,"count(*)","sub_category_status='0'");
		//$data['productActiveRecords']  = $this->boost_model->getValue(PRODUCT,"count(*)","product_status='1'");
		//$data['productSuspendRecords'] = $this->boost_model->getValue(PRODUCT,"count(*)","product_status='0'");
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('dashboard',$data);
		$this->load->view('template/footer',$data);
	}
	
	
}
