<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>

  <ul class="nav nav-tabs" >
					<li class="active">
						<a  data-toggle="tab" href="#1"><i class="material-icons cor1">&#xE1B8;</i>&nbsp;Chart</a>
					</li>
					<!--<li><a  data-toggle="tab" href="#2"><i class="material-icons cor2">&#xE8E5;</i>&nbsp;Market</a>
					</li>
					<li><a  data-toggle="tab" href="#3"><i class="material-icons cor2">&#xE24F;</i>&nbsp;Historical Data</a>
					</li>-->
				</ul>
				
				  

	<div id="container" style="height: 600px; min-width: 310px">
	
	   <div class="highcharts-background" style="background-color: white;font-size: 23px;padding-bottom: 40px;padding-top: 40px;">
	     <span style="padding-left: 20px;">Bitcoin Charts</span> 
		 
		 <div align="center" style="padding-top:80px" class="flash2"></div> 
		
	   </div>
	   
	  
	   
	   <div id="container1" style="height: 600px; min-width: 310px"> </div>
	   
	     

	</div>

       

<script>

var seriesOptions = [],
    seriesCounter = 0,
    names = ['Mktcap','Price', 'Volume24Hour'];
	
	
	 $(".flash2").show();
     $(".flash2").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");

/*
 * Create the chart when all data is loaded
 * @returns {undefined}
 */
function createChart() {

    Highcharts.stockChart('container1', {

        rangeSelector: {
            selected: 4
        },

        yAxis: {
            labels: {
                formatter: function () {
                    return (this.value > 0 ? ' + ' : '') + this.value + '%';
                }
            },
            plotLines: [{
                value: 0,
                width: 2,
                color: 'silver'
            }]
        },

        plotOptions: {
            series: {
                compare: 'percent',
                showInNavigator: true
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
            valueDecimals: 2,
            split: true
        },

        series: seriesOptions
    });
}

$.each(names, function (i, name) {

    $.getJSON('<?php echo base_url(); ?>Coin_page/json_chart?coumn=' + name.toLowerCase() + '.json&currency=<?php echo $currency; ?>&cid=<?php echo $cid; ?>',    function (data) {

        seriesOptions[i] = {
            name: name,
            data: data
        };

        // As we're loading the data asynchronously, we don't know what order it will arrive. So
        // we keep a counter and create the chart when all the data is loaded.
        seriesCounter += 1;

        if (seriesCounter === names.length) {
			$(".flash2").hide();
            createChart();
        }
    });
});

<?php
if($this->uri->segment(2)=="chart")
{ ?>
function displaybalance(){
	

	 var currency = "<?php echo $currency; ?>";
	
     var dataString = 'currency='+ currency;
	 $("#bal_al").html("Loading...");
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('ajax_portfolio/load_portfolio_balance_display?page=details');?>',
           data: dataString,
           cache: false,
           success: function(result){
			//alert(result);
                 $("#pageData3").html(result);
           }
      });
}
displaybalance();
<?php } ?>

</script>
<?php
if($this->uri->segment(2)=="chart")
{ ?>
<div id="pageData3"></div> 
<?php } ?>