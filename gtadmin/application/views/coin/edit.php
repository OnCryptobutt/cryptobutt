<!-- Content Wrapper. Contains page content -->
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {    vertical-align: middle; }
    .btn{margin:10px;}
    /* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coin
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
		 
		  <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Coin Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
				<div class="box-body">
					<table class="table table-bordered">
						<tr><td><b>Coin Name</b></td><td><?php echo $records->name;?></td><td><b>Coin Image</b></td>
						<td><?php if($records->image){ ?><img width='50' height='50' src="<?php echo COIN_URL."".$records->image;?>"> <?php } else { echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; } ?></td>
						<td><b>Coin Status</b></td><td><?php if($records->status=="1"){ ?> <span class="label label-success">Active</span><?php } ?><br/>
						<?php if($records->status=="0"){ ?> <span class="label label-danger">Suspend</span><?php } ?></td>
						</tr>
						<!--<tr><td><b>Coin Image</b></td><td><?php if($records->image){ ?><img width='50' height='50' src="<?php echo COIN_URL."".$records->image;?>"> <?php } else { echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; } ?></td></tr>-->
						<!--<tr><td><b>Coin Status</b></td><td><?php if($records->status=="1"){ ?> <span class="label label-success">Active</span><?php } ?><br/>
						<?php if($records->status=="0"){ ?> <span class="label label-danger">Suspend</span><?php } ?></td></tr>-->
					</table>
              </div>
          </div>
		  
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
			  
			    <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Image</label>

                  <div class="col-sm-10">
                    <input type="file" name="image" >
					<span class="red"><?php echo form_error('image');?></span>
					<span class="red">Upload image size should not exceed 2 mb</span><br/>
					<span class="red">Supported formats (jpg,gif,png)</span>
                  </div>
                </div>
			  
			   <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Coin Code</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Coin Code" value="<?php echo $records->name;?>">
					<span class="red"><?php echo form_error('name');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Coin Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="coinname" class="form-control" id="inputEmail3" placeholder="Coin Name" value="<?php echo $records->coinname;?>">
					<span class="red"><?php echo form_error('coinname');?></span>
                  </div>
                </div>
				
				<!--
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Website</label>

                  <div class="col-sm-10">
                    <input type="text" name="website" class="form-control" id="inputEmail3" placeholder="Website" value="<?php echo $records->website;?>">
					<span class="red"><?php echo form_error('website');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">website 2</label>

                  <div class="col-sm-10">
                    <input type="text" name="website2" class="form-control" id="inputEmail3" placeholder="Website 2" value="<?php echo $records->website2;?>">
					<span class="red"><?php echo form_error('website2');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">explore</label>

                  <div class="col-sm-10">
                    <input type="text" name="explore" class="form-control" id="inputEmail3" placeholder="Explore" value="<?php echo $records->explore;?>">
					<span class="red"><?php echo form_error('explore');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">explore 2</label>

                  <div class="col-sm-10">
                    <input type="text" name="explore2" class="form-control" id="inputEmail3" placeholder="Explore 2" value="<?php echo $records->explore2;?>">
					<span class="red"><?php echo form_error('explore2');?></span>
                  </div>
                </div>
				-->
				
                <!--<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="coin_content"><?php echo $records->coin_content;?></textarea>
					<span class="red"><?php echo form_error('coin_content');?></span>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                   <input type="file" name="coin_photo" value=""/>
				   <?php if($records->coin_photo){ echo "<img src='".coin_URL."thumb/".$records->coin_photo_name."_150.".$records->coin_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?>
                  </div>
                </div>-->
				
				<!--
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Message Board 1</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="message_board1" id="inputEmail3" placeholder="Message Board 1" value="<?php echo $records->message_board1;?>">
					<span class="red"><?php echo form_error('message_board1');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Message Board 2</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="message_board2" id="inputEmail3" placeholder="Message Board 2" value="<?php echo $records->message_board2;?>">
					<span class="red"><?php echo form_error('message_board2');?></span>
                  </div>
                </div>
				-->
				
				 <div class="form-group">
	<div class="row ">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
		<label for="Application" class="btn btn-default">Application <input type="checkbox" id="Application" class="badgebox" <?php if($records->Application=="1"){?> checked='true'<?php } ?> name="Application" value="1">
		<span class="badge">&check;</span></label>
		
        <label for="Coin" class="btn btn-primary">Coin <input type="checkbox" id="Coin" class="badgebox" <?php if($records->Coin=="1"){?> checked='true'<?php } ?> name="Coin" value="1">
        <span class="badge">&check;</span></label>
        
        <label for="Currency" class="btn btn-info">Currency <input type="checkbox" id="Currency" class="badgebox" <?php if($records->currency=="1"){?> checked='true'<?php } ?> name="Currency" value="1">
       <span class="badge">&check;</span></label>
       
        <label for="ERC20" class="btn btn-success">ERC20  <input type="checkbox" id="ERC20" class="badgebox" <?php if($records->erc20=="1"){?> checked='true'<?php } ?> name="erc20" value="1">
        <span class="badge">&check;</span></label>
        
        <label for="Exchange" class="btn btn-warning">Exchange <input type="checkbox" id="Exchange" class="badgebox" <?php if($records->Exchange=="1"){?> checked='true'<?php } ?> name="Exchange" value="1">
        <span class="badge">&check;</span></label>
        </div></div>
        	<div class="row ">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
        <label for="Hybrid" class="btn btn-danger">Hybrid <input type="checkbox" id="Hybrid" class="badgebox" <?php if($records->Hybrid=="1"){?> checked='true'<?php } ?> name="Hybrid" value="1">
        <span class="badge">&check;</span></label>
        
        
        
        <label for="Mineable" class="btn btn-default">Mineable <input type="checkbox" id="Mineable" class="badgebox"  <?php if($records->Mineable=="1"){?> checked='true'<?php } ?> name="Mineable" value="1">
        <span class="badge">&check;</span></label>
		
        <label for="Platform" class="btn btn-primary">Platform <input type="checkbox" id="Platform" class="badgebox" <?php if($records->Platform=="1"){?> checked='true'<?php } ?> name="Platform" value="1">
        <span class="badge">&check;</span></label>
        
        <label for="Privacy" class="btn btn-info">Privacy <input type="checkbox" id="Privacy" class="badgebox" <?php if($records->Privacy=="1"){?> checked='true'<?php } ?> name="Privacy" value="1">
       <span class="badge">&check;</span></label>
       
        <label for="Token" class="btn btn-success">Token  <input type="checkbox"  id="Token" class="badgebox" <?php if($records->Token=="1"){?> checked='true'<?php } ?> name="Token" value="1">
        <span class="badge">&check;</span></label>
        
         </div></div>
	</div>
</div>
				
				
				
				
			<!--	<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Application</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Application=="1"){?> checked='true'<?php } ?> name="Application" value="1">
					
                  </div>
                </div>-->
                <!--	<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Coin</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Coin=="1"){?> checked='true'<?php } ?> name="Coin" value="1">
                  </div>
                </div>-->
                 <!--	<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Currency</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->currency=="1"){?> checked='true'<?php } ?> name="Currency" value="1">
                  </div>
                </div>-->
                	<!--<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">ERC20</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->erc20=="1"){?> checked='true'<?php } ?> name="erc20" value="1">
                  </div>
                </div>-->
                <!--	<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Exchange</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Exchange=="1"){?> checked='true'<?php } ?> name="Exchange" value="1">
                  </div>
                </div>-->
                <!--	<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Hybrid</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Hybrid=="1"){?> checked='true'<?php } ?> name="Hybrid" value="1">
                  </div>
                </div>-->
				 <!--<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Mineable</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Mineable=="1"){?> checked='true'<?php } ?> name="Mineable" value="1">
					
                  </div>
                </div>-->
					<!--<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Platform</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Platform=="1"){?> checked='true'<?php } ?> name="Platform" value="1">
					
                  </div>
                </div>-->
			<!--<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Privacy</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Privacy=="1"){?> checked='true'<?php } ?> name="Privacy" value="1">
					
                  </div>
                </div>
				
			<div class="form-group col-sm-2">
                  <label for="inputPassword3" class="col-sm-2 control-label">Token</label>

                  <div class="col-sm-10">
                    <input type="checkbox" <?php if($records->Token=="1"){?> checked='true'<?php } ?> name="Token" value="1">
					
                  </div>
                </div>-->
				<div class="form-group"></div>
				<!--
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <select class="form-control" name="website">
						<option value="0">--Select--</option>
						<option value="1">Website</option>
						<option value="2">Message board</option>
						<option value="3">Facebook</option>
						<option value="4">Telegram</option>
						<option value="5">Youtube</option>
						<option value="6">Github</option>
						<option value="7">Reddit</option>
						<option value="8">Soundcloud</option>
						<option value="9">Wee chat</option>
						<option value="10">Linked in</option>
					</select>
                  </div>
				  
				  <div class="col-sm-4">
                   <input type="text" class="form-control" name="add_url" id="inputEmail3" placeholder="Add URL" value="<?php ?>">
					<span class="red"><?php ?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <select class="form-control" name="website">
						<option value="0">--Select--</option>
						<option value="1">Website</option>
						<option value="2">Message board</option>
						<option value="3">Facebook</option>
						<option value="4">Telegram</option>
						<option value="5">Youtube</option>
						<option value="6">Github</option>
						<option value="7">Reddit</option>
						<option value="8">Soundcloud</option>
						<option value="9">Wee chat</option>
						<option value="10">Linked in</option>
					</select>
                  </div>
				  
				  <div class="col-sm-4">
                   <input type="text" class="form-control" name="add_url" id="inputEmail3" placeholder="Add URL" value="<?php ?>">
					<span class="red"><?php ?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <select class="form-control" name="website">
						<option value="0">--Select--</option>
						<option value="1">Website</option>
						<option value="2">Message board</option>
						<option value="3">Facebook</option>
						<option value="4">Telegram</option>
						<option value="5">Youtube</option>
						<option value="6">Github</option>
						<option value="7">Reddit</option>
						<option value="8">Soundcloud</option>
						<option value="9">Wee chat</option>
						<option value="10">Linked in</option>
					</select>
                  </div>
				  
				  <div class="col-sm-4">
                   <input type="text" class="form-control" name="add_url" id="inputEmail3" placeholder="Add URL" value="<?php ?>">
					<span class="red"><?php ?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <p style="font-size: 23px;color: #187db7;font-weight: 600;">+ Add More</p>
                  </div>

                </div>-->
				
				
				
				<?php if(is_array($links)){ 
				
				foreach($links as $link)
				{ ?>
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <select class="form-control" name="type_update[<?php echo $link->links_id;?>]">
						<option value="">--Select--</option>
						<?php
						if(is_array($link_type_list))
{
	foreach($link_type_list as $link_type_lis)
	{
	?>
	<option <?php if($link->links_type==$link_type_lis->link_type_id){ echo "selected";}?> value="<?php echo $link_type_lis->link_type_id;?>"><?php echo $link_type_lis->link_type_name;?> <?php if($link->links_type_is==$link_type_lis->link_type_id){echo $link->links_type_count;}?></option>
	<?php
	}
}
?>
					</select>
                  </div>
				  
				  <div class="col-sm-4">
                   <input type="text" class="form-control" name="url_update[<?php echo $link->links_id;?>]" id="inputEmail3" placeholder="Add URL" value="<?php echo $link->links_url;?>">
					<span class="red"><?php ?></span>
                  </div>
				   <a href="<?php echo base_url('coin/edit/'.$this->uri->segment(3).'/?linksid='.$link->links_id);?>" onclick="return confirm('Are you sure want to delete this record?')" href="#" class="btn btn-danger">Delete</a>
                </div>
				<?php }  } ?>
				
				
				
				
				<div id="InputSlider">
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <select class="form-control" name="type[]">
						<option value="">--Select--</option>
						<?php
						if(is_array($link_type_list))
{
	foreach($link_type_list as $link_type_lis)
	{
	?>
	<option value="<?php echo $link_type_lis->link_type_id;?>"><?php echo $link_type_lis->link_type_name;?></option>
	<?php
	}
}
?>
					</select>
                  </div>
				  
				  <div class="col-sm-4">
                   <input type="text" class="form-control" name="url[]" id="inputEmail3" placeholder="Add URL" value="<?php ?>">
					<span class="red"><?php ?></span>
                  </div>
                </div>
				</div>
				
				<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-4">
                    <p id="AddMoreImageBox" style="font-size: 23px;color: #187db7;font-weight: 600;cursor:pointer;">+ Add More</p>
                  </div>

                </div>
				
				<!--<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Role</th>
								<th>Photo</th>
								<th>Full name</th>
								<th>Linkedin</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Advisor</td>
								<td><img style="height:50px" src="http://www.wonderwrks.com/wp-content/uploads/2016/05/malecostume-512.png" class="img-responsive"></td>
								<td>First name middle name last name</td>
								<td>Linkedin url</td>
								<td><i class="fa fa-check-circle" style="color: #05ceef;"></i> Verified</td> 
							</tr>
							<tr>
								<td>Management</td>
								<td><img style="height:50px" src="https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png" class="img-responsive"></td>
								<td>First name middle name last name</td>
								<td>Linkedin url</td>
								<td><i class="fa fa-times-circle" style="color: red;"></i> Not Verified</td> 
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-primary" onclick="clicktoshow()">+ Add People</button>
				</div>
				
				<div id="display_true" class="form-group" style="display:none;">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-5">
                    <select class="form-control" name="website">
						<option value="0">--Select Role--</option>
						<option value="1">Advisor</option>
						<option value="2">Management</option>
						<option value="3">Team</option>
					</select>
                  </div>
				  
				  <div class="col-sm-5">
					<select class="form-control" name="website">
						<option value="0">--Select People--</option>
						<option value="1">Raja</option>
						<option value="2">Ravi</option>
						<option value="3">Rajesh</option>
					</select>
                  </div>
				  <div class="col-sm-4">
						<button type="button" class="btn btn-primary" onclick="clicktoshow()">+ Save and Add People</button>
				  </div>
				</div>-->
				<?php //print_r($keypeople_list); ?>
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>COIN</th>
								<th>PICTURE</th>
								<th>FULL NAME</th>
								<th>ROLE TYPE</th>
								<th>ROLE TITLE</th>
								<th>SOCIAL</th>
								<th>VERIFY</th>
							</tr>
						</thead>
						
						<tbody id="appendContent">
						<?php if(is_array($keypeople_name))
						{ 
					       foreach($keypeople_name as $people)
						   {
					        ?>
							<tr>
								<td><?php echo $people->keypeople_coinid;?></td>
								<td>
								
								<?php if($people->keypeople_filename!='') { ?>
								
								   <img style="height:50px" src="<?php echo 'https://www.cryptobutt.com/img/'.$people->keypeople_filename;?>" class="img-responsive">
								   
								<?php } else { ?>
								
								    <img style="height:50px" src="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" class="img-responsive">
									
								<?php } ?>
								
								</td>
								
								<td><?php echo $people->keypeople_fullname; ?></td>
							
								<td><?php echo $people->keypeople_roletype;?></td>
								<td><?php echo $people->keypeople_roletitle;?></td>
								<td>
								<select id="socialcontent" name="socialcontent" class="form-control" onchange="socialFun(this.value, <?php echo $people->keypeople_id ?>)">
								    <option value="">--select--</option>
								    <?php
								            $sql = "SELECT * FROM ci_keypeoplesociallink WHERE social_people_id='".$people->keypeople_id."'";
                                    	    $query = $this->db->query($sql);
                                    	    if ($query->num_rows() > 0)
                                    	   {
                                    			foreach($query->result() as $row)
                                    			{
                                    			    $sql1 = "SELECT * FROM ci_link_type WHERE link_type_id='".$row->social_links_type."'";
                                    	            $query1 = $this->db->query($sql1);
                                    	            if ($query1->num_rows() > 0)
                                    	            {
                                	                	foreach($query1->result() as $row1)
                                    		        	{
                                    				?>
                                    				<option value="<?php echo $row1->link_type_id; ?>"><?php echo $row1->link_type_name; ?></option>
                                    				<?php
                                    		        	}
                                    	            }
                                    			}
                                           }
								    ?>
										
									
                                    </select>
								
								</td> 
								<td><div id="<?php echo $people->keypeople_id;?>"><label for="label<?php echo $people->keypeople_id;?>" class="btn <?php if($people->verify_link=="true"){?> btn-success <?php } else{ echo 'btn-danger'; } ?>">VERIFY  
<input type="checkbox" <?php if($people->verify_link=="true"){?> checked='true'<?php } ?> id="label<?php echo $people->keypeople_id;?>" class="badgebox1 badgebox"  name="verify"  onchange="if(this.checked) this.value='true'; else this.value='false';verifyFun(this.value, <?php echo $people->keypeople_id ?>);">
        <span class="badge">✓</span></label></div></td>
							</tr>
						   <?php } } ?>
							
						</tbody>
					</table>
				
				</div>
				
			<!--	<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Role</th>
								<th>Photo</th>
								<th>Full name</th>
								<th>Role Type</th>
								<th>Social Profile</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody id="appendContent">
						<?php if(is_array($key_list))
						{ 
					       foreach($key_list as $key_lis)
						   {
					        ?>
							<tr>
								<td><?php echo $key_lis->role_name;?></td>
								<td>
								
								<?php if($key_lis->people_photo!='') { ?>
								
								   <img style="height:50px" src="<?php echo PEOPLE_URL.$key_lis->people_photo;?>" class="img-responsive">
								   
								<?php } else { ?>
								
								    <img style="height:50px" src="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" class="img-responsive">
									
								<?php } ?>
								
								</td>
								
								<td><?php echo $key_lis->people_first_name." ".$key_lis->people_middle_name." ".$key_lis->people_last_name;?></td>
							
								<td><?php echo $key_lis->key_people_roletype;?></td>
								<td><?php

$linkedurl ="";
												   $linkedtype ="";
												    $linkedsymbol ="";
												   $primaryQuery = "SELECT * FROM ".SOCIAL." WHERE social_people_id='".$key_lis->people_id."' and social_primary='1' limit 1";
											$resPrimaryQuery = $this->db->query($primaryQuery);
												 if($resPrimaryQuery->num_rows()>0)
												 {
													 $rowPrimaryQuery = $resPrimaryQuery->row();
													 $linkedurl = $rowPrimaryQuery->social_links_url;
													 $linkedtype = $this->boost_model->getValue(LINK_TYPE,"link_type_name","link_type_id='".$rowPrimaryQuery->social_links_type."'");
													  $linkedsymbol = $this->boost_model->getValue(LINK_TYPE,"link_type_font_awesome","link_type_id='".$rowPrimaryQuery->social_links_type."'");
												 }
												 else
												 {
													 $primaryQuery = "SELECT * FROM ".SOCIAL." WHERE social_people_id='".$key_lis->people_id."' limit 1";
													 $resPrimaryQuery = $this->db->query($primaryQuery);
													 if($resPrimaryQuery->num_rows()>0)
													 {
														 $rowPrimaryQuery = $resPrimaryQuery->row();
														  $linkedurl = $rowPrimaryQuery->social_links_url;
													 $linkedtype = $this->boost_model->getValue(LINK_TYPE,"link_type_name","link_type_id='".$rowPrimaryQuery->social_links_type."'");
													  $linkedsymbol = $this->boost_model->getValue(LINK_TYPE,"link_type_font_awesome","link_type_id='".$rowPrimaryQuery->social_links_type."'");
													 }
												 }

								echo $linkedtype;?></td>
								<td>
								<?php if($key_lis->key_people_verified=="Y")
								{?><i class="fa fa-check-circle" style="color: #05ceef;"></i> Verified
								<?php } ?>
								<?php if($key_lis->key_people_verified=="N")
								{?><i class="fa fa-check-circle" style="color: red;"></i> Not Verified
								<?php } ?>
								
								<?php if($key_lis->key_people_verified=="P")
								{?><i class="fa fa-check-circle" style="color: orange;"></i> Pending
								<?php } ?>
								
								</td> 
								<td><a href="<?php echo base_url('coin/edit/'.$this->uri->segment(3).'/?keypeople='.$key_lis->key_people);?>" onclick="return confirm('Are you sure want to delete this record?')" href="#" class="btn btn-danger">Delete</a></td>
							</tr>
						   <?php } } ?>
							
						</tbody>
					</table>
					<button style="cursor:pointer;" id="buttonHide" type="button" class="btn btn-primary" onclick="clicktoshow2()">+ Add People</button>
				</div>-->
				
				<div id="display_true2" class="form-group" style="display:none;">
                 

                  <div class="col-sm-3">
                    <select class="form-control" id="roleid" name="website">
						<option value="">--Select Role--</option>
						<?php if(is_array($role_list))
						{ 
							foreach($role_list as $role_lis)
							{
					    ?>
						<option value="<?php echo $role_lis->role_id;?>"><?php echo $role_lis->role_name;?></option>
							<?php } } ?>
					</select>
                  </div>
				  
				  <div class="col-sm-3">
					<select class="form-control" id="peopleid" name="website">
						<option value="">--Select People--</option>
						<?php if(is_array($people_list))
						{ 
							foreach($people_list as $people_lis)
							{
					    ?>
						<option value="<?php echo $people_lis->people_id;?>"><?php echo $people_lis->people_first_name." ".$people_lis->people_middle_name." ".$people_lis->people_last_name;?></option>
							<?php } } ?>
					</select>
                  </div>
				  
				  <div class="col-sm-3">
					<select class="form-control" id="profilestatus" name="profilestatus">
						<option value="">--Select Profile Verified--</option>
						<option value="Y">Yes</option>
						<option value="N">No</option>
						<option value="P">Pending</option>
					</select>
                  </div>
				   <div class="col-sm-3">
				       <input type="text" name="roletype" class="form-control" id="roletype" placeholder="Role Type" />
				
                  </div>
				  <p>&nbsp;</p>
				 <!-- <div class="col-sm-4">
						<button type="button" class="btn btn-primary" onclick="AddKeyPeople()">+ Save and Add People</button>
				  </div>-->
				</div>
                				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>coin/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
<script>
function clicktoshow() {
	document.getElementById('display_true').style.display = "block";

}
function clicktoshow2() {
	document.getElementById('display_true2').style.display = "block";
	document.getElementById('buttonHide').style.display = "none";

}

</script>