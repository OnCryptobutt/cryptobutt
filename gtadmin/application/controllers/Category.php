<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('category_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "category-list";
		$data['title'] = ucfirst("category List");
		
		/** Start Activate Records **/
		
		if($this->input->post('act')=="Activate")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->category_model->categoryActivate($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are activated successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			   
			}
		}
		
		/** End Activate Records **/
		
		
		/** Start Suspend Records **/
		
		if($this->input->post('act')=="Suspend")
		{
			if(isset($_POST['check']))
			{
				$check = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->category_model->categorySuspend($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Suspended successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Suspend Records **/
		
		/** Start Delete Records **/
		
		if($this->input->post('act')=="Delete")
		{
			if(isset($_POST['check']))
			{
				$check      = $_POST['check'];
				$checkCount = count($check);
				
				if($checkCount>0)
				{
					foreach( $check as $key=>$value)
					{
						 $this->category_model->categoryDelete($value);
					}
					
					$data['success'] = $this->boost_model->showNotify("success","Records are Deleted successfully");
					
				}
				else
				{
					$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
				}
			}
			else
			{
				$data['success'] = $this->boost_model->showNotify("error","Please select the record!");
			}
		}
		
		/** End Delete Records **/
		
		
		$config = array();
        $config["base_url"] =  base_url('category/index');
        $config["total_rows"] = $this->category_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->category_model->category_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();

		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('category/list',$data);
		$this->load->view('template/footer',$data);
	}
	
	
	public function add()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "category-add";
		$data['title'] = "Add category";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		if($this->input->post('act')=="add")
		{
			$this->form_validation->set_rules("category_name","Name","required|is_unique[".CATEGORY.".category_name]");
			/** $this->form_validation->set_rules("category_content","Content","required"); **/
		    $this->form_validation->set_rules("category_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
				$pID = $this->category_model->categoryInsert();
				
				if($_FILES['category_photo']['name']!="")
				{
					
					$ext_array = explode(".",$_FILES['category_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "category_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = CATEGORY_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('category_photo'))
					{
						// case - failure
						
					
						echo $this->upload->display_errors();
						
					}
					else
					{
						
						$this->category_model->categoryInsertUpdate($file_name,$photo_name,$ext,$pID);	
					}
					
					$source_path = CATEGORY_PATH.$file_name;
					$target_path = CATEGORY_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
				}
				
			
				$this->session->set_flashdata('success',"Records added successfully");
				redirect(base_url()."category/add/");
			}
			
				
		}
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('category/add',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function Edit()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "category-edit";
		$data['title'] = "Edit category";
		

		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
		
		if($this->input->post('act')=="edit")
		{
					
			$this->form_validation->set_rules("category_name","Name","required|edit_unique_common[".CATEGORY.".category_name.".$pID.".category_id]");
			/** $this->form_validation->set_rules("category_content","Content","required"); **/
			$this->form_validation->set_rules("category_order","Order","required|max_length[11]|numeric");
			if($this->form_validation->run()==TRUE)
			{
		
				$this->category_model->categoryEdit($pID);
				
				if($_FILES['category_photo']['name']!="")
				{
				    
					$this->category_model->categoryImageDelete($pID);
					
					$ext_array = explode(".",$_FILES['category_photo']['name']);
					
					$photoname = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($ext_array[0]));
					$photoname = implode("-",explode(" ",$photoname));
				   
					$ext = $ext_array[sizeof($ext_array)-1];
					
					$photo_name = "category_".$pID."_".$photoname;
					
					$file_name    = $photo_name.".".$ext;
					$file_name_150    = $photo_name.".".$ext;
					
					$config['upload_path'] = CATEGORY_PATH;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['file_name'] = $file_name ;

					//load upload class library
					$this->load->library('upload', $config);

				$this->upload->do_upload('category_photo');
				
						
						$this->category_model->categoryInsertUpdate($file_name,$photo_name,$ext,$pID);	
					
					
					 $source_path = CATEGORY_PATH.$file_name;
					$target_path = CATEGORY_PATH . 'thumb/'.$file_name_150;
					
				
					$thumb_marker = "_150";
					
					
					$this->boost_model->create_thumbnail($source_path,$target_path,150,150,$thumb_marker);
					
					
				}
				
			
				$this->session->set_flashdata('success',"Records are updated successfully");
				redirect(base_url()."category/edit/".$pID."/");
				
			}
			
				
		}
		
		$data['records'] = $this->category_model->getcategoryDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('category/edit',$data);
		$this->load->view('template/footer',$data);
		
	}
	
	public function View()
	{
		$data = "";
		$data['success'] = "";
		$data['basename'] = "category-view";
		$data['title'] = "View category";
		
		if($this->session->flashdata('success'))
		{
			$data['success'] = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		$pID = $this->uri->segment(3);
	
		
		$data['records'] = $this->category_model->getcategoryDetails($pID);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('category/view',$data);
		$this->load->view('template/footer',$data);
		
	}
}
