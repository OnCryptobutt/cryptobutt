<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cms
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <table class="table table-bordered">
					<tr><td><b>Name</b></td><td><?php echo $records->cms_name;?></td></tr>
					<tr><td><b>Content</b></td><td><?php echo $records->cms_content;?></td></tr>
					<!--<tr><td><b>Image</b></td><td><?php if($records->cms_photo){ echo "<img src='".CMS_URL."thumb/".$records->cms_photo_name."_150.".$records->cms_photo_ext."'>";}?></td></tr>-->
					<tr><td><b>Order</b></td><td><?php echo $records->cms_order;?></td></tr>
					<tr><td><b>Site Title</b></td><td><?php echo $records->cms_site_title;?></td></tr>
					<tr><td><b>Meta Description</b></td><td><?php echo $records->cms_meta_description;?></td></tr>
					<tr><td><b>Meta Keywords</b></td><td><?php echo $records->cms_meta_keyword;?></td></tr>
					<tr><td><b>Status</b></td><td> <?php if($records->cms_status=="1"){?>Active<?php } ?><?php if($records->cms_status=="0"){?>Suspend<?php } ?></td></tr>
				</table>
				
              </div>
			   <div class="box-footer">
                
				<a href="<?php echo base_url();?>cms/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  