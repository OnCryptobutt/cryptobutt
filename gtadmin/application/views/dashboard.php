<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
	  
        <a href="<?php echo base_url();?>settings/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Settings</span>
              <span class="info-box-number"><small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div></a>
		
		
        <a href="<?php echo base_url();?>coin/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-contao"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">coin</span>
               <span class="info-box-number"><?php if($coinActiveRecords){?>Active(<?php echo $coinActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($coinSuspendRecords){?>Suspend(<?php echo $coinSuspendRecords;?>)<?php }?></span>
            </div>
           
          </div>
         
        </div></a>
		
        <!--
        <a href="<?php echo base_url();?>cms/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-contao"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">CMS</span>
               <span class="info-box-number"><?php if($cmsActiveRecords){?>Active(<?php echo $cmsActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($cmsSuspendRecords){?>Suspend(<?php echo $cmsSuspendRecords;?>)<?php }?></span>
            </div>
           
          </div>
         
        </div></a>
        

     
	  
        <a href="<?php echo base_url();?>banner/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-sliders"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Banner</span>
              <span class="info-box-number"><?php if($bannerActiveRecords){?>Active(<?php echo $bannerActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($bannerSuspendRecords){?>Suspend(<?php echo $bannerSuspendRecords;?>)<?php }?></span>
            </div>
            
          </div>
        
        </div></a>
      
		
		
		
        <a href="<?php echo base_url();?>gallery/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-picture-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Gallery</span>
              <span class="info-box-number"><?php if($galleryActiveRecords){?>Active(<?php echo $galleryActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($gallerySuspendRecords){?>Suspend(<?php echo $gallerySuspendRecords;?>)<?php }?></span>
            </div>
            
          </div>
          
        </div></a>
       
		
		
		 
        <a href="<?php echo base_url();?>user/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">User</span>
              <span class="info-box-number"><?php if($userActiveRecords){?>Active(<?php echo $userActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($userSuspendRecords){?>Suspend(<?php echo $userSuspendRecords;?>)<?php }?></span>
            </div>
            
          </div>
          
        </div></a>
        
		
		
		 
        <a href="<?php echo base_url();?>category/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-tags"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Category</span>
              <span class="info-box-number"><?php if($categoryActiveRecords){?>Active(<?php echo $categoryActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($categorySuspendRecords){?>Suspend(<?php echo $categorySuspendRecords;?>)<?php }?></span>
            </div>
            
          </div>
          
        </div></a>
       
		
		
			 
        <a href="<?php echo base_url();?>sub_category/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-maroon"><i class="fa fa-tags"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sub Category</span>
              <span class="info-box-number"><?php if($subCategoryActiveRecords){?>Active(<?php echo $subCategoryActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($subCategorySuspendRecords){?>Suspend(<?php echo $subCategorySuspendRecords;?>)<?php }?></span>
            </div>
            
          </div>
          
        </div></a>
       
		
		
			
        <a href="<?php echo base_url();?>product/"><div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-olive"><i class="fa fa-product-hunt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Product</span>
              <span class="info-box-number"><?php if($productActiveRecords){?>Active(<?php echo $productActiveRecords;?>)<?php }?></span>
			  <span class="info-box-number"><?php if($productSuspendRecords){?>Suspend(<?php echo $productSuspendRecords;?>)<?php }?></span>
            </div>
           
          </div>
          
        </div></a>
        -->
		
      </div>
      <!-- /.row -->
	  
	  
      
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
