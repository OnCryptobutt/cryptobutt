<?php
class New_cron_home_page4 extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model');
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('coin_details_model');
		
    }

	function index()
	{
		$history_sql = "";
		$insert_sql = "";
		$update_sql = "";
		$time_start = microtime(true);
		$coins_parmeters = $this->coin_details_model->getParameter();
		$update_sql = "";

		$sql = "SELECT *
FROM ci_coin
LEFT JOIN ci_coin_history_temp_USD ON ci_coin.id = ci_coin_history_temp_USD.coin_id
 where  ci_coin.status='1' ORDER BY CAST(ci_coin_history_temp_USD.mktcap AS DECIMAL(38,10)) desc limit 31,470";
		$query = $this->db->query($sql);
		
		if($query->num_rows())
		{
			
			for($i=0;$i<1;$i++)
		{
		
		    
			foreach($query->result_array() as $row)
			{
				
				foreach($coins_parmeters as $coins_parmeter)
				{
				
				
				$Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='.$row['name'].'&tsyms='.$coins_parmeter),true);
				
				$currency_array = explode(",",$coins_parmeter);
				
					foreach($currency_array as $currency)
							{
				
				if($currency!=$row['name'])
								{
									if(isset($Data['RAW'][$row['name']][$currency]))
									{
										
								$det = $Data['RAW'][$row['name']][$currency];
								
								if(is_null($det['SUPPLY']))
										{
											$det['SUPPLY'] = "";
										}
										if(is_null($det['MKTCAP']))
										{
											$det['MKTCAP'] = "";
										}
										if(is_null($det['CHANGEPCT24HOUR']))
										{
											$det['CHANGEPCT24HOUR'] = "";
										}
										if(is_null($det['CHANGE24HOUR']))
										{
											$det['CHANGE24HOUR'] = "";
										}
										if(is_null($det['LASTMARKET']))
										{
											$det['LASTMARKET'] = "";
										}
										if(is_null($det['LOW24HOUR']))
										{
											$det['LOW24HOUR'] = "";
										}
										if(is_null($det['HIGH24HOUR']))
										{
											$det['HIGH24HOUR'] = "";
										}
										if(is_null($det['OPEN24HOUR']))
										{
											$det['OPEN24HOUR'] = "";
										}
										if(is_null($det['VOLUME24HOURTO']))
										{
											$det['VOLUME24HOURTO'] = "";
										}
										if(is_null($det['VOLUME24HOUR']))
										{
											$det['VOLUME24HOUR'] = "";
										}
										if(is_null($det['LASTTRADEID']))
										{
											$det['LASTTRADEID'] = "";
										}
										if(is_null($det['LASTVOLUMETO']))
										{
											$det['LASTVOLUMETO'] = "";
										}
										if(is_null($det['LASTVOLUME']))
										{
											$det['LASTVOLUME'] = "";
										}
										if(is_null($det['LASTUPDATE']))
										{
											$det['LASTUPDATE'] = "";
										}
										
										
								$api_date_time = date("Y-m-d H:i:s",$det['LASTUPDATE']);
								
								
								
								//$this->db2->query($history_sql);
						
								/** history temp file **/
								
								$existtempid = $this->db->select('id')->from("ci_coin_history_temp_".$currency)->where("coin_id",$row['coin_id'])->get();
								if($existtempid->num_rows()>0)
								{
									$exid = $existtempid->row()->id;
									
									echo $exid."<br/>";
									
								
									$update_sql = "UPDATE ci_coin_history_temp_".$currency." SET
								
								lastupdate = '".$det['LASTUPDATE']."',
								lastvolume = '".$det['LASTVOLUME']."',
								lastvolumeto = '".$det['LASTVOLUMETO']."',
								lasttradeid = '".$det['LASTTRADEID']."',
								volume24hour = '".$det['VOLUME24HOUR']."',
								volume24hourto = '".$det['VOLUME24HOURTO']."',
								open24hour = '".$det['OPEN24HOUR']."',
								high24hour = '".$det['HIGH24HOUR']."',
								low24hour = '".$det['LOW24HOUR']."',
								lastmarket = '".$det['LASTMARKET']."',
								change24hour = '".$det['CHANGE24HOUR']."',
								changepct24hour = '".$det['CHANGEPCT24HOUR']."',
								supply = '".$det['SUPPLY']."',
								mktcap = '".$det['MKTCAP']."',
								api_updated_date_time = '".$api_date_time."',
								
								price = '".$det['PRICE']."' WHERE id='".$exid."';";
								
							
									
								 $this->db->query($update_sql); 
								}
								else
								{
									
									$insert_sql = "INSERT into ci_coin_history_temp_".$currency." SET
								
								lastupdate = '".$det['LASTUPDATE']."',
								lastvolume = '".$det['LASTVOLUME']."',
								lastvolumeto = '".$det['LASTVOLUMETO']."',
								lasttradeid = '".$det['LASTTRADEID']."',
								volume24hour = '".$det['VOLUME24HOUR']."',
								volume24hourto = '".$det['VOLUME24HOURTO']."',
								open24hour = '".$det['OPEN24HOUR']."',
								high24hour = '".$det['HIGH24HOUR']."',
								low24hour = '".$det['LOW24HOUR']."',
								lastmarket = '".$det['LASTMARKET']."',
								change24hour = '".$det['CHANGE24HOUR']."',
								changepct24hour = '".$det['CHANGEPCT24HOUR']."',
								supply = '".$det['SUPPLY']."',
								mktcap = '".$det['MKTCAP']."',
								api_updated_date_time = '".$api_date_time."',
								
								price = '".$det['PRICE']."';";
									
									
								$this->db->query($insert_sql);
									
								
								}
								/** history temp file **/
									}
								}
							}
				
			}
			
			/** $update_data = array("coin_id_chunk1"=>$row['id'],
			"updated_time"=>NOW);
			$this->db->set($update_data)->where("id",$cron_id)->update(CRON_NEW);**/
			echo $row['id']." ".$row['name']."<br/>"; 
			
			
			}
			//echo $update_sql;
			// Execute multi query
//mysqli_multi_query($db2,$history_sql);
//mysqli_multi_query($db1,$update_sql);
//mysqli_multi_query($db1,$insert_sql);

//if($insert_sql)
//{
	//mysqli_multi_query($db1,$insert_sql);
//}
		
		//$update_sql ="";
		//$insert_sql ="";
			
			
			
		}
        
            

  



	}
		
	
	//mysqli_close($db1);
		




		
		
		$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;

//execution time of the script
//echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';
$totalSecs   = ($execution_time * 60); 
echo '<b>Total Execution sec:</b> '.$totalSecs.' sec';

        
		
		$this->db->close();
	
	}
}
?>