<script src="<?php echo base_url(); ?>js/highstock.js"></script>
<script>
$(".cd_hours a").click(function() {
    $('.cd_hours a').removeClass( "cd_hours_active" );
    if($(this).hasClass( "cd_hours_active" )){
        $(this).removeClass( "cd_hours_active" );
    }else{
        $(this).addClass( "cd_hours_active" );
    }
});

function chartData(dataUrl){

$.getJSON(dataUrl, function (data) {
 Highcharts.setOptions({
        	lang:{
            	rangeSelectorZoom: ''
            }
        });
Highcharts.setOptions({
    colors: ['#753ac8']
});

  // Create the chart
  var chart = Highcharts.stockChart('container1', {
     
    chart: {
         backgroundColor: '#FFFFFF',
      height: 400
    },
    scrollbar: {
        enabled: false
},
navigator: {
            enabled: false
        },
    title: {
      text: ''
    },

    subtitle: {
      text: ''
    },

    rangeSelector: {
        inputEnabled: true,
		  enabled:false
    },

    series: [{
      name: 'Price',
      data: data,
      type: 'area',
      threshold: null,
      tooltip: {
        valueDecimals: 2
      }
    }],

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          chart: {
            height: 300
          },
          subtitle: {
            text: null
          },
          navigator: {
            enabled: false
          }
        }
      }]
    }
    
  });


});
}
chartData('<?php echo base_url(); ?>/img/cryptobuttchart/oneweek.php?coinName=<?php echo $this->uri->segment(1); ?>');
function priceChange(hours){ 
setTimeout(function(){
var chart = $("#container1").highcharts(),
            sLen = chart.series.length,
            max = chart.yAxis[0].dataMax,
			min = chart.yAxis[0].dataMin,
            series,
            index,
            i = 0;
        
        for(; i < sLen; i++) {
            s = chart.series[i];
            index = s.yData.indexOf(max);
			index1 = s.yData.indexOf(min);
            if (index >= 0) {
                series = s;
                break;
            }else if (index1 >= 0) {
				 series = s;
                break;
            }
        };
       
        var maxValue = s.yData[index];
		var minValue = s.yData[index1];
		document.getElementById('changeprice_high').innerHTML = maxValue.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
        document.getElementById('changeprice_low').innerHTML = minValue.toLocaleString('en-US', { style: 'currency', currency: 'USD' });;
       if(hours == 1){ 
		    document.getElementById('changepricetxt_high').innerHTML = "1 hour high";
			document.getElementById('changepricetxt_low').innerHTML = "1 hour low";
		}else if(hours == 24){ 
			document.getElementById('changepricetxt_high').innerHTML = "1 day high";
			document.getElementById('changepricetxt_low').innerHTML = "1 day low";
		}else if(hours == 168){
			document.getElementById('changepricetxt_high').innerHTML = "1 week high";
			document.getElementById('changepricetxt_low').innerHTML = "1 week low";
		}else if(hours == 720){
			document.getElementById('changepricetxt_high').innerHTML = "1 month high";
			document.getElementById('changepricetxt_low').innerHTML = "1 month low";
		}else if(hours == 2160){
			document.getElementById('changepricetxt_high').innerHTML = "3 months high";
			document.getElementById('changepricetxt_low').innerHTML = "3 months low";
		}else if(hours == 4320){
			document.getElementById('changepricetxt_high').innerHTML = "6 months high";
			document.getElementById('changepricetxt_low').innerHTML = "6 months low";
		}else if(hours == 8640){
			document.getElementById('changepricetxt_high').innerHTML = "1 year high";
			document.getElementById('changepricetxt_low').innerHTML = "1 year low";
		}
	},3000);
}

		 function loadmore()
		 {
		 var totalcoins = '<?php echo $count; ?>';
		 
		  totalcoins = Number(totalcoins);
		  var val = document.getElementById("row_nos").value;
		  pagedisplays = val * <?php echo "10";//echo $this->settings->settings_records_per_page_front;?>;
		  if(totalcoins>pagedisplays)
		  {
			
			 var pageids = document.getElementById("page_id").value;
			 var pageidsarray = pageids.split(",");
			 var i = pageidsarray.indexOf(val);

             if(i > -1){ 
			 }
			 else
			 {
				  UserPagination('0','0','','');
				  document.getElementById('page_id').value=pageids+','+val;
			 }
			  
		  }
		  
		 }
  function UserPagination(pageId,liId,currency,symbol){
	 var val = document.getElementById("row_nos").value;
	  if(currency!='')
	  {
	    document.getElementById('currency_value').value=currency;
		var pid='0';
		$("#row_nos").val(pid);
		
		
		$(".flash").show();
	    UserPagination('0','0','','');
		
	  }
	  
	  if(symbol!='')
	  {
		   $("#currency_val").html(symbol+" "+currency);
	  }
	 currency1 = $("#currency_value").val();
	 var page_Id=val;
	 var content = document.getElementById("pageData2");
     content.innerHTML = content.innerHTML+'<tr class="sets"><td colspan="7"><div align="center" class="flash" ></div></td></tr>';
     $(".flash").fadeIn(400).html('<div align="center"  id="loader"></div>');
	 
	 if(currency1!='')
	 {
		 var dataString = 'pageId='+ page_Id+'&cid='+<?php echo $cid; ?>+'&currency='+currency1;
	 }
     else{ 
       var dataString = 'pageId='+ page_Id+'&cid='+<?php echo $cid; ?>;
	 }
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_market_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){
				 $(".sets").remove();
				  $(".flash").remove();
				  $(".flash3").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
				 var content = document.getElementById("pageData2");
                 content.innerHTML = content.innerHTML+result;
				 document.getElementById("row_nos").value = Number(val)+1;
           }
      });
}
UserPagination("0","0",'','');
  function currency_change(value1){
	valu = value1.split("@");
	currency = valu[0];
	symbol = valu[1];
      $("#currency_value").val(currency);
      currency1 = $("#currency_value").val();	
	  $("#currency_val").html(symbol+" "+currency1);
     $(".flash3").show();
     $(".flash3").fadeIn(400).html('<div align="center"  id="loader"></div>');
	var page_Id='0'; 
	 var dataString = 'pageId='+ page_Id+'&cid='+<?php echo $cid; ?>+'&currency='+currency1;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_market_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){

                 $(".flash").hide();
				 $(".flash3").hide();
                 $("#pageData2").html(result);
				 document.getElementById("row_nos").value = 1;
				 document.getElementById("page_id").value = 0;
           }
      });
}
  function UserPagination2(pageId,litId){
     $(".flash1").show();
     $(".flash1").fadeIn(400).html('<div align="center"  id="loader"></div>');
	 $("#pageData").hide();
	 
	 var currency ='<?php echo $currency ?>';
	
	 
     var dataString = 'pageId='+ pageId+'&cid='+<?php echo $cid; ?>+'&currency='+currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_historic_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){
			  
                 $(".flash1").hide();
                 $(".links a").removeClass("In-active current") ;
                 $("#"+litId+" a").addClass( "In-active current" );
				 $("#pageData").show();
                 $("#pageData").html(result);
				
				
           }
      });
}

UserPagination2("0","0");
function redirect(val) {
	window.location = "<?php echo base_url($this->uri->segment(1));?>?currency="+val;
}
$(document).ready(function(){
	
	$("#coin_name").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
		}
		});
	});
});

$(document).ready(function(){
	$("#coin_name_mobile").keyup(function(){
		$.ajax({
		type: "POST",
		url: '<?php echo base_url('Ajax_search/search');?>',
		data:'keyword='+$(this).val(),
		beforeSend: function(){
		},
		success: function(data){
			$("#suggesstion-box_mobile").show();
			$("#suggesstion-box_mobile").html(data);
			$("#coin_name_mobile").css("background","#FFF");
		}
		});
	});
});
function selectCountry(val) {
	window.location = "<?php echo base_url();?>"+val;
	<?php if($this->detect->isMobile())
	{ ?>
$("#coin_name_mobile").val(val);
$("#suggesstion-box_mobile").hide();

	<?php }else{ ?>
	$("#coin_name").val(val);
$("#suggesstion-box").hide();
	<?php } ?>
//tops('search','');
}
</script>