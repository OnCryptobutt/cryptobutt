<?php
class Product_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function product_list($limit, $start)
	{
		
		
		$this->db->select('*')->from(PRODUCT);
		
		if($this->input->get('act')=="search")
		{
			$this->db->like('product_name',$this->input->get('search'));
		}
		$this->db->limit($limit,$start);
		
		$query = $this->db->get();
		
		
		
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getproductDetails($id)
    {
	  
	   $query = $this->db->select('*')->from(PRODUCT)->where('product_id',$id)->get();
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
		
		$this->db->select('*')->from(PRODUCT);
		
		if($this->input->get('act')=="search")
		{
			$this->db->like('product_name',$this->input->get('search'));
		}
		
      return $this->db->count_all_results();
    }
   
    public function productActivate($id)
    {
		$this->db->set('product_status','1');
		$this->db->where('product_id',$id);
		$this->db->update(PRODUCT);
	 
    }
		
	public function productSuspend($id)
	{
		
		$this->db->set('product_status','0');
		$this->db->where('product_id',$id);
		$this->db->update(PRODUCT);
		
	}
		
	public function productDelete($id)
	{
		$photoname = $this->boost_model->getValue(PRODUCT,"product_photo_name","product_id='".$id."'");
		$photoext = $this->boost_model->getValue(PRODUCT,"product_photo_ext","product_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(PRODUCT_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(PRODUCT_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(PRODUCT_PATH.$photoname.".".$photoext))
			{
				unlink(PRODUCT_PATH.$photoname.".".$photoext);
			}
		}
		
		$this->db->where('product_id',$id);
		$this->db->delete(PRODUCT);
		
	}
	
	public function productImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(PRODUCT,"product_photo_name","product_id='".$id."'");
		$photoext = $this->boost_model->getValue(PRODUCT,"product_photo_ext","product_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(PRODUCT_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(PRODUCT_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(PRODUCT_PATH.$photoname.".".$photoext))
			{
				unlink(PRODUCT_PATH.$photoname.".".$photoext);
			}
		}
	}
		
	public function productInsert()
	{
		$slug = url_title($this->input->post('product_name'), 'dash', true);
		
		
		$data = array(	"product_category_id"  => $this->input->post('product_category_id'),
		"product_sub_category_id"   => $this->input->post('product_sub_category_id'),
		"product_name"              => $this->input->post('product_name'),
		"product_price"             => $this->input->post('product_price'),
		"product_content"            => $this->input->post('product_content'),
		"product_status"             => $this->input->post('product_status'),
		"product_order"              => $this->input->post('product_order'),
		"product_site_title"         => $this->input->post('product_site_title'),
		"product_meta_description"   => $this->input->post('product_meta_description'),
		"product_meta_keyword"       => $this->input->post('product_meta_keyword'),
		"product_seo"                => $slug,
		"product_created_time"       => NOW,
		"product_updated_time"       => NOW);
		
		$this->db->set($data)->insert(PRODUCT);
		return $this->db->insert_id();
	}
	
	public function productEdit($id)
	{
		$slug = url_title($this->input->post('product_name'), 'dash', true);
		
		
		$data = array("product_category_id"       => $this->input->post('product_category_id'),
		"product_sub_category_id"   => $this->input->post('product_sub_category_id'),
		"product_name"              => $this->input->post('product_name'),
		"product_price"             => $this->input->post('product_price'),
		"product_content"           => $this->input->post('product_content'),
		"product_status"            => $this->input->post('product_status'),
		"product_order"             => $this->input->post('product_order'),
		"product_site_title"        => $this->input->post('product_site_title'),
		"product_meta_description"  => $this->input->post('product_meta_description'),
		"product_meta_keyword"      => $this->input->post('product_meta_keyword'),
		"product_seo"               => $slug,
		"product_updated_time"      => NOW);
		
		$this->db->set($data)->where('product_id',$id)->update(PRODUCT);
	}
		
	public function productInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		
		$data = array("product_photo"      => $file_name,
					  "product_photo_name" => $photo_name,
					  "product_photo_ext"  => $ext);
		
		$this->db->set($data)->where('product_id',$id)->update(PRODUCT);
	}
	
	public function getCategoryList($id)
	{
	
		$query = $this->db->select(array('category_id','category_name'))->from(CATEGORY)->where('category_status',"1")->get();
		
		if($query->num_rows()>0)
			{
				$data = '';
				foreach($query->result_array() as $row_sel)
				{
					$data .= "<option value=\"".$row_sel["category_id"]."\"";
					if($row_sel["category_id"]==$id)
					{
						$data .= " selected=\"SELECTED\"";
					}
					$data .= ">".stripslashes($row_sel["category_name"])."</option>";
				}
			}
			return $data;
	}
	
	public function getSubCategoryList($id,$parent_id)
	{
	
	
		
		$query = $this->db->select(array('sub_category_id','sub_category_name'))->from(SUB_CATEGORY)->where(array('sub_category_status'=>"1",'sub_category_parent'=>$parent_id))->get();
		
	   
		$data = '';
		if($query->num_rows()>0)
			{
				
				foreach($query->result_array() as $row_sel)
				{
					
					$data .= "<option value=\"".$row_sel["sub_category_id"]."\"";
					if($row_sel["sub_category_id"]==$id)
					{
						$data .= " selected=\"SELECTED\"";
					}
					$data .= ">".stripslashes($row_sel["sub_category_name"])."</option>";
				}
			}
			return $data;
	}
	
	public function getCategory()
	{
	
		$query = $this->db->select(array('category_id','category_name'))->from(CATEGORY)->where('category_status',"1")->get();
		
		if($query->num_rows()>0)
			{
				$data = '';
				foreach($query->result() as $row_sel)
				{
					$data[] = $row_sel;
				}
			}
			return $data;
	}
		
}
?>