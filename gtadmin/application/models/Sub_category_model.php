<?php
class Sub_category_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function sub_category_list($limit, $start)
	{
		$sql = "SELECT * FROM ".SUB_CATEGORY." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getsub_categoryDetails($id)
    {
	   $sql = "SELECT * FROM ".SUB_CATEGORY." WHERE sub_category_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      return $this->db->count_all(SUB_CATEGORY);
    }
   
    public function sub_categoryActivate($id)
    {
	   $sql = "UPDATE ".SUB_CATEGORY." SET sub_category_status='1' WHERE sub_category_id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function sub_categorySuspend($id)
	{
		$sql = "UPDATE ".SUB_CATEGORY." SET sub_category_status='0' WHERE sub_category_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function sub_categoryDelete($id)
	{
		$photoname = $this->boost_model->getValue(SUB_CATEGORY,"sub_category_photo_name","sub_category_id='".$id."'");
		$photoext = $this->boost_model->getValue(SUB_CATEGORY,"sub_category_photo_ext","sub_category_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(SUB_CATEGORY_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(SUB_CATEGORY_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(SUB_CATEGORY_PATH.$photoname.".".$photoext))
			{
				unlink(SUB_CATEGORY_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".SUB_CATEGORY." WHERE sub_category_id='$id'";
		$query = $this->db->query($sql);
	}
	
	
	public function sub_categoryImageDelete($id)
	{
		$photoname = $this->boost_model->getValue(SUB_CATEGORY,"sub_category_photo_name","sub_category_id='".$id."'");
		$photoext = $this->boost_model->getValue(SUB_CATEGORY,"sub_category_photo_ext","sub_category_id='".$id."'");
		
		if($photoname)
		{
			if(file_exists(SUB_CATEGORY_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(SUB_CATEGORY_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(SUB_CATEGORY_PATH.$photoname.".".$photoext))
			{
				unlink(SUB_CATEGORY_PATH.$photoname.".".$photoext);
			}
		}
	
	}
		
	public function sub_categoryInsert()
	{
		$slug = url_title($this->input->post('sub_category_name'), 'dash', true);
		
		$sql = "INSERT INTO ".SUB_CATEGORY." SET 
		sub_category_parent            = '".$this->input->post('sub_category_parent')."',
		sub_category_name              = '".$this->input->post('sub_category_name')."',
		sub_category_content           = '".$this->input->post('sub_category_content')."',
		sub_category_status            = '".$this->input->post('sub_category_status')."',
		sub_category_order             = '".$this->input->post('sub_category_order')."',
		sub_category_site_title        = '".$this->input->post('sub_category_site_title')."',
		sub_category_meta_description  = '".$this->input->post('sub_category_meta_description')."',
		sub_category_meta_keyword      = '".$this->input->post('sub_category_meta_keyword')."',
		sub_category_seo               = '".$slug."',
		sub_category_created_time      = '".NOW."',
		sub_category_updated_time      = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function sub_categoryEdit($id)
	{
		$slug = url_title($this->input->post('sub_category_name'), 'dash', true);
			
		$sql = "UPDATE ".SUB_CATEGORY." SET 
		sub_category_parent            = '".$this->input->post('sub_category_parent')."',
		sub_category_name              = '".$this->input->post('sub_category_name')."',
		sub_category_content           = '".$this->input->post('sub_category_content')."',
		sub_category_status            = '".$this->input->post('sub_category_status')."',
		sub_category_order             = '".$this->input->post('sub_category_order')."',
		sub_category_site_title        = '".$this->input->post('sub_category_site_title')."',
		sub_category_meta_description  = '".$this->input->post('sub_category_meta_description')."',
		sub_category_meta_keyword      = '".$this->input->post('sub_category_meta_keyword')."',
		sub_category_seo               = '".$slug."',
		sub_category_updated_time      = '".NOW."' WHERE sub_category_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function sub_categoryInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".SUB_CATEGORY." SET 
		
		sub_category_photo      = '".$file_name."',
		sub_category_photo_name = '".$photo_name."',
		sub_category_photo_ext  = '".$ext."' WHERE sub_category_id='".$id."'";
		
		$this->db->query($sql);
	}
	
	
	public function getCategoryList($id)
	{
	
		$sql = "SELECT category_id,category_name FROM ".CATEGORY." WHERE category_status='1'";
	    $query = $this->db->query($sql);
		if($query->num_rows()>0)
			{
				$data = '';
				foreach($query->result_array() as $row_sel)
				{
					$data .= "<option value=\"".$row_sel["category_id"]."\"";
					if($row_sel["category_id"]==$id)
					{
						$data .= " selected=\"SELECTED\"";
					}
					$data .= ">".stripslashes($row_sel["category_name"])."</option>";
				}
			}
			return $data;
	}
		
}
?>