<?php
class User_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function user_list($limit, $start)
	{
		$sql = "SELECT * FROM ".USER." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getuserDetails($id)
    {
	   $sql = "SELECT * FROM ".USER." WHERE id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      //return $this->db->count_all(ADMIN);
	  //$this->db->where('admin_type', 'ADMIN');
      return $this->db->count_all_results(USER);
    }
   
    public function userActivate($id)
    {
	   $sql = "UPDATE ".USER." SET status='1' WHERE id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function userSuspend($id)
	{
		$sql = "UPDATE ".USER." SET status='0' WHERE id='$id'";
		$query = $this->db->query($sql);
	}
		
		
	public function userDelete($id)
	{
		
		$this->db->where('id',$id);
		//$this->db->where('admin_type','ADMIN');
		$this->db->delete(USER);
		
	}
	
	public function userInsert()
	{
		
		$sql = "INSERT INTO ".USER." SET 
		
		user_name    		= '".$this->input->post('user_name')."',
		password    		= '".$this->input->post('password')."',
		email       		= '".$this->input->post('email')."',
		gender       		= '".$this->input->post('gender')."',
		firstName       	= '".$this->input->post('firstName')."',
		lastName        	= '".$this->input->post('lastName')."',
		status      		= '".$this->input->post('status')."',
		created_time 		= '".NOW."',
		updated_time 		= '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function userEdit($id)
	{
			
		$sql = "UPDATE ".ADMIN." SET 
		
		user_name    		= '".$this->input->post('user_name')."',
		password    		= '".$this->input->post('password')."',
		email       		= '".$this->input->post('email')."',
		gender       		= '".$this->input->post('gender')."',
		firstName       	= '".$this->input->post('firstName')."',
		lastName        	= '".$this->input->post('lastName')."',
		status      		= '".$this->input->post('status')."',
		updated_time 		= '".NOW."' WHERE id='".$id."'";
			
		$this->db->query($sql);
	}
	
		
}
?>