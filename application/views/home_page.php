<input type="hidden" id="hidden_search" value="<?php echo $this->input->get('search');?>"/>
<input type="hidden" id="hidden_per_page" value=""/>
<input type="hidden" id="hidden_currency" value="<?php echo $this->input->get('currency');?>"/>
<input type="hidden" id="hidden_sorting" value=""/>
<input type="hidden" id="row_no" value="1">
<input type="hidden" id="pageids" value="0">
<input type="hidden" id="coins_count" value="<?php if(isset($total_list)) { echo $total_list; }?>">
<input type="hidden" id="sorting_price" value="DESC"/>
<input type="hidden" id="sorting_market" value="DESC"/>
<input type="hidden" id="sorting_change" value="24H"/>
<input type="hidden" id="records_per_page_front" value="<?php echo $this->settings->settings_records_per_page_front;?>"/>
<input type="hidden" id="user_id" value="<?php echo $this->session->userdata('user_id'); ?>"/>
<section class="bg-light sticky-top purple new_hed_adj" id="navbar">
			<section class="container desktop-marg">
			  <nav class="navbar navbar-expand-lg navbar-light">
				<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
					<a href="javascript:;">
                        <div class="col-1 col-md-1 tog"></div>
						<div class="tog col-3 col-md-3 coin-tag">
							<p class="collapse_nav_price">Crypto assets</p>
							<p class="collapse_nav_htl">All (<?php echo count($total_coin); ?>)</p>
						</div>
                        <div class="tog coin_cap col-3 col-md-3">
							<p class="collapse_nav_price">Market Cap</p>
							<p id="sorting_market_html" class="collapse_nav_htl">High to Low</p>
						</div>
						<div class="tog col-4 col-md-2">
							<p class="collapse_nav_price">Price</p>
							<p id="sorting_price_html" class="collapse_nav_htl">---</p>
						</div>
						<div class="tog col-2 col-md-2" style="width: 12%;">
							<p class="collapse_nav_price">Change</p>
							<p id="sorting_change_html" class="collapse_nav_htl">1 Day</p>

						</div>
                        <div class="tog col-2 col-md-2" style="width:11%">
                            <p class="collapse_nav_price" style="visibility:hidden">1 Day high</p>
                            <p class="collapse_nav_htl" id="filter_txt1">1 Day high</p>
						</div>
						<div class="tog col-2 col-md-2" style="width:12%">
						    <p class="collapse_nav_price"  style="visibility:hidden">1 Day low</p>
						    <p class="collapse_nav_htl" id="filter_txt2">1 Day low</p>
						</div>
					</a>
				</div>
			  </nav>
			  <div class="collapse" id="collapse1">
				  <div class="card card-block">
					<div class="c_depth" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        <div class="col-1 col-md-1 tog"></div>
						<div class="tog col-2 col-md-3" style="color:#FFEB3B;left: 25px;">All</div>
						<div class="tog coin_cap col-4 col-md-3 select-txt">
							<p><a id="f3" href="javascript:void(0);" onclick="dropValueAtID('sorting_market_html','High to Low','sorting_market','DESC','f3');changePagination();" style="color:#FFEB3B;">High to Low</a></p>
							<p><a id="f4" href="javascript:void(0);" onclick="dropValueAtID('sorting_market_html','Low to High','sorting_market','ASC','f4');changePagination();">Low to High</a></p>
						</div>
						<div class="tog col-4 col-md-2 select-txt">
							<p><a id="f1" href="javascript:void(0);" onclick="dropValueAtID('sorting_price_html','High to Low','sorting_price','DESC','f1');changePagination();">High to Low</a></p>
							<p><a id="f2" href="javascript:void(0);" onclick="dropValueAtID('sorting_price_html','Low to High','sorting_price','ASC','f2');changePagination();">Low to High</a></p>
						</div>
						<div class="tog col-4 col-md-2 select-txt" style="width:12%">
                        <p><a id="f5" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 hour','sorting_change','1H','f5');changePagination();">1 hour</a></p>
						<p><a id="f6" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Day','sorting_change','24H','f6');changePagination();" style="color:#FFEB3B;">1 Day</a></p>
						<p><a id="f7" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Week','sorting_change','1W','f7');changef7Pagination();">1 Week</a></p>
						<p><a id="f8" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Month','sorting_change','1M','f8');changePagination();">1 Month</a></p>
                        <p><a id="f9" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','3 Months','sorting_change','3M','f9');changePagination();">3 Months</a></p>
                        <p><a id="f10" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','6 Months','sorting_change','6M','f10');changePagination();">6 Months</a></p>
                        <p><a id="f11" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','1 Year','sorting_change','12M','f11');changePagination();">1 Year</a></p>
                        <p><a id="f12" href="javascript:void(0);" onclick="dropValueAtID('sorting_change_html','All','sorting_change','ALL','f12');changePagination();">All</a></p>
						</div>
					</div>
				  </div>
				</div>
			</section>
		</section>
		<section id="pageData" style="min-height:400px;position:relative;">
		</section>	
<style>
.stylish-input-group .input-group-addon{
    background: white !important; 
}
.stylish-input-group .form-control{
    border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.dropdown-toggle::after {
    display: none;
}
.btn-danger.focus, .btn-danger:focus {
    box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.5);
}

.btn-danger:not([disabled]):not(.disabled):active, .show>.btn-danger.dropdown-toggle {
    color: #753ac8;
    background-color: #ffffff;
    border-color: #ffffff;
    box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.5);
}

.btn-danger {
    color: #753ac8;
    background-color: #ffffff;
    border-color: #ffffff;
}
.btn-danger:hover {
    color: #753ac8;
    background-color: #ffffff;
    border-color: #ffffff;
}	
.row_background
{
	
background-color : #FFCDD2 !important;

}

.row_background3
{
	
background-color : #ccdefc !important;

}
.row_background2
{
	
background-color : #B9F6CA !important;

}
.flash
{
	margin-left: 45%;
}

.green
{
	color:green !important;
}

.red
{
	color:red !important;
}

.black
{
	color:black !important;
	font-weight:bold !important;
	padding:5px;
}
</style>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border:1px solid #753ac8;">
      <div class="modal-header" style="background:#753ac8;">
        <button type="button" class="close" data-dismiss="modal" style="color:#fff">&times;</button>
        <h4 class="modal-title" ></h4>
      </div>
      <div class="modal-body">
        <p>Please login to continue. <a href="<?php echo base_url("hauth/Login/Google");?>" style="color:#753ac8">Click Here</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background:#753ac8;color:#fff">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/home_js.js"></script>