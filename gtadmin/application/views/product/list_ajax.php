<style type="text/css">
ul.tsc_pagination { margin:4px 0; padding:0px; height:100%; overflow:hidden; font:12px \'Tahoma\'; list-style-type:none; }
ul.tsc_pagination li { float:left; margin:0px; padding:0px; margin-left:5px; }
ul.tsc_pagination li:first-child { margin-left:0px; }
ul.tsc_pagination li a { color:black; display:block; text-decoration:none; padding:7px 10px 7px 10px; }
ul.tsc_pagination li a img { border:none; }
ul.tsc_paginationC li a { color:#707070; background:#FFFFFF; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; border:solid 1px #DCDCDC; padding:6px 9px 6px 9px; }
ul.tsc_paginationC li { padding-bottom:1px; }
ul.tsc_paginationC li a:hover,
ul.tsc_paginationC li a.current { color:#FFFFFF; box-shadow:0px 1px #EDEDED; -moz-box-shadow:0px 1px #EDEDED; -webkit-box-shadow:0px 1px #EDEDED; }
ul.tsc_paginationC01 li a:hover,
ul.tsc_paginationC01 li a.current { color:#893A00; text-shadow:0px 1px #FFEF42; border-color:#FFA200; background:#FFC800; background:-moz-linear-gradient(top, #FFFFFF 1px, #FFEA01 1px, #FFC800); background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #FFFFFF), color-stop(0.02, #FFEA01), color-stop(1, #FFC800)); }
ul.tsc_paginationC li a.In-active {
   pointer-events: none;
   cursor: default;
}
</style> 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url()?>product/">product</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
			
			
			
			
		
			 
				  
            <!-- /.box-header -->
            <div class="box-body">
			
			<?php if($success) echo $success;?>
			
			<div align="left">
			
			<select multiple id="e1" style="width:300px">
			<?php if(is_array($getCategory)){
				foreach($getCategory as $row){?>
        <option value="<?php echo $row->category_id;?>"><?php echo $row->category_name;?></option>
			<?php } } ?>
    
    </select>
<input type="checkbox" id="checkbox" >Select All


			    <?php 
			$attributes = array('method' => 'get');	
				echo form_open('',$attributes);?>
				    <input type="hidden" name="act" value="search"/>
					<input type="text" name="search" value="<?php echo $this->input->get('search');?>"/>
					<input class="btn btn-primary" type="submit" value="Search"/>
				<?php echo form_close();?>
			</div>
			
				<form id="form1" name="form1" method="post" action="">
			 <div align="right">
                  <a href="<?php echo base_url();?>product/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>
                  <input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>
                  <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
				  <br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th><input type="checkbox" id="selecctall"/></th>
				  <th>Image</th>
                  <th>Product Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="pageData">
				
					 

					 
               
                </tbody>
                
              </table>
			  
			 
					<?php if($count > 0){ 
 $content ="";
$content .='<ul class="tsc_pagination tsc_paginationC tsc_paginationC01">
    <li class="first link" id="first">
        <a  href="javascript:void(0)" onclick="changePagination(\'0\',\'first\')">F i r s t</a>
    </li>';
    for($i=0;$i<$paginationCount;$i++){
 
        $content .='<li id="'.$i.'_no" class="link">
          <a  href="javascript:void(0)" onclick="changePagination(\''.$i.'\',\''.$i.'_no\')">
              '.($i+1).'
          </a>
    </li>';
    }
 
    $content .='<li class="last link" id="last">
         <a href="javascript:void(0)" onclick="changePagination(\''.($paginationCount-1).'\',\'last\')">L a s t</a>
    </li>
    <li class="flash"></li>
</ul>';

echo $content;
					} ?>
			  
            </div>
			</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
 