<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coin_list extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
    }

	function index()
	{
		
		header('Content-type: application/json');
		$success ="";

		$Data = json_decode(file_get_contents('https://www.cryptocompare.com/api/data/coinlist/'),true);
		
		
		$run_from = $this->input->get('run_from');
		
		if($this->input->get('cron_type')=="overwrite")
		{
			$cron_type = $this->input->get('cron_type');
		}
		else
		{
			$cron_type = "update";
		}
		
		
		$BaseImageUrl = $Data['BaseImageUrl'];
		
		if($Data['Response']=="Success")
		{
	        $i = 0;
			$u = 0;
			foreach($Data['Data'] as $data)
			{
				$exist = $this->boost_model->getValue(COIN,"count('*')","name='".$data['Name']."'");
				
				if($exist>0)
				{
					//exist
					$update_data = array(
					
					"fullname"=>$data['FullName'],
					"algorithm"=>$data['Algorithm'],
					"prooftype"=>$data['ProofType'],
					"fullypremined"=>$data['FullyPremined'],
					"preminedvalue"=>$data['PreMinedValue'],
					"totalcoinsupply"=>$data['TotalCoinSupply'],
					"totalcoinsfreefloat"=>$data['TotalCoinsFreeFloat'],
					
					"api_id"=>$data['Id'],
					"updated_time"=>NOW
					);
					$this->db->set($update_data)->where("name",$data['Name'])->update(COIN);
					$u++;
				}
				else
				{
					if(isset($data['ImageUrl']))
					{
					$url=$BaseImageUrl.$data['ImageUrl'];
					$name = basename($url);
					}
					else{
						$name = "";
					}
					
					$insert_data = array("name"=>$data['Name'],
					"coinname"=>$data['CoinName'],
					"fullname"=>$data['FullName'],
					"algorithm"=>$data['Algorithm'],
					"prooftype"=>$data['ProofType'],
					"fullypremined"=>$data['FullyPremined'],
					"preminedvalue"=>$data['PreMinedValue'],
					"totalcoinsupply"=>$data['TotalCoinSupply'],
					"totalcoinsfreefloat"=>$data['TotalCoinsFreeFloat'],
					"sortorder"=>$data['SortOrder'],
					"api_id"=>$data['Id'],
					"created_time"=>NOW,
					"updated_time"=>NOW,
					"image"=>$name);
					
					$this->db->set($insert_data)->insert(COIN);
					
                    if(isset($data['ImageUrl']))
					{
						$data = file_get_contents($url);
						$new = COIN_PATH.$name;
						file_put_contents($new, $data);
					}
				$i++;	
				}
				
			}
			header('Content-type: text/html');
			if($run_from=="manual")
			{
				    if($i>0)
					{
					$success = "".$i." Coins has inserted.<br/>";
					}
					 if($u>0)
					{
			        $success .="".$u." Coins has updated.</font><br/>";
					
					}
		
			$this->session->set_flashdata('success',$success );
		    redirect(base_url()."coin");
			}
			else{
				 if($i>0)
					{
					echo "<font color='green'>".$i." Coins has inserted.</font><br/>";
					}
					if($u>0)
					{
			        echo "<font color='green'>".$u." Coins has updated.</font><br/>";
					}
					
				
			}
		}
		else
		{
			header('Content-type: text/html');
			if($run_from=="manual")
			{
			$success ="API Does not work. Please check the API";
			$this->session->set_flashdata('error',$success );
		    redirect(base_url()."coin");
			}
			else
			{
				echo "<font color='red'>API Does not work. Please check the API</font>";
			}
		}
	
	}
}
