<!DOCTYPE html>

<html lang="en">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://use.fontawesome.com/c596d80306.js"></script>

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">

	<!-- <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet"> -->

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
	
	<link rel="stylesheet" href="<?php echo base_url();?>css/customize_new.css">

	<title><?php if(isset($title)){ echo $title; }?></title>
	<meta name="keyword" content="<?php if(isset($meta_keywords)){ echo $meta_keywords; }?>"/>
	<meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; }?>"/>
	
	<style type="text/css">
ul.tsc_pagination { padding:0px; height:100%; overflow:hidden; font:12px \'Tahoma\'; list-style-type:none; }
ul.tsc_pagination li { float:left; margin:0px; padding:0px; margin-left:5px; }
ul.tsc_pagination li:first-child { margin-left:0px; }
ul.tsc_pagination li a { color:black; display:block; text-decoration:none; padding:7px 10px 7px 10px; }
ul.tsc_pagination li a img { border:none; }
ul.tsc_paginationC li a { color:#707070; background:#FFFFFF; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; border:solid 1px #DCDCDC; padding:6px 9px 6px 9px; }
ul.tsc_paginationC li { padding-bottom:1px; }
ul.tsc_paginationC li a:hover,
ul.tsc_paginationC li a.current { color:#FFFFFF; box-shadow:0px 1px #EDEDED; -moz-box-shadow:0px 1px #EDEDED; -webkit-box-shadow:0px 1px #EDEDED; }
ul.tsc_paginationC01 li a:hover,
ul.tsc_paginationC01 li a.current { color:#893A00; text-shadow:0px 1px #FFEF42; border-color:#FFA200; background:#FFC800; background:-moz-linear-gradient(top, #FFFFFF 1px, #FFEA01 1px, #FFC800); background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #FFFFFF), color-stop(0.02, #FFEA01), color-stop(1, #FFC800)); }
ul.tsc_paginationC li a.In-active {
   pointer-events: none;
   cursor: default;
}

input[type=text] {
    width: 13px;
    box-sizing: border-box;
	border:0;
    outline: none;
    font-size: 16px;
    background-color: white;
    background-image: url('https://www.cryptobutt.com/uploads/searchicon.png') !important;
	background-repeat: no-repeat !important;
    background-position: 10px 6px !important; 
    background-repeat: no-repeat;
    padding: 20px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
    float:right;
	
}

@media (min-width: 768px) { 
	input[type=text]:focus {
		width: 300px;
		border-top: 0;
		border-right:0;
		border-left:0;
		border-bottom: 1px solid #ddd;
	}
}

@media (max-width: 767px) { 
	input[type=text]:focus {
		width: 150px;
		border-top: 0;
		border-right:0;
		border-left:0;
		border-bottom: 1px solid #ddd;
	}
}

.col-xs-12.col-lg-12.coin_lists.div_hoer {
    z-index: 1020 !important;
}
div#suggesstion-box {
    margin-top: 3px;
}
</style>

<script>



	function one1() {
		document.getElementById('suggesstion-box').style.display = "none";
	}
	
	//setInterval(function(){ 
	

 //}, 1000);
	
</script>

  </head>

  <body>
  
  <?php if($this->input->get('currency'))
			{
				$currency = $this->input->get('currency');
			}
			else
			{
					if($this->session->userdata('user_id'))
					{
						$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
					
						
					}
					else
					{
						$currency = "";
					}
					
					if($currency=="")
					{
					$currency = "USD";
					}
			}
			?>

    <section class="container-fluid">

		<header class="top">

			<figure class="figure">

				<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>img/logo.png" class="figure-img img-fluid rounded logo hidden-xs-down" alt="Criptobutt"></a>

				<a href="<?php echo base_url();?>"><img id="mobile_img" src="<?php echo base_url();?>img/m_logo.png" class="figure-img img-fluid rounded logo hidden-sm-up" alt="Criptobutt"></a>

			</figure>
			
			
			<section class="float-lg-right main_nav">
	
				<span class="fl_left"><a class="navbar-brand <?php if($basename=='home') { ?>active <?php } ?>" href="<?php echo base_url();?>"><i class="fa fa-connectdevelop" aria-hidden="true"></i> Coin</a></span>
				
				<span class="fl_left" >
					<form>
						<input type="text" id="coin_name" name="search" onfocusout="this.value='';one1();" placeholder="Search crypto asset" style="height: 14px;cursor: pointer;"><br>
						<div id="suggesstion-box" style="display: none;"></div>
					</form>
				</span>
				
				<?php if($this->session->userdata('user_id')){
           
			?>
			
            <!--<li class="list-inline-item logout"><a id="logout" href="<?php echo base_url('Logout');?>" class="nav-link">Logout <i class="fa fa-sign-out" aria-hidden="true"></i>	</a></li>-->
			
			 
			 	
				
				<span><a class="navbar-brand <?php if($basename=='profile') { ?>active <?php } ?>" href="<?php echo base_url('profile');?>"><img class="img-fluid user" src="<?php echo $query_user_row->photoURL;?>"> <span class="coin_cap"><?php echo $query_user_row->firstName;?></span></a></span>
				
			<?php } else { ?>
			
				<span><a class="navbar-brand" href="<?php echo base_url('hauth/login/Google');?>"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Login</a></span>
			<?php } ?>
								
			</section>

		</header>
		
		<input type="hidden" id="searchstatus" value="0"/>
		<script>
		function searchToggle()
		{
			var searchstatus = document.getElementById("searchstatus").value;
			if(searchstatus==0)
			{
			
			document.getElementById("popupsearch1").style.display="block";
			//document.getElementById("pageData").style.display="none";
			//document.getElementById("removesticky").style.display="none";
			
			document.getElementById("searchstatus").value="1";
			}
			else
			{
			document.getElementById("popupsearch1").style.display="none";
			//document.getElementById("pageData").style.display="block";	
			//document.getElementById("removesticky").style.display="block";
			//document.getElementById("searchstatus").value="0";
				
			}
		}
		</script>
		
		<!--<div id="popupsearch1" class="text-right heade_alg" style="display:none;">
			<form id="searchForm" action="#">
			  <div class="form-group">
			   <input type="search" id="coin_name" name="search" placeholder="Search crypto asset" style="width:100%;padding: 3px 25px;border-bottom: 2px solid #753ac8;border-left: 2px solid #eeeeee;border-top: 2px solid #eeeeee;height: 50px;">
				<span class="input-group-btn">
					<button class="btn btn-danger" type="button" style="padding: 0;margin-top: -39px;margin-left: 4px;height: 0px;">
						<i class="fa fa-search"></i>
					</button>
				</span>
				<span class="input-group-btn" style="float: right;">
					<button class="btn btn-danger" type="button" style="padding: 0;margin-top: -39px;margin-left: -23px;height: 0px;">
						<div onclick="searchToggle();" class="close-btn"><i style="color: black;" class="fa fa-window-close" aria-hidden="true"></i></div>
					</button>
				</span>
				<div id="suggesstion-box"></div>
		
			  </div>
			</form>	
		</div>-->
		
			
		<!--Check device script start-->
		
		<script type="text/javascript">
	function chk(val)
	{ 
	
     var textval = document.getElementById('coin_name').value;
	 if(textval=='' && val!='no')
	 {  //alert(val);
 
 
		//document.getElementById('suggesstion-box').style.display = "none"; 
		$("#suggesstion-box").hide();
	 }
	}
		
			var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
			var element = document.getElementById('mobile_img');
			if (isMobile) {
				element.style.display = "block"
			} else {
				element.style.display = "none"
			}
		</script>
		
		<!--Check device script End-->
		
		