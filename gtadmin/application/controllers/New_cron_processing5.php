<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
class New_cron_processing5 extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->db2 = $this->boost_model->getHistoryDb();
		$this->load->model('coin_details_model');
    }

	function index()
	{
		$time_start = microtime(true);
		$coins_parmeters = $this->coin_details_model->getParameter();
		$start = $this->settings->settings_process5_start;
		$upto = $this->settings->settings_process5_end;
		
		$cron_details    = $this->coin_details_model->getCronDetailsProcess_new("5");
		
		$cron_id = $cron_details->id;
		
		$query = $this->db->select('id,name')->from(COIN);
		if($cron_details->coin_id_chunk5>0)
				{
					$this->db->where("id >=",$cron_details->coin_id_chunk5);
				}
				else
				{
					$this->db->where("id >=",$start);
				}
		
		
		$this->db->where("id <",$upto);
	
		$this->db->order_by("id","ASC");
		
		$query = $this->db->get();
		
		if($query->num_rows())
		{
		    
			foreach($query->result_array() as $row)
			{
				
				foreach($coins_parmeters as $coins_parmeter)
				{
				
				
				$Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='.$row['name'].'&tsyms='.$coins_parmeter),true);
				
				$currency_array = explode(",",$coins_parmeter);
				
					foreach($currency_array as $currency)
							{
				
				if($currency!=$row['name'])
								{
									if(isset($Data['RAW'][$row['name']][$currency]))
									{
										
								$det = $Data['RAW'][$row['name']][$currency];
								
								if(is_null($det['SUPPLY']))
										{
											$det['SUPPLY'] = "";
										}
										if(is_null($det['MKTCAP']))
										{
											$det['MKTCAP'] = "";
										}
										if(is_null(['CHANGEPCT24HOUR']=="NULL"))
										{
											$det['CHANGEPCT24HOUR'] = "";
										}
										if(is_null(['CHANGE24HOUR']=="NULL"))
										{
											$det['CHANGE24HOUR'] = "";
										}
										if(is_null(['LASTMARKET']=="NULL"))
										{
											$det['LASTMARKET'] = "";
										}
										if(is_null(['LOW24HOUR']=="NULL"))
										{
											$det['LOW24HOUR'] = "";
										}
										if(is_null(['HIGH24HOUR']=="NULL"))
										{
											$det['HIGH24HOUR'] = "";
										}
										if(is_null(['OPEN24HOUR']=="NULL"))
										{
											$det['OPEN24HOUR'] = "";
										}
										if(is_null(['VOLUME24HOURTO']=="NULL"))
										{
											$det['VOLUME24HOURTO'] = "";
										}
										if(is_null(['VOLUME24HOUR']=="NULL"))
										{
											$det['VOLUME24HOUR'] = "";
										}
										if(is_null(['LASTTRADEID']=="NULL"))
										{
											$det['LASTTRADEID'] = "";
										}
										if(is_null(['LASTVOLUMETO']=="NULL"))
										{
											$det['LASTVOLUMETO'] = "";
										}
										if(is_null(['LASTVOLUME']=="NULL"))
										{
											$det['LASTVOLUME'] = "";
										}
										if(is_null(['LASTUPDATE']=="NULL"))
										{
											$det['LASTUPDATE'] = "";
										}
										
										
								$api_date_time = date("Y-m-d H:i:s",$det['LASTUPDATE']);
								$update_data = array("price"=>$det['PRICE'],
							   "name"=>$row['name'],
							    "coin_id"=>$row['id'],
								"cron_id"=>$cron_id,
								"lastupdate"=>$det['LASTUPDATE'],
								"lastvolume"=>$det['LASTVOLUME'],
								"lastvolumeto"=>$det['LASTVOLUMETO'],
								"lasttradeid"=>$det['LASTTRADEID'],
								"volume24hour"=>$det['VOLUME24HOUR'],
								"volume24hourto"=>$det['VOLUME24HOURTO'],
								"open24hour"=>$det['OPEN24HOUR'],
								"high24hour"=>$det['HIGH24HOUR'],
								"low24hour"=>$det['LOW24HOUR'],
								"lastmarket"=>$det['LASTMARKET'],
								"change24hour"=>$det['CHANGE24HOUR'],
								"changepct24hour"=>$det['CHANGEPCT24HOUR'],
								"supply"=>$det['SUPPLY'],
								"mktcap"=>$det['MKTCAP'],
								"api_updated_date_time"=>$api_date_time,
								"created_time"=>NOW);
								$this->db2->set($update_data)->insert("ci_coin_history_".$currency);
								/** history temp file **/
								$existtempid = $this->db->select('id')->from("ci_coin_history_temp_".$currency)->where("coin_id",$row['id'])->get();
								if($existtempid->num_rows()>0)
								{
									$exid = $existtempid->row()->id;
									$this->db->set($update_data)->where("id",$exid)->update("ci_coin_history_temp_".$currency);
								}
								else
								{
									$this->db->set($update_data)->insert("ci_coin_history_temp_".$currency);
								}
								/** history temp file **/
									}
								}
							}
				
			}
			
			$update_data = array("coin_id_chunk5"=>$row['id'],
			"updated_time"=>NOW);
			$this->db->set($update_data)->where("id",$cron_id)->update(CRON_NEW);
			echo $row['id']." ".$row['name']."<br/>";
			
			
			}
			
			
		}
		
		$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;

//execution time of the script
//echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';
$totalSecs   = ($execution_time * 60); 
echo '<b>Total Execution sec:</b> '.$totalSecs.' sec';

           $update_data = array("chunk5_status"=>"COMPLETED",
			"updated_time"=>NOW);
			$this->db->set($update_data)->where("id",$cron_details->id)->update(CRON_NEW);
			echo "<br/><font color='red'>All Coins has updated</font><br/>";
		
		
	
	}
}
