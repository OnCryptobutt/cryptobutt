 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url()?>user/">User</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
			
			
			
			
			<form id="form1" name="form1" method="post" action="">
			 
				  
            <!-- /.box-header -->
            <div class="box-body">
			
			<?php if($success) echo $success;?>
			
			 <div align="right">
                  <!--<a href="<?php echo base_url();?>user/add/"> <input type="button" name="act" value="add" class="btn btn-sm btn-primary"/></a>-->
                  <input type="submit" name="act" value="Activate" class="btn btn-sm btn-success"/>
                  <input type="submit" name="act" value="Suspend" class="btn btn-sm btn-warning"/>
                  <input onclick="return confirm('Are you sure want to delete selected records?');" type="submit" name="act" value="Delete" class="btn btn-sm btn-danger"/>
                  </div>
				  <br/>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th><input type="checkbox" id="selecctall"/></th>
				  <th>Image</th>
				  <th>User Name</th>
                  <th>User Email</th>
				  <th>Created date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				
					  <?php 
					 
					  if(is_array($lists)){
					  foreach($lists as $list){
						
						  ?>
					  <tr>
					  
					    <td><input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $list->id;?>"> </td>
						  <td><a target="_blank" href="<?php echo $list->profileURL;?>"><img width="50" src="<?php echo $list->photoURL;?>"/></a></td>
						<td><?php echo $list->firstName." ".$list->lastName;?></td>
						<td><?php echo $list->email;?></td>
						<td><?php echo date("Y-m-d h:i:s",strtotime($list->created_time));?></td>
						<td>
						<?php if($list->status=='1'){?><span class="label label-success">Active</span><?php } ?>
						<?php if($list->status=='0'){?><span class="label label-danger">Suspend</span><?php } ?>
						</td>
						<td><!--<a href="<?php echo base_url();?>user/edit/<?php echo $list->admin_id;?>/"><span class="fa fa-pencil"></span></a> |-->
						
						<a href="<?php echo base_url();?>user/view/<?php echo $list->id;?>/"><span class="fa fa-eye"></span></a>
						</td>
					  </tr>
					  <?php } }

              else{					  ?>
			  
			  <tr><td align="center" colspan="6"><font color="red">--No records found!--</font></td></tr>
			  <?php } ?>
				
               
                </tbody>
                
              </table>
			  
			  <div align="right"><?php echo $pagination;?></div>
			  
            </div>
			</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 