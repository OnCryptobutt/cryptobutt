<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 199px;">
   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Work
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
			  
             <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="work_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo $records->work_name;?>">
					<span class="red"><?php echo form_error('work_name');?></span>
                  </div>
                </div>
				
				
                <!--<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="service_content"><?php echo $records->service_content;?></textarea>
					<span class="red"><?php echo form_error('service_content');?></span>
                  </div>
                </div>-->
				
				
			
				
				<div class="form-group">
					<label class="col-lg-2 control-label">Service Slider</label>
					
					<div class="col-lg-10">
					 <div>
								  <?php 
								  if(is_array($service_sliders))
								  {
									  ?>
								   
								      <?php 
									  foreach($service_sliders as $service_slider)
									  {
										  ?>
										   <input type="text" value="<?php echo $service_slider->work_slider_name;?>" placeholder=""  name="work_slider_name_update[<?php echo $service_slider->work_slider_id;?>]"/>&nbsp;&nbsp;<input style="display: initial;" type="file" name="work_slider_photo_update[<?php echo $service_slider->work_slider_id;?>]"/>&nbsp;
										   <?php if($service_slider->work_slider_photo){ echo "<img  width='50' height='50' src='".WORK_URL."thumb/".$service_slider->work_slider_photo_name."_150.".$service_slider->work_slider_photo_ext."'>";}else{ echo "<img src='".UPLOAD_URL."noimage_50.jpg'>"; }?>
										   <a href="<?php echo base_url('work/edit/'.$records->work_id.'/?workSliderDelId='.$service_slider->work_slider_id);?>" onclick="return confirm('Are you sure want to delete this record?')" href="#" class="btn btn-danger">Delete</a><br/>
										  <?php
									  }
								  }
								  ?>
								  </div>
								  
                           <div id="InputSlider" >
                                <input type="text" value="" placeholder=""  name="work_slider_name[]"/>
							
								<input style="display: initial;" type="file" name="work_slider_photo[]"/>&nbsp;<br/>
								
       
							</div>
							</div>
							 
					   </div>
   
				<div class="form-group">
                                  <label class="col-lg-2 control-label"></label>
                                  <div class="col-lg-10">
								  
								  <span style="color:red;">Note : Upload Image size only on (1280x853 Pixels).</span><br/>
				 <span class="small"><a class="btn btn-info" id="AddMoreImageBox" href="#">Add More Slider</a></span>
					  </div>
					  </div>
					  
			  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="work_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo $records->work_order; ?>">
					<span class="red"><?php echo form_error('work_order');?></span>
                  </div>
                </div>
					  
			 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" <?php if($records->work_status=="1"){?> checked='true'<?php } ?> name="work_status" value="1"> Enable<br/>
					<input type="radio" <?php if($records->work_status=="0"){?> checked='true'<?php } ?> name="work_status" value="0"> Disable
                  </div>
                </div>
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-warning">Submit</button>
				<a href="<?php echo base_url();?>work/"><button type="button" class="btn btn-warning">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  
  </div>