<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        SMS
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="add">
              <div class="box-body">
               
				
              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>

                  <div class="col-sm-10">
                    <input type="text" name="pnone" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo set_value('pnone');?>">
					<span class="red"><?php echo form_error('pnone');?></span>
                  </div>
                </div>
                
                
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Message</label>

                  <div class="col-sm-10">
                    <textarea name="msg" class="form-control" id="inputEmail3"></textarea>
					<span class="red"><?php echo form_error('msg');?></span>
                  </div>
                </div>
				
              
				
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url();?>user/"><button type="button" class="btn btn-primary">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  