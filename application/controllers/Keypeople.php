<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keypeople extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->load->model('home_model');
		$this->detect = new Mobile_Detect();
		
	/*	if($this->session->userdata('user_id')=="")
		{
			redirect(base_url());
		}
	*/	
		
    }
	
	public function index()
	{
	
		
		$data = "";

		$time_start = microtime(true);
		$data['basename'] = "keypeople";
		if($this->uri->segment(1)!='')
		{
		    $popleId = $this->uri->segment(1);
			$query_people = $this->db->select('*')->from("ci_addkeypeople")->where("keypeople_id",$popleId)->get();
			
	    if($query_people->num_rows()>0)
			 {
				 $query_people_row = $query_people->row();
			 }
			 
			 $data['query_people_row'] =  $query_people_row;
		}
		else
		{
			$data['$query_people_row'] =  "";
		}
		 $popleName = urldecode($this->uri->segment(2));
		$sql = "SELECT * FROM ci_addkeypeople INNER JOIN ci_coin ON ci_addkeypeople.keypeople_coinid = ci_coin.name INNER JOIN
    ci_coin_history_temp_USD ON ci_coin_history_temp_USD.name = ci_addkeypeople.keypeople_coinid WHERE ci_addkeypeople.keypeople_fullname='".$popleName."'";

		$query =$this->db->query($sql);
     	if($query->num_rows()>0)
		{
			$data['keypeople_row'] = $query->result();
				
		}
		 $popleid = urldecode($this->uri->segment(1));
	 $sql = "SELECT * FROM ci_link_type INNER JOIN ci_keypeoplesociallink ON ci_link_type.link_type_id = ci_keypeoplesociallink.social_links_type WHERE ci_keypeoplesociallink.social_people_id='".$popleid."'";

		$query =$this->db->query($sql);
     	if($query->num_rows()>0)
		{
			$data['keypeople_rowlinks'] = $query->result();
				
		}
		
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			$currency = "USD";
		}
		$data['currency_symbol'] = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		
		if($this->session->userdata('user_id')){
		    
		  $query_user = $this->db->select('*')->from(USER)->where("id",$this->session->userdata('user_id'))->get();
		  
			 if($query_user->num_rows()>0)
			 {
				 $query_user_row = $query_user->row();
			 }
			 
			 $data['query_user_row'] =  $query_user_row;
		}
		else
		{
			$data['query_user_row'] =  "";
		}
		
		$data['js'] = $this->load->view('profile_js',$data,TRUE);
		$this->load->view('template/header',$data);
		$this->load->view('keypeople',$data);
		$this->load->view('template/footer',$data);
		
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;

		//execution time of the script
		//echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';
		$totalSecs   = ($execution_time * 60); 
		//echo '<b>Total Execution sec:</b> '.$totalSecs.' sec';
		
	}
	
	function getPagination($count){
      $paginationCount= floor($count / $this->settings->settings_records_per_page_front);
      $paginationModCount= $count % $this->settings->settings_records_per_page_front;
      if(!empty($paginationModCount)){
               $paginationCount++;
      }
      return $paginationCount;
     }
     public function logindialog(){
		$this->load->view('signin/Logindialog');
	}
     
     
     
     
}
