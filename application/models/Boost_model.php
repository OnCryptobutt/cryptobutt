<?php
class Boost_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	public function showNotify($type,$msg)
	{
		if($type=="success")
		{
			$msg = " <div id='SiteNotifyMessage' class=\"alert alert-success alert-dismissible\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                
              ".$msg."
              </div>";
		}
		elseif($type=="error")
		{
			$msg = " <div id='SiteNotifyMessage' class=\"alert alert-danger alert-dismissible\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                
              ".$msg."
              </div>";
		}
		
		return $msg;
	}
	
	function create_thumbnail($fileName,$dest,$width,$height,$thumb_marker) 
    {
		$config['image_library']  = 'gd2';
        $this->load->library('image_lib');
		$config['source_image']   = $fileName;       
        $config['create_thumb']   = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']          = $width;
        $config['height']         = $height;
        $config['new_image']      = $dest; 
        $config['thumb_marker']   = $thumb_marker; 		
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize())
        { 
            echo $this->image_lib->display_errors();
        }        
    }
	
	public function getValue( $table,$field,$where )
	{
    	$sql   = "SELECT ".$field." FROM ".$table." WHERE ".$where;
    	$query = $this->db->query($sql);
		$row   = $query->row();
		if($query->num_rows()>0)
		{
    	return $row->$field;
		}
	}
	
	public function getValue2( $table,$field,$where )
	{
		$this->db2 = $this->getHistoryDb();
    	$sql   = "SELECT ".$field." FROM ".$table." WHERE ".$where;
    	$query = $this->db2->query($sql);
		$row   = $query->row();
		if($query->num_rows()>0)
		{
    	return $row->$field;
		}
	}
	
	public function checkAdminAuthentication()
	{
    	if($this->session->userdata('admin_id')=="")
		{
			redirect(base_url()."login/");
		}
	}
	
	public function loadSettings()
	{
		$this->db->select("*");
		$this->db->from(SETTINGS);
		$this->db->where("settings_id","1");
		$query = $this->db->get();
		$row = $query->row();
		$row->currency_list = $this->currency_list("No");
		$row->currency_only = $this->currency_list("Yes");
		if($this->input->get('currency'))
		{
		$currency = $this->input->get('currency');
		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "";
			}
			
			if($currency=="")
			{
			$currency = "USD";
			}
		}
		$row->currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		$row->currency = $currency ;
		return $row;
		
	}
	
	public function currency_list($only_currency="No")
	{
		$this->db->select("*");
		$this->db->from(CURRENCY);
		$this->db->where("status","1");
		if($only_currency=="Yes")
		{
			$this->db->where("is_coin","0");
		}
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$row1[] = $row;
			}
			
			return $row1;
		}
	}
	
	
	
	public function loadPaginationConfig()
	{
		$config['full_tag_open'] = '<div align="" class="paging"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</li></a>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
        
		return $config;
	}
	
	public function phpMail($from,$to,$message,$subject,$fromName,$cc,$bcc)
	{
		    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
            $this->email->set_header('Content-type', 'text/html');
			$this->email->from($from, $fromName);
			$this->email->to($to);
			if($cc)
			{
				$this->email->cc($cc);
			}
			if($bcc)
			{
				$this->email->bcc($bcc);
			}
			$this->email->subject($subject);
			$this->email->message($message);

			$this->email->send();
			
	}
	
	function convertCurrency($amount, $from, $to){
    $url  = "https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to";
    $data = file_get_contents($url);
    preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
    $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
    return round($converted, 3);
}

public function getHistoryDb()
	{
		$dsn1 = 'mysqli://cryptobu_coin:cryptobutt@123@localhost/cryptobu_crypto2';
        $this->master_db = $this->load->database($dsn1, true); 

        return 	$this->master_db;	
	}
	
	public function clear_all_cache()
	{
    $CI =& get_instance();
$path = $CI->config->item('cache_path');

    $cache_path = ($path == '') ? APPPATH.'cache/' : $path;

    $handle = opendir($cache_path);
    while (($file = readdir($handle))!== FALSE) 
    {
        //Leave the directory protection alone
        if ($file != '.htaccess' && $file != 'index.html')
        {
           @unlink($cache_path.'/'.$file);
        }
    }
    closedir($handle);
	
	if(file_exists(BASE_PATH."error_log"))
		{
			unlink(BASE_PATH."error_log");
			//echo "error log remove from frontend";
		}
	
	
		}

}
?>