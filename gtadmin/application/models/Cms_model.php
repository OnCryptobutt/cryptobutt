<?php
class Cms_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function cms_list($limit, $start)
	{
		$sql = "SELECT * FROM ".CMS." limit ".$start.",".$limit;
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
			{
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
   
    public function getCmsDetails($id)
    {
	   $sql = "SELECT * FROM ".CMS." WHERE cms_id='".$id."'";
       $query = $this->db->query($sql);
	   
	   if ($query->num_rows() > 0)
	   {
			return $query->row();
       }
       
    }
   
    public function record_count()
    {
      return $this->db->count_all(CMS);
    }
   
    public function cmsActivate($id)
    {
	   $sql = "UPDATE ".CMS." SET cms_status='1' WHERE cms_id='$id'";
	   $query = $this->db->query($sql);
    }
		
	public function cmsSuspend($id)
	{
		$sql = "UPDATE ".CMS." SET cms_status='0' WHERE cms_id='$id'";
		$query = $this->db->query($sql);
	}
		
	public function cmsDelete($id)
	{
		$photoname = $this->boost_model->getValue(CMS,"cms_photo_name","cms_id='".$id."' AND cms_delete='Y'");
		$photoext = $this->boost_model->getValue(CMS,"cms_photo_ext","cms_id='".$id."' AND cms_delete='Y'");
		
		if($photoname)
		{
			if(file_exists(CMS_PATH."thumb/".$photoname."_150.".$photoext))
			{
				unlink(CMS_PATH."thumb/".$photoname."_150.".$photoext);
			}
			
			if(file_exists(CMS_PATH.$photoname.".".$photoext))
			{
				unlink(CMS_PATH.$photoname.".".$photoext);
			}
		}
		$sql = "DELETE FROM ".CMS." WHERE cms_id='$id' AND cms_delete='Y'";
		$query = $this->db->query($sql);
	}
		
	public function cmsInsert()
	{
		$slug = url_title($this->input->post('cms_name'), 'dash', true);
		
		$sql = "INSERT INTO ".CMS." SET 
		
		cms_name         		= '".$this->input->post('cms_name')."',
		cms_content      		= '".$this->input->post('cms_content')."',
		cms_status         		= '".$this->input->post('cms_status')."',
		cms_order               = '".$this->input->post('cms_order')."',
		cms_site_title          = '".$this->input->post('cms_site_title')."',
		cms_meta_description    = '".$this->input->post('cms_meta_description')."',
		cms_meta_keyword        = '".$this->input->post('cms_meta_keyword')."',
		cms_seo          		= '".$slug."',
		cms_created_time 		= '".NOW."',
		cms_updated_time		 = '".NOW."'";
		
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	public function cmsEdit($id)
	{
		$slug = url_title($this->input->post('cms_name'), 'dash', true);
			
		$sql = "UPDATE ".CMS." SET 
		
		cms_name         		= '".$this->input->post('cms_name')."',
		cms_content     		= '".$this->input->post('cms_content')."',
		cms_status       		= '".$this->input->post('cms_status')."',
		cms_order        		= '".$this->input->post('cms_order')."',
		cms_site_title          = '".$this->input->post('cms_site_title')."',
		cms_meta_description    = '".$this->input->post('cms_meta_description')."',
		cms_meta_keyword        = '".$this->input->post('cms_meta_keyword')."',
		cms_seo          		= '".$slug."',
		cms_updated_time 		= '".NOW."' WHERE cms_id='".$id."'";
			
		$this->db->query($sql);
	}
		
	public function cmsInsertUpdate($file_name,$photo_name,$ext,$id)
	{
		$sql = "UPDATE ".CMS." SET 
		
		cms_photo      = '".$file_name."',
		cms_photo_name = '".$photo_name."',
		cms_photo_ext  = '".$ext."' WHERE cms_id='".$id."'";
		
		$this->db->query($sql);
	}
		
}
?>