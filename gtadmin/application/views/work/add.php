<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Work
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Add Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
			<input type="hidden" name="act" value="add">
              <div class="box-body">
			  
	         <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="work_name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo set_value('work_name');?>">
					<span class="red"><?php echo form_error('work_name');?></span>
                  </div>
                </div>  
				
				
				<div class="form-group">
					<label class="col-lg-2 control-label">Service Slider</label>
					
					<div class="col-lg-10">
					 
				
				<div id="InputSlider" >
                                <input type="text" value="" placeholder=""  name="work_slider_name[]"/>&nbsp;<input style="display: initial;" type="file" name="work_slider_photo[]"/>&nbsp;
							
							
       
							</div>
						
							 
					   </div>
					     </div>
   
				<div class="form-group">
                  <label class="col-lg-2 control-label"></label>
                    <div class="col-lg-10">
					<span style="color:red;">Note : Upload Image size only on (1280x853 Pixels).</span><br/>
				        <span class="small"><a class="btn btn-info" id="AddMoreImageBox" href="#">Add More Slider</a></span>
			       </div>
			   </div>
			   
			   <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Order</label>

                  <div class="col-sm-10">
                    <input type="text" name="work_order" class="form-control" id="inputEmail3" placeholder="Order" value="<?php echo set_value('work_order');?>">
					<span class="red"><?php echo form_error('work_order');?></span>
                  </div>
                </div>
			   
			   <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <input type="radio" checked='true' name="work_status" value="1"> Enable<br/>
					<input type="radio" name="work_status" value="0"> Disable
                  </div>
                </div>
                
					
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-warning">Submit</button>
				<a href="<?php echo base_url();?>work/"><button type="button" class="btn btn-warning">List</button></a>
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
 
 </div>
 
 