<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in | CryptoButt</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <!--<link rel="stylesheet" href="https://static.parastorage.com/services/third-party/fonts/Helvetica/fontFace.css">-->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
	<link rel="stylesheet" href="https://static.parastorage.com/services/login-statics/1.375.0/styles/main.css">
	<link rel="stylesheet" href="https://static.parastorage.com/services/login-statics/1.376.0/styles/main.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.toast.css">
  </head>
<!------ Include the above in your HEAD tag ---------->
<style>

.container.desktop .forgot-password-link:hover {
    color: #005EA5;
}
.container.desktop{overflow:auto;}
.btn.btn-primary1 {
    background-color: #753ac8;
    border-color: #753ac8;
    outline: none;
    color: #fff !important;
    padding-top: 8px;
}
.input-lg1 {
    padding: .200rem 3rem;
}
.container, body, html{max-width:100% !important;}
.container.desktop login .login-button, .container.desktop signup .login-button{padding-left:25px;}

/* The container */
.container-check {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 16px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container-check input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}
input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {background-color: #fff;}
/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 16px;
    width: 16px;
    background-color: #eee;
	border:1px solid #753ac8;
}

/* On mouse-over, add a grey background color */
.container-check:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container-check input:checked ~ .checkmark {
    background-color: #753ac8;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container-check input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container-check .checkmark:after {
    left: 5px;
    top: 1px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
input[type=text]:focus {
    border: 1px solid #753ac8;
}

.form-control:focus {
    border-color: #753ac8 !important;
    box-shadow: 0 0 0 1px #753ac8 !important;
}
#circle_tick,#circle_tick1,#circle_tick2{font-size: 18px;color: purple;float: right;position: relative;top: -32px;left: 25px;}
#loademail,#loading,#resetloading{float: right;position: relative;top: -35px;left: 25px;}


</style>
<body>
<div class="container desktop">
	<div class="header">
		<img class="wix-logo"  src="<?php echo base_url(); ?>img/logo.png" alt="title" style="height:65px">
		<a class="x-close login-svg-font-icons-x" href="<?php echo base_url(); ?>" style="text-decoration:none"></a>
	</div>
	<login id="login_temp">
			<div>
				<div class="login" name="loginForm">
				    <div id="signin-1">
    					<div class="log-in-title title">Log In</div>
    					<div class="new-user" style="text-align:center;">
    						New to Cryptobutt account?
    						<a class="signup-link" id="signin-login" href="javascript:;" style="color:#005EA5;">Sign Up</a>
    					</div>
					</div>
					<div id="signup-1" style="display:none">
    					<div class="log-in-title title">Sign Up</div>
    					<div class="new-user" style="text-align:center;">
    						Already have a Cryptobutt account?
    						<a class="signup-link" id="signup-login" href="javascript:;" style="color:#005EA5;">Log In</a>
    					</div>
					</div>
					<div class="signin-section">
						<div class="signin-with-email" id="signin-2">
						<form >
						 <?php echo validation_errors(); ?>   
						<div class="form-group" autocomplete="off">
						  <label for="usr">Email:</label>
						  <input type="text" class="form-control" value="" id="usr"   autocomplete="nope">
						  <i class="fa fa-check" id="circle_tick" style="display:none;"></i>
						  <div id="loginmessage"></div>
						  <span id="loademail" style="display:none;"><img src="<?php echo base_url(); ?>img/siginloader.gif" alt="Ajax Indicator" /></span>
						</div>
						<div class="form-group">
						  <label for="pwd">Password:</label>
						  <input type="password" value="" class="form-control" id="pwd" autocomplete="new-password">
						  <div id="loginmessage1"></div>
						</div>
						<div class="remember-me-and-forgot-password">
							<label class="remember-me-section">
								<div class="remember-me-text"></div>
								<label for="remember" class="container-check">Remember Me
								<input type="checkbox" id="remember" class="remember-me">
								<span class="checkmark"></span>
								</label>
							</label>
							<a class="forgot-password-link" id="forgot_link" href="javascript:;" style="color:#005ea5;">Forgot Password?</a>
						</div>
						<div class="recaptcha-widget" id="recaptcha-widget"></div>
						<div class="login-button">
						<div class="button-spinner">
						<a class="btn btn-primary1 input-lg input-lg1" id="signin_submit"  href="javascript:;">Login</a>
						
						</div>
						</div>
						</form>
						</div>
					
						<div class="signin-with-email" id="signup-2" style="display:none;">
						<form id="signupForm" autocomplete="off">
						<div class="form-group">
						  <label for="r_username">Email:</label>
						  <input type="email" class="form-control" name="r_username" id="r_username"  autocomplete="nope">
						  <i class="fa fa-check" id="circle_tick1" style="display:none;"></i>
						  <div id="message"></div>
                          <span id="loading"><img src="<?php echo base_url(); ?>img/siginloader.gif" alt="Ajax Indicator" /></span>
						</div>
						<div class="form-group">
						  <label for="usr-again">Type your email again:</label>
						  <input type="text" class="form-control" name="usr-again" id="usr-again" autocomplete="nope">
						    <div id="message1"></div><div id="emessage1"></div>
						</div>
						<div class="form-group">
						  <label for="r_pwd">Password:</label>
						  <input type="password"  class="form-control" id="r_pwd" autocomplete="off">
						  <div id="message2"></div>
						</div>
						<div class="form-group">
						  <label for="pwd-again">Type your password again:</label>
						  <input type="password" class="form-control" id="pwd-again" autocomplete="off">
						  <div id="message3"></div> <div id="emessage3"></div>
						</div>
						<div class="login-button">
					        <div class="button-spinner">
						        <a class="btn btn-primary1 input-lg input-lg1" id="signup_submit"  href="javascript:;">Sign Up</a>
						    </div>
						</div>
						</form>
						</div>
					<div class="divider"></div>
					<social-signin >
					<div>
					<div class="sozial-buttons new">
					<!--<div class="social-button facebook-button">
					<div class="buttonContentWrapper"  tabindex="0">
					<div class="buttonIcon">
					<div class="buttonSvgImage"></div>
					</div>
					<span class="buttonContents">Continue with Facebook</span>
					</div>
					
					</div>-->
					<div class="social-button google-button" onclick="signinGoogle()">
					<div class="buttonContentWrapper" tabindex="0">
					<div class="buttonIcon">
					<div class="buttonSvgImage"></div>
					</div>
					<span class="buttonContents">Continue with Google</span>
					</div>
					
					</div>
					</div>
					</div></social-signin>
					</div>
			<terms-of-use class="small"><div class="terms-of-use" style="color:red">*We do not email you or share your email address</div></terms-of-use>
			</div>
		</div>
	</login>
	<forgot-password id="forgot_pwd" style="display:none">
        <div style="margin:8% 0;">
        <form class="forgot-password" name="forgotPasswordForm">
        <div class="title-and-instructions">
        	<div class="title">Reset Password</div>
        	<div class="forgot-password-instructions content">Enter your email address &amp; we'll send you a link to reset your password.</div>
        			<div class="form-group" style="width:40%">
        			  <label for="resetpass">e.g., youremail@gmail.com</label>
        			  <input type="email" autocomplete="email"  name="email" class="form-control"  id="resetpass" autofocus="" >
        			  <i class="fa fa-check" id="circle_tick2" style="display:none;"></i>
						  <div id="resetmessage"></div>
                          <span id="resetloading" style="display:none"><img src="<?php echo base_url(); ?>img/siginloader.gif" alt="Ajax Indicator" /></span>
        			</div>
        	
        </div>
        <div class="back-next-buttons" style="margin:20px 15px">
        <a class="btn btn-primary1 input-lg input-lg1"  id="back_forgot" style="margin:0 15px;padding-top: 3px;"  href="javascript:;">Back</a>
        <div class="button-spinner">
        <a class="btn btn-primary1 input-lg input-lg1" name="submit" id="send_btn" style="padding-top: 3px;"  href="javascript:;">Send</a>
        
        </div>
        </div>
        </form>
        </div>
    </forgot-password>
    <forgot-password-confirmation id="forgot_confirm"  style="display:none;">
        <div style="margin:8% 0;"></div>
        <form class="forgot-password-confirmation" >
        <div class="title ng-binding">Sent! Check Your Email</div>
        <div class="content ng-binding">Head over to your mailbox to get your reset link and create your brand new password.</div>
        <div class="button">
        <a class="btn btn-primary1 input-lg input-lg1" name="submit" style="padding-top:3px;" href="<?php echo base_url(); ?>signin/login" >Done</a>
        </div>
        </form>
        </div>
    </forgot-password-confirmation>
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.toast.js"></script>
<script>
$("#signin-login").click(function() {
    $("#signin-1").css("display","none");
    $("#signup-1").css("display","block");
    $("#signin-2").css("display","none");
    $("#signup-2").css("display","block");
});

$("#signup-login").click(function() {
    $("#signin-1").css("display","block");
    $("#signup-1").css("display","none");
    $("#signin-2").css("display","block");
    $("#signup-2").css("display","none");
});
$("#forgot_link").click(function() {
    $("#forgot_pwd").css("display","block");
    $("#login_temp").css("display","none");
});
$("#back_forgot").click(function() {
    $("#forgot_pwd").css("display","none");
    $("#login_temp").removeAttr("style");
});
$("#send_btn").click(function() {
    data  = $('#resetpass').val();
    var len = data.length;
      if(len < 1) {
             $('#resetmessage').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }
    $('#resetpass').blur(function(){ 
        var email_val = $("#resetpass").val();
        var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        if(filter.test(email_val)){ 
            // show loader
            $('#resetloading').show();
            $.post("<?php echo site_url()?>/signin/email_checklogin", {
                email: email_val
            }, function(response){ 
                $('#resetloading').hide();
                $('#resetmessage').html('').html(response.message).show();
            });
            return false;
        } else{
           
            $('#resetmessage').html('').html("<span style='color:#f00;'>Hmmm, that’s not a valid email address.</span>").show();
        }
    });
    if($('#resetpass').val()!=''){
        var emailfilter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        if(emailfilter.test(data)){
            var dataString = 'email='+$('#resetpass').val();
        $.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>signin/resetMail',
           data: dataString,
           cache: false,
           success: function(response){ 
			   if(response == 'success') {
			     $("#forgot_pwd").css("display","none");
                 $("#forgot_confirm").css("display","block");
    			 $.toast({
                    heading: 'Success',
                    text: 'Sent! Check Your Email. Head over to your mailbox to get your reset link and create your brand new password.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
			} 
                 
           }
    
    });
        }
    }
    
    
  
});
function signinGoogle(){ 
 window.location.href = "<?php echo base_url(); ?>hauth/login/Google";
}
$(document).ready(function() {
        /// make loader hidden in start
    $('#loading').hide(); 
     $('#r_username').blur(function(){ 
        var email_val = $("#r_username").val();
        var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        if(filter.test(email_val)){ 
            // show loader
            $('#loading').show();
            $.post("<?php echo site_url()?>/signin/email_check", {
                email: email_val
            }, function(response){ 
                $('#loading').hide();
                $('#message').html('').html(response.message).show();
            });
            return false;
        } else{
           
            $('#message').html('').html("<span style='color:#f00;'>Hmmm, that’s not a valid email address.</span>").show();
        }
    });
    $('#signup_submit').click(function(event){
    
        data  = $('#r_username').val();
        data1 = $('#r_pwd').val();
        data2  = $('#usr-again').val();
        data3  = $('#pwd-again').val();
        var len = data.length, len1 = data1.length, len2 = data2.length, len3 = data3.length;
        if(len < 1) {
             $('#message').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#message').html('');
        }
        if(len2 < 1) { 
             $('#emessage1').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#emessage1').html('');
        }
        if(len1 < 1) {
             $('#message2').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#message2').html('');
        }
         
        if(len3 < 1) {
             $('#emessage3').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#emessage3').html('');
        }
        if($('#r_username').val() != $('#usr-again').val()) {
            $('#message1').html('').html("<span style='color:#f00;'>Emails don't match. Take another look.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#message1').html('');
        }
         if($('#r_pwd').val() != $('#pwd-again').val()) {
            $('#message3').html('').html("<span style='color:#f00;'>Password don't match.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#message3').html('');
        }
       
     if(($('#r_username').val()!='' && $('#r_pwd').val() !='' && $('#r_username').val() == $('#usr-again').val() && $('#r_pwd').val() == $('#pwd-again').val())) {

        var dataString = 'email='+ $('#r_username').val()+'&password='+$('#r_pwd').val();
        $.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>signin/signup',
           data: dataString,
           cache: false,
           success: function(response){ 
			   if(response == 'success') {
			     $('#signup-1').hide();
			     $('#signin-1').show();
			     $('#signup-2').hide();
			     $('#signin-2').show();
    			 $.toast({
                    heading: 'Success',
                    text: 'User has been saved successfully.',
                    showHideTransition: 'slide',
                     position: 'bottom-right',
                    icon: 'success'
                });
			} else {						
				$.toast({
                heading: 'Error',
                text: 'That email’s already in our system, choose another one',
                showHideTransition: 'fade',
                 position: 'bottom-right',
                icon: 'error'
            });
			}
                 
           }
    
    });
     } 
    });

     $('#usr').blur(function(){ 
        var email_val = $("#usr").val();
        var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        if(filter.test(email_val)){ 
            // show loader
            $('#loademail').show();
            $.post("<?php echo site_url()?>/signin/email_checklogin", {
                email: email_val
            }, function(response){ 
                $('#loademail').hide();
                $('#loginmessage').html('').html(response.message).show();
            });
            return false;
        } else{
           
            $('#loginmessage').html('').html("<span style='color:#f00;'>Hmmm, that’s not a valid email address.</span>").show();
        }
    });
$('#signin_submit').click(function(event){
    
        data  = $('#usr').val();
        data1 = $('#pwd').val();
        var len = data.length, len1 = data1.length;
        if(len < 1) {
             $('#loginmessage').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#loginmessage').html('');
        }
        
        if(len1 < 1) {
             $('#loginmessage1').html('').html("<span style='color:#f00;'>This field is required.</span>").show();
            // Prevent form submission
            event.preventDefault();
        }else{
             $('#loginmessage1').html('');
        }

     if($('#usr').val()!='' && $('#pwd').val() !='' ) {

        var dataString = 'email='+ $('#usr').val()+'&password='+$('#pwd').val();
        $.ajax({
           type: "POST",
           url: '<?php echo base_url(); ?>signin/signin',
           data: dataString,
           cache: false,
           success: function(response){ 
			 if(response=='success') {
    			 window.location = '<?php echo base_url(); ?>profile';
			} else {						
				$.toast({
                heading: 'Error',
                text: 'Double check your password and try again.',
                showHideTransition: 'fade',
                 position: 'bottom-right',
                icon: 'error'
            });
			}
                 
           }
    
    });
     } 
    });     
    
    
    });   

</script>
</body>