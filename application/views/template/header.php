<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://use.fontawesome.com/c596d80306.js"></script> 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/customize_new.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.toast.css">
	<title>Crypto Exchange Rates | Cryptobutt</title>
	<meta name="keyword" content="<?php if(isset($meta_keywords)){ echo $meta_keywords; }?>"/>
	<meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; }?>"/>
	<style type="text/css">
	.b-select-wrap {
    height: 40px;
    width: 85px;
    color: #753ac8;
    overflow: hidden;
    position: relative;
    margin: 0 20px;
}
.b-select-wrap:after {
	content:"\f0d7";
    font-family: FontAwesome;
    padding: 2px 8px;
    position: absolute;
    right: 15px;
    top: 0;
    z-index: 1;
    text-align: center;
    width: 10%;
    height: 100%;
    pointer-events: none;
	font-size:24px;
}

.b-select {
    height: 40px;
    width: 100%;
    padding: 5px 15px;
    background-color: #ececec;
    border: 0;
    outline: none;
    font-size: 16px;
    -webkit-appearance: none; /* for webkit browsers */
    -moz-appearance: none; /* for firefox */
    appearance: none; /* for modern browsers */
}
/* remove default caret for ie */
.b-select::-ms-expand {
   display:none;
}
</style>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"> </script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/jquery.visible.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/jquery.toast.js"></script>
<script>
	function one1() {
		document.getElementById('suggesstion-box').style.display = "block";
	}
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118010155-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118010155-1');
</script>
  </head>
  <body>
  <?php if($this->input->get('currency'))
			{
				$currency = $this->input->get('currency');
			}
			else
			{
					if($this->session->userdata('user_id'))
					{
						$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
					}
					else
					{
						$currency = "";
					}
					
					if($currency=="")
					{
					$currency = "USD";
					}
			}
			?>
    <section class="container-fluid">
		<header class="top" style="position:relative">
			<figure class="figure">
				<a href="<?php echo base_url();?>"><img id="desktop_img" src="<?php echo base_url();?>img/logo.png" class="figure-img img-fluid rounded logo hidden-xs-down" alt="Criptobutt">
				<span class="logo-text">IMDB for Crypto assets</span>
				</a>
				<a href="<?php echo base_url();?>"><img id="mobile_img" src="<?php echo base_url();?>img/m_logo.png" class="figure-img img-fluid rounded  hidden-sm-up" alt="Criptobutt"></a>
			</figure>
			<section class="float-lg-right main_nav">
			    	<div class="dropdown show country b-select-wrap">
					  <select onchange="redirect(this.value)" id="select"  class="b-select">
					  <?php $query=$this->db->select('*')->from(CURRENCY)->where(array("status"=>"1","is_coin"=>"0"))->get();
					  if($query->num_rows()>0)
					  {
						 foreach($query->result() as $row)
						 {
						 ?> 
						  <option <?php if($row->currency_code==$currency){ echo "selected";}?> value="<?php echo $row->currency_code;?>"><?php echo $row->symbol;?>&nbsp;<?php echo $row->currency_code;?></option>
						 <?php 
						 } 
						 } ?>
					  </select>
					</div>
			    <span class="fl_left">
						<input type="text" id="coin_name" name="search" class="search" autocomplete="off" placeholder="Search coin name/ people"  >
						<div id="suggesstion-box" style="display: none;"></div>
				</span>
				<span class="fl_left"><a class="navbar-brand <?php if($basename=='home') { ?>active <?php } ?>" href="<?php echo base_url();?>">
				    <i class="fa fa-connectdevelop" aria-hidden="true"></i> Coin</a></span>

				<?php if($this->session->userdata('user_id')){?>
				<span><a class="navbar-brand <?php if($basename=='profile') { ?>active <?php } ?>" href="<?php echo base_url('profile');?>">
				    <?php
				        if (file_exists($query_user_row->photoURL)) {
				    ?>
				    <img class="img-fluid user" src="<?php echo $query_user_row->photoURL;?>">
				    <?php
				        } else{
			            ?>
			             <i class="fa fa-user-circle"  style="font-size: 16px;"></i>
			            <?php
				        }
				        $iparr = explode("@", $query_user_row->email); 
				        ?>
				   <span class="coin_cap"><?php echo $iparr[0];?></span> <i class="fa fa-caret-down" style="color:#6F777B;margin-left: 6px;"></i></a></span>
			<?php } else { ?>
				<span><a class="navbar-brand"  href="<?php echo base_url('signin/login');?>"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Login</a></span>
			<?php } ?>
			</section>
		</header>
		<input type="hidden" id="searchstatus" value="0"/>
		<script>
		function searchToggle()
		{
			var searchstatus = document.getElementById("searchstatus").value;
			if(searchstatus==0)
			{
			document.getElementById("popupsearch1").style.display="block";
			document.getElementById("searchstatus").value="1";
			}
			else
			{
			document.getElementById("popupsearch1").style.display="none";
			}
		}
			var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
			var element = document.getElementById('mobile_img');
			if (isMobile) {
				element.style.display = "block"
			} else {
				element.style.display = "none"
			}
		</script>