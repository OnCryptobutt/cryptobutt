<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
class Coin_details_new extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->load->model('coin_details_model');
		$this->settings = $this->boost_model->loadSettings();
    }

	function index()
	{
		define("CHUNK","70");
		//header('Content-type: application/json');
		$success = "";
		$coins_parmeters = $this->coin_details_model->getParameter();
		$cron_details    = $this->coin_details_model->getCronDetails();
		$columns         = $this->coin_details_model->getColumns();
		//$coins_lists     = $this->coin_details_model->getCoinList($cron_details,$uptoid);
		$coins_lists     = $this->coin_details_model->getCoinList_new($cron_details);
		$Data['Response'] ="";
		
		//$coins_lists = explode(",",$coins_lists);
		
		//print_r($coins_lists);
		//exit;
		
		if(is_array($coins_lists))
		{
			$chunk=1;
			foreach($coins_lists as $coins_list)
			{
			
			    $coinlist = explode(",",$coins_list);
				foreach($coins_parmeters as $coins_parmeter)
				{
					$Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='.$coins_list.'&tsyms='.$coins_parmeter),true);
					
					$currency_array = explode(",",$coins_parmeter);
					
					
					
					
					
					if(is_array($coinlist))
					{
					
					foreach($coinlist as $list)
					{
					
				if(isset($Data['RAW']) && isset($Data['RAW'][$list]))
				{
					if(is_array($columns))
					{
						foreach($columns as $column)
						{
							$coin_id = $this->boost_model->getValue(COIN,"id","name='".$list."'");
							
							$exist_id = $this->boost_model->getValue(CRON,"coin_history_id","coin_id='".$coin_id."' AND column_id='".$column->id."' AND id='".$cron_details->id."'");
						
							if($exist_id)
							{
								$history_id = $exist_id;
							
							}
							else
							{
								$exist_id = $this->boost_model->getValue(COIN_HISTORY,"id","coin_id='".$coin_id."' AND column_id='".$column->id."' AND cron_id='".$cron_details->id."'");
								if($exist_id)
								{
									$history_id = $exist_id;
							
								}
								else
								{
						
									$insert_array = array("coin_id"      => $coin_id,
														  "name"         => $list,
														  "column_id"    => $column->id,
														  "cron_id"      => $cron_details->id,
														  "created_time" => NOW);
									$this->db->set($insert_array)->insert(COIN_HISTORY);
									$history_id = $this->db->insert_id();
								}
							}
							
							/** Temp Table **/
							
								$exist_id_temp = $this->boost_model->getValue(COIN_HISTORY_TEMP,"id","coin_id='".$coin_id."' AND column_id='".$column->id."'");
								if($exist_id_temp)
								{
									$history_id_temp = $exist_id_temp;
							
								}
								else
								{
						
									$insert_array_temp = array("coin_id"      => $coin_id,
														  "name"         => $list,
														  "column_id"    => $column->id,
														  "cron_id"      => $cron_details->id,
														  "created_time" => NOW);
									$this->db->set($insert_array_temp)->insert(COIN_HISTORY_TEMP);
									$history_id_temp = $this->db->insert_id();
								}
							
							/** End Temp Table **/
						
							$update_array = array(  "coin_history_id" 	=> $history_id,
													"coin_name"			=> $list,
													"coin_id"			=> $coin_id,
													"column_id"			=> $column->id,
													"updated_time"		=> NOW
												 );
							$this->db->set($update_array)->where("id",$cron_details->id)->update(CRON); 
						
							foreach($currency_array as $currency)
							{
								if($currency!=$list)
								{
									if(isset($Data['RAW'][$list][$currency]))
									{
								$det = $Data['RAW'][$list][$currency];
								$api_date_time = date("Y-m-d H:i:s",$det['LASTUPDATE']);
								$update_data = array($currency=>$det[strtoupper($column->name)],
								"api_updated_time"=>$det['LASTUPDATE'],
								"api_updated_date_time"=>$api_date_time,
								"created_time"=>NOW);
								$this->db->set($update_data)->where("id",$history_id)->update(COIN_HISTORY);
								/** history temp file **/
								$this->db->set($update_data)->where("id",$history_id_temp)->update(COIN_HISTORY_TEMP);
								/** history temp file **/
									}
								}
							}
					}
				}
		       
			    }
				//echo "<font color='red'>".date("d M Y h:i A")."</font>&nbsp;|&nbsp;<font color='green'>".$list." Coin has updated</font><br/>";
				
					}
					}
		
				}
				
				
				//echo "<font color='red'>".date("d M Y h:i A")."</font>&nbsp;|&nbsp;<font color='green'>".$coins_list->name." Coin has updated</font><br/>";
				echo "<font color='red'>".date("d M Y h:i A")."</font>&nbsp;|&nbsp;<font color='green'> $chunk chunk[".CHUNK."coins] has updated</font><br/>";
				$chunk++;
				
				
		    }
			
			
			$update_data = array("status"=>"COMPLETED",
			"updated_time"=>NOW);
			$this->db->set($update_data)->update(CRON);
			echo "<br/><font color='red'>All Coins has updated</font><br/>";
		
		}
		
		
	
	
	}
}
