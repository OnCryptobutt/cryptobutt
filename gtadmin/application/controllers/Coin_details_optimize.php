<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
class Coin_details_optimize extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->load->model('coin_details_model');
		$this->settings = $this->boost_model->loadSettings();
    }

	function index()
	{
		
		//header('Content-type: application/json');
		$success = "";
		$coins_parmeters = $this->coin_details_model->getParameter();
		$cron_details    = $this->coin_details_model->getCronDetails();
		$columns         = $this->coin_details_model->getColumns();
		$coins_lists     = $this->coin_details_model->getCoinList($cron_details);
		$Data['Response'] ="";
		
		if(is_array($coins_lists))
		{
			foreach($coins_lists as $coins_list)
			{
				$time_start = microtime(true);
				
					$Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='.$coins_list->name.'&tsyms='.$coins_parmeters[0]),true);
					
					//$currency_array = explode(",",$coins_parmeters);
					
				if(isset($Data['RAW']) && isset($Data['RAW'][$coins_list->name]))
				{
					
							
								
						
									$insert_array = array("coin_id"      => $coins_list->id,
														  "name"         => $coins_list->name,
														  "column_id"    => '1',
														  "cron_id"      => $cron_details->id,
														  "created_time" => NOW);
									$this->db->set($insert_array)->insert(COIN_HISTORY);
									$history_id = $this->db->insert_id();
								
							
							
							/** Temp Table **/
							
								$exist_id_temp = $this->boost_model->getValue(COIN_HISTORY_TEMP,"id","coin_id='".$coins_list->id."' AND column_id='1'");
								if($exist_id_temp)
								{
									$history_id_temp = $exist_id_temp;
							
								}
								else
								{
						
									$insert_array_temp = array("coin_id"      => $coins_list->id,
														  "name"         => $coins_list->name,
														  "column_id"    => $column->id,
														  "cron_id"      => $cron_details->id,
														  "created_time" => NOW);
									$this->db->set($insert_array_temp)->insert(COIN_HISTORY_TEMP);
									$history_id_temp = $this->db->insert_id();
								}
							
							/** End Temp Table **/
						 
						
							/**foreach($currency_array as $currency)
							{
								if($currency!=$coins_list->name)
								{
								$det = $Data['RAW'][$coins_list->name][$currency];
								$api_date_time = date("Y-m-d H:i:s",$det['LASTUPDATE']);
								$update_data = array($currency=>$det[strtoupper('price')],
								"api_updated_time"=>$det['LASTUPDATE'],
								"api_updated_date_time"=>$api_date_time,
								"created_time"=>NOW);
								$this->db->set($update_data)->where("id",$history_id)->update(COIN_HISTORY);
							
								$this->db->set($update_data)->where("id",$history_id_temp)->update(COIN_HISTORY_TEMP);
								
								}
							}**/
							
							
							
	
						
					
						
								
								if(isset($Data['RAW'][$coins_list->name]["usd"]))
								{
									$update_data = array();
									$api_date_time = date("Y-m-d H:i:s",$Data['RAW'][$coins_list->name]["usd"]['LASTUPDATE']);
										
									$update_data["api_updated_time"] = $api_date_time;
									$update_data["created_time"] = NOW;
										
									$update_data["USD"] = $Data['RAW'][$coins_list->name]["usd"]['price'];
									$update_data["AUD"] = $Data['RAW'][$coins_list->name]["aud"]['price'];
									$update_data["BRL"] = $Data['RAW'][$coins_list->name]["brl"]['price'];
									$update_data["CAD"] = $Data['RAW'][$coins_list->name]["cad"]['price'];
									$update_data["CHF"] = $Data['RAW'][$coins_list->name]["chf"]['price'];
									$update_data["CLP"] = $Data['RAW'][$coins_list->name]["clp"]['price'];
									$update_data["CNY"] = $Data['RAW'][$coins_list->name]["cny"]['price'];
									$update_data["CZK"] = $Data['RAW'][$coins_list->name]["czk"]['price'];
									$update_data["DKK"] = $Data['RAW'][$coins_list->name]["dkk"]['price'];
									$update_data["EUR"] = $Data['RAW'][$coins_list->name]["eur"]['price'];
									$update_data["GBP"] = $Data['RAW'][$coins_list->name]["gbp"]['price'];
									$update_data["HKD"] = $Data['RAW'][$coins_list->name]["hkd"]['price'];
									$update_data["HUF"] = $Data['RAW'][$coins_list->name]["huf"]['price'];
									$update_data["IDR"] = $Data['RAW'][$coins_list->name]["idr"]['price'];
									$update_data["ILS"] = $Data['RAW'][$coins_list->name]["ils"]['price'];
									$update_data["INR"] = $Data['RAW'][$coins_list->name]["inr"]['price'];
									$update_data["JPY"] = $Data['RAW'][$coins_list->name]["jpy"]['price'];
									$update_data["KRW"] = $Data['RAW'][$coins_list->name]["krw"]['price'];
									$update_data["MXN"] = $Data['RAW'][$coins_list->name]["mxn"]['price'];
									$update_data["MYR"] = $Data['RAW'][$coins_list->name]["myr"]['price'];
									$update_data["NOK"] = $Data['RAW'][$coins_list->name]["nok"]['price'];
									$update_data["NZD"] = $Data['RAW'][$coins_list->name]["nzd"]['price'];
									$update_data["PHP"] = $Data['RAW'][$coins_list->name]["php"]['price'];
									$update_data["PKR"] = $Data['RAW'][$coins_list->name]["pkr"]['price'];
									$update_data["PLN"] = $Data['RAW'][$coins_list->name]["pln"]['price'];
									$update_data["RUB"] = $Data['RAW'][$coins_list->name]["rub"]['price'];
									$update_data["SEK"] = $Data['RAW'][$coins_list->name]["sek"]['price'];
									$update_data["SGD"] = $Data['RAW'][$coins_list->name]["sgd"]['price'];
									$update_data["THB"] = $Data['RAW'][$coins_list->name]["thb"]['price'];
									$update_data["TRY"] = $Data['RAW'][$coins_list->name]["try"]['price'];
									$update_data["TWD"] = $Data['RAW'][$coins_list->name]["twd"]['price'];
									$update_data["ZAR"] = $Data['RAW'][$coins_list->name]["zar"]['price'];
									$update_data["BTC"] = $Data['RAW'][$coins_list->name]["btc"]['price'];
								$update_data["ETH"] = $Data['RAW'][$coins_list->name]["eth"]['price'];
									
									
									print_r($update_data);
									
										$this->db->set($update_data)->where("id",$history_id)->update(COIN_HISTORY);
								/** history temp file **/
								$this->db->set($update_data)->where("id",$history_id_temp)->update(COIN_HISTORY_TEMP);
								
								}
								
							
					
				
		       
			    }
				
				
		
				
				
				
				echo "<font color='red'>".date("d M Y h:i A")."</font>&nbsp;|&nbsp;<font color='green'>".$coins_list->name." Coin has updated</font><br/>";
				$time_end = microtime(true);

//dividing with 60 will give the execution time in minutes other wise seconds
$execution_time = ($time_end - $time_start)/60;

//execution time of the script
echo '<b>Total Execution Time:</b> '.$execution_time.' Mins<br/>';
				
		    }
			
			
			$update_data = array("status"=>"COMPLETED",
			"updated_time"=>NOW);
			$this->db->set($update_data)->update(CRON);
			echo "<br/><font color='red'>All Coins has updated</font><br/>";
		
		}
		
		
	
	
	}
}
