 
 <?php $this->load->view('common_coin'); ?>
 
 
 <!--<script src = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/vendor/jquery.min.js"></script>-->
  <!-- <script src = "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.0.1/js/foundation.min.js"></script>
   <script src="https://code.highcharts.com/highcharts.js"></script>
   <script src="https://code.highcharts.com/modules/exporting.js"></script>-->
   
<!--<script src="<?php echo base_url(); ?>js/highstock.js"></script>
<script src="<?php echo base_url(); ?>js/exporting.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/daterangepicker.css" />-->

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />   
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  
   

		
		
		
<!-- coin page start -->

 <div class="row alg-con">
			<div class="col-md-12">
				
			<div class="well2 well-sm feed_cor">
				<div class="row">
					<div id="exTab2" class="container">	
						
				<ul class="nav nav-tabs" >
					<li class="active">
						<a  data-toggle="tab" href="#1"><i class="material-icons cor1">&#xE1B8;</i>&nbsp;Chart</a>
					</li>
					<li><a  data-toggle="tab" href="#2"><i class="material-icons cor2">&#xE8E5;</i>&nbsp;Market</a>
					</li>
					<li><a  data-toggle="tab" href="#3"><i class="material-icons cor2">&#xE24F;</i>&nbsp;Historical Data</a>
					</li>
				</ul>

						<div class="tab-content">
							<div class="tab-pane active bottom_border_trans" id="1">
								<div class="row">
								
								
								
								<!-- High chart start-->
								
								
							<div id="container" >
	
								   <div class="highcharts-background" style="background-color: white;font-size: 23px;padding-bottom: 40px;padding-top: 40px;">
									 <span style="padding-left: 20px;">Bitcoin Charts</span> 
									 <span style="padding-left: 760px;padding-top: 40px;">
										<a href="<?php echo base_url('coin_page/chart/'.$cname.'?currency='.$currency); ?>" target="_blank"><button type="button" class="btn btn-warning" style="background-color: #FFD700 !important; color: black !important;"><i class="fa fa-external-link" aria-hidden="true"></i> Open in new window</button></a>
									 </span>
								   </div>
								   
								<!-- chart show id --> 
								
								   <div id="container1" > </div>
								   
								   
								<!-- -->   
								   
	   

	                       </div>
									
									
									<div align="center" class="flash2"></div>
									
									
									
									
									<script>

									var seriesOptions = [],
										seriesCounter = 0,
										names = ['Mktcap','Price', 'Volume24Hour'];

									/*
									 * Create the chart when all data is loaded
									 * @returns {undefined}
									 */
									 $(".flash2").show();
                                     $(".flash2").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
									 
									function createChart() {
										
										 

										Highcharts.stockChart('container1', {

											rangeSelector: {
												selected: 1
											},

											yAxis: {
												labels: {
													formatter: function () {
														return (this.value > 0 ? ' + ' : '') + this.value + '%';
													}
												},
												plotLines: [{
													value: 0,
													width: 2,
													color: 'silver'
												}]
											},

											plotOptions: {
												series: {
													compare: 'percent',
													showInNavigator: true
												}
											},

											tooltip: {
												pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
												valueDecimals: 2,
												split: true
											},

											series: seriesOptions
										});
									}

									$.each(names, function (i, name) {

										$.getJSON('<?php echo base_url(); ?>Coin_page/json_chart?coumn=' + name.toLowerCase() + '.json&currency=<?php echo $currency; ?>&cid=<?php echo $cid; ?>',    function (data) {

										  
										 
											seriesOptions[i] = {
												name: name,
												data: data
											};

											// As we're loading the data asynchronously, we don't know what order it will arrive. So
											// we keep a counter and create the chart when all the data is loaded.
											seriesCounter += 1;

											if (seriesCounter === names.length) {
												 $(".flash2").hide();
												createChart();
												
											}
										});
									});
                                     
									</script>
									
									
								<!--- echd -->	
										
										<style>
										.highcharts-credits { display:none; }
										</style>
								</div>
							</div>
							
							<?php
                            if($this->session->userdata('user_id'))
							{
								$currency3 = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
								
								 $sym=$this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency3."'");
							}
							else
							{
								$currency3 = "";
							}
							
							if($currency3=="")
							{
							  $currency3 = "USD";
							  $sym="$";
							}
					?>
							
							
							
							<div class="tab-pane bottom_border_trans" id="2">
								<div class="bitco">Bitcoin Markets</div>
								<div class="pull-right col-xs-5 col-sm-3 col-md-3 col-lg-2 alg sect dsk_align mar-top">
									<div class="btn-group">
										<!--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">$ USD <span class=caret"></span></button>-->
												<div align="center" class="flash3"></div>
									
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										   <span id="currency_val"><?php echo $sym; ?> <?php echo $currency3; ?></span> <span class="caret"></span>
									    </button>
										
										<ul class="dropdown-menu" role="menu">
											
									<?php if(is_array($this->settings->currency_list)) {
									   foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										<!--<li><a href="#"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>-->
										
					                   <li><a onclick="currency_change('<?php echo $currency_list->currency_code; ?>','<?php echo $currency_list->symbol;?>')" href="javascript:void(0);"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>
										
									  
										
									   <?php } } ?>
										
										</ul>
									</div>
								</div></br></br></br></br>
								<div class="row col-md-12 custyle table-responsive">
									<table class="table table-striped custab">
										<thead>
											<tr>
												<th>#</th>
												<th>Source</th>
												<th>Pair</th>
												<th>Volume (24h)</th>
												<th>Price</th>
												<th>Volume (%)</th>
												<th >Updated</th>
											</tr>
										</thead>
										
										 <tbody id="pageData2">
				
                                         </tbody>
										
										
										
									</table>
									
						            <!--<div align="center" class="flash"></div>-->
									
						           <!--<span id="market_count"><?php if(isset($count)) { echo $count; }?></span>-->
								   <input type="hidden" id="market_count" value="<?php echo $count; ?>">
								   <input type="hidden" id="row_nos" value="0">
								   <input type="hidden" id="page_id" value="0">
								   <input type="hidden" id="currency_value" value="">
<!--- for pagination -->
						
								</div>
							</div>
							
							
							<div class="tab-pane bottom_border_trans" id="3">
								<div class="bitco desktop">Historical data for Bitcoin</div>
								<!-- mobile view start-->
								<div class="text-center bitco mobile">Historical data for Bitcoin</div>
								<!-- mobil view end -->
								<div class="pull-left col-xs-5 col-sm-3 col-md-3 col-lg-3 mar-top2 desktop">
									
								</div>
								
								<div class="pull-right col-xs-7 col-sm-3 col-md-3 col-lg-3 alg sect dsk_align mar-top2 desktop">
								<div  class="flash2"></div>
									<div class="btn-group pull-right" id="reportrange" >
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="dateshow"><span></span>
											<div class="caret"></div>
										</button>
										
										
										
									</div>
								</div><br><br>
								<!--  mobile view start-->
								
								<div class="text-center col-xs-12 col-sm-3 col-md-3 col-lg-3 mobile">
									Currency in USD
								</div>
								
								<div class="text-center col-xs-12 col-sm-3 col-md-3 col-lg-3 alg sect dsk_align mar-top2 mobile">
									<div  class="flash2"></div>
									  <div class="btn-group pull-right" id="reportrange1" >
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="dateshow"><span></span>
											<div class="caret"></div>
										</button>
									
									</div>
								</div><br><br>
								
								<!--  mobile view end -->
													
								<div class="row col-md-12 custyle table-responsive">
									<table class="table table-striped custab">
									
										<thead>
											<tr>
												<th>Date</th>
												<th>Open</th>
												<th>High</th>
												<th>Low</th>
												<th>Close</th>
												<th>Volume</th>
												<th>Market Cap</th>
											</tr>
										</thead>
										
										
										
										<tbody id="pageData">
				
                                        </tbody>
										
									
									</table>
									
									<div align="center" class="flash1"></div>
									
									
									
									
									
								</div>
								
							</div>
						</div>
					
					
					

						<div class="tab-content">
							<div class="tab-pane bottom_border_trans" id="2">
								<div class="bitco">Bitcoin Markets</div>
								<div class="pull-right col-xs-5 col-sm-3 col-md-3 col-lg-2 alg sect dsk_align mar-top">
									<div class="btn-group">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span id="currency_val"><?php echo $sym; ?> <?php echo $currency3; ?></span> <span class="caret"></span>
									</button>
									
									<ul class="dropdown-menu" role="menu">
									
									<?php if(is_array($this->settings->currency_list)) {
									   foreach($this->settings->currency_list as $currency_list)
									   {
								     ?>
										 <li><a onclick="currency_change('<?php echo $currency_list->currency_code; ?>','<?php echo $currency_list->symbol;?>')" href="javascript:void(0);"><?php echo $currency_list->symbol;?> <?php echo $currency_list->currency_code;?></a></li>
										
									   <?php } } ?>
									
									
									</ul>
									
								
									
									
								</div>
								</div>
								<div style="margin-top:45px;"></div>
								<div class="row col-md-12 custyle table-responsive">
									<table class="table table-striped custab">
										<thead>
											<tr>
												<th>#</th>
												<th>Source</th>
												<th>Pair</th>
												<th>Volume (24h)</th>
												<th>Price</th>
												<th>Volume (%)</th>
												<th >Updated</th>
											</tr>
										</thead>
										
										
										<tbody id="pageData2">
				
                                         </tbody>
										
										
									</table>
									
									
									
								</div>
							</div>
							
						</div>
						
						
					

						<div class="tab-content">
							<div class="tab-pane bottom_border_trans" id="3">
								<div class="bitco desktop">Historical data for Bitcoin</div>
								<!-- mobile view start-->
								<div class="text-center bitco mobile">Historical data for Bitcoin</div>
								<!-- mobil view end -->
								<div class="pull-left col-xs-5 col-sm-3 col-md-3 col-lg-3 mar-top2 desktop">
									Currency in USD
								</div>
								
								
								
								<!--  mobile view end -->
													
								<div class="row col-md-12 custyle table-responsive">
									<table class="table table-striped custab">
										<thead>
											<tr>
												<th>Date</th>
												<th>Open</th>
												<th>High</th>
												<th>Low</th>
												<th>Close</th>
												<th>Volume</th>
												<th >Market Cap</th>
											</tr>
										</thead>
										
										 <tbody id="pageData">
				
                                         </tbody>
										
										
									</table>
									
									
									
									
									
									
								</div>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
			
			
			
			</div>
		</div>

<!-- market page start -->



<script>

$(document).on('scroll', function(){ 
    
	
	
	 if($(document).height()-400 <= ($(document).scrollTop() + $(document).height()))
	  {
		
		 
		 var totalcoins = '<?php echo $count; ?>';
		 
		  totalcoins = Number(totalcoins);
		  var val = document.getElementById("row_nos").value;
		  pagedisplays = val * <?php echo $this->settings->settings_records_per_page_front;?>;
		  if(totalcoins>pagedisplays)
		  {
			
			 var pageids = document.getElementById("page_id").value;
			 var pageidsarray = pageids.split(",");
			 var i = pageidsarray.indexOf(val);

             if(i > -1){ 
			 }
			 else
			 {
				  UserPagination('0','0','','');
				  document.getElementById('page_id').value=pageids+','+val;
			 }
			  
		  }
		 
		
	  }
	  
    });
	
	
	
</script>


<script>
  function UserPagination(pageId,liId,currency,symbol){
	  
	 
	 var val = document.getElementById("row_nos").value;
	 
	
	  if(currency!='')
	  {
	    document.getElementById('currency_value').value=currency;
		var pid='0';
		$("#row_nos").val(pid);
		
		
		$(".flash").show();
	    UserPagination('0','0','','');
		
	  }
	  
	  if(symbol!='')
	  {
		  
		   $("#currency_val").html(symbol+" "+currency);
	  }
  
	
	 
	 
	 currency1 = $("#currency_value").val();
	
	 var page_Id=val;
	
	 var content = document.getElementById("pageData2");
     content.innerHTML = content.innerHTML+'<tr class="sets"><td colspan="7"><div align="center" class="flash" ></div></td></tr>';
     $(".flash").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
	 
	 if(currency1!='')
	 {
		 var dataString = 'pageId='+ page_Id+'&cid='+<?php echo $cid; ?>+'&currency='+currency1;
	 }
     else{ 
       var dataString = 'pageId='+ page_Id+'&cid='+<?php echo $cid; ?>;
	 }
	 
	 
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_market_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){
			  
				 $(".sets").remove();
				  $(".flash").remove();
				  $(".flash3").hide();
                 $(".link a").removeClass("In-active current") ;
                 $("#"+liId+" a").addClass( "In-active current" );
				 
				 var content = document.getElementById("pageData2");
                 content.innerHTML = content.innerHTML+result;
				 document.getElementById("row_nos").value = Number(val)+1;
				 
           }
      });
}

UserPagination("0","0",'','');

</script>
<script>

  function currency_change(currency,symbol){
	
      $("#currency_value").val(currency);
      currency1 = $("#currency_value").val();	
	  
	  $("#currency_val").html(symbol+" "+currency1);  

     $(".flash3").show();
     $(".flash3").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
	 
	var page_Id='0'; 
	 
    
	 var dataString = 'pageId='+ page_Id+'&cid='+<?php echo $cid; ?>+'&currency='+currency1;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_market_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){

                 $(".flash").hide();
				 $(".flash3").hide();
                 $("#pageData2").html(result);
				 document.getElementById("row_nos").value = 1;
				 document.getElementById("page_id").value = 0;
           }
      });
}


</script>



<script>
  function UserPagination2(pageId,litId){
	  
	  
     $(".flash1").show();
     $(".flash1").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
	 $("#pageData").hide();
	 
	 var currency ='<?php echo $currency ?>';
	
	 
     var dataString = 'pageId='+ pageId+'&cid='+<?php echo $cid; ?>+'&currency='+currency;
     $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_historic_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){
			  
                 $(".flash1").hide();
                 $(".links a").removeClass("In-active current") ;
                 $("#"+litId+" a").addClass( "In-active current" );
				 $("#pageData").show();
                 $("#pageData").html(result);
				
				
           }
      });
}


UserPagination2("0","0");

</script>




<script type="text/javascript">
$(function() {

    var start = moment.utc().startOf('year');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Last 7 Days': [moment.utc().subtract(6, 'days'), moment.utc()],
           'Last 30 Days': [moment.utc().subtract(30, 'days'), moment.utc()],
           'Last 3 Months': [moment.utc().subtract(3, 'months'), moment.utc()],
           'Last 12 Months': [moment.utc().subtract(12, 'months'), moment.utc()],
           'Year To Date': [moment.utc().startOf('year'), moment.utc()],
           'All Time': ["04-28-2017", moment.utc()]
        }
    }, cb);

    cb(start, end);
	
	
	
	 $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
    //do something, like clearing an input
    var startDate = picker.startDate.format('YYYY-MM-DD');
    var endDate = picker.endDate.format('YYYY-MM-DD H:i:s')
	
	
	
	 $(".flash1").show();
     $(".flash1").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
	 $("#pageData").hide();
	
	 var currency ='<?php echo $currency ?>';
	
	 var dataString ='&cid='+<?php echo $cid; ?>+'&from='+startDate+'&to='+endDate+'&currency='+currency;
	  
       $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_historic_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){
			   

                 $(".flash1").hide();
				 
				 $("#pageData").show();
                 $("#pageData").html(result);
				 $("#pages").hide();
				 
           }
      });
   
  }); 
	
    
});
</script>

<?php if($this->detect->isMobile())
		{ ?>

<script type="text/javascript">
$(function() {

    var start = moment.utc().startOf('year');
    var end = moment();

    function cb(start, end) {
        $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange1').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Last 7 Days': [moment.utc().subtract(6, 'days'), moment.utc()],
           'Last 30 Days': [moment.utc().subtract(30, 'days'), moment.utc()],
           'Last 3 Months': [moment.utc().subtract(3, 'months'), moment.utc()],
           'Last 12 Months': [moment.utc().subtract(12, 'months'), moment.utc()],
           'Year To Date': [moment.utc().startOf('year'), moment.utc()],
           'All Time': ["04-28-2017", moment.utc()]
        }
    }, cb);

    cb(start, end);
	
	
	
	 $('#reportrange1').on('apply.daterangepicker', function(ev, picker) {
    //do something, like clearing an input
    var startDate = picker.startDate.format('YYYY-MM-DD');
    var endDate = picker.endDate.format('YYYY-MM-DD H:i:s')
	
	
	
	 $(".flash1").show();
     $(".flash1").fadeIn(400).html("<img src='<?php echo UPLOAD_URL;?>loader.gif'>");
	 $("#pageData").hide();
	
	 var dataString ='&cid='+<?php echo $cid; ?>+'&from='+startDate+'&to='+endDate;
	  
       $.ajax({
           type: "POST",
           url: '<?php echo base_url('Market_ajax_list/load_historic_lists');?>',
		   data: dataString,
           cache: false,
           success: function(result){
			   

                 $(".flash1").hide();
				 
				 $("#pageData").show();
                 $("#pageData").html(result);
				 $("#pages").hide();
				 
           }
      });
	
	
	
   
  }); 
	
    
});
</script>

		<?php } ?>



		
		