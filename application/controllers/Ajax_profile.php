<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '-1');
class Ajax_profile extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->detect = new Mobile_Detect();
    }
	
	function updaterow()
	{
		$id = $this->input->post('id');
		$currency = $this->input->post('currency');
		$prev_price = $this->input->post('prev_price');
		
		
		
		if($currency=="")
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "USD";
			}
		}
		
		$row ="";
		$row['price'] = $this->boost_model->getValue("ci_coin_history_temp_".$currency,"price","coin_id='".$id."' order by id desc");
		if($row['price']===$prev_price)
		{
			$row['price_change'] = "No";
		}
		else
		{
			$row['price_change'] = "Yes";
			
		}
		$row['change24hour'] = $this->boost_model->getValue("ci_coin_history_temp_".$currency,"change24hour","coin_id='".$id."' order by id desc");
		$row['volume24hour'] = $this->boost_model->getValue("ci_coin_history_temp_".$currency,"volume24hour","coin_id='".$id."' order by id desc");
		$row['mktcap'] = $this->boost_model->getValue("ci_coin_history_temp_".$currency,"mktcap","coin_id='".$id."' order by id desc");
		
		$row['price'] = number_format($row['price'],2);
		//$row['volume24hour'] = number_format($row['volume24hour'],2);
		$row['mktcap'] = number_format($row['mktcap'],2);
		$row['change24hour'] = number_format($row['change24hour'],2);
		$row['volume24hour'] = ($row['change24hour'] / 100) * $row['price'];
		echo json_encode($row);
		exit;
	}
	function load_data()
	{
		$time_start = microtime(true);
		$data = "";
		define('PAGE_PER_NO',8); 
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }

		 if(isset($_POST['per_page']))
		 {
			
			 if($_POST['per_page']>0)
			 {
				  $per_page = $_POST['per_page'];
			 }
			 else
			 {
				 $per_page = $this->settings->settings_records_per_page_front;
			 }
		 }
		 else
		 {
			 $per_page = $this->settings->settings_records_per_page_front;
		 }
        $pageLimit= $per_page*$id;

		$ar = "";
		$ar1="";
		if(isset($_POST['e1']))
		{
			$ar1 = explode(",",$_POST['e1']);
			foreach($ar1 as $key=>$value)
			{
				$ar[$key] =$value;
			}
		}
		$this->db->select('*');
		$this->db->from(COLUMN);
		$this->db->where("status","1");
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row2)
			{
				$row_columns[] = $row2;
			}
		}
	
	if($this->input->post('currency') && $this->input->post('currency')!="undefined")
		{
			$currency = $this->input->post('currency');
		}
		else
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
				//echo "df";
				
			}
			else
			{
				$currency = "";
			}
			
			if($currency=="")
			{
			$currency = "USD";
			}
		}
		if($this->session->userdata('user_id'))
			{
				
				$update_array = array("base_currency"=>$currency);
				$this->db->set($update_array)->where("id",$this->session->userdata('user_id'))->update(USER);
			}
			
			$sorting_option = $this->input->post('sorting');
			
			
			if($this->input->post('sorting_price'))
			{
				$sorting_price = $this->input->post('sorting_price');
			}
			else
			{
				$sorting_price = "DESC";
			}
			
			
			if($this->input->post('sorting_market'))
			{
				$sorting_market = $this->input->post('sorting_market');
			}
			else
			{
				$sorting_market = "DESC";
			}
			
		
		 $sorting_change = $this->input->post('sorting_change');
		
			if(isset($sorting_change))
			{
				
			}
			else
			{
				 $sorting_change ="24H";
			}
			
			if($sorting_option=="price")
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".price AS DECIMAL(38,10)) ".$sorting_price;
			}
			elseif($sorting_option=="marketcap")
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) ".$sorting_market;
			}
			elseif($sorting_option=="volume")
			{
				//$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".changepct24hour AS DECIMAL(38,10)) desc";
				
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) desc";
			}
			else
			{
				$sorting = "ORDER BY CAST(ci_coin_history_temp_".$currency.".mktcap AS DECIMAL(38,10)) desc";
			}

			$search_text = $this->input->post('search');
			
			if(trim($search_text)!="")
			{
				$ser = COIN.".coinname LIKE '".$this->input->post('search')."%' or ".COIN.".name LIKE '".$this->input->post('search')."%'";
			}
			else
			{
				$ser = COIN.".status='1'";
			}
if($this->input->post('watchname')){
    $sql = "SELECT * FROM ci_watchlist WHERE watchlist_name='".$this->input->post('watchname')."'";
    $query =$this->db->query($sql);
    $rowwatch =  $query->result_array();
    $watchnameQuery = "AND ci_watchlistassets.watch_nameid='".$rowwatch[0]['watchlist_id']."'";
    $watchlistQuery = "AND watchlist_name='".$this->input->post('watchname')."'";
}else{
    $watchnameQuery = '';
    $watchlistQuery = '';
}
		$sql = "SELECT *
FROM ci_coin
LEFT JOIN ci_coin_history_temp_".$currency." ON ci_coin.id = ci_coin_history_temp_".$currency.".coin_id
LEFT JOIN ci_watchlistassets ON ci_coin.id = ci_watchlistassets.watch_coinid
 where   $ser and ci_coin_history_temp_".$currency.".price!='0.00' AND ci_watchlistassets.watch_userid='".$this->session->userdata('user_id')."' $watchnameQuery GROUP BY ci_watchlistassets.watch_coinid $sorting limit $pageLimit,$per_page";
		
		$query =$this->db->query($sql);
			$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
				if($sorting_change=="24H")
				{
					$hour = 24;
				}
				elseif($sorting_change=="1H")
				{
					$hour = 1;
				}
				elseif($sorting_change=="1W")
				{
					$hour = 168;
				}
				elseif($sorting_change=="1M")
				{
					$hour = 720;
				}
				elseif($sorting_change=="3M")
				{
					$hour = 2160;
				}
				elseif($sorting_change=="ALL")
				{
					$hour = 8760;
				}
				else
				{
					$hour = 24;
				}
	$meeting_time1 = date('Y-m-d H:i:s', time() - 60 * 60 * $hour);
	$meeting_time1_string = strtotime($meeting_time1);
		if ($query->num_rows() > 0)
		{
			$i = $pageLimit+1;
            foreach ($query->result_array() as $row)
			{
				if($sorting_option=="marketcap" && $sorting_market=="DESC")
				{
				$update_data = array("sortorder"=>$i);
				$this->db->set($update_data)->where("id",$row['coin_id'])->update(COIN);
				 $row['sortorder'] = $i;
				}
				if($sorting_change!="24H" && $sorting_change!="")
				{
//$old_price = $this->boost_model->getValue2("ci_coin_history_".$currency,"price","coin_id='".$row['coin_id']."' and created_time < '$meeting_time1' ORDER by id desc limit 1");
	 $Data2 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym='.$row['name'].'&tsyms='.$currency.'&ts='.$meeting_time1_string),true);
	$old_price = $Data2[$row['name']][$currency];
	 $newprice = $row['price'] - $old_price;
	 $change24 = $newprice / $row['price'];
	 $change24 = number_format($change24 * 100,2);
	$calc = ($change24 / 100);
		        $row['volume24hour'] = $calc * $row['price'];
		        $row['change24hour'] = $change24; 
				}
				else
				{
				
				 $row['volume24hour'] = $row['change24hour'];
				 $row['change24hour'] = $row['changepct24hour'];
				}
                $data[] = $row; 
				
				$i++;
            }
          
        }
		if (is_array($data))
		{
            foreach ($data as $row)
			{
				$list = $row;
				$list['id'] = $list['coin_id'];
				
				$exist = $this->boost_model->getValue("ci_watchlistassets","count(*)","watch_userid='".$this->session->userdata('user_id')."' AND watch_coinid='".$list['id']."'");
				if($exist>0)
				{
					$heartcls = "";
				}
				else
				{
					$heartcls = "-o";
				}
				
				if($list['change24hour']>0)
							{
							$changecolor='up';
							$minus='+';
							}
							else
							{
								$changecolor='down';
								$minus='-';
							}
			
					$url = base_url().$list['name'];
					$url = "'".$url."'";
					if($sorting_change=="24H")
    				{
    					$highprice = $list['high24hour'];
    					$lowprice = $list['low24hour'];
    				}
    				elseif($sorting_change=="1H")
    				{
    					$highprice = $list['high1hour'];
    					$lowprice = $list['low1hour'];
    				}
    				elseif($sorting_change=="1W")
    				{
    					$highprice = $list['high1week'];
    					$lowprice = $list['low1week'];
    				}
    				elseif($sorting_change=="1M")
    				{
    					$highprice = $list['high1month'];
    					$lowprice = $list['low1month'];
    				}
    				elseif($sorting_change=="3M")
    				{
    					$highprice = $list['high3months'];
    					$lowprice = $list['low3months'];
    					
    				}elseif($sorting_change=="6M")
    				{
    					$highprice = $list['high6months'];
    					$lowprice = $list['low6months'];
    				}
    				elseif($sorting_change=="12M")
    				{
    					$highprice = $list['high1year'];
    					$lowprice = $list['low1year'];
    				}
    				elseif($sorting_change=="ALL")
    				{
    					$highprice = $list['allhigh'];
    					$lowprice = $list['alllow'];
    				}
    				else
    				{
    					$highprice = $list['high24hour'];
    					$lowprice = $list['low24hour'];
    				}
					echo '<section class="coin_list" id="'.$list['id'].'">
				<section class="container coin_group desktop-marg" >
					<div id="c_'.$list['id'].'" class="row coin_dist"  >
					<!--<a class="coin_fav" onclick="removeFavouriteid('.$list['id'].')" id="heart_'.$list['id'].'" href="javascript:;">-->
						<div class="col-2 col-md-1 bg_alg4 watch_heart" id="'.$list['id'].'" style="position:relative;display:inline-block;cursor:pointer;">
                        	 
                    	        <i class="fa fa-heart'.$heartcls.' heart_cor" aria-hidden="true"></i>
                    	   
                    	    <div class="watch_tool">
                            	<div class="watch_tooltips">
                            		<span>
                            			<a class="watch_hover" href="javascrpt:;">
                            				<ul>
                            					<li><i class="fa fa-heart heart_cor" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                            					<li>On hold (15 Crypto assets)</li>
                            				</ul>
                            			</a>';
                            			if($this->input->post('watchname')){
                            			    $watchlist_sql = "SELECT * FROM ci_watchlist WHERE user_id='".$this->session->userdata('user_id')."' AND watchlist_name='".$this->input->post('watchname')."' ORDER BY watchlist_id DESC";
    		                                $watchlist_query =$this->db->query($watchlist_sql);
    		                                if ($watchlist_query->num_rows() > 0)
                                        	{
                                        	    foreach ($watchlist_query->result_array() as $row)
        		                                {
        		                                    
                                        			echo '<a class="watch_hover" href="'.base_url().'profile/watchlist">
                                            				<ul>
                                            					<li><i class="fa fa-plus" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                                            					<li>Create new watch list</li>
                                            				</ul>
                                            			</a>
                                        			
                                        			<a class="watch_hover" id="'.$list['id'].'" rev="'.$row['watchlist_id'].'"  href="javascrpt:;" onclick="removeFromlist('.$list['id'].');">
                                        				<ul style="border:1px solid #ccc;">
                                        					<li><i class="fa fa-trash" aria-hidden="true"  style="color:#753ac8;font-weight:bold;"></i></li>
                                        					<li>Remove from the '.$row['watchlist_name'].'</li>
                                        				</ul>
                                        			</a>';
        	                                    }
                                        	}
                            			}else{
                            			$watchlist_sql = "SELECT * FROM ci_watchlist WHERE user_id='".$this->session->userdata('user_id')."' ORDER BY watchlist_id DESC";
		                                $watchlist_query =$this->db->query($watchlist_sql);
		                                if ($watchlist_query->num_rows() > 0)
                                    	{
                                    	    foreach ($watchlist_query->result_array() as $row)
    		                                {
    		                                    $watch_sql = "SELECT * FROM ci_watchlistassets WHERE watch_nameid ='".$row['watchlist_id']."' AND watch_userid='".$this->session->userdata('user_id')."'";
        		                                $watch_query =$this->db->query($watch_sql);
                                    			echo '<a class="watch_hover watchlist" id="'.$list['id'].'" rev="'.$row['watchlist_id'].'"  href="javascrpt:;">
                                    				<ul>
                                    					<li><i class="fa fa-heart-o heart_cor watch'.$row['watchlist_id'].'" aria-hidden="true"  style="color:#753ac8;font-weight:bold;"></i></li>
                                    					<li>'.$row['watchlist_name'].' ('.$watch_query->num_rows().' Crypto assets)</li>
                                    				</ul>
                                    			</a>';
    	                                    }
                                    	}
                            			}
                    				if($this->input->post('watchname')==''){
                            		echo '<a class="watch_hover" href="'.base_url().'profile/watchlist">
                            				<ul>
                            					<li><i class="fa fa-plus" aria-hidden="true" style="color:#753ac8;font-weight:bold;"></i></li>
                            					<li>Create new watch list</li>
                            				</ul>
                            			</a>
                            			<a class="watch_hover" href="javascrpt:;" id="removeFromlist_'.$list['id'].'" onclick="removeprofileFromlist('.$list['id'].');">
                            				<ul style="border:1px solid #ccc;">
                            					<li><i class="fa fa-trash" aria-hidden="true" style="color:gray;font-weight:bold;"></i></li>
                            					<li>Remove from the list</li>
                            				</ul>
                            			</a>';
                            			}
                            		echo '</span>
                            	</div>
                            </div>
                        </div> <!--</a>-->
						<div style="cursor:pointer;" onclick="gotocoin('.$url.');" class="col-4 col-md-3 coin_merge bg_alg1"><span class="coin_count">'.$list['sortorder'].'</span><span class="coin_img"><a href="'.base_url().$list['name'].'"><img src="'.COIN_URL.$list['image'].'" alt="'.$list['name'].'"></a></span><span class="coin_name">'.$list['coinname'].'<br>
						<a href="'.base_url().$list['name'].'"><span class="coin_sh coin_cap">'.$list['name'].'</span></a></span></div>
                        <div style="cursor:pointer;" onclick="gotocoin('.$url.');" class="col-3 col-md-3 coin_cap bg_alg3"><span><i class="fa fa-'.strtolower($currency).'" aria-hidden="true"></i>&nbsp;<span id="beat'.$list['id'].'market">'.number_format($list['mktcap'],0).'</span></span></div>
						<div style="cursor:pointer;" onclick="gotocoin('.$url.');" class="col-3 col-md-2 bg_alg2"><span><i class="fa fa-'.strtolower($currency).'" aria-hidden="true"></i>&nbsp;<span id="beat'.$list['id'].'price">'.number_format($list['price'],4).'</span></span></div>
						<div class="col-2 col-md-2 bg_alg4" style="max-width:12%">
							<div style="cursor:pointer;" onclick="gotocoin('.$url.');" id="changecolor'.$list['id'].'" class="text-left tooltip1 coin_hr c_'.$changecolor.'">
								<span style="z-index:1018 !important" id="beat'.$list['id'].'minus2">'.$minus.'</span><span style="font-size:14px;z-index:1018 !important;" id="beat'.$list['id'].'change">'.number_format(abs($list['change24hour']),2).'</span><span style="font-size:14px;z-index:1018 !important;">%</span> 
								<span class="toolhover_cls"> <b><span id="beat'.$list['id'].'minus">'.$minus.'</span></b> <b>'.$currency_symbol.'&nbsp;<span id="beat'.$list['id'].'hour"> '.number_format(abs($list['volume24hour']),2).'</span></b> </span>
							</div>
						</div>
						<div style="cursor:pointer;max-width: 11%;" onclick="gotocoin('.$url.');" class="col-2 col-md-2 bg_alg4" style="max-width: 12%;">
							<span><i class="fa fa-usd" aria-hidden="true"></i>&nbsp;<span "beath'.$list['id'].'price">'.number_format($highprice,2).'</span></span>
				    	</div>
				    	<div style="cursor:pointer;max-width: 12%;" onclick="gotocoin('.$url.');" class="col-2 col-md-2 bg_alg4" style="max-width: 12%;">
							<span><i class="fa fa-usd" aria-hidden="true"></i>&nbsp;<span "beatl'.$list['id'].'price">'.number_format($lowprice,2).'</span></span>
				    	</div>
					</div>
				</section>
			</section>';
				
				
                
            }
            
        }
		else
		{
			echo '<section class="container" >
				        <section class="row">
        					<section class="col-sm-12">
        					    <section style="background: #FFEB3B;padding: 2% 0;margin: 2% auto;width:85%">
        						    <section style="text-align: center;font-size: 12px;font-weight: bold;"><i class="fa fa-heart-o heart_cor" aria-hidden="true"></i>&nbsp; No crypto assets on your watch list, search and add</section>
        						</section>
        					</section>
    					</section>
			        </section>';
		}
		

			
			$sql = "SELECT *
FROM ci_coin
LEFT JOIN ci_coin_history_temp_".$currency." ON ci_coin.id = ci_coin_history_temp_".$currency.".coin_id
LEFT JOIN ci_portfolio ON ci_coin.id = ci_portfolio.portfolio_coin_id
 where   $ser and ci_coin_history_temp_".$currency.".price!='0.00' AND ci_portfolio.portfolio_user_id='".$this->session->userdata('user_id')."' $sorting";
		
		//echo $sql;
		$query =$this->db->query($sql);
		
		$paginationCount = $this->getPagination($query->num_rows(),$per_page);
		
		//echo $_POST['pageId'];
		
		$prev_id = $_POST['pageId']-1;
		$next_id = $_POST['pageId']+1;
		
		echo "<script>document.getElementById(\"coins_count\").value = ".$query->num_rows().";document.getElementById(\"coins_count1\").innerHTML  = ".$query->num_rows().";</script>";
		
	
		$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;

exit();
}


function load_portfolio()
{
	    $currency = $this->input->post('currency');
		
	    if($currency=="")
		{
			$currency = "USD";
		}
		
		$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }
		 
		  $pageLimit= $this->settings->settings_records_per_page_front*$id;
		
		echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
			
			$this->db->select('*');
			$this->db->from(PORTFOLIO);
			$this->db->where("portfolio_user_id",$this->session->userdata('user_id'));
			$this->db->limit($this->settings->settings_records_per_page_front,$pageLimit);
			$query = $this->db->get();
			//echo $this->db->last_query();
			$coinrowscount = $query->num_rows();
			if($query->num_rows()>0)
			{
				foreach($query->result_array() as $row)
				{
					
				$row['price'] = $this->boost_model->getValue(COIN_HISTORY_TEMP,$currency,"coin_id='".$row['portfolio_coin_id']."' AND column_id='1' order by id desc");
				$row['change24hour'] = $this->boost_model->getValue(COIN_HISTORY_TEMP,$currency,"coin_id='".$row['portfolio_coin_id']."' AND column_id='12' order by id desc");
				$query = $this->db->select('image,coinname,name')->from(COIN)->where("id",$row['portfolio_coin_id'])->get();
				if($query->num_rows()>0)
				{
					$row_coin = $query->result_array();
				}
				
				if($row['change24hour']>0)
				{
					$mobile24hourclass = "mobile-second9";
				}
				else
				{
					$mobile24hourclass = "mobile-second8";
				}
				
			    
				echo '<div class="panel panel-default mar-top">
				      <div class="panel-heading" role="tab" id="heading'.$row['portfolio_id'].'">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$row['portfolio_id'].'" aria-expanded="true" aria-controls="collapse'.$row['portfolio_id'].'">
							<i class="more-less glyphicon glyphicon-plus icon-align"></i>
							<div class="row desktop">
								<div class="col-xs-2 col-md-1 text-center mob_res2">
									<img width="54" src="'.COIN_URL.$row_coin[0]['image'].'" alt="bootsnipp" class="img-rounded img-responsive img_width"/>
								</div>
								<div class="col-xs-3 col-md-3 text-center pad-top">
									<div class="text-col3"><a href="#" target="_blank">'.$row_coin[0]['coinname'].' ('.$row_coin[0]['name'].')</a>&nbsp;<span class="glyphicon glyphicon-heart"></span></div>
									<div class="mobile-second"><span class="mobile-second7">'.$currency_symbol.' '.$row['price'].'</span><span class="'.$mobile24hourclass.'">'.$row['change24hour'].'%</span></div>
								</div>
								<div class="col-xs-2 col-md-2 text-center pad-top">
									<div class="text-col4">0.2352 '.$row_coin[0]['name'].'</div>
									<div class="text-col11">Total Coin</div>
								</div>
								<div class="col-xs-2 col-md-2 text-center pad-top">
								   <div class="text-col4">£ 620.30 <span class="bott">+ 1.06%</span></div>
									<div class="text-col11">Wallet Balance</div>
								</div>
								<div class="col-xs-3 col-md-2 text-center pad-top1">
								   <div class="text-col12">Add Balance +</div>
								</div>
							</div>
							
							<!-- mobile view start -->
								<div class="row mobile">
									<div class="row">
										<div class="col-xs-3 col-md-1 text-center mob_res4">
											<img src="'.COIN_URL.$row_coin[0]['image'].'" alt="bootsnipp" class="img-rounded img-responsive img_width"/>
										</div>
										<div class="col-xs-6 col-md-3 text-center pad-top">
											<div class="text-col3"><a href="#" target="_blank">'.$row_coin[0]['coinname'].' ('.$row_coin[0]['name'].')</a>&nbsp;<span class="glyphicon glyphicon-heart"></span></div>
											<div class="mobile-second top-10"><span class="mobile-second7">'.$currency_symbol.' '.$row['price'].'</span><span class="'.$mobile24hourclass.'">'.$row['change24hour'].'%</span></div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-2 text-center pad-top">
											<div class="col-xs-1">
											</div>
											<div class="col-xs-5 left">
												<div class="text-col11 brown">Total Coin</div>
											</div>
											<div class="col-xs-5 right">
												<div class="text-col4">0.2352 BTC</div>
											</div>
											<div class="col-xs-1">
											</div>
										</div>
									</div><div class="row">	
										<div class="col-xs-12 col-md-2 text-center pad-top">
											<div class="col-xs-1">
											</div>
											<div class="col-xs-5 left top-10 brown">
												<div class="text-col11">Wallet Balance</div>
											</div>
											<div class="col-xs-5 right top-10">
												<div class="text-col4">£ 620.30 <span class="bott">+ 1.06%</span></div>
											</div>
											<div class="col-xs-1">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-2 text-center pad-top1">
											<div class="col-xs-1">
											</div>
											<div class="col-xs-5 left top-10 brown border-top">
												<div class="text-col12 top-bot-10">Add Balance</div>
											</div>
											<div class="col-xs-5 right top-10 border-top">
												<div class="text-col13 top-bot-10">+</div>
											</div>
											<div class="col-xs-1">
											</div>
										</div>
									</div>
								</div>
							<!-- mobile view end -->
						</a>
					</h4>
				</div>
				<div id="collapse'.$row['portfolio_id'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$row['portfolio_id'].'">
					<div class="panel-body">
					  <div id="details_'.$row['portfolio_coin_id'].'" class="row col-md-12 custyle table-responsive desktop">
					  
					  <span class="flash_'.$row['portfolio_coin_id'].'"></span>';
					  
					  $this->db->select('*');
					  $this->db->from(PORTFOLIO_DETAILS);
					  $this->db->where("portfolio_details_portfolio_id",$row['portfolio_id']);
					  $query = $this->db->get();
					  if($query->num_rows()>0)
					  {
					  
						echo '<table class="table table-striped custab">
							<thead class="table-info">
								<tr>
									<th>#</th>
									<th>Date</th>
									<th>Coin brought</th>
									<th>Exchange type</th>
									<th>Amount spent</th>
									<th></th>
								</tr>
							</thead>';
							$i=1;
							foreach($query->result_array() as $row_details)
							{
							$coinname = $this->boost_model->getValue(COIN,"coinname","id='".$row_details['portfolio_details_coin_id']."'");
																
							echo '<tr>
								<td>'.$i.'</td>
								<td>'.$row_details['portfolio_details_date'].'</td>
								<td>'.$row_details['portfolio_details_coin_brought'].' '.$coinname.'</td>
								<td>'.$coinname.' / '.$row_details['portfolio_details_exchange_type'].'</td>
								<td>'.$row_details['portfolio_details_amount'].' '.$row_details['portfolio_details_exchange_type'].'</td>
								<td class="text-center"><a onclick="get_edit_section('.$row_details['portfolio_details_id'].')" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"> Edit &nbsp;<span class="glyphicon glyphicon-edit"></span></a></td>
							</tr>';
							$i++;
							}
						
						echo '</table>';
						
					  }
						
					  echo '</div>
					  
					  <!-- Mobile view start -->
					  
					   <div class="row col-md-12 custyle mobile">
							<table class="table table-striped custab">
								<thead class="table-info">
									<tr>
										<th>Coin</th>
										<th>Exchange</th>
										<th>Amount</th>
										<th></th>
									</tr>
								</thead>
								<tr>
									<td>0.224 BTC</td>
									<td>BTC / USD </td>
									<td>500 USD</td>
									<td class="text-center"><a class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"> Edit &nbsp;<span class="glyphicon glyphicon-edit"></span></a></td>
								</tr>
								<tr>
									<td>0.0112 BTC</td>
									<td>BTC / LTC</td>
									<td>25.6 LTC</td>
									<td class="text-center"><a class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"> Edit &nbsp;<span class="glyphicon glyphicon-edit"></span></a></td>
								</tr>
							</table>
						</div>					  
					  
					  <!-- Mobile view end -->
					  
					  <div class="row form-alg">
                            <form>
                                <div class="col-lg-4 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Amount of COIN bought</label>
										
                                        <input id="bought_'.$row['portfolio_coin_id'].'" name="text" type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Exchange Type</label>
                                        <select id="exchange_'.$row['portfolio_coin_id'].'" name="text" class="form-control">';
										if(is_array($this->settings->currency_list))
										{
											foreach($this->settings->currency_list as $currency_list)
											{
												echo '<option value="'.$currency_list->currency_code.'">'.$row_coin[0]['name'].' / '.$currency_list->currency_code.'</option>';
											}
										}
											
											
										echo '</select>
                                    </div>
                                </div>
								<div class="col-lg-4 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Amount Spent</label>
                                        <input id="amount_'.$row['portfolio_coin_id'].'" name="text" type="text" class="form-control">
                                    </div>
                                </div>
								
                                <!--<div class="col-lg-2 col-md-12 col-sm-6 col-xs-4 desktop">
                                    <button type="button" onclick="savecoin('.$row['portfolio_coin_id'].','.$row['portfolio_id'].')" class="btn btn-success btn-block">Save Coin</button>
                                </div>-->
								<div class="col-lg-3 col-md-12 col-sm-6 col-xs-6 desktop">
                                    <button type="button" onclick="savecoin('.$row['portfolio_coin_id'].','.$row['portfolio_id'].')" class="btn btn-success btn-block">Save & Add another coin</button>
                                </div>
								<div class="col-lg-4 col-md-12 col-sm-6 col-xs-2 can-alg desktop">
                                    <a href="#" class="forgot-password">Cancel</a>
                                </div>
								
								<!-- Mobile view start -->
								<div class="row">
									<div class="col-lg-2 col-md-12 col-sm-6 col-xs-12 mobile">
										<div class="col-xs-6 bottom-5">
											<button class="btn btn-success btn-block">Save Coin</button>
										</div>
										<div class="col-xs-6">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3 col-md-12 col-sm-6 col-xs-12 mobile">
										<div class="col-xs-8 bottom-5">
											<button class="btn btn-success btn-block">Save & Add another coin</button>
										</div>
										<div class="col-xs-4">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-md-12 col-sm-6 col-xs-12 can-alg mobile">
										<div class="col-xs-4">
											<a href="#" class="forgot-password">Cancel</a>
										</div>
										<div class="col-xs-8">
										</div>
									</div>
								</div>
								
								<!-- Mobile view end -->
								
                            </form>
                        </div>
					</div>
				</div>
			</div>';
			}
			}


			echo '</div>';
			
			$this->db->select('*');
			$this->db->from(PORTFOLIO);
			$this->db->where("portfolio_user_id",$this->session->userdata('user_id'));
			$query = $this->db->get();
			
			$paginationCount = $this->getPagination($query->num_rows(),$this->settings->settings_records_per_page_front);
			
			if(is_array($coinrowscount)){ 
 $content ="";
$content .='<ul class="tsc_pagination tsc_paginationC tsc_paginationC01 pagina">
    <li class="first link" id="first">
        <a  href="javascript:void(0)" onclick="changePagination(\'0\',\'first\')">F i r s t</a>
    </li>';
    for($i=0;$i<$paginationCount;$i++){
 
        $content .='<li id="'.$i.'_no" class="link">
          <a  href="javascript:void(0)" onclick="changePagination(\''.$i.'\',\''.$i.'_no\')">
              '.($i+1).'
          </a>
    </li>';
    }
 
    $content .='<li class="last link" id="last">
         <a href="javascript:void(0)" onclick="changePagination(\''.($paginationCount-1).'\',\'last\')">L a s t</a>
    </li>
    
</ul>';

echo $content;
		}
			
			
			
			echo '<div class="row form-alg desktop">
					<form>
						<div class="col-lg-4 col-md-12 col-sm-6 col-xs-2">
						</div>
						<div class="col-lg-4 col-md-12 col-sm-6 col-xs-8 text-center">
							<div class="form-group">
								<label class="control-label required" for="email">No balance add to display</label>
								<button class="btn btn-primary btn-block but-red"><span class="butt-siz">+ Add balance</span></button>
							</div>
						</div>
						<div class="col-lg-4 col-md-12 col-sm-6 col-xs-2">
						</div>
					</form>
				</div>
		</div>';
		exit;
}


function load_portfolio_heading()
{
	echo '<div class="col-md-12 desktop">
            <div class="well3 well-sm">
                <div class="row">
					<div class="col-xs-4 col-md-4 text-center set bod-right">
						<div class="text-col8">Your Total Investment</div>
						<div class="text-col9">£ 1019.99</div>
                    </div>
					<div class="col-xs-4 col-md-4 text-center set bod-right">
						<div class="text-col8">Your Total Balance</div>
						<div class="text-col9">£ 1280.45</div>
                    </div>
					<div class="col-xs-4 col-md-4 text-center set">
						<div class="text-col8">Your Total Profit</div>
						<div class="text-col10">+ £ 260.46 <span class="bott">15.42%</span></div>
                    </div>
				</div>
            </div>
        </div>
		
		
		<!-- Mobile View Start-->
		<div class="col-md-12 mobile">
            <div class="well3 well-sm">
                <div class="row">
					
					<div class="col-xs-6 col-md-4 text-center set bod-right">
						<div class="text-col8">Your Total Investment</div>
						<div class="text-col9">£ 1019.99</div>
                    </div>
					<div class="col-xs-6 col-md-4 text-center set">
						<div class="text-col8">Your Total Balance</div>
						<div class="text-col9">£ 1280.45</div>
                    </div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4 text-center set">
						<div class="text-col8">Your Total Profit</div>
						<div class="text-col10">+ £ 260.46 <span class="bott">15.42%</span></div>
                    </div>
					
                </div>
            </div>
        </div>';
		exit;
}

 function getPagination($count,$per_page){
      $paginationCount= floor($count / $per_page);
      $paginationModCount= $count % $per_page;
      if(!empty($paginationModCount)){
               $paginationCount++;
      }
      return $paginationCount;
     }
	public function saveWatchlistname()
	{
	    $name    = $this->input->post('name');
	    $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'watchlist_name' => $name,
                'watchlist_date' => date("Y-m-d")
            );
        $id = $this->home_model->insertWatchlistname($data);
        echo $id;
	}
	public function watchlist_data()
	{
	    $coinId    = $this->input->post('coinId');
	    $watchId   = $this->input->post('watchId');
	    $data = array(
                'watch_userid' => $this->session->userdata('user_id'),
                'watch_nameid' => $watchId,
                'watch_coinid' => $coinId,
                'watch_date' => date("Y-m-d")
            );
        $id = $this->home_model->insertwatchlist_data($data);
        echo $id;
	}
	public function favdata()
	{
	    $coinId    = $this->input->post('coinId');
	    $userId    = $this->session->userdata('user_id');
        $data      = $this->home_model->getfavdata($coinId,$userId);
        echo json_encode($data);
	}
	public function removefromlist()
	{
	    $coinId    = $this->input->post('coinId');
	    $userId    = $this->input->post('userId');
	    $watchname = $this->input->post('watchName');
        $data      = $this->home_model->deletewatchlist($coinId,$userId,$watchname);
        echo $data;
	}
	public function removefromhomelist()
	{
	    $coinId    = $this->input->post('coinId');
	    $userId    = $this->input->post('userId');
        $data      = $this->home_model->deletehomewatchlist($coinId,$userId);
        echo $data;
	}
	public function saveWallet()
	{
	    $addresstype    = $this->input->post('addresstype');
	    $walletname     = $this->input->post('walletname');
	    $eaddress       = $this->input->post('eaddress');
	    $data = array(
                'address_type' => $addresstype,
                'wallet_name' => $walletname,
                'ethereum_address' => $eaddress,
                'wallet_userid' => $this->session->userdata('user_id')
            );
        $id = $this->home_model->insertWalletaddress($data);
        echo $id;
	}
}


