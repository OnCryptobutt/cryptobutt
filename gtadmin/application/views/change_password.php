<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
		
			<?php if($success) echo $success;?>
			
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="form-horizontal">
			<input type="hidden" name="act" value="edit">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Old Password</label>

                  <div class="col-sm-10">
                    <input type="password" name="old_password" class="form-control" id="inputEmail3" placeholder="Old Password" value="<?php echo set_value('old_password');?>">
					<span class="red"><?php echo form_error('old_password');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">New Password</label>

                  <div class="col-sm-10">
                    <input type="password" name="new_password" class="form-control" id="inputEmail3" placeholder="New Password" value="<?php echo set_value('new_password');?>">
					<span class="red"><?php echo form_error('new_password');?></span>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Confirm Password</label>

                  <div class="col-sm-10">
                    <input type="password" name="confirm_password" class="form-control" id="inputEmail3" placeholder="Confirm Password" value="<?php echo set_value('confirm_password');?>">
					<span class="red"><?php echo form_error('confirm_password');?></span>
                  </div>
                </div>
                
                
				
				
              </div>
			   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				
              </div>
            
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  