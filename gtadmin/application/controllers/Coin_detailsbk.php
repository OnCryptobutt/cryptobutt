<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coin_details extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->load->model('coin_details_model');
		$this->settings = $this->boost_model->loadSettings();
    }

	function index()
	{
		
		//header('Content-type: application/json');
		$success ="";
		
		$coins_parmeters = $this->coin_details_model->getParameter();
		
		
		$cron_details = $this->coin_details_model->getCronDetails();
		
		$columns = $this->coin_details_model->getColumns();
		
		$coins_lists = $this->coin_details_model->getCoinList($cron_details);
		
		
		
		if(is_array($coins_lists))
		{
			
			foreach($coins_lists as $coins_list)
			{
				foreach($coins_parmeters as $coins_parmeter)
				{
		$Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='.$coins_list->name.'&tsyms='.$coins_parmeter),true);
		//echo 'https://min-api.cryptocompare.com/data/pricemultifull?fsyms='.$coins_list->name.'&tsyms='.$coins_parmeter;
		
		$currency_array = explode(",",$coins_parmeter);
		
		
		
		        if(is_array($columns))
				{
					foreach($columns as $column)
					{
						
						$exist_id = $this->boost_model->getValue(CRON,"coin_history_id","coin_id='".$coins_list->id."' AND column_id='".$column->id."'");
						
						if($exist_id)
						{
							$history_id = $exist_id;
							
						}
						else
						{
							$exist_id = $this->boost_model->getValue(COIN_HISTORY,"id","coin_id='".$coins_list->id."' AND column_id='".$column->id."'");
							if($exist_id)
							{
							$history_id = $exist_id;
							
							}
							else
							{
						
						$insert_array = array("coin_id"=>$coins_list->id,
						"name"=>$coins_list->name,
						"column_id"=>$column->id,
						"created_time"=>NOW
						);
						$this->db->set($insert_array)->insert(COIN_HISTORY);
						$history_id = $this->db->insert_id();
							}
						}
						
						$update_array = array("coin_history_id"=>$history_id,
						"coin_name"=>$coins_list->name,
						"coin_id"=>$coins_list->id,
						"column_id"=>$column->id,
						"updated_time"=>NOW
						);
						$this->db->set($update_array)->where("id",$cron_details->id)->update(CRON); 
						
						foreach($currency_array as $currency)
						{
							if($currency!=$coins_list->name)
							{
							$det = $Data['RAW'][$coins_list->name][$currency];
							$api_date_time = date("Y-m-d H:i:s",$det['LASTUPDATE']);
							$update_data = array($currency=>$det[strtoupper($column->name)],
							"api_updated_time"=>$det['LASTUPDATE'],
							"api_updated_date_time"=>$api_date_time,
							"created_time"=>NOW);
							$this->db->set($update_data)->where("id",$history_id)->update(COIN_HISTORY);
							}
						}
					}
				}
		       
		
				}
				
				
				echo "<font color='red'>".date("d M Y h:i A")."</font>&nbsp;|&nbsp;<font color='green'>".$coins_list->name." Coin has updated</font><br/>";
				
		    }
			
			
			$update_data = array("status"=>"COMPLETED",
			"updated_time"=>NOW);
			$this->db->set($update_data)->update(CRON);
			echo "<br/><font color='red'>All Coins has updated</font><br/>";
		
		}
		
		
	
	
	}
}
