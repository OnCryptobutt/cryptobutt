<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot_password extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	
	 */
	 
	 function __construct()
    {
		parent::__construct();
		$this->load->model('forgot_password_model');
		$this->load->model('boost_model');
		$this->settings = $this->boost_model->loadSettings();
    }
	
	public function validate_captcha()
	{
		if($this->input->post('captcha')!="")
		{
			if($this->input->post('captcha') != $this->session->userdata['captchaWord'])
			{
				$this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
				return false;
			}else{
				return true;
			}

		}
		else
		{
			$this->form_validation->set_message('validate_captcha', 'The Captcha field is required.');
			return false;
		}
	
	}
	
	public function index()
	{
		$data['title']    = "Forgot Password";
		
		if($this->session->flashdata('success'))
		{
			$data['success']  = $this->boost_model->showNotify("success",$this->session->flashdata('success'));
		}
		
		
		if($this->input->post('act')=="forgot")
		{
			$this->form_validation->set_rules('email','Email','required|valid_email');
			 $this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha');
			
			if($this->form_validation->run()==TRUE)
			{
				if($this->forgot_password_model->authentication()==TRUE)
				{
					 $this->forgot_password_model->sendMail();
		
					 $this->session->set_flashdata("success","Password send to your email id.");
					 redirect(base_url('forgot_password'));
				}
				else
				{
				   $data['error'] = "Invalid Email";
				}
			}
			
		}
		else
		{
			$data['error'] = "Access Denied";
		}
		
		$vals = array('img_path'      => UPLOAD_PATH."captcha/",'img_url'       => UPLOAD_URL."captcha/",'expiration' => 7200);

		$cap = create_captcha($vals);
		$data['captcha'] = $cap['image'];

		$this->session->set_userdata('captchaWord', $cap['word']);

			
		
		$this->load->view('forgot_password',$data);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */