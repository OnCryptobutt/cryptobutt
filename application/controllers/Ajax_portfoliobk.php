<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_portfolio extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->settings = $this->boost_model->loadSettings();
		$this->detect = new Mobile_Detect();
    }
	
	
function load_portfolio()
{
	    $currency = $this->input->post('currency');
		
	    if($currency=="")
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "USD";
			}
		}
		
		$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		
		if(isset($_POST['pageId']) && !empty($_POST['pageId']))
		{
           $id=$_POST['pageId'];
         }
		 else
		 {
		   $id='0';
		 }
		 
		  $pageLimit= $this->settings->settings_records_per_page_front*$id;
		
		echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
			
			$this->db->select('*');
			$this->db->from(PORTFOLIO);
			$this->db->where("portfolio_user_id",$this->session->userdata('user_id'));
			$this->db->limit($this->settings->settings_records_per_page_front,$pageLimit);
			$query = $this->db->get();
			//echo $this->db->last_query();
			$coinrowscount = $query->num_rows();
			if($query->num_rows()>0)
			{
				$totalinvest ="";
				$balanceinvest = "";
				$profit = "";
				foreach($query->result_array() as $row)
				{
					
				$row['price'] = $this->boost_model->getValue(COIN_HISTORY,$currency,"coin_id='".$row['portfolio_coin_id']."' AND column_id='1' order by id desc");
				$row['change24hour'] = $this->boost_model->getValue(COIN_HISTORY,$currency,"coin_id='".$row['portfolio_coin_id']."' AND column_id='12' order by id desc");
				$query = $this->db->select('image,coinname,name')->from(COIN)->where("id",$row['portfolio_coin_id'])->get();
				
				$row['wallet_details'] = $this->wallet_details($row['portfolio_id'],$currency);
				
				$totalinvest = $totalinvest + $row['wallet_details']['boughttimevalue'];
				$balanceinvest = $balanceinvest + $row['wallet_details']['total_value'];
				
				if($query->num_rows()>0)
				{
					$row_coin = $query->result_array();
				}
				
				if($row['change24hour']>0)
				{
					$mobile24hourclass = "mobile-second9";
				}
				else
				{
					$mobile24hourclass = "mobile-second8";
				}
				
				if($row['wallet_details']['increasep']>0)
				{
					$percentage = "<span class=\"bott\"> + ".$row['wallet_details']['increasep']." % </span>";
				}
				else
				{
					$percentage = "<span class=\"bott2\">".$row['wallet_details']['increasep']." % </span>";
				}
				
			    
				echo '<div class="panel panel-default mar-top">
				      <div class="panel-heading" role="tab" id="heading'.$row['portfolio_id'].'">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$row['portfolio_id'].'" aria-expanded="true" aria-controls="collapse'.$row['portfolio_id'].'">
							<i class="more-less glyphicon glyphicon-plus icon-align"></i>
							<div class="row desktop">
								<div class="col-xs-2 col-md-1 text-center mob_res2">
									<img width="54" src="'.COIN_URL.$row_coin[0]['image'].'" alt="bootsnipp" class="img-rounded img-responsive img_width"/>
								</div>
								<div class="col-xs-3 col-md-3 text-center pad-top">
									<div class="text-col3"><a href="#" target="_blank">'.$row_coin[0]['coinname'].' ('.$row_coin[0]['name'].')</a>&nbsp;<span class="glyphicon glyphicon-heart"></span></div>
									<div class="mobile-second"><span class="mobile-second7">'.$currency_symbol.' '.$row['price'].'</span><span class="'.$mobile24hourclass.'">'.$row['change24hour'].'%</span></div>
								</div>
								<div class="col-xs-2 col-md-2 text-center pad-top">
									<div class="text-col4">'.$row['wallet_details']['total_coin'].' '.$row_coin[0]['name'].'</div>
									<div class="text-col11">Total Coin</div>
								</div>
								<div class="col-xs-2 col-md-2 text-center pad-top">
								   <div class="text-col4">'.$currency_symbol.' '.$row['wallet_details']['total_value'].' '.$percentage.'</div>
									<div class="text-col11">Wallet Balance</div>
								</div>
								<div class="col-xs-3 col-md-2 text-center pad-top1">
								   <div class="text-col12">Add Balance +</div>
								   <div class="text-col12"><a onclick="delete_portfolio('.$row['portfolio_id'].')" href="javascript:void(0)" class="btn btn-danger btn-xs">Delete<span class="glyphicon glyphicon-close"></span></a></div>
								</div>
							</div>
							
							<!-- mobile view start -->
								<div class="row mobile">
									<div class="row">
										<div class="col-xs-3 col-md-1 text-center mob_res4">
											<img src="'.COIN_URL.$row_coin[0]['image'].'" alt="bootsnipp" class="img-rounded img-responsive img_width"/>
										</div>
										<div class="col-xs-6 col-md-3 text-center pad-top">
											<div class="text-col3"><a href="#" target="_blank">'.$row_coin[0]['coinname'].' ('.$row_coin[0]['name'].')</a>&nbsp;<span class="glyphicon glyphicon-heart"></span></div>
											<div class="mobile-second top-10"><span class="mobile-second7">'.$currency_symbol.' '.$row['price'].'</span><span class="'.$mobile24hourclass.'">'.$row['change24hour'].'%</span></div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-2 text-center pad-top">
											<div class="col-xs-1">
											</div>
											<div class="col-xs-5 left">
												<div class="text-col11 brown">Total Coin</div>
											</div>
											<div class="col-xs-5 right">
												<div class="text-col4">'.$row['wallet_details']['total_coin'].' '.$row_coin[0]['name'].'</div>
											</div>
											<div class="col-xs-1">
											</div>
										</div>
									</div><div class="row">	
										<div class="col-xs-12 col-md-2 text-center pad-top">
											<div class="col-xs-1">
											</div>
											<div class="col-xs-5 left top-10 brown">
												<div class="text-col11">Wallet Balance</div>
											</div>
											<div class="col-xs-5 right top-10">
												<div class="text-col4">'.$currency_symbol.' '.$row['wallet_details']['total_value'].' '.$percentage.'</div>
											</div>
											<div class="col-xs-1">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-2 text-center pad-top1">
											<div class="col-xs-1">
											</div>
											<div class="col-xs-5 left top-10 brown border-top">
												<div class="text-col12 top-bot-10">Add Balance</div>
												
											</div>
											<div class="col-xs-5 right top-10 border-top">
												<div class="text-col13 top-bot-10">+</div>
												 <div class="text-col12"><a onclick="delete_portfolio('.$row['portfolio_id'].')" href="javascript:void(0)" class="btn btn-danger btn-xs">Delete<span class="glyphicon glyphicon-close"></span></a></div>
											</div>
											<div class="col-xs-1">
											</div>
										</div>
									</div>
								</div>
							<!-- mobile view end -->
						</a>
					</h4>
				</div>
				<div id="collapse'.$row['portfolio_id'].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$row['portfolio_id'].'">
					<div class="panel-body">
					  <div id="details_'.$row['portfolio_coin_id'].'" class="row col-md-12 custyle table-responsive desktop">
					  
					  <span class="flash_'.$row['portfolio_coin_id'].'"></span>';
					  
					  $this->db->select('*');
					  $this->db->from(PORTFOLIO_DETAILS);
					  $this->db->where("portfolio_details_portfolio_id",$row['portfolio_id']);
					  $query = $this->db->get();
					  if($query->num_rows()>0)
					  {
						  
						  
					  
						echo '<table class="table table-striped custab">
							<thead class="table-info">
								<tr>
									<th>#</th>
									<th>Date</th>
									<th>Coin brought</th>
									<th>Exchange type</th>
									<th>Amount spent</th>
									<th></th>
								</tr>
							</thead>';
							$i=1;
						
							foreach($query->result_array() as $row_details)
							{
							$coinname = $this->boost_model->getValue(COIN,"coinname","id='".$row_details['portfolio_details_coin_id']."'");
							
							
																
							echo '<tr>
								<td>'.$i.'</td>
								<td>'.$row_details['portfolio_details_date'].'</td>
								<td>'.$row_details['portfolio_details_coin_brought'].' '.$coinname.'</td>
								<td>'.$coinname.' / '.$row_details['portfolio_details_exchange_type'].'</td>
								<td>'.$row_details['portfolio_details_amount'].' '.$row_details['portfolio_details_exchange_type'].'</td>
								<td class="text-center"><a onclick="get_edit_section('.$row_details['portfolio_details_id'].')" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"> Edit &nbsp;<span class="glyphicon glyphicon-edit"></span></a>
								<a onclick="delete_portfolio_details('.$row_details['portfolio_details_id'].')" class="btn btn-danger btn-xs"> Delete &nbsp;<span class="glyphicon glyphicon-close"></span></a></td>
							</tr>';
							$i++;
							}
						
						echo '</table>';
						
					  }
						
					  echo '</div>';
					  
					  
					  
					  echo ' <!-- Mobile view start -->
					  <div class="row col-md-12 custyle mobile">';
					   $this->db->select('*');
					  $this->db->from(PORTFOLIO_DETAILS);
					  $this->db->where("portfolio_details_portfolio_id",$row['portfolio_id']);
					  $query = $this->db->get();
					  if($query->num_rows()>0)
					  {
						  echo '<table class="table table-striped custab">
								<thead class="table-info">
									<tr>
										<th>Coin</th>
										<th>Exchange</th>
										<th>Amount</th>
										<th></th>
									</tr>
								</thead>';
						  	foreach($query->result_array() as $row_details)
							{
							$coinname = $this->boost_model->getValue(COIN,"coinname","id='".$row_details['portfolio_details_coin_id']."'");
							
							echo '<tr>
									<td>'.$row_details['portfolio_details_coin_brought'].' '.$coinname.'</td>
									<td>'.$coinname.' / '.$row_details['portfolio_details_exchange_type'].'</td>
									<td>'.$row_details['portfolio_details_amount'].' '.$row_details['portfolio_details_exchange_type'].'</td>
									<td class="text-center"><a onclick="get_edit_section('.$row_details['portfolio_details_id'].')" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"> Edit &nbsp;<span class="glyphicon glyphicon-edit"></span></a>
								<a onclick="delete_portfolio_details('.$row_details['portfolio_details_id'].')" class="btn btn-danger btn-xs"> Delete &nbsp;<span class="glyphicon glyphicon-close"></span></a></td>
								</tr>';
							$i++;
							}
							
							echo '</table>';
					  }
					  
					  echo '</div>';
							
					
					  
					 echo ' <div class="row form-alg">
                            <form>
                                <div class="col-lg-4 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Amount of COIN bought</label>
										
                                        <input id="bought_'.$row['portfolio_coin_id'].'" name="text" type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Exchange Type</label>
                                        <select id="exchange_'.$row['portfolio_coin_id'].'" name="text" class="form-control">';
										if(is_array($this->settings->currency_list))
										{
											foreach($this->settings->currency_list as $currency_list)
											{
												echo '<option value="'.$currency_list->currency_code.'">'.$row_coin[0]['name'].' / '.$currency_list->currency_code.'</option>';
											}
										}
											
											
										echo '</select>
                                    </div>
                                </div>
								<div class="col-lg-4 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label required" for="email">Amount Spent</label>
                                        <input id="amount_'.$row['portfolio_coin_id'].'" name="text" type="text" class="form-control">
                                    </div>
                                </div>
								
                                <!--<div class="col-lg-2 col-md-12 col-sm-6 col-xs-4 desktop">
                                    <button type="button" onclick="savecoin('.$row['portfolio_coin_id'].','.$row['portfolio_id'].')" class="btn btn-success btn-block">Save Coin</button>
                                </div>-->
								<div class="col-lg-3 col-md-12 col-sm-6 col-xs-6 desktop">
                                    <button type="button" onclick="savecoin('.$row['portfolio_coin_id'].','.$row['portfolio_id'].')" class="btn btn-success btn-block">Save & Add another coin</button>
                                </div>
								<div class="col-lg-4 col-md-12 col-sm-6 col-xs-2 can-alg desktop">
                                    <a href="#" class="forgot-password">Cancel</a>
                                </div>
								
								<!-- Mobile view start -->
								<!--<div class="row">
									<div class="col-lg-2 col-md-12 col-sm-6 col-xs-12 mobile">
										<div class="col-xs-6 bottom-5">
											<button class="btn btn-success btn-block">Save Coin</button>
										</div>
										<div class="col-xs-6">
										</div>
									</div>
								</div>-->
								<div class="row">
									<div class="col-lg-3 col-md-12 col-sm-6 col-xs-12 mobile">
										<div class="col-xs-8 bottom-5">
											<button type="button" onclick="savecoin('.$row['portfolio_coin_id'].','.$row['portfolio_id'].')" class="btn btn-success btn-block">Save & Add another coin</button>
										</div>
										<div class="col-xs-4">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-md-12 col-sm-6 col-xs-12 can-alg mobile">
										<div class="col-xs-4">
											<a href="#" class="forgot-password">Cancel</a>
										</div>
										<div class="col-xs-8">
										</div>
									</div>
								</div>
								
								<!-- Mobile view end -->
								
                            </form>
                        </div>
					</div>
				</div>
			</div>';
			}
			
				$profit = $balanceinvest - $totalinvest;
				
				$profitp = round($profit/$totalinvest*100,2);
				
				if($profitp>0)
				{
					$profitphtml = $currency_symbol." + ".$profit." <span class=\"bott\">".$profitp." % </span>";
				}
				else
				{
					$profitphtml = "<font color=\"red\">".$currency_symbol." ".$profit." <span class=\"bott2\">".$profitp." % </span></font>";
				}
				
				if($this->detect->isMobile())
				{
					
				echo "<script>
				document.getElementById('port_total_invest_mobile').innerHTML ='".$currency_symbol." ".$totalinvest."';
				document.getElementById('port_total_bal_mobile').innerHTML ='".$currency_symbol." ".$balanceinvest."';
				document.getElementById('port_total_prof_mobile').innerHTML ='".$profitphtml."';
				document.getElementById('bal_al_mobile').innerHTML ='Balance is ".$currency_symbol." ".$balanceinvest."';
				</script>";
				}
				else
				{
					
				
					echo "<script>
				document.getElementById('port_total_invest').innerHTML ='".$currency_symbol." ".$totalinvest."';
				document.getElementById('port_total_bal').innerHTML ='".$currency_symbol." ".$balanceinvest."';
				document.getElementById('port_total_prof').innerHTML ='".$profitphtml."';
				document.getElementById('bal_al').innerHTML ='Your Total Balance is ".$currency_symbol." ".$balanceinvest."';
			
				</script>";
				}
				
			}


			echo '</div>';
			
			$this->db->select('*');
			$this->db->from(PORTFOLIO);
			$this->db->where("portfolio_user_id",$this->session->userdata('user_id'));
			$query = $this->db->get();
			
			$paginationCount = $this->getPagination($query->num_rows(),$this->settings->settings_records_per_page_front);
			
			if(is_array($coinrowscount)){ 
 $content ="";
$content .='<ul class="tsc_pagination tsc_paginationC tsc_paginationC01 pagina">
    <li class="first link" id="first">
        <a  href="javascript:void(0)" onclick="changePagination(\'0\',\'first\')">F i r s t</a>
    </li>';
    for($i=0;$i<$paginationCount;$i++){
 
        $content .='<li id="'.$i.'_no" class="link">
          <a  href="javascript:void(0)" onclick="changePagination(\''.$i.'\',\''.$i.'_no\')">
              '.($i+1).'
          </a>
    </li>';
    }
 
    $content .='<li class="last link" id="last">
         <a href="javascript:void(0)" onclick="changePagination(\''.($paginationCount-1).'\',\'last\')">L a s t</a>
    </li>
    
</ul>';

echo $content;
		}
			
			
			
			echo '<div class="row form-alg desktop">
					<form>
						<div class="col-lg-4 col-md-12 col-sm-6 col-xs-2">
						</div>
						<div class="col-lg-4 col-md-12 col-sm-6 col-xs-8 text-center">
							<div class="form-group">
								<label class="control-label required" for="email">No balance add to display</label>
								<button class="btn btn-primary btn-block but-red"><span class="butt-siz">+ Add balance</span></button>
							</div>
						</div>
						<div class="col-lg-4 col-md-12 col-sm-6 col-xs-2">
						</div>
					</form>
				</div>
		</div>';
		exit;
}


function load_portfolio_balance_display()
{
	    $currency = $this->input->post('currency');
		
	    if($currency=="")
		{
			if($this->session->userdata('user_id'))
			{
				$currency = $this->boost_model->getValue(USER,"base_currency","id='".$this->session->userdata('user_id')."'");
				
			}
			else
			{
				$currency = "USD";
			}
		}
		
		$currency_symbol = $this->boost_model->getValue(CURRENCY,"symbol","currency_code='".$currency."'");
		
			
			$this->db->select('*');
			$this->db->from(PORTFOLIO);
			$this->db->where("portfolio_user_id",$this->session->userdata('user_id'));
			$query = $this->db->get();
			$coinrowscount = $query->num_rows();
			if($query->num_rows()>0)
			{
				$totalinvest ="";
				$balanceinvest = "";
				$profit = "";
				foreach($query->result_array() as $row)
				{
					
			
				
				$row['wallet_details'] = $this->wallet_details($row['portfolio_id'],$currency);
				
				$totalinvest = $totalinvest + $row['wallet_details']['boughttimevalue'];
				$balanceinvest = $balanceinvest + $row['wallet_details']['total_value'];
				
					  
					  
					
			}
			
				$profit = $balanceinvest - $totalinvest;
				
				$profitp = round($profit/$totalinvest*100,2);
				
				if($profitp>0)
				{
					$profitphtml = $currency_symbol." + ".$profit." <span class=\"bott\">".$profitp." % </span>";
				}
				else
				{
					$profitphtml = "<font color=\"red\">".$currency_symbol." ".$profit." <span class=\"bott2\">".$profitp." % </span></font>";
				}
				
				if($this->detect->isMobile())
				{
					
				echo "<script>
				document.getElementById('port_total_invest_mobile').innerHTML ='".$currency_symbol." ".$totalinvest."';
				document.getElementById('port_total_bal_mobile').innerHTML ='".$currency_symbol." ".$balanceinvest."';
				document.getElementById('port_total_prof_mobile').innerHTML ='".$profitphtml."';
				document.getElementById('bal_al_mobile').innerHTML ='Balance is ".$currency_symbol." ".$balanceinvest."';
				</script>";
				}
				else
				{
					
				
					echo "<script>
				document.getElementById('port_total_invest').innerHTML ='".$currency_symbol." ".$totalinvest."';
				document.getElementById('port_total_bal').innerHTML ='".$currency_symbol." ".$balanceinvest."';
				document.getElementById('port_total_prof').innerHTML ='".$profitphtml."';
				document.getElementById('bal_al').innerHTML ='Your Total Balance is ".$currency_symbol." ".$balanceinvest."';
			
				</script>";
				}
				
			}


		
			
			
		}
					
		
			




function load_portfolio_heading()
{
	
	echo '<div class="col-md-12 desktop">
            <div class="well3 well-sm">
                <div class="row">
					<div class="col-xs-4 col-md-4 text-center set bod-right">
						<div class="text-col8">Your Total Investment</div>
						<div class="text-col9" id="port_total_invest"></div>
                    </div>
					<div class="col-xs-4 col-md-4 text-center set bod-right">
						<div class="text-col8">Your Total Balance</div>
						<div class="text-col9" id="port_total_bal"></div>
                    </div>
					<div class="col-xs-4 col-md-4 text-center set">
						<div class="text-col8">Your Total Profit</div>
						<div class="text-col10" id="port_total_prof"></div>
                    </div>
				</div>
            </div>
        </div>
		
		
		<!-- Mobile View Start-->
		<div class="col-md-12 mobile">
            <div class="well3 well-sm">
                <div class="row">
					
					<div class="col-xs-6 col-md-4 text-center set bod-right">
						<div class="text-col8">Your Total Investment</div>
						<div class="text-col9" id="port_total_invest_mobile"></div>
                    </div>
					<div class="col-xs-6 col-md-4 text-center set">
						<div class="text-col8">Your Total Balance</div>
						<div class="text-col9" id="port_total_bal_mobile"></div>
                    </div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4 text-center set">
						<div class="text-col8">Your Total Profit</div>
						<div class="text-col10" id="port_total_prof_mobile"></div>
                    </div>
					
                </div>
            </div>
        </div>';
		exit;
}

 function getPagination($count,$per_page){
      $paginationCount= floor($count / $per_page);
      $paginationModCount= $count % $per_page;
      if(!empty($paginationModCount)){
               $paginationCount++;
      }
      return $paginationCount;
     }
	 
	 
	 function wallet_details($id,$currency)
	 {
		 $this->db->select('*');
		 $this->db->from(PORTFOLIO_DETAILS);
		 $this->db->where("portfolio_details_portfolio_id",$id);
		 $query = $this->db->get();
		 $response = array();
		 $response['total_coin'] = "";
		  $response['total_value'] = "";
		  $response['increasep'] ="";
		   $response['boughttimevalue'] ="";
		  
		 if($query->num_rows()>0)
		 {
			 $total_coin = "";
			 $total_value = "";
			 $boughttimevalue= "";
			 foreach($query->result_array() as $row)
			 {
				 $total_coin = $total_coin + $row['portfolio_details_coin_brought'];
				 
				 $coin_name = $this->boost_model->getValue(COIN,"name","id='".$row['portfolio_details_coin_id']."'");
				 
				 $time = time() - 5;
				 
				 $Data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym='.$coin_name.'&tsyms='.$row['portfolio_details_exchange_type'].'&ts='.$time),true);
				 
				 $coin_rate = $row['portfolio_details_coin_brought'] * $Data[$coin_name][$row['portfolio_details_exchange_type']];
				 
				 //echo $coin_rate."<br/>";
				
				 
				 if($row['portfolio_details_exchange_type']!=$currency)
		  {
		$now_value = $this->boost_model->convertCurrency($coin_rate, $row['portfolio_details_exchange_type'], $currency);
		  }
		  else
		  {
			  $now_value = $coin_rate;
		  }
		
		$datebought = strtotime($row['portfolio_details_date']);
		
		/** $Data1 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym='.$coin_name.'&tsyms='.$row['portfolio_details_exchange_type'].'&ts='.$datebought),true);
		 
		 
		  $coin_rate_bought = $row['portfolio_details_coin_brought'] * $Data1[$coin_name][$row['portfolio_details_exchange_type']]; **/
		  
		  $coin_rate_bought = $row['portfolio_details_amount'];
		  
		  if($row['portfolio_details_exchange_type']!=$currency)
		  {
			  
		  
		  	$now_value_bought = $this->boost_model->convertCurrency($coin_rate_bought, $row['portfolio_details_exchange_type'], $currency);
		  }
		  else
		  {
			  $now_value_bought = $coin_rate_bought;
		  }
		
		 //echo $total_value."<br/>";
		  $boughttimevalue = $boughttimevalue+$now_value_bought;
		
		    $total_value = $total_value+$now_value;
			 }
			 $response['total_coin'] = $total_coin;
			  $response['total_value'] = $total_value;
			   $response['boughttimevalue'] = $boughttimevalue;
			   
			   $Increase = $total_value - $boughttimevalue;
			   
			   $increasep = $Increase/$boughttimevalue*100;

               $response['increasep'] = round($increasep,2);


		 }	
         return $response;
		 
	 }
	 
}


