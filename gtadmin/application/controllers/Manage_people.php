<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_people extends CI_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('manage_people_model');
		$this->load->model('boost_model'); /** Load Basic Functions **/
		$this->boost_model->checkAdminAuthentication(); /** Check Valid User **/
		$this->settings = $this->boost_model->loadSettings();
		$this->db2 = $this->boost_model->getHistoryDb();
    }

	public function index()
	{
		$data  = "";
		$check = "";
		$data['success'] = "";
		$data['basename'] = "manage_people-list";
		$data['title'] = ucfirst("manage_people List");
		
		
		$config = array();
        $config["base_url"] =  base_url('manage_people/index');
        $config["total_rows"] = $this->manage_people_model->record_count();
        $config["per_page"] = $this->settings->settings_records_per_page;
		//$config["per_page"] =1;
        $config["uri_segment"] = 3;
		
		$paginationConfig = $this->boost_model->loadPaginationConfig();
		
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		
		$config = array_merge($config,$paginationConfig);
		
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
        $data["lists"] = $this->manage_people_model->manage_people_list($config["per_page"], $page);
        $data["pagination"] = $this->pagination->create_links();
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('manage_people/list',$data);
		$this->load->view('template/footer',$data);
	}	
}
